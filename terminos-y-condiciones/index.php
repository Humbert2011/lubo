

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Lubo - TyCos</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <!-- icon -->
		<link rel="icon" type="image/x-icon" href="../img/favicon/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
		<link rel="manifest" href="../img/favicon/site.webmanifest">
		<link rel="mask-icon" href="../img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
        <link href="../css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/theme.css" rel="stylesheet" type="text/css" media="all"/>
		<link rel="stylesheet" href="../css/frmregistro.css">
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="../js/config/config.js"></script> <!-- url -->
		<script type="text/javascript" src="../js/sw/servicios.js" ></script>
        <script type="text/javascript">
			function cargando() {
				// validar token
				var token = sessionStorage.getItem('Token');
				console.log(token)
				if (token != null) {
					document.getElementById('log-user').innerHTML = 'Logout'
					document.getElementById('log-user').onclick = logout
				}
			}
		</script>
    </head>
    <body onload="cargando()">
				
		<div class="nav-container">
			<?php 
				$currentPage = 'menu-tycos';
				include_once("../menus/menu-sticky-black-in.php");
			?>
		</div>
		
		<div class="main-container">
			<header class="fullscreen-element no-pad centered-text">
				<div class="background-image-holder parallax-background overla">
					<img class="background-image" style="background-color: transparent;" alt="Background Image" src="../img/politicas2.png">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-md-12 text-left">
							<span class="text-white alt-font"></span>
							<h1 class="text-white h1-hola" style="color: #4a4a4a;"><b>Términos<br> <span style="color: #ED3093;">y Condiciones</span></b>
							</h1>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</header>
		<!-- end divider -->
			
			
			
			<section class="video-inline">
				<div class="container principal">
				<div class="row">
					<div class="col-md-12 wow bounceIn">
						<br>
						<img src="../img/LuboPink.png" width="100px" style="margin-top: 20px; margin-bottom: 20px;">
	<p><b>Términos de Uso y Condiciones de Servicios</b></p>
	<p>Versión 1.1 - Fecha de actualización: 1° de Junio de 2020.</p>
	<p>Términos y Condiciones de Uso de Lubo Pasajero</p>
	<p>Lubo es una marca de Startdust Inc. S.A de C.V (en lo sucesivo denominada como “Lubo” o la “Compañía”), una sociedad anónima de capital variable constituida en los Estados Unidos Mexicanos, con domicilio en el número 45 de la calle Corregidora de la ciudad de Huauchinango, Puebla, México inscrita ante la secretaria de economía por medio del instrumento 18440.</p>
	<p>Esta aplicación en lo sucesivo se denominará como <b>“Lubo”</b>. Al usuario que acceda, navegue o utilice Lubo y los sitios web de Lubo en lo sucesivo se le denominará como el <b>“Pasajero”</b>. Lubo y sus filiales prestan al Pasajero servicios en línea de transporte privado en automóvil con base en las necesidades de desplazamiento del Pasajero a través de Lubo, con interacción de información fuera de línea y en línea en tiempo real y una efectiva adecuación de recursos fuera de línea a través del análisis de una gran cantidad de datos (en lo sucesivo denominados como los <b>“Servicios”</b>). El usuario que acceda, navegue o utilice Lubo Driver y los Sitios web relevantes de Lubo en lo sucesivo se le denominará como el <b>“Conductor”</b>. El Conductor que sea designado al Pasajero a través de Lubo se denominará en lo sucesivo como el <b>“Conductor Designado”</b>.</p>
	
	<p>El Pasajero se sujetará a los presentes Términos y Condiciones de Uso de Lubo (en lo sucesivo denominados como los <b>“Términos y Condiciones”</b>).</p>
	<p><b>Antes de utilizar Lubo, favor lea los presentes Términos y Condiciones con atención (especialmente las partes resaltadas en negritas). Tenga en cuenta que el Pasajero no podrá acceder, ni tener pleno acceso a los Servicios hasta que no haya proporcionado todos los documentos e información que se requieren, aprobados por Lubo y Lubo, y se obligue a sujetarse a los presentes Términos y Condiciones. Si el Pasajero elige y utiliza los Servicios, se considerará que el Pasajero ha leído y aceptado todos y cada uno de los términos establecidos en este documento y acordado que los presentes Términos y Condiciones son legalmente vinculantes para el Pasajero y la Compañía. El Pasajero observará los siguientes términos en buena fe al utilizar Lubo.</b></p>
	
	<p><b>Los presentes Términos y Condiciones son aplicables para todos los usuarios o pasajeros de Lubo, y están sujetos a modificación en cualquier momento como resultado de ajustes a la política comercial. El Pasajero debe visitar frecuentemente esta página web y/o Lubo para mantenerse al tanto de los términos vigentes. Las notificaciones, términos u otros requerimientos expresamente especificados o integrados en la página web y/o en Lubo, formarán parte integral de los presentes Términos y Condiciones, ciertas partes de los términos podrán ser sustituidas por avisos legales, términos o requerimientos más actualizados expresamente especificados o integrados en la página web y/o en Lubo. Al aceptar los Términos y Condiciones, el Pasajero también habrá leído con atención y aceptado dichos términos sustituidos o referidos. Si el Pasajero sigue utilizando los servicios de la Compañía, se considerará que el Pasajero ha aceptado los Términos y Condiciones actualizados o el Pasajero dejará de utilizar Lubo inmediatamente.</b></p>
	
	<p><b>Los presentes Términos y Condiciones expresamente dejan sin efecto cualquier acuerdo o arreglo previo que la Compañía haya celebrado con el Pasajero para efectos de utilizar Lubo y los Servicios. Lubo podrá dar por terminados los presentes Términos y Condiciones o cualquier Servicio con respecto al Pasajero inmediatamente, o en general, dejar de ofrecer o negar el acceso a los Servicios o a cualquier parte de los mismos, si Lubo considera que el Pasajero ha incurrido en cualquier incumplimiento.</b></p>
	
	<p><b>Asimismo, Lubo se reserva el derecho de negarse a prestar los Servicios al Pasajero o negar al Pasajero el uso de Páginas Web, servicios o aplicaciones de Lubo sin causa alguna.</b></p>
	
	<p><b>1- Registro del Pasajero</b></p>
	<ol>
		<li>
		Para utilizar el servicio en línea de transporte privado, el Pasajero debe descargar Lubo, instalarla en su dispositivo móvil y completar exitosamente los procesos de registro. Al registrarse, el Pasajero deberá asegurarse que toda la información que haya proporcionado sea precisa, completa y válida.
		</li>
		<li>
		El Pasajero reconoce y acuerda que una vez que el Pasajero sea un usuario registrado de Lubo, la cuenta de servicio del Pasajero en la Compañía se abrirá de forma predeterminada. La cuenta y contraseña predeterminadas deberán ser la cuenta y contraseña de la cuenta de Lubo del Pasajero.
		</li>
		<li>
		El Pasajero tendrá plena capacidad de ejercicio de conformidad con el Código Civil Federal. En caso de que el Pasajero no tenga plena capacidad de ejercicio o si el Pasajero es una persona con capacidad de ejercicio limitada, el Pasajero deberá viajar con su tutor, y dicho tutor asumirá cualquier responsabilidad y consecuencia derivada de o relacionada con los presente Términos y Condiciones.
		</li>
	</ol>
	<br>
	<p><b>2- Servicios</b></p>
	<ol>
		<li>
		Los servicios en línea de transporte privado se prestan al Pasajero a través de Lubo. El Pasajero podrá elegir y utilizar los Servicios a través de Lubo, descargada e instalada en el dispositivo móvil del Pasajero. Los Servicios permiten al Pasajero solicitar y programar servicios de transporte prestados por el Conductor Designado, quien no es un empleado, funcionario ni representante de la Compañía.
		</li>
		<li>
		El Pasajero reconoce que su capacidad de obtener servicios de transporte y/o logística a través del uso de los Servicios no constituye a la Compañía como un proveedor de servicios de transporte, logística o mensajería ni como un operador de transporte.
		</li>
		<li>
		A menos que la Compañía acuerde lo contrario en un contrato por escrito adicional con el Pasajero, los Servicios están disponibles únicamente para uso personal y no comercial del Pasajero.
		</li>
		<li>
		Para utilizar los Servicios, el Pasajero deberá proporcionar a la Compañía cierta información personal. El Pasajero se obliga a mantener la información personal que requiera Lubo en forma precisa, completa y actualizada. Si no cumple con dicha obligación, se podrá impedir al Pasajero acceder a o utilizar los Servicios.
		</li>
	</ol>
	
	<p><b>3- Uso de los servicios</b></p>
	<ol>
		<li>
		Una orden de contratación de servicios de transporte privado en automóvil comenzará cuando el sistema vincule exitosamente una orden tras recibir una solicitud del Pasajero.
		</li>
		<li>
		Lubo hará esfuerzos razonables para prestar los Servicios al Pasajero sujeto a la disponibilidad de vehículos en el momento y lugar en que el Pasajero haga la solicitud.
		</li>
		<li>
		Lubo no será responsable por pérdida o daño alguno cuando un Pasajero solicite a un Conductor Designado transportar únicamente artículos, sin que el Pasajero viaje en el vehículo, y dicho Conductor acepte la solicitud, ya que Lubo no puede garantizar que dichos artículos no serán dañados por el Conductor Designado o cualquier tercero cuando los artículos sean transportados. Si el Pasajero insiste en encargar al Conductor que transporte artículos para el Pasajero, el encargo se considerará como una conducta privada entre Pasajero y Conductor.
		</li>
		<li>
		El Pasajero no viajará con artículos peligrosos, que estén prohibidos o sean considerados inapropiados por alguna ley, norma o reglamento aplicable; de lo contrario, el Conductor tendrá derecho a rehusarse a prestar el servicio y el Pasajero asumirá cualquier consecuencia y responsabilidad que resulte de transportar de dichos artículos.
		</li>
		<li>
		El Pasajero no incurrirá en conducta incivilizada alguna (incluyendo fumar, beber alcohol, escupir u otras conductas) al utilizar los Servicios.
		</li>
		<li>
		El Pasajero no pedirá a los Conductores, ni hará que éstos lleven a cabo, acto alguno que viole cualquier reglamento de tránsito u otra ley o reglamento aplicable. En el caso de una sanción administrativa, lesión, accidente de tráfico, daños al vehículo u otras pérdidas derivadas de tales actos prohibidos por parte del Pasajero, el Pasajero indemnizará a la parte que haya sufrido la(s) pérdida(s).
		</li>
		<li>
		<b>El Pasajero no publicará ni revelará información personal de cualquier otro pasajero o conductor obtenida al utilizar los Servicios a persona alguna para efecto alguno distinto a disfrutar del Servicio, salvo que dicha información haya sido obtenida a través de otro canal legítimo o según se permita conforme a las leyes aplicables.</b>
		</li>
		<li>
		El Pasajero será responsable de todas las actividades que ocurran en su cuenta, y se obliga a mantener la seguridad y confidencialidad del nombre de usuario y contraseña de la cuenta del Pasajero en todo momento. Salvo que la Compañía permita lo contrario por escrito, el Pasajero únicamente podrá tener una Cuenta.
		</li>
		<li>
		Al utilizar los Servicios, el Pasajero acepta que la Compañía podrá contactarlo vía telefónica o por mensaje de texto en cualquiera de los números telefónicos que haya proporcionado o a nombre del Pasajero en relación con su cuenta, incluyendo para fines de marketing.
		</li>
		<li>
		La recopilación y uso de información personal en relación con los Servicios se describe en el Aviso de Privacidad de Lubo.
		</li>
		<li>
		El Pasajero acepta y reconoce que no existe una relación de subordinación o laboral entre el Conductor, o cualquier persona o entidad que administre las actividades del Conductor, y Lubo o cualquiera de sus afiliadas derivadas de cualquiera y todas las actividades realizadas por el Conductor, sujetas a los que se aplican los Términos y Condiciones, mencionadas en las leyes y regulaciones laborales, de seguridad social y fiscales de México aplicables. El Pasajero reconoce que cualquier individuo o entidad que administre o esté relacionado con las actividades realizadas por el Conductor no está empleado, ni es propiedad ni está bajo el control de Lubo.
		</li>
	</ol>
		
	<p><b>4- Obligaciones y Garantías</b></p>
	<ol>
		<li>
		El Pasajero garantiza que la información proporcionada a Lubo será cierta, precisa y completa. En relación con los Servicios, Lubo se reserva el derecho de verificar la información del Pasajero.
		</li>
		<li>
		Al utilizar los Servicios o Lubo, el Pasajero también se obliga a lo siguiente: 
			<ul>
				<li>
				El Pasajero utilizará los Servicios o descargará Lubo para uso personal y no revenderá los mismos a tercero alguno;
				</li>
				<li>
				el Pasajero no utilizará los Servicios ni Lubo para fin ilegitimo alguno, incluyendo sin limitación, transmitir o almacenar materiales ilegales o para fines fraudulentos;
				</li>
				<li>
				el Pasajero no utilizará los Servicios ni Lubo para acosar u obstaculizar a otros ni para causarles inconvenientes.
				</li>
				<li>
				el Pasajero no afectará la operación normal de la red;
				</li>
				<li>
				el Pasajero no intentará dañar los Servicios ni a Lubo;
				</li>
				<li>
				el Pasajero proporcionará la documentación de identificación que solicite Lubo razonablemente;
				</li>
				<li>
				el Pasajero efectuará todos los pagos por los Servicios solicitados; y
				</li>
				<li>
				el Pasajero cumplirá con todas las leyes aplicables del país/región, provincia y/o ciudad en donde utilice la aplicación o los Servicios.
				</li>
			</ul>
		</li>
		<li>
		Es obligación del Pasajero mantener todo el hardware o software del dispositivo móvil actualizado para que soporte los requerimientos vigentes de Lubo. La Compañía no será responsable de problema alguno que pueda surgir cuando el Pasajero no utilice la versión más reciente y actualizada de Lubo y/o utilice Lubo en cualquier dispositivo móvil que no alcance los requerimientos relevantes vigentes de Lubo. El Pasajero acepta que la Compañía podrá actualizar los requerimientos de software y hardware del teléfono móvil de Lubo de tiempo en tiempo.
		</li>
	</ol>
	
	<p><b>5- Pago</b></p>
	<ol>
		<li><b>
		El Pasajero está de acuerdo y acepta los precios correspondientes por los servicios vigentes a la fecha o conforme se actualicen en Lubo. El Pasajero podrá verificar los precios por los servicios de Lubo. Estos precios pueden cambiar de tiempo en tiempo con base en la oferta y demanda del mercado y el Pasajero únicamente será responsable de monitorearlos precios de los servicios y elegir los servicios que considere convenientes. El precio puede ajustarse de vez en cuando de conformidad con las solicitudes de viaje en tiempo real.</b>
		</li>
		<li><b>
		Después de utilizar los Servicios tras enviar una orden de viaje, el Pasajero pagará inmediatamente el cargo total de la orden presentada por Lubo. Si hay cualquier cargo vencido, la Compañía tendrá derecho a negarse a prestar Servicios al Pasajero. El Pasajero entiende y acepta que la Compañía tendrá derecho a enviar información con respecto al incumplimiento de contrato por parte del Pasajero a una sociedad de información crediticia externa.</b>
		</li>
		<li>
		El Pasajero podrá pagar en Lubo a través de su cuenta de pago electrónico externa y sistemas de pago bancario en línea. El procesamiento de los pagos estará sujeto a los presentes Términos y Condiciones y a los términos y políticas de los proveedores de servicios de pago electrónico y bancos emisores de tarjetas de crédito/débito. Lubo no será responsable de error alguno cometido por los proveedores de servicios de pago electrónico o por los bancos. Lubo obtendrá los detalles de las operaciones específicas relacionadas con el uso de los Servicios por el Pasajero. Lubo cumplirá estrictamente con las leyes y reglamentos aplicables y las políticas de la Compañía al utilizar dicha información.
		</li>
		<li>
		El Pasajero reconoce y acepta que todos y cada uno de los comprobantes fiscales que el Pasajero requiera por los servicios de transportación serán emitidos por el Conductor u otro individuo o entidad que administra las actividades del Conductor, según sea aplicable conforme a las leyes y reglamentos aplicables. El Pasajero será responsable por (i) solicitar todos y cada uno de los comprobantes fiscales e (ii) enviar toda la información personal requerida por Lubo y sitios web relevantes a partir de la fecha en que los medios para solicitar los comprobantes fiscales y para enviar dicha información están disponibles en Lubo o en el sitio web relevante de Lubo, o cualquier otro medio que Lubo considere adecuado para tal propósito. Aunque Lubo podrá facilitarles a los Conductores la emisión de comprobantes, Lubo no será responsable por la emisión de cualquier comprobante requerido por los Pasajeros por los servicios de transportación prestados, toda vez que Lubo no presta dicho servicio, ni será responsable de cualquier error o falla en la entrega de los comprobantes fiscales correspondientes.De ninguna manera Lubo será responsable por cualquier error, incoherencia o mal funcionamiento de Lubo o sitios web relevantes que puedan afectar la emisión de comprobantes fiscales. El Pasajero reconoce que es responsabilidad exclusiva del Conductor o de la persona o entidad que presta el servicio de transporte, quien corresponda, entregar debidamente los comprobantes fiscales al Pasajero.
		</li>
		<li>
		El Conductor reconoce y acepta que Lubo podrá facilitar la emisión de los comprobantes fiscales que el Conductor u otro individuo o entidad que administra las actividades del Conductor estén obligados a emitir de conformidad con las leyes y reglamentos aplicables. El Pasajero reconoce que dicha facilidad podrá consistir en la emisión de dichos comprobantes fiscales por parte de Lubo, o cualquier tercero designado por Lubo para tales efectos, a nombre y por cuenta del Conductor, en los términos de la regla 2.7.1.3. de la Resolución Miscelánea Fiscal para 2018 (o cualquier disposición que la sustituya), o mediante cualquier otro mecanismo que Lubo considere apropiado. El Pasajero reconoce que ni Lubo ni cualquier tercero designado por Lubo para dicho propósito serán responsables por la emisión de los comprobantes fiscales derivados de la aplicación de dicha regla 2.7.1.3., requeridos por los Pasajeros por los servicios de transportación. Ni Lubo ni cualquier tercero designado por Lubo para tales efectos no serán responsables por cualquier error o falla en la emisión de los comprobantes fiscales aplicables.
		</li>
		<li>
		El Pasajero podrá gestionar sus viajes. El Pasajero podrá verificar el estado de su orden en Lubo.
		</li>
		<li>
		El Pasajero verificará el monto que efectivamente pagará por la orden inmediatamente después de completar cada orden. Si el Pasajero tiene cualquier objeción en cuanto al monto del pago, contactará a Lubo.
		</li>
		<li>
		Los servicios de telecomunicaciones y los cobros relacionados con estos, en los que se incurra por el uso de Lubo, son prestados por los operadores de telecomunicaciones y no por Lubo.
		</li>
	</ol>
	
	<p><b>6- Indemnización</b></p>
	<ol>
		Al utilizar los Servicios de Lubo, el Pasajero se obliga a indemnizar a Lubo por todas y cada una de las reclamaciones, costos, compensación, pérdidas, deudas y gastos, incluyendo sin limitación, honorarios de abogados y gastos y costas judiciales, derivadas de o relacionadas con lo siguiente (en lo sucesivo denominados como un “Acto Ilícito”):
		<ul>
			<li>
			el Pasajero incumple o viola cualquier término de los presentes Términos y Condiciones o cualquier ley o reglamento aplicable (ya sea o no que se mencione en este instrumento);
			</li>
			<li>
			el Pasajero infringe cualquier derecho de cualquier tercero;
			</li>
			<li>
			el Pasajero abusa de Lubo o los Servicios;
			</li>
			<li>
			el Pasajero causa daños al vehículo o su equipo interno debido a negligencia grave o dolo.
			</li>
		</ul>
	</ol>
	
	<p><b>7- Restricciones Regulatorias Locales </b></p>
	<p>
		Lubo no está dirigido a persona alguna en cualquier jurisdicción en la que (debido a la nacionalidad, residencia u otro motivo) el acceso a o disponibilidad a Lubo esté prohibido o estaría sujeto a cualquier restricción, incluyendo requerimientos de registro u otros requerimientos dentro de dicha jurisdicción. Lubo se reserva el derecho de limitar el acceso a Lubo a cualquiera de estas personas. Las personas que accedan a Lubo lo hacen por iniciativa propia y serán responsables de cumplir con las leyes y reglamentos locales aplicables. Lubo no será responsable frente a ningún usuario por pérdida o daño alguno, ya sea conforme una disposición contractual, por responsabilidad civil (incluyendo negligencia), incumplimiento de un deber conforme a la ley o por otro motivo, aun cuando sea previsible, derivado de o relacionado con el uso por una persona en una jurisdicción donde el acceso a o la disponibilidad de Lubo esté prohibido o sujeto cualquier restricción. En caso de duda, el Pasajero deberá obtener asesoría legal independiente.
	</p>
	
	<p><b>8- Ausencia de Garantías </b></p>
	<ol>
		<li>
		LuboPassenger se ofrece “en el estado en que se encuentra”. Ninguna garantía es dada ni expresa ni implícita, con respecto a todo el contenido de Lubo y en el material publicado en la aplicación, incluyendo sin limitación, el contenido.
		</li>
		<li>
		Salvo por lo expresamente establecido en los presentes Términos y Condiciones, todas las garantías, condiciones y declaraciones, expresas o implícitas por ley o de otra manera establecidas por Lubo (incluyendo, sin limitación, garantías en cuanto a calidad satisfactoria, idoneidad para un fin o pericia y cuidado) quedan excluidas por esta disposición cuando la ley lo permita.
		</li>
	</ol>
	
	<p><b>9- Limitación de la Responsabilidad de Lubo</b></p>
	<ul>
		<li>
		La información proporcionada y los Servicios recomendados al Pasajero en los sitios web de Lubo o Lubo son únicamente para referencia del Pasajero. Lubo hará esfuerzos razonables para garantizar la precisión de dicha información, en el entendido de que Lubo no garantiza que dicha información este libre de cualquier error, defecto, software malicioso o virus.
		</li>
		<li>
		Lubo no será responsable por daño alguno que resulte del uso del (o cualquier imposibilidad de usar el) sitios web de Lubo o Lubo. Asimismo, Lubo no será responsable por daño alguno que resulte del uso de (o cualquier imposibilidad de usar) las herramientas de comunicación electrónica de los sitios web de Lubo o Lubo, incluyendo, sin limitación, daños causados por fallas de transmisión, mal funcionamiento de Internet o retraso en las comunicaciones electrónicas, intercepción o manipulación de comunicaciones electrónicas por un tercero o programas de cómputo utilizados para la comunicación electrónica y transmisión del virus, falta de energía, huelgas u otras disputas laborales, disturbios, insurrecciones, revueltas; terremotos, incendios, inundación, tormentas, explosiones, guerras; actos del gobierno, órdenes de autoridades judiciales o administrativas o cualquier otra causa de fuerza mayor u omisión de terceros.
		</li>
		<li>
		Lubo no será responsable por cualquier daño indirecto, emergente, especial, ejemplar, punitivo o incidental, incluyendo pérdida de utilidades, pérdida de información, lesiones o daños materiales relacionados con o que de otra manera resulten de cualquier uso de los Servicios, independientemente de la negligencia (que sea activa, afirmativa, única o concurrente) de Lubo, aún si Lubo ha sido advertida de la posibilidad de tales daños.
		</li>
		<li>
		Lubo no asume responsabilidad alguna con respecto a la precisión, integridad suficiencia y confiabilidad de la información y contenido incluido en Lubo o en los sitios web de Lubo, incluyendo sin limitación textos, imágenes, datos, opiniones, páginas web o enlaces, a pesar de sus esfuerzos de proporcionar información precisa y exhaustiva en la medida posible. Lubo se deslinda de cualquier responsabilidad por cualquier error u omisión y no otorga garantía expresa o implícita alguna.
		</li>
		<li>
		El Pasajero entiende y reconoce que cuando solicita servicios de transporte en automóvil en línea en Lubo, la Compañía procesa una gran cantidad de información en el servidor interno con base en dicha solicitud y proporciona información sobre los vehículos disponibles al dispositivo móvil del Pasajero, después de lo cual el Conductor exitosamente vinculado provee servicios de transporte fuera de línea al Pasajero.
		</li>
	</ul>
	
	<p><b>10- Autorización y Licencia </b></p>
	<ul>
		<li>
		Sujeto al cumplimiento por parte del Pasajero con los presentesTérminos y Condiciones, Lubo otorga al Pasajero una licencia limitada, no exclusiva y no transferible para descargar e instalar una copia de las aplicaciones en un solo dispositivo móvil que el Pasajero posea o controle para utilizar dicha copia de las aplicaciones únicamente para el uso personal del propio Pasajero.
		</li>
		<li>
		El Pasajero no podrá: (1) otorgar licencias o sub-licencias, vender, revender, transmitir, ceder, distribuir o de otra manera explotar comercialmente o poner a disposición de terceros los servicios o aplicaciones en forma alguna; (2) modificar o crear obras derivadas basadas en los Servicios o las aplicaciones; (3) crear “enlaces” de Internet a los Servicios, o “enmarcar” o “reproducir” cualquier aplicación en cualquier otro servidor o dispositivo inalámbrico o basado en el Internet; (4) llevar a cabo ingeniería de reversa o acceder a las aplicaciones con el fin de diseñar o crear un producto o Servicio competitivo, diseñar o crear un producto utilizando ideas o gráficos similares a los Servicios o las aplicaciones, o copiar cualesquier ideas, características, funciones o gráficos de los Servicios o las aplicaciones; o (5) lanzar un programa o script automatizado, o cualquier programa que pueda hacer múltiples solicitudes a servidores por segundo, o que dificulte u obstaculice indebidamente la operación y/o desempeño de los Servicios o las aplicaciones.
		</li>
		<li>
		Asimismo, el Pasajero no deberá: (1) enviar spam o mensajes duplicados o no solicitados en violación de las leyes aplicables; (2) enviar o almacenar material violatorio, obsceno, amenazante, difamatorio o que de otra manera sea ilegal o ilícito, incluyendo material que infrinja los derechos de privacidad de terceros; (3) enviar o almacenar material que contenga virus de software, gusanos, caballos troyanos u otro código informático, archivos, scripts, agentes o programas perjudiciales; (4) interferir con o afectar la integridad o el funcionamiento de los sitios web de Lubo, sus aplicaciones o los Servicios o los datos contenidos en los mismo; ni (5) intentar obtener acceso no autorizado al sitios web de Lubo, sus aplicaciones, los Servicios o sistemas o redes relacionados.
		</li>
		<li>
		Lubo tendrá derecho a investigar y promover un juicio por cualquiera de los incumplimientos antes mencionados en la mayor medida permitida por la ley. Lubo podrá participar en y asistir a las autoridades encargadas del orden público en las demandas en contra de cualquier Pasajero que haya incumplido los presentes Términos y Condiciones. Si Lubo determina que cualquier contenido infringe los presentes Términos y Condiciones o de otra manera perjudica a los sitios web de Lubo, Lubo y/o los Servicios o aplicaciones relacionados, Lubo se reserva el derecho de eliminar o prohibir el acceso a dicho contenido en cualquier momento sin previa notificación
		</li>
	</ul>
	
	<p><b>11- Política de Propiedad Intelectual</b></p>
	<ol>
		<li>
		Lubo y sus filiales son titulares de los derechos de propiedad intelectual de todo el contenido distribuido en Lubo, incluyendo, sin limitación, el software proporcionado y los productos o servicios relacionados, y tales derechos de propiedad intelectual están protegidos por las leyes. La falta de una declaración de titularidad en cierto contenido no implica que Lubo no tenga la titularidad del mismo o no pueda hacer valer sus derechos sobre el mismo y el Pasajero respetará los legítimos derechos e intereses del titular y utilizará dicho contenido legalmente de conformidad con las leyes y reglamentos aplicables y el principio de buena fe.
		</li>
		<li>
		Sin el consentimiento por escrito de Lubo, ninguna persona física o moral utilizará, copiará, modificará, hará extractos de, incluirá con otros productos para su uso o venta, vinculará ni transmitirá vía un híper-vínculo, almacenará en un sistema de recuperación de información ni utilizará para ningún otro fin comercial cualquier parte del software, de los productos o servicios, de la información o las palabras antes mencionadas en forma alguna bajo ninguna circunstancia, excepto para la descarga o impresión para uso personal no comercial, en el entendido de que no se realizará modificación alguna a lo anterior, y que la declaración de derechos de autor u otra titularidad contenida en los mismos se conservará.
		</li>
		<li>
		Las marcas registradas y logotipos (en lo sucesivo conjuntamente denominados como “Marcas Comerciales”) utilizadas y mostradas en dicho software constituirán las Marcas Comerciales, registradas o no registradas, de Lubo y sus filiales en la industria de contratación de servicios de transporte terrestre y otros campos relacionados, mismas que están protegidas por las leyes. Ninguna persona utilizará contenido alguno de dicho software, “Lubo” y nombres similares, así como las Marcas Comerciales en forma alguna sin el consentimiento por escrito de Lubo.
		</li>
		<li>
		Si el Pasajero imprime, copia, descarga, modifica o vincula cualquier parte del Contenido disponible a través de las páginas web o las aplicaciones, en incumplimiento de los presentes Términos y Condiciones, el derecho del Pasajero de utilizar los sitios web de la Compañía y las aplicaciones podrán suspenderse inmediatamente y el Pasajero deberá, a discreción de la Compañía, devolver o destruir cualesquier copias (electrónicas o en otro formato) de los materiales que haya realizado.
		</li>
	</ol>
	
	<p><b>12- Servicios y Enlaces de Terceros</b></p>
	<ul>
		<li>
		Durante el uso de las páginas web y aplicaciones relevantes, Lubo podrá, de tiempo en tiempo, proporcionar al Pasajero enlaces a sitios web propiedad de y controlados por terceros, para facilitar la comunicación del Pasajero con, la compra de productos o servicios a o para participar en actividades promocionales ofertadas por dichos terceros. Al hacer clic en dichos enlaces links, el Pasajero abandonará el sitio web de Lubo o Lubo y visitará sitios web alojados por dichos terceros que están fuera del control de Lubo, y donde dichos terceros han elaborado sus propios términos, condiciones y políticas de privacidad. Por lo tanto, Lubo no será responsable del contenido y las actividades de dichas páginas web y Lubo no asumirá obligación alguna al respecto. El Pasajero entenderá plenamente el contenido y las actividades de dichos sitios web y asumirá plenamente la responsabilidad legal y los riesgos derivados de la navegación o acceso a dichos sitios web por parte del Pasajero.
		</li>
	</ul>
	
	<p><b>13- Términos</b></p>
	<ol>
		<li>
		Los presentes Términos y Condiciones entre el Pasajero y Lubo no tendrán un término de vigencia establecido.
		</li>
		<li>
		La Compañía determinará a su entera discreción, si ha ocurrido un Acto Ilícito. Si el Pasajero comete cualquier Acto Ilícito, la Compañía podrá, a su entera discreción, tomar las medidas que considere convenientes, incluyendo sin limitación, dar por terminados los presentes Términos y Condiciones con el Pasajero y dejar de prestar Servicios al Pasajero, según requiera el caso. Un Acto Ilícito podrá resultar en las siguientes medidas:
			<ul>
				<li>
				La Compañía tendrá derecho a cobrar a la parte responsable cualquier cantidad que supere el monto de la indemnización que corresponda conforme a la ley, así como a tomar medidas en contra de las partes incumplidas o infractoras.
				</li>
				<li>
				El Pasajero tendrá derecho a dar por terminados los Convenios en cualquier momento cancelando la cuenta de usuario del Pasajero en cualquier momento de conformidad con las instrucciones publicadas en el sitio web de Lubo. Después de dicha cancelación, el Pasajero no podrá utilizar Lubo ni los Servicios correspondientes hasta que el Pasajero se registre nuevamente y vuelva a instalar Lubo exitosamente.
				</li>
			</ul>
		</li>
		<li>
		Aunque Lubo de por terminados los presentes Términos y Condiciones, el Pasajero cumplirá con sus obligaciones de pago y será responsable por cualquier daño o perjuicio que pueda derivar de dicho incumplimiento.
		</li>
		<li>
		Lubo no estará obligada a notificar la terminación de los presentes Términos y Condiciones con anticipación. Después de la terminación, Lubo enviará la notificación respectiva de conformidad con los presentes Términos y Condiciones.
		</li>
	</ol>
	
	
	<p><b>14- Fuerza Mayor</b></p>
	<p>
		En caso de que se presente una fuerza mayor, la parte afectada podrá suspender temporalmente el cumplimiento de sus obligaciones conforme al presente instrumento hasta que el efecto de dicha causa de fuerza mayor cese, y no incurrirá en incumplimiento de contrato; en el entendido, no obstante que dicha parte hará su mejor esfuerzo para resolver dicha causa y mitigar las pérdidas. Fuerza mayor significa cualquier causa impredecible e inevitable (aun cuando sea previsible) fuera del control de las partes que impida, afecte o demore el cumplimiento por una parte de todas o cualquiera de sus obligaciones conforme al presente instrumento. Dichas causas incluyen, sin limitación, terremotos, guerra, modificación del gobierno, de las leyes reglamentos y políticas gubernamentales, virus de cómputo, ataques de hackers o suspensión de Servicios prestados por empresas de telecomunicación.
	</p>
	
	<p><b>15- Disposiciones</b></p>
	<ol>
		<li>
		Si se declara la nulidad de ciertos términos de los presentes Términos y Condiciones, pero los demás términos pueden seguir siendo válidos y su exigibilidad no se ve afectada, Lubo determinará si continuará cumpliendo o no con tales otros términos.
		</li>
		<li>
		Lubo podrá entregar una notificación publicando una notificación general en su sitio web y/o en Lubo o enviando un correo electrónico o mensaje de texto a la dirección de correo electrónico o número de teléfono móvil registrado en la información de la cuenta del Pasajero. Las notificaciones, que podrán publicarse de tiempo en tiempo, constituirán parte de los presentes Términos y Condiciones.
		</li>
		<li>
		El Pasajero no cederá ninguno de los derechos conforme a los presentes Términos y Condiciones sin el previo consentimiento por escrito de Lubo.
		</li>
	</ol>
	
	<p><b>16- Otros Términos Aplicables</b></p>
	<p>Los presentes Términos y Condiciones hacen referencia a los siguientes términos adicionales, los cuales también serán aplicables al uso de los sitios web, contenido, productos, Servicios y aplicaciones de la Compañía por el Pasajero, mismos que, al utilizarlos, el Pasajero se obliga a cumplir:</p>
	<ol>
		<li>
		Aviso de Privacidad de Lubo establece los términos conforme a los cuales los datos personales y otra información recopiladas o proporcionada por el Pasajero deberá ser tratada.
		</li>
	</ol>
	
	<p><b>17- Ley Aplicable</b></p>
	<p>
		Los presentes Términos y Condiciones se regirán por las leyes aplicables en México. Cualquier conflicto, reclamación o controversia derivada de o relacionada con el incumplimiento, terminación, cumplimiento, interpretación o validez de los presentes Términos y Condiciones o el uso de nuestro sitio web o Lubo, se someterá a la jurisdicción de un tribunal competente de la Ciudad de Huauchinango, México y las partes en este acto expresa e irrevocablemente renuncian a cualquier otra jurisdicción que pueda corresponderles en virtud de sus domicilios respectivos, presentes o futuros.
	</p>
	
	<p><b>18- Subsistencia </b></p>
	<p>
		Aún cuando los presentes Términos y Condiciones den por terminados o anulen, las disposiciones relacionadas con la responsabilidad por incumplimiento, propiedad industrial, obligación de confidencialidad del Pasajero, ley aplicable y jurisdicción subsistirán.
	</p>
	
					</div>
					
				</div>
			</div>
			</section>
		</div>
		
		
		
        <div class="clearfix">
	        
		<div class="footer-container">
			<section class="sliderQR section-footer" style="background-color: transparent;">
		        <?php include_once('../menus/footer-in.php');?>
			</section>
		</div>
		
		
		
				
		<script src="../js/jquery.min.js"></script>
        <script src="../js/jquery.plugin.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.flexslider-min.js"></script>
        <script src="../js/smooth-scroll.min.js"></script>
        <script src="../js/skrollr.min.js"></script>
        <script src="../js/scrollReveal.min.js"></script>
        <script src="../js/lightbox.min.js"></script>
        <script src="../js/scripts.js"></script>
        
        
        <script>
    	$(document).ready(function (){
		    //validateB();
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
                document.getElementById("headerLogos").style.display="none";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		});
	</script>
	<script>
		document.getElementsByTagName("BODY")[0].onresize = function() {myFunction()};
		function myFunction() {
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		}
		
	</script>
        
        <script type="text/javascript">
	    	jQuery(document).ready(function() {
	    	jQuery('.telefono').keypress(function(tecla) {
	        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
	   			 });
			});
	    </script>
	
	
	    <script type="text/javascript">
			$(document).ready(function() {
				$("#frmValidate").hide()
				$("#datos").hide()
			});
			var input=  document.getElementById('numero1');
			input.addEventListener('input',function(){
			  if (this.value.length > 10) 
			     this.value = this.value.slice(0,10); 
			})
		</script>
	
	
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#numero22").val(value);
	          });
	      });
		</script>
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#telefono3").val(value);
	          });
	      });
		</script>
	
    </body>
</html>
				