<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<title>Lubo</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- icon -->
	<link rel="icon" type="image/x-icon" href="img/favicon/favicon.ico" />
	<link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
	<link rel="manifest" href="img/favicon/site.webmanifest">
	<link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript" src="js/eskju.jquery.scrollflow.min.js"></script>

	<link href="css/flexslider.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/line-icons.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
	
	<link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-NX8TKGG');
	</script>
	<!-- End Google Tag Manager -->


	<!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
	<script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="css/quick/style.css"> <!-- Resource style -->
	<!-- Add jQuery basic library -->
	<script type="text/javascript" src="jquery-lib.js"></script>
	<script type="text/javascript" src="fancybox/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="fancybox/helpers/jquery.fancybox-media.js"></script>
	<link href="fancybox/jquery.fancybox.css" rel="stylesheet">
	<script type="text/javascript" src="js/config/config.js"></script><!-- url -->
	<script type="text/javascript" src="js/sw/servicios.js"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150520430-1"></script>
	<script>
		if (modoServidor === 'prod') {
			window.dataLayer = window.dataLayer || [];

			function gtag() {
				dataLayer.push(arguments);
			}
			gtag('js', new Date());
			gtag('config', 'UA-150520430-1');
		}
	</script>
	<!--  -->
	<script type="text/javascript">
		function cargando() {
			// validar token
			var token = sessionStorage.getItem('Token');
			console.log(token)
			if (token != null) {
				document.getElementById('log-user').innerHTML = 'Logout'
				document.getElementById('log-user').onclick = logout
			}
		}
	</script>

	<!-- Facebook Pixel Code -->
	<script>
		! function(f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function() {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '189972612211292');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=189972612211292&ev=PageView&noscript=1" /></noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150520430-1"></script>
	<script>
		if (modoServidor === 'prod') {
			window.dataLayer = window.dataLayer || [];

			function gtag() {
				dataLayer.push(arguments);
			}
			gtag('js', new Date());
			gtag('config', 'UA-150520430-1');
		}
	</script>

</head>

<body onload="cargando()">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX8TKGG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="nav-container">
		<?php
		$currentPage = 'menu-inicio';
		include_once("menus/menu-stickyRD.php");
		?>

	</div>

	<div class="main-container">
		<header class="fullscreen-element no-pad centered-text margin-section">
			<div class="background-image-holder parallax-background overlay">
				<img class="background-image" alt="Background Image" src="img/inicioPink.png">
			</div>

			<div class="container align-vertical">
				<div class="row">
					<div class="text-center">
						<h1 class="text-white h1-hol titulo-new">Descubre <br><span class="titulo-new-span">la ciudad.</span></h1>
						<img class="imgE1" src="img/E1.png" alt="Imagen E1">

					</div>
				</div>
				<!--end of row-->
			</div>
			<!--end of container-->
		</header>


		<style>

		</style>


		<section class="video-inline margin-section" style="background-color: #9b00dc; height: auto;">
			<div class="container">
				<div class="row row-mas-ganancias">
					<div class="col-sm-5 col-md-6 col-mas-ganancias">
						<h1 class="text-white proximaNovaBold titulo-comun"> <b>Menos comisión. <br>Más ganancias.</b></h1>
						<p class="text-white proximaNovaRegular parrafo-comun"> Nuestras comisiones son las más bajas del mercado <br>
							conduce con nosotros y descubre la diferencia.</p>
						<a style="margin-top:60px" class="btn-conoce-mas_pink" href="https://ayuda.lubo.com.mx/ganancias-y-promociones/comisiones/comisiones-de-lubo.html">Conoce más.</a>


						<a style="margin-top:60px" class="link-block-registrate text-white proximaNovaRegular" href="driver/registro/iniciar">Regístrate como conductor...</a>

					</div>
					<div class="col-sm-5 col-md-6">
						<img id="imgGanancias" src="img/Conductor-Ganancias.png" alt="">

					</div>
				</div>
				<!--end of row-->
			</div>
			<!--end of container-->
		</section>



		<style>

		</style>

		<section class="margin-section" style="background-color: transparent;">
			<center>
				<h1 class="h1-ventajas text-white proximaNovaBold"><b>Tu seguridad <br> es MUY importante.</b></h1>
			</center>
			<div class="row backGMorado">
			</div>
			<div class="container container-fluid">

				<div class="row conten">
					<div class="col-xs-6 col-md-3" style="background-color: transparent;">
						<center>
							<div class=" scrollflow -pop -opacity" style="width:80%; background-color: rgba(155,0,220,0);">
								<div class="card-b">
									<h2 class="text-white text-left proximaNovaBold" style="font-size:30px"><b> Conductor</b></h2>
									<br>
									<p class="text-white text-left proximaNovaRegular text-seguridad">Te cuidamos durante todo el camino pues tu eres el motor de lubo. <br> Queremos que puedas crecer junto a nosotros y salir adelante.</p>

								</div>
								<a class="btn-conoce-mas-tu-seguridad" href="#">conoce más</a>
							</div>
						</center>
					</div>
					<div class="col-xs-6 col-md-3" style="margin-top:-75px">
						<center>
							<div class="scrollflow -pop -opacity" style="width:106%; margin-top:12px">
								<img id="imgConductor" src="img/Conductor.png" alt="Card image">

							</div>
						</center>
					</div>
					<div class="col-xs-6 col-md-3" style=" margin-top:-75px">
						<center>
							<div class="scrollflow -pop -opacity" style="width:100%">
								<img id="imgPasajera" src="img/Pasajera.png" alt="Card image">

							</div>
						</center>
					</div>
					<div class="col-xs-6 col-md-3" style="background-color: transparent;">
						<center>
							<div class=" scrollflow -pop -opacity" style="width:80%; background-color: rgba(155,0,220,0);">
								<div class="card-b">
									<h2 id="textBlack" class="text-white text-left proximaNovaBold" style="font-size:30px"><b> Pasajero</b></h2>
									<br>
									<p id="textBlack" class="text-left proximaNovaRegular text-seguridad text-seguridad2 ">Queremos que siempre llegues seguro y a tiempo a tus destinos del día a día. <br> Nuestros conductores están plenamente verificados y son expertos a la hora de conducir.</p>
								</div>
								<a class="btn-conoce-mas-tu-seguridad btn-seguirdad2" href="#">conoce más</a>

							</div>
						</center>
					</div>
				</div>
				<!--end of row-->
			</div>
			<!--end of container-->

		</section>
		<!-- end divider -->




		<section class="video-inline">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-covid">
						<h1 class="text-black proximaNovaBold titulo-covid"><b>Nuestro compromiso</b></h1>
						<h1 class="text-black proximaNovaBold" style="font-size:25px; margin-top: -35px;"><b>ante el COVID-19</b></h1>
						<p style="font-size: 20px;  color: #000;" class="proximaNovaRegular">Adoptamos y aplicamos estrictas medidas de seguridad e higiene para cuidarte a ti y así cuidarnos todos. <br>
							Queremos que puedas moverte conduciendo o viajando por la ciudad tranquilo y seguro con nosotros.</p>
						<br>
<!-- 						<a class="btn-conoce-mas_pink" href="#">conoce más</a> -->
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 ">
						<img src="img/Compromiso.png" alt="">
					</div>
				</div>
				<!--end of row-->
			</div>
			<!--end of container-->
		</section>

		<style>

		</style>
		
		<section style="background-color: transparent; padding-top: 20px;">
			<div class="container-fluid" style="background-color: #ec3092;">
				<div class="row">
					<div class="col-lg-6" style="background-color: #ec3092;">
						<div class="" style="background-color: transparent; padding: 60px;">
							<h1 id="descubre-diferencia" class="text-white proximaNovaBold"><b>Una nueva manera para viajar.</b></h1>
							<p class="text-white proximaNovaBold" style="font-size: 20px;"> Descubre la diferencia</p>
			
							<a style="margin-top:50px; margin-bottom: 50px;" class="btn-conoce-mas_purple proximaNovaRegular" href="viaja">conoce más</a>
			
							<a class="link-block-registrate proximaNovaRegular" href="#">Regístrate como usuario</a>
						</div>
					</div>
					<div class="col-lg-6 noPadding">
						<img class="img-responsive" src="img/Pasajera2.jpg" alt="">
					</div>
				</div>
			</div>
		</section>

	</div>

	<section style="padding-bottom: 0px;">
		<div class="container-fluid" style="padding-left: 0px; padding-right: 0px;">
			<img class="img-responsive" src="img/curvasF.png" alt="" width="100%">
		</div>
	</section>
	
	<footer style="background-color: #B600E1;">
		<?php include_once('menus/footerRD.php');?>
	</footer>
	
	




	<script src="js/jquery.min.js"></script>
	<script src="js/jquery.plugin.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.flexslider-min.js"></script>
	<script src="js/smooth-scroll.min.js"></script>
	<script src="js/skrollr.min.js"></script>
	<script src="js/scrollReveal.min.js"></script>
	<script src="js/scripts.js"></script>



</body>

</html>