<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Lubo</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- icon -->
		<link rel="icon" type="image/x-icon" href="img/favicon/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
		<link rel="manifest" href="img/favicon/site.webmanifest">
		<link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
        <link href="css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
		<link href="css/theme.css" rel="stylesheet" type="text/css" media="all"/>
		<link href="css/filedrag.css" rel="stylesheet" type="text/css" />
		
		<!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NX8TKGG');</script>
		<!-- End Google Tag Manager -->
		
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="js/config/config.js"></script><!-- url -->
        <script type="text/javascript" src="js/sw/servicios.js" ></script>


		<!-- Facebook Pixel Code -->
		<script>
				!function(f,b,e,v,n,t,s)
				{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
				fbq('init', '189972612211292');
				fbq('track', 'PageView');
			</script>
			<noscript><img height="1" width="1" style="display:none"
				src="https://www.facebook.com/tr?id=189972612211292&ev=PageView&noscript=1"
			/></noscript>
		<!-- End Facebook Pixel Code -->
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150520430-1"></script>
		<script>
		if(modoServidor === 'prod'){
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-150520430-1');
		}
		</script>
		<!-- End Global site tag (gtag.js) - Google Analytics -->
		
    </head>
    <body>
		<!-- Google Tag Manager (noscript) -->
	    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX8TKGG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->		
		<div class="nav-container">
			<?php 
				$currentPage = 'menu-login';
				include_once("menus/menu-sticky.php");
			?>
		</div>
		
		<div class="main-container">
			<section class="no-pad login-page fullscreen-element">
			
				<div class="background-image-holder">
					<img class="background-image" alt="Poster Image For Mobiles" src="img/login.jpg">
				</div>
			
				<div class="container align-vertical">
					<div class="row">
						<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
							<h1 class="text-white"><b>Recuperar contraseña</b></h1>
							<div class="photo-form-wrapper clearfix">
								<!-- <form> -->
									<input class="form-email" id="correo" type="text" placeholder="Correo electrónico">
									<!-- <input id="imagen" class="loadImg" type="image" src="" alt="" > -->
									<input id="entrar" onclick="recuperar($('#correo').val())" class="login-btn btn-filled" type="submit" value="Enviar correo">
								<!-- </form> -->
							</div><!--end of photo form wrapper-->
							<a href="login" class="text-white"><b>Iniciar sesión</b></a>
							<br><br>
							<p id="mensaje" style="display:none;" class="text-white">Te acabamos de enviar un correo para que puedas recuperar tu contraseña</p>
							<p id="tiempo" class="text-white" ></p>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
		</div>
		
		
		
		
				
		<script src="https://www.youtube.com/iframe_api"></script>
		<script src="js/jquery.min.js"></script>
        <script src="js/jquery.plugin.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.flexslider-min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/skrollr.min.js"></script>
        <script src="js/spectragram.min.js"></script>
        <script src="js/scrollReveal.min.js"></script>
        <script src="js/isotope.min.js"></script>
        <script src="js/twitterFetcher_v10_min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <script src="js/scripts.js"></script>
        
        
        <script>
    	$(document).ready(function (){
		    //validateB();
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="img/footerLubo.png";
                document.getElementById("headerLogos").style.display="none";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="img/footerMobile.png";
		    }
		});
	</script>
	<script>
		document.getElementsByTagName("BODY")[0].onresize = function() {myFunction()};
		function myFunction() {
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="img/footerLubo.png";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="img/footerMobile.png";
		    }
		}
		
	</script>
        
    </body>
</html>
				