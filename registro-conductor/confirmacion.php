<!--<?php
if (!isset($_GET['tel'])&&!isset($_GET['codigo'])){
 	header('Location: iniciar.php');	
 }
?>-->
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Lubo - Registro</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <!-- icon -->
		<link rel="icon" type="image/x-icon" href="../img/favicon/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
		<link rel="manifest" href="../img/favicon/site.webmanifest">
		<link rel="mask-icon" href="../img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
        <link href="../css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/theme.css" rel="stylesheet" type="text/css" media="all"/>
		<link rel="stylesheet" href="../css/frmregistro.css">
		<link rel="stylesheet" href="../css/frmvalidacion.css">
		
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="../js/config/config.js"></script><!-- url -->
		<script type="text/javascript" src="../js/sw/servicios.js" ></script>

<script>
	$(document).ready(function(){
		var a = document.getElementById("a"),
		    b = document.getElementById("b"),
		    c = document.getElementById("c");
		a.onkeyup = function() {
		    if (this.value.length === parseInt(this.attributes["maxlength"].value)) {
		        b.focus();
		    }
		}

		b.onkeyup = function() {
		    if (this.value.length === parseInt(this.attributes["maxlength"].value)) {
		        c.focus();
		    }
		}
		c.onkeyup = function() {
		    if (this.value.length === parseInt(this.attributes["maxlength"].value)) {
		        d.focus();
		    }
		}
	});
</script>
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
				
		<div class="nav-container">
			<?php 
				$currentPage = 'menu-driver';
				include_once("../menus/menu-sticky-in.php");
			?>
		</div>
		
		<div class="main-container">
			<header class="fullscreen-element no-pad centered-text">
				<div class="background-image-holder parallax-background overlay">
					<img class="background-image" alt="Background Image" src="../img/2inicio.png">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-md-12 text-left">
							<span class="text-white alt-font"></span>
							<h1 class="text-white h1-hola"><b>Excelente decisión</b>
								<br>
								<span class="subtitulo-registro">
								Estás a punto de ser un luboDriver, sigue unos cuantos pasos para
								<br>
								finalizar tu registro
								</span>
							</h1>
							<h2 class="text-white h2-msj-welcome">Conducir nunca había sido tan satisfactorio, seguro y confiable</h2>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</header>
		<!-- end divider -->
			
			
			<section class="video-inline" id="frmValidate">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-5 col-md-5">
							<form  name="xd" class="validacion">
								<br>
								<p class="textop">Código de verificación</p>
								<br>
								<center>
								<p>Enviado a <?php echo '+'.$_GET['codigo'].$_GET['tel']; ?></p>
								</center>
								<br>
								<center>
								<div class="row form-row" style="width: 80%">
							      <div class="col-md-3 col-xs-3 md-push-right-half sm-push-right-half xs-push-right-half">
									  <input type="text" class="cNumber" name="a" placeholder="0" id="a" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
				
									  <div class="error-notice"></div>
							      </div>
							      <div class="col-md-3 col-xs-3 md-push-left-half xs-push-left-half">
				
									  <input type="text" class="cNumber" name="b" placeholder="0" id="b" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
				
									  <div class="error-notice"></div>
							      </div>
							      <div class="col-md-3 col-xs-3 md-push-right-half sm-push-right-half xs-push-right-half">
									  <input type="text" class="cNumber" name="c" placeholder="0" id="c" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
				
									  <div class="error-notice"></div>
							      </div>
							      <div class="col-md-3 col-xs-3 md-push-right-half sm-push-right-half xs-push-right-half">
									  <input type="text" class="cNumber" name="d" placeholder="0" id="d" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
				
									  <div class="error-notice"></div>
							      </div>
							    </div> 
								</center>
								<br>
								<div class="container-fluid">
									<center><p id="reenviar"> &nbsp;<span></span>  </p></center>
								</div>
						 		<center >
									 <input type="hidden" name="telefono" id="telefono" value="<?php echo $_GET['tel']; ?>">
									 <input type="hidden" name="codigoPais" id="codigoPais" value="<?php echo $_GET['codigo'] ?>">
									<div class="form-group" align="center" style="margin-top: 50px;">
							  			<button id="boton" class="btn btn-success" onclick="validarcodigo($('#a').val(),$('#b').val(),$('#c').val(),$('#d').val(),$('#telefono').val(),$('#codigoPais').val())" type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo3">
											Siguiente
										</button>
									</div>	
								</center>
								<br>
								<br>				
							</form>
						</div>
						<div class="hidden-xs col-sm-1 col-md-1">
						</div>
						<div class="hidden-xs col-sm-6 col-md-6 align-vertical">
							<center>
							<br>
							<h2 class="h2-eleccion">Controla tu cuenta de manera correcta, nosotros lo facilitamos.</h2>
							</center>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
		</div>
		
		
		
        <div class="clearfix">
	        
		<div class="footer-container">
			<section class="sliderQR section-footer" style="background-color: transparent;">
		        <?php include_once('../menus/footer-in.php');?>
			</section>
		</div>
			
		<script src="https://www.youtube.com/iframe_api"></script>
		<script src="../js/jquery.min.js"></script>
        <script src="../js/jquery.plugin.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.flexslider-min.js"></script>
        <script src="../js/smooth-scroll.min.js"></script>
        <script src="../js/skrollr.min.js"></script>
        <script src="../js/spectragram.min.js"></script>
        <script src="../js/scrollReveal.min.js"></script>
        <script src="../js/isotope.min.js"></script>
        <script src="../js/twitterFetcher_v10_min.js"></script>
        <script src="../js/lightbox.min.js"></script>
        <script src="../js/scripts.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.format.js"></script>
        
        
        <script>
		
		$(document).ready(function (){
			crono()
		})
		function crono() {
			var segundos = 30
			var temp = setInterval(function () {
				document.getElementById('reenviar').innerHTML = ' No recibí mi codigo &nbsp; Reenviar en ' + segundos + '(s)'
				if (segundos <= 0) {
					clearInterval(temp)
					document.getElementById('reenviar').innerHTML = '<a style="cursor: pointer;" onclick="reen()">Reenviar</a>' 
				}
				segundos = segundos - 1
			},1000)
		}
		function reen(){
			var tel = <?php echo $_GET['tel'] ?>;
			var codigo = <?php echo $_GET['codigo'] ?>;
			parametros = {
				'CodigoPais':codigo,
				'Telefono':tel
			}
			$.ajax({
	                data:  parametros,
	                url:   url+'auth/validarNumero',
	                type:  'POST',
	                dataType: 'json',
	                cache: false,
	                beforeSend: function () {
				         //alert('Envindo Datos por ajax')
	                        return true
	                },
	                success: function(data)
	                {
	                	if(data['response']){ 
							// $(location).attr('href','confirmacion.php?tel='+telefono+'&codigo='+codigo+'#frmValidate')
							alert("Se reenvio el mensaje")
							crono()
	                	}else{
							alert(data['errors'])
							if (data['message'] == "1") {
								$(location).attr('href','../login.php')
							}
	                	}           
						return true
	                }
	    })
		}
    	$(document).ready(function (){
		    //validateB();
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
                document.getElementById("headerLogos").style.display="none";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		});
	</script>
	<script>
		document.getElementsByTagName("BODY")[0].onresize = function() {myFunction()};
		function myFunction() {
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		}
		
	</script>
        
        <script type="text/javascript">
	    	jQuery(document).ready(function() {
	    	jQuery('.telefono').keypress(function(tecla) {
	        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
	   			 });
			});
	    </script>
	
	
	    <script type="text/javascript">
			$(document).ready(function() {
				// $("#frmValidate").hide()
				$("#datos").hide()
			});
			var input=  document.getElementById('numero1');
			input.addEventListener('input',function(){
			  if (this.value.length > 10) 
			     this.value = this.value.slice(0,10); 
			})
		</script>
	
	
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#numero22").val(value);
	          });
	      });
		</script>
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#telefono3").val(value);
	          });
	      });
		</script>
	
    </body>
</html>
				