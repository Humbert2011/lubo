

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Lubo - Registro</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- icon -->
		<link rel="icon" type="image/x-icon" href="../img/favicon/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
		<link rel="manifest" href="../img/favicon/site.webmanifest">
		<link rel="mask-icon" href="../img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
        
        <link href="../css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/theme.css" rel="stylesheet" type="text/css" media="all"/>
		<link rel="stylesheet" href="../css/frmregistro.css">
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="../js/config/config.js"></script><!-- url -->
        <script type="text/javascript" src="../js/sw/servicios.js"></script>
        
        <script>
	    	function limpiarEstilo(){
		    	var element = document.getElementById("numero1");
				element.classList.remove("input-error");
			}
			function load() {
				var token = sessionStorage.getItem('Token');
				console.log(token)
				var idPersona = sessionStorage.getItem('IdPersona')
				if (token!=null) {
					$.ajax({
								// data:  parametros,
								url:   url+'registro/obtener/'+idPersona,
								type:  'GET',
								dataType: 'json',
								cache: false,
								beforeSend: function () {
									 //alert('Envindo Datos por ajax')
										return true
								},
								success: function(data)
								{
									if(data['response']){
										var result = data['result']
										var status = result['Status']
										// console.log(status)
											if (status <= 1 ) {
												$(location).attr('href','registro-auto.php#registro')
											}else{
												$(location).attr('href','mis-documentos.php#documentos')
											}
									}else{
										if(data['errors']==null){
											alert(data['message'])
										}else{
											alert(data['errors'])
										}
									}           
									return true
								}
							})
				}
				// validar(token)
			}
	    </script>

    </head>
    <body onload='load()'>
				
		<div class="nav-container">
			<?php 
				$currentPage = 'menu-driver';
				include_once("../menus/menu-sticky-in.php");
			?>
		</div>
		
		<div class="main-container">
			<header class="fullscreen-element no-pad centered-text">
				<div class="background-image-holder parallax-background overlay">
					<img class="background-image" alt="Background Image" src="../img/2inicio.png">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-md-12 text-left">
							<span class="text-white alt-font"></span>
							<h1 class="text-white h1-hola"><b>Excelente decisión</b>
								<br>
								<span class="subtitulo-registro">
								Estás a punto de ser un luboDriver, sigue unos cuantos pasos para
								<br>
								finalizar tu registro
								</span>
							</h1>
							<h2 class="text-white h2-msj-welcome">Conducir nunca había sido tan satisfactorio, seguro y confiable</h2>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</header>
		<!-- end divider -->
			
			
			
			<section class="video-inline">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-5 col-md-5">
							<form  name="xd" class="reg" id="frmRegistry">
								<br>
								<p class="textop">Registrarse</p>
								<br>
								<br>
								<br>
								<div class="group">
									<!--<input type="number" id="numero1" name="fiel1" class="telefono" min="1" max="11" placeholder="Número de teléfono">-->
									<input type="text" id="numero1" required id="nombre" name="fiel1" class="input-medium bfh-phone telefono imp2" placeholder="Número de teléfono" onkeypress="limpiarEstilo()" data-country="MX">
									<select id="imp2" class="seleccion" >
										<option value="52">+52</option>
										<option value="54">+54</option>
										<option value="32">+32</option>
										<option value="11">+11</option>
										<option value="23">+23</option>
									</select>
								</div>
								<br>
								<div class="container-fluid">
									<div class="row p-5">
									</div>
									<div class="row p-4">
									</div>		
								</div>
								<br>  
								<center>
								<div class="error-notice"><span id="telefono-error" class="signup-error"></span></div>
								</center>
						 		<center>
									<div class="form-group" align="center" style="margin-top: 50px;">
							  			<button id="botonderegistro" onClick="validarnumero($('#numero1').val(),$('#imp2').val())" type="button"  class="btn btn-info">
												Unirme
										</button>
									</div>	
								</center>
								<br>
								<br>				
							</form>
						</div>
						<div class="hidden-xs col-sm-1 col-md-1">
						</div>
						<div class="hidden-xs col-sm-6 col-md-6 align-vertical">
							<center>
							<br>
							<h2 class="h2-eleccion">Nosotros te avisamos cuando haya un nuevo viaje, no pierdes ninguno, a menos que no quieras hacerlo.</h2>
							</center>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
		</div>
		
		
		
        <div class="clearfix">
	        
		<div class="footer-container">
			<section class="sliderQR section-footer" style="background-color: transparent;">
		        <?php include_once('../menus/footer-in.php');?>
			</section>
		</div>
		
		
		
		
		<script src="../js/jquery.min.js"></script>
        <script src="../js/jquery.plugin.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.flexslider-min.js"></script>
        <script src="../js/smooth-scroll.min.js"></script>
        <script src="../js/skrollr.min.js"></script>
        <script src="../js/spectragram.min.js"></script>
        <script src="../js/scrollReveal.min.js"></script>
        <script src="../js/lightbox.min.js"></script>
        <script src="../js/scripts.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.format.js"></script>
        
        
        <script>
    	$(document).ready(function (){
		    //validateB();
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
                document.getElementById("headerLogos").style.display="none";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		});
	</script>
	
	<script>
		document.getElementsByTagName("BODY")[0].onresize = function() {myFunction()};
		function myFunction() {
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		}
		
	</script>
        
        <script type="text/javascript">
	    	// jQuery(document).ready(function() {
	    	// jQuery('.telefono').keypress(function(tecla) {
	        // if(tecla.charCode < 48 || tecla.charCode > 57) return false;
	   		// 	 });
			// });
	    </script>
	
	
	    <script type="text/javascript">
			$(document).ready(function() {
				$("#frmValidate").hide()
				$("#datos").hide()
			});
			var input=  document.getElementById('numero1');
			input.addEventListener('input',function(){
			  if (this.value.length > 10) 
			     this.value = this.value.slice(0,10); 
			})
		</script>
	
	
		<script>
	    //   $(document).ready(function () {
	    //       $("#numero1").keyup(function () {
	    //           var value = $(this).val();
	    //           $("#numero22").val(value);
	    //       });
		//   });
		$(document).on('keypress',function(e) {
			if(e.which == 13) {
				e.preventDefault();
				// alert('inf')
				validarnumero($('#numero1').val(),$('#imp2').val())
			}
		});
		</script>
		<script>
	    //   $(document).ready(function () {
	    //       $("#numero1").keyup(function () {
	    //           var value = $(this).val();
	    //           $("#telefono3").val(value);
	    //       });
	    //   });
		</script>
	
    </body>
</html>
				