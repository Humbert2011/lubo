

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Lubo - Datos Personales</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <!-- icon -->
		<link rel="icon" type="image/x-icon" href="../img/favicon/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
		<link rel="manifest" href="../img/favicon/site.webmanifest">
		<link rel="mask-icon" href="../img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
        <link href="../css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/theme.css" rel="stylesheet" type="text/css" media="all"/>
		<link rel="stylesheet" href="../css/frmregistro.css">
		<link rel="stylesheet" href="../css/frmdatos.css">
		<link rel="stylesheet" href="../css/documentos.css">
		<link rel="stylesheet" href="../css/frmdatos.css">
		<link rel="stylesheet" type="text/css" href="../css/filedrag.css">

        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->

        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">


        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../js/config/config.js"></script><!-- url -->
		<script type="text/javascript" src="../js/sw/img.js" ></script><!-- cambio -->
        <script type="text/javascript" src="../js/sw/servicios.js" ></script>
        <script type="text/javascript">
			function cargando() {
				// validar token
				var token = sessionStorage.getItem('Token');
				// 
				validar(token)
				// console.log(autenticar)
				var idPersona = sessionStorage.getItem('IdPersona')
				// ocultar elementos de aprobacion
				$('.btn-circle1').hide()
				// document.getElementsByClassName('btn-circle1').display = none
				// obtenere documentos de persona
				obtenerDocs(idPersona)
				// obtener auto
				obtenerA(idPersona)
				// console.log(persona)
				document.getElementById('log-user').innerHTML = 'Logout'
				document.getElementById('log-user').onclick = logout

				$.ajax({
						// data:  parametros,
						url:   url+'auth/getData/'+token,
						type:  'GET',
						dataType: 'json',
						cache: false,
						beforeSend: function () {
							//alert('Envindo Datos por ajax')
								return true
						},
						success: function(auth)
						{
						if(auth['response']){
									//console.log(auth)
									idUsr = auth['result']['id']
									imgPerfil = auth['result']['Imagen']
									if(imgPerfil == ''){
										$.ajax({
										url:   url+'img/verImgenPerfil/'+idUsr,
										type:  'GET',
										dataType: 'json',
										cache: false,
										beforeSend: function () {
												return true
										},
										success: function(verImg)
										{
											//console.log(verImg)
											if(verImg['response']){
												bloqueoImgPerfil()
											}
										}
										})
									}else{
										bloqueoImgPerfil()
									}
									return true
								}else{
									$(location).attr('href','../login.php')
								}           
							return true
						}
				}) 

			}

			let bloqueoImgPerfil = () => {
				document.getElementById('tramiteperfil').style.display = 'inline'
				document.getElementById('btperfil').disabled = true
				document.getElementById('imgperfil').disabled = true
				contarDocs()
			}

		</script>

 
	</head>
    <body onload="cargando()">
				
		<div class="nav-container">
			<?php 
				$currentPage = 'menu-driver';
				include_once("../menus/menu-sticky-in.php");
			?>
		</div>
		
		<div class="main-container">
			<header class="fullscreen-element no-pad centered-text">
				<div class="background-image-holder parallax-background overlay">
					<img class="background-image" alt="Background Image" src="../img/2inicio.png">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-md-12 text-left">
							<span class="text-white alt-font"></span>
							<h1 class="text-white h1-hola"><b>Excelente decisión</b>
								<br>
								<span class="subtitulo-registro">
								Estás a punto de ser un luboDriver, sigue unos cuantos pasos para
								<br>
								finalizar tu registro
								</span>
							</h1>
							<h2 class="text-white h2-msj-welcome">Conducir nunca había sido tan satisfactorio, seguro y confiable</h2>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</header>
		<!-- end divider -->
		<br>
		<br>
		<center>
			<h2 class="h2-eleccion">Mis documentos</h2>
		</center>
		<br>
		<div class="container-fluid">
	 	    <div class="row" id="documentos" >
			    <div class="col-12 col-md-12 col-lg-4">
					<br>
					<center>
						<br>
						<form id="ineFrente" >
						<div class="clase">
							<input id ="img6" type="file" name="img" onchange='cambio("tit6")' class="documentoAlta">
							<button id="tramite6" type="button"  class="btn-circle1 color" disabled="true"><span class="glyphicon glyphicon-ok"></span></button>
							<img class="card-img-top" src="../img/iconos-doc/card.png"  >
							<div class="card-body">
							    <p  class="card-text" style="">Identificación (frente)</p></p>
								<h7 class="card-title"><b>INE</b></h7>
								<p id="tit6" >Arrastra tu archivo o da click en esta área.</p>
							</div>
						</div>
						</form>
						<br>
						<button id="bt6" type="button" class="btn btnsubir color h" onclick="cargarDocumento(sessionStorage.getItem('IdPersona'),6,document.getElementById('ineFrente'))" >
							<b class="texto" >Subir</b>
						</button>
					</center>
			    </div>
			    <div class="col-12 col-md-12 col-lg-4">
					<br>
					<center>
						<br>
						<form id="ineAtras" >
						<div class="clase">
							<input id ="img7" type="file" name="img" onchange='cambio("tit7")' class="documentoAlta">
							<button id="tramite7" type="button" class="btn-circle1 color" disabled="true"><span class="glyphicon glyphicon-ok"></span></button>
							<img class="card-img-top" src="../img/iconos-doc/card.png"  >
							<div class="card-body">
							    <p class="card-text" >Identificación oficial (atrás)</p></p>
								<h7 class="card-title"><b>INE</b></h7>
								<p id="tit7" >Arrastra tu archivo o da click en esta área.</p>
							</div>
						</div>
						</form>
						<br>
						<style type="text/css">
	
						</style>
						<button id="bt7" type="button" class="btn btnsubir color h" onclick="cargarDocumento(sessionStorage.getItem('IdPersona'),7,document.getElementById('ineAtras'))" >
							<b class="texto" >Subir</b>
						</button>
					</center>
				</div>
			    <div class="col-12 col-md-12 col-lg-4">
					<br>
					<center>
						<br>
						<form id="curp">
						<div class="clase">
							<input id ="img11" type="file" name="img" onchange='cambio("tit11")' class="documentoAlta">
							<button id="tramite11" type="button" class="btn-circle1 color" disabled="true"><span class="glyphicon glyphicon-ok"></span></button>
							<img class="card-img-top" src="../img/iconos-doc/curp2.png">
							<div class="card-body">
							    <p class="card-text" style="">Clave Única de Registro</p></p>
								<h7 class="card-title"><b>CURP</b></h7>
								<p id="tit11" >Arrastra tu archivo o da click en esta área.</p>
							</div>
						</div>
						</form>
						<br>
						<button id="bt11" type="button" class="btn btnsubir color h" onclick="cargarDocumento(sessionStorage.getItem('IdPersona'),11,document.getElementById('curp'))" >
							<b class="texto" >Subir</b>
						</button>
					</center>
			    </div>
				<div class="col-12 col-md-12 col-lg-4">
					<br>
					<center>
						<br>
						<form id="comprobantededomicilio">
						<div class="clase">
							<input id ="img10" type="file" name="img" onchange='cambio("tit10")' class="documentoAlta">		
							<button id="tramite10" type="button" class="btn-circle1 color" disabled="true"><span class="glyphicon glyphicon-ok"></span></button>
							<img class="card-img-top" src="../img/iconos-doc/comprobante1.png">
							<div class="card-body">
							    <p class="card-text" style="">Comprobante de Domicilio</p></p>
								<h7 class="card-title"><b>****</b></h7>
								<p id="tit10" >Arrastra tu archivo o da click en esta área.</p>
							</div>
						</div>
						</form>
						<br>
						<button id="bt10" type="button" class="btn btnsubir color h" onclick="cargarDocumento(sessionStorage.getItem('IdPersona'),10,document.getElementById('comprobantededomicilio'))" >
							<b class="texto" >Subir</b>
						</button>
					</center>
				</div>
				<div class="col-12 col-md-12 col-lg-4">
					<br>
					<center>
						<br>
						<form id="licenciadeconducir">
						<div class="clase">
							<input id ="img8" type="file" name="img" onchange='cambio("tit8")' class="documentoAlta">		
							<button id="tramite8" type="button" class="btn-circle1 color" disabled="true"><span class="glyphicon glyphicon-ok"></span></button>
							<img class="card-img-top" src="../img/iconos-doc/icono2.png" >
							<div class="card-body">
							    <p class="card-text" style="">Licencia de Conducir</p></p>
								<h7 class="card-title"><b>****</b></h7>
								<p id="tit8" >Arrastra tu archivo o da click en esta área.</p>
							</div>
						</div>
						</form>
						<br>
						<button id="bt8" type="button" class="btn btnsubir color h" onclick="cargarDocumento(sessionStorage.getItem('IdPersona'),8,document.getElementById('licenciadeconducir'))" >
							<b class="texto" >Subir</b>
						</button>
					</center>
				</div>
				<div class="col-12 col-md-12 col-lg-4">
					<br>
					<center>
						<br>
						<form id="targetadecirculacion">
						<div class="clase">
							<input id ="img2" type="file" name="img" onchange='cambio("tit2")' class="documentoAlta">
							<button id="tramite2" type="button" class="btn-circle1 color" disabled="true"><span class="glyphicon glyphicon-ok"></span></button>
							<img class="card-img-top" src="../img/iconos-doc/icono2.png">
							<div class="card-body">
							    <p class="card-text" style="">Tarjeta de Circulación</p></p>
								<h7 class="card-title"><b>****</b></h7>
								<p id="tit2" >Arrastra tu archivo o da click en esta área.</p>
							</div>
						</div>
						</form>
						<br>
						<button id="bt2" type="button" class="btn btnsubir color h" onclick="cargarDocumento(sessionStorage.getItem('IdAuto'),2,document.getElementById('targetadecirculacion'))" >
							<b class="texto" >Subir</b>
						</button>
					</center>
				</div>
				<div class="col-12 col-md-12 col-lg-4">
					<br>
					<center>
						<br>
						<form id="polizadeseguro">
						<div class="clase">
							<input id ="img1" type="file" name="img" onchange='cambio("tit1")' class="documentoAlta">
							<button id="tramite1" type="button" class="btn-circle1 color" disabled="true"><span class="glyphicon glyphicon-ok"></span></button>
							<img class="card-img-top" src="../img/iconos-doc/3.png">
							<div class="card-body">
							    <p class="card-text" style="">Póliza de Seguro</p></p>
								<h7 class="card-title"><b>****</b></h7>
								<p id="tit1" >Arrastra tu archivo o da click en esta área.</p>
							</div>
						</div>
						</form>
						<br>
						<button id="bt1" type="button" class="btn btnsubir color h" onclick="cargarDocumento(sessionStorage.getItem('IdAuto'),1,document.getElementById('polizadeseguro'))" >
							<b class="texto" >Subir</b>
						</button>
					</center>
				</div>
				<div class="col-12 col-md-12 col-lg-4">
					<br>
					<center>
						<br>
						<form id="antecedentesnopenales">
						<div class="clase">
							<input id ="img5" type="file" name="img" onchange='cambio("tit5")' class="documentoAlta">
							<button id="tramite5" type="button" class="btn-circle1 color" disabled="true"><span class="glyphicon glyphicon-ok"></span></button>
							<img class="card-img-top" src="../img/iconos-doc/3.png">
							<div class="card-body">
							    <p class="card-text" style="">C. antecedentes no Penales</p></p>
								<h7 class="card-title"><b>****</b></h7>
								<p id="tit5" >Arrastra tu archivo o da click en esta área.</p>
							</div>
						</div>
						</form>
						<br>
						<button id="bt5" type="button" class="btn btnsubir color h" onclick="cargarDocumento(sessionStorage.getItem('IdPersona'),5,document.getElementById('antecedentesnopenales'))" >
							<b class="texto" >Subir</b>
						</button>
					</center>
				</div>
				<div class="col-12 col-md-12 col-lg-4">
					<br>
					<center>
						<br>
						<form id="rfc">
						<div class="clase">
							<input id ="img9" type="file" name="img" onchange='cambio("tit9")' class="documentoAlta">
							<button id="tramite9" type="button" class="btn-circle1 color" disabled="true"><span class="glyphicon glyphicon-ok"></span></button>
							<img class="card-img-top" src="../img/iconos-doc/card.png">
							<div class="card-body">
							    <p class="card-text" style="">Reg. Federal de Contribuyente</p></p>
								<h7 class="card-title"><b>RFC</b></h7>
								<p id="tit9" >Arrastra tu archivo o da click en esta área.</p>
							</div>
						</div>
						</form>
						<br>
						<button id="bt9" type="button" class="btn btnsubir color h" onclick="cargarDocumento(sessionStorage.getItem('IdPersona'),9,document.getElementById('rfc'))" >
							<b class="texto" >Subir</b>
						</button>
					</center>
				</div>
			
				<div class="col-12 col-md-12 col-lg-4">
					<br>
					<center>
						<br>
						<form id="perfil">
						<div class="clase">
							<input id ="imgperfil" type="file" name="img" onchange='cambio("titperfil")' class="documentoAlta">
							<button id="tramiteperfil" type="button" class="btn-circle1 color" disabled="true"><span class="glyphicon glyphicon-ok"></span></button>
							<img class="card-img-top" src="../img/iconos-doc/perfil.png" width="35%">
							<div class="card-body">
								<br>
								<p class="card-text" style="">Fotografía Personal</p></p>
							    <h7 class="card-title"><b>SELFIE</b></h7>
							    <p id="titperfil" >Arrastra tu archivo o da click en esta area.</p>
							</div>
						</div>
						</form>
						<br>
						<button id="btperfil" type="button" class="btn btnsubir color h" onclick="cargarDocumento(sessionStorage.getItem('IdPersona'),'perfil',document.getElementById('perfil'))" >
							<b class="texto" >Subir</b>
						</button>
					</center>
				</div>
				<div class="col-12 col-md-12 col-lg-4">
					<br>
					<center>
						<div class="row">
					  		<div class="p-5"  style="height:108px;">	
					  		</div>
						</div>
						<br>
						<!-- <button type="button" onclick="terminar()" class="btn color1 arriba z" id="myBtn" >
							<b class="texto">Terminar</b>
						</button> -->
					</center>
				</div>

	 		</div>
	 	</div>

	 	<div class="container" id="midialogo">
		  <div id="id01" class="w3-modal" role="dialog" aria-hidden="true" style="display: none;">
		    <div class="w3-modal-content  modal-dialog-centered" id="content" style="margin-top: 870px;">
		      	<div class="w3-container">
		      		<form role="form" class="exitB" >
		            	<center>
		            		<br>
		            		<h2 class="h2-eleccion">Bien :)</h2>
		            		<br>
		            		<h2>Vamos a comprobar tus documentos cuando hayamos <br> finalizado, enviaremos una notificación a tu correo</h2>
		            		<br>
		            		<h2 class="h2-eleccion">Gracias por elegir LuboDriver</h2>
		            		<br>
		            		<button type="button" class="btn-circle color" onclick="exitModal()"><span class="glyphicon glyphicon-ok"></span></button>
		            		<br>
		            		<br>
		            	</center>
		        	</form>
		      	</div>
		    </div>
		  </div>
		</div>

        <div class="clearfix">
		<div class="footer-container">
			<section class="sliderQR section-footer" style="background-color: transparent;">
		        <?php include_once('../menus/footer-in.php');?>
			</section>
		</div>

		<style type="text/css">
			.color{
				background-color:#ED3093;
			}
			.texto{
				color:white;
			}
			.h:hover{
				background-color:#ED3093;

			}
			.z:hover{
				background-color:#870080;

			}
			.color1{
				background-color:#870080;
			}
			.color3{
				background-color:#F1133F;
			}
			.btn-circle {
			  width: 55px;
			  height:55px;
			  color: white;
			  text-decoration: none;
			  border-radius: 28px;
			  outline: none;
			}
			.btn-circle1 {
			background-color:#2EA352;
			width:30px;
			height:30px;
			color: white;
			text-decoration: none;
			border-radius: 15px;
			outline: none;
			}
		</style>
		
		<script src="https://www.youtube.com/iframe_api"></script>
		<script src="../js/jquery.min.js"></script>
        <script src="../js/jquery.plugin.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.flexslider-min.js"></script>
        <script src="../js/smooth-scroll.min.js"></script>
        <script src="../js/skrollr.min.js"></script>
        <script src="../js/spectragram.min.js"></script>
        <script src="../js/scrollReveal.min.js"></script>
        <script src="../js/isotope.min.js"></script>
        <script src="../js/twitterFetcher_v10_min.js"></script>
        <script src="../js/lightbox.min.js"></script>
        <script src="../js/scripts.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.format.js"></script>

        <script>
        // cargado de cambios
  //       $(document).ready(function(e){
		// 		$('#ineFrente').change(function () {
		// 		// $('#pDrag').text(this.files.length + " file(s) selected");
		// 		console.log(this.files);
		// 		document.getElementById("tit1").innerHTML = "Archivo seleccionado "+this.files[0].name;
		// 		// document.getElementById('pDrag').text = this.files.length + "Archivo seleccionado"
		// 		});
		// });
        //cargado de cambios
		$(document).ready(function (){
			//validateB();
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
                document.getElementById("headerLogos").style.display="none";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		});
		</script>
		<script>
		function topFunction() {
		  document.body.scrollTop = 850;
		  document.documentElement.scrollTop = 850;
		}
		</script>
		<script>
			document.getElementsByTagName("BODY")[0].onresize = function() {myFunction()};
			function myFunction() {
			    var x = "Total Width: " + screen.width;
			    if ($(window).width() >= 768){
		            image = document.getElementById('imgHeader');
					image.src ="../img/footerLubo.png";
			    }
			    if ($(window).width() <= 767){
	    			image = document.getElementById('imgHeader');
					image.src ="../img/footerMobile.png";
			    }
			}
			
		</script>
        
        <script type="text/javascript">
	    	jQuery(document).ready(function() {
	    	jQuery('.telefono').keypress(function(tecla) {
	        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
	   			 });
			});
	    </script>
	
	
	    <script type="text/javascript">
			$(document).ready(function() {
				$("#frmValidate").hide()
				$("#datos").hide()
			});
			var input=  document.getElementById('numero1');
			input.addEventListener('input',function(){
			  if (this.value.length > 10) 
			     this.value = this.value.slice(0,10); 
			})
		</script>
	
	
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#numero22").val(value);
	          });
	      });
		</script>
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#telefono3").val(value);
	          });
	      });
		</script>
    </body>
</html>
				