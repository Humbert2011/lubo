

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Lubo</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <!-- icon -->
		<link rel="icon" type="image/x-icon" href="img/favicon/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
		<link rel="manifest" href="img/favicon/site.webmanifest">
		<link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script type="text/javascript" src="js/eskju.jquery.scrollflow.min.js"></script>

        <link href="css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/theme.css" rel="stylesheet" type="text/css" media="all"/>
        
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NX8TKGG');</script>
		<!-- End Google Tag Manager -->
        
        
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="css/quick/style.css"> <!-- Resource style -->
        <!-- Add jQuery basic library -->
		<script type="text/javascript" src="jquery-lib.js"></script>
		<script type="text/javascript" src="fancybox/jquery.fancybox.pack.js"></script>
		<script type="text/javascript" src="fancybox/helpers/jquery.fancybox-media.js"></script>
		<link href="fancybox/jquery.fancybox.css" rel="stylesheet">
		<script type="text/javascript" src="js/config/config.js"></script><!-- url -->
        <script type="text/javascript" src="js/sw/servicios.js" ></script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150520430-1"></script>
		<script>
		if(modoServidor === 'prod'){
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-150520430-1');
		}
		</script>
		<!--  -->
        <script type="text/javascript">
			function cargando() {
				// validar token
				var token = sessionStorage.getItem('Token');
				console.log(token)
				if (token != null) {
					document.getElementById('log-user').innerHTML = 'Logout'
					document.getElementById('log-user').onclick = logout
				}
			}
		</script>
		
		<!-- Facebook Pixel Code -->
			<script>
				!function(f,b,e,v,n,t,s)
				{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
				fbq('init', '189972612211292');
				fbq('track', 'PageView');
			</script>
			<noscript><img height="1" width="1" style="display:none"
				src="https://www.facebook.com/tr?id=189972612211292&ev=PageView&noscript=1"
			/></noscript>
		<!-- End Facebook Pixel Code -->
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150520430-1"></script>
		<script>
		if(modoServidor === 'prod'){
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-150520430-1');
		}
		</script>
		
    </head>
    <body onload="cargando()">
	    <!-- Google Tag Manager (noscript) -->
	    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX8TKGG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
				
		<div class="nav-container">
			<?php 
				$currentPage = 'menu-inicio';
				include_once("menus/menu-sticky.php");
			?>
			
		</div>
		
		<div class="main-container">
			<header class="fullscreen-element no-pad centered-text">
				<div class="background-image-holder parallax-background overlay">
					<img class="background-image" alt="Background Image" src="img/1inicio.png">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-md-12 text-left">
							<span class="text-white alt-font"></span>
							<h1 class="text-white h1-hola">¿Quieres ser diferente? Conduce <b>Cool</b> con <b>Lubo</b></h1>
							<h2 class="text-white h2-msj-welcome">Comienza a conducir con nosotros, asigna tus tiempos y explota tus habilidades como conductor con lubo</h2>
							<a href="driver/registro/iniciar" class="btn btn-primary btn-filled">Iniciar</a>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</header>
			
			
			<section class="pure-text-centered">
				<center>
				<h1 class="h1-ventajas"><b>Ventajas y Beneficios</b></h1>
				</center>
				<div class="container container-full">
					<div class="row" style="background-color: #870080; height: 270px;">
					</div>
					<div class="row" style="margin-top: -230px;">
						<div class="col-xs-12 col-sm-4 col-md-4" style="background-color: transparent;">
							<center>
							<div class="card scrollflow -pop -opacity">
						    	<img class="card-img-top m-4" src="img/icon.png" alt="Card image" style="width:70%">
							    <div class="card-body">
							       	<p class="card-title">ADIOS A LAS JORNADAS DE TRABAJO</p>
							       	<p class="card-text" style="">Trabajas y descansas en el momento que tu así lo desees</p>
								</div>
							</div>
							</center>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4" style="background-color: transparent;">
							<center>
							<div class="card scrollflow -pop -opacity">
						    	<img class="card-img-top m-4" src="img/notificacion.png" alt="Card image" style="width:70%">
							    <div class="card-body" style="margin-top: 20px;">
							       	<p class="card-title"><b>NO PIERDES VIAJES</b></p>
								    <p class="card-text">Con la app oficial para conductores  recibes las solicitudes de viaje en la comodidad de tu móvil.</p>
								</div>
							</div>
							</center>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4" style="background-color: transparent;">
							<center>
							<div class="card scrollflow -pop -opacity">
						    	<img class="card-img-top m-4" src="img/moneda.png" alt="Card image" style="width:70%">
							    <div class="card-body">
							    	<div class="row">
						  				<div class="col-12"  style="height:11px;">	
							  			</div>
							  	    </div>
							       	<p class="card-title ">LLEVAS UN CONTROL DE GANANCIAS</p>
								   	<p class="card-text">Desde la app oficial te mostramos <br> el historial de ganancias durante el día y en tu semana</p>		
								</div>
							</div>
							</center>
						</div>
						</div>
					</div><!--end of row-->
		
				</div><!--end of container-->
			</section>
		<!-- end divider -->
			
			
			<section class="image-divider overlay duplicatable-content">
				<div class="background-image-holder parallax-background">
					<img class="background-image" alt="Background Image" src="img/12inicio.jpg">
				</div>
				
				<div class="container">
					
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 align-vertica no-align-mobile">
							<h1 class="no-margin-bottom text-white h1-viaja">Viaja con nosotros</h1>
						</div>
						
						<div class="col-xs-12 col-sm-4 col-md-4">
							<center>
							<div class="card1 scrollflow -slide-right -opacity">
						    	<img class="card1-img-top m-4" src="img/shield.png" alt="Card image" style="width:45%">
							    <div class="card1-body">
							       	<p class="card1-title">Tú seguridad es muy importante para nosotros</p>
								</div>
							</div>
							</center>
						</div>
						
						<div class="col-xs-12 col-sm-4 col-md-4">
							<center>
							<div class="card1 scrollflow -slide-right -opacity">
						    	<img class="card1-img-top m-4" src="img/reloj.png" alt="Card image" style="width:45%">
							    <div class="card1-body">
							       	<p class="card1-title">Queremos que siempre llegues a tiempo a tu destino</p>
								</div>
							</div>
							</center>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
			
			<section class="video-inline">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-5 col-slider">
							<center>
							<div class="image-slider image-gallery" style="background-color: transparent;">
								<ul class="slides">
									<li><img alt="Image Slider Image" src="img/slider/1.png" width="60%"></li>
									<li><img alt="Image Slider Image" src="img/slider/2.png" width="60%"></li>
								</ul>
							</div><!--end of image slider-->
							</center>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-7 ">
							<center>
							<h1 class="space-bottom-medium h1-lubo"><img src="img/luboAsk.png" width="250px;"></h1>
							<p class="lead space-bottom-medium p-lubo">
								Somos una empresa dedicada al transporte y queremos que tú saques el mejor provecho a la hora de conducir y viajar, pues queremos prosperar junto contigo. Somos una nueva oportunidad de realizar tus actividades cotidianas tanto al moverte de un lugar a otro como para generar ingresos siendo un socio conductor.
							</p>
							<h3 class="h3-viajas"><b>¿Viajas o conduces?</b></h3>
							<br>
							<h2 class="h2-eleccion">La elección la haces tú ;)</h2>
							</center>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
		</div>
		
		
		
        <div class="clearfix">
	        
        </div>
	        
		<div class="footer-container">
			<section class="sliderQR section-footer">
				<?php include_once('menus/footer.php');?>
			</section>
		</div>
		
		
		
		
				
		<script src="js/jquery.min.js"></script>
        <script src="js/jquery.plugin.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.flexslider-min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/skrollr.min.js"></script>
        <script src="js/scrollReveal.min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/scripts.js"></script>
        
        
        <script>
    	$(document).ready(function (){
		    //validateB();
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="img/footerLubo.png";
                document.getElementById("headerLogos").style.display="none";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="img/footerMobile.png";
		    }
		});
	</script>
	<script>
		document.getElementsByTagName("BODY")[0].onresize = function() {myFunction()};
		function myFunction() {
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="img/footerLubo.png";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="img/footerMobile.png";
		    }
		}
		
	</script>
	
	<!--Script para el fancybox-->
	<script type="text/javascript">
	$(document).ready(function(){
			
		$(".fancybox").fancybox({
			openEffect  : 'none',
			closeEffect : 'none',
			iframe : {
				preload: false
			}
		});
		
		
		
		$(".various").fancybox({
			maxWidth	: 900,
			maxHeight	: 800,
			fitToView	: false,
			width		: '70%',
			height		: '70%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none'
		});
		
		
		
		$('.fancybox-media').fancybox({
			openEffect  : 'none',
			closeEffect : 'none',
			helpers : {
				media : {}
			}
		});
	});
	</script>
        
    </body>
</html>
				