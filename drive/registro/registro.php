<?php
if (!isset($_GET['telefono'])){
 	header('Location: iniciar.php');	
 }
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Lubo - Datos Personales</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <!-- icon -->
		<link rel="icon" type="image/x-icon" href="../img/favicon/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
		<link rel="manifest" href="../img/favicon/site.webmanifest">
		<link rel="mask-icon" href="../img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
        <link href="../css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/theme.css" rel="stylesheet" type="text/css" media="all"/>
		<link rel="stylesheet" href="../css/frmregistro.css">
		<link rel="stylesheet" href="../css/frmdatos.css">
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="../js/sw/servicios.js" ></script>
		
		<script>
	    	function limpiarEstiloNombre(){
		    	var element = document.getElementById("nombre");
				element.classList.remove("input-error");
				
				var link = document.getElementById('name-error');
				link.style.display = 'none'; //or
	    	}
	    	
	    	function limpiarEstiloApellidos(){
		    	var element = document.getElementById("apellidos");
				element.classList.remove("input-error");
				
				var link = document.getElementById('apellidos-error');
				link.style.display = 'none'; //or
	    	}
	    	
	    	function limpiarEstiloCorreo(){
		    	var element = document.getElementById("correo");
				element.classList.remove("input-error");
				
				var link = document.getElementById('email-error');
				link.style.display = 'none'; //or
	    	}
	    	
	    	function limpiarEstiloPassword(){
		    	var element = document.getElementById("contraseña");
				element.classList.remove("input-error");
				
				var link = document.getElementById('password-error');
				link.style.display = 'none'; //or
			}
			
			function load() {
				var token = sessionStorage.getItem('Token');
				console.log(token)
				var idPersona = sessionStorage.getItem('IdPersona')
				if (token!=null) {
					$.ajax({
								// data:  parametros,
								url:   url+'registro/obtener/'+idPersona,
								type:  'GET',
								dataType: 'json',
								cache: false,
								beforeSend: function () {
									 //alert('Envindo Datos por ajax')
										return true
								},
								success: function(data)
								{
									if(data['response']){
										var result = data['result']
										var status = result['Status']
										// console.log(status)
											if (status <= 1 ) {
												$(location).attr('href','registro-auto.php#registro')
											}else{
												$(location).attr('href','mis-documentos.php#documentos')
											}
									}else{
										if(data['errors']==null){
											alert(data['message'])
										}else{
											alert(data['errors'])
										}
									}           
									return true
								}
							})
				}
				// validar(token)
			}

	    </script>
		
    </head>
    <body onload="load()">
				
		<div class="nav-container">
			<?php 
				$currentPage = 'menu-driver';
				include_once("../menus/menu-sticky-in.php");
			?>
		</div>
		
		<div class="main-container">
			<header class="fullscreen-element no-pad centered-text">
				<div class="background-image-holder parallax-background overlay">
					<img class="background-image" alt="Background Image" src="../img/2inicio.png">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-md-12 text-left">
							<span class="text-white alt-font"></span>
							<h1 class="text-white h1-hola"><b>Excelente decisión</b>
								<br>
								<span class="subtitulo-registro">
								Estás a punto de ser un luboDriver, sigue unos cuantos pasos para
								<br>
								finalizar tu registro
								</span>
							</h1>
							<h2 class="text-white h2-msj-welcome">Conducir nunca había sido tan satisfactorio, seguro y confiable</h2>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</header>
		<!-- end divider -->
			
			
			
			<section class="video-inline" id="registro">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-5 col-md-5">
							<form  name="xd" class="datos">
							<br>
							<p class="textop">Datos personales</p>
							<br>
							<br>
							<div class="form-group">
								<input type="text" id="nombre" required="nombre" class="campotexto" placeholder="Nombre" onkeypress="limpiarEstiloNombre()">
							</div>
							<center><div class="error-notice"><span id="name-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="text" id="apellidos" required="apellidos" class="campotexto" placeholder="Apellidos" name=""  onkeypress="limpiarEstiloApellidos()">
							</div>
							<center><div class="error-notice"><span id="apellidos-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="email"  id="correo" required="correo" class="campotexto" placeholder="Correo Electronico" name="" onkeypress="limpiarEstiloCorreo()">
							</div>
							<center><div class="error-notice"><span id="email-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="password" id="contraseña" required="contraseña" class="campotexto" placeholder="Contraseña" name="" onkeypress="limpiarEstiloPassword()">
							</div>
							<center><div class="error-notice"><span id="password-error" class="signup-error"></span></div></center>
							<div class="form-group">
								
							</div>
							<center>
							<div class="form-group">
								<h3>Género</h3>
								<fieldset>
								<div class="radio-item">
								    <input type="radio" id="ritema" name="ritem" value="M">
								    <label for="ritema">Mujer</label>
								</div>
								
								<div class="radio-item">
								    <input type="radio" id="ritemb" name="ritem" value="H">
								    <label for="ritemb">Hombre</label>
								</div>
								</fieldset>
							</div>
							</center>	
							<br>
							<input type="hidden" name="telefono" id="telefono" value="<?php echo $_GET['telefono'] ?>" >
							<center>
								<div class="error-notice"><span id="telefono-error" class="signup-error"></span></div>
							</center>
							<br>
					 		<center>
								<div class="form-group" align="center">
						  			<button id="boton"  onClick="registrar($('#nombre').val(),$('#apellidos').val(),$('#correo').val(),$('#contraseña').val(),$('#telefono').val(),$('input:radio[name=ritem]:checked').val())" type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo3">
										Registro
									</button>
								</div>	
							</center>
							<br>
							<br>				
						</form>
						</div>
						<div class="hidden-xs col-sm-1 col-md-1">
						</div>
						<div class="hidden-xs col-sm-6 col-md-6 align-vertical">
							<center>
							<br>
							<h2 class="h2-eleccion">Ve tus ganancias del día o si lo prefieres de tu semana.</h2>
							</center>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
		</div>
		
		
		
        <div class="clearfix">
	        
		<div class="footer-container">
			<section class="sliderQR section-footer" style="background-color: transparent;">
		        <?php include_once('../menus/footer-in.php');?>
			</section>
		</div>
		
		
		
		<script src="../js/jquery.min.js"></script>
        <script src="../js/jquery.plugin.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.flexslider-min.js"></script>
        <script src="../js/smooth-scroll.min.js"></script>
        <script src="../js/skrollr.min.js"></script>
        <script src="../js/scrollReveal.min.js"></script>
        <script src="../js/lightbox.min.js"></script>
        <script src="../js/scripts.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.format.js"></script>
        
        
        <script>
    	$(document).ready(function (){
		    //validateB();
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
                document.getElementById("headerLogos").style.display="none";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		});
	</script>
	<script>
		document.getElementsByTagName("BODY")[0].onresize = function() {myFunction()};
		function myFunction() {
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		}
		
	</script>
        
        <script type="text/javascript">
	    	jQuery(document).ready(function() {
	    	jQuery('.telefono').keypress(function(tecla) {
	        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
	   			 });
			});
	    </script>
	
	
	    <script type="text/javascript">
			$(document).ready(function() {
				$("#frmValidate").hide()
				$("#datos").hide()
			});
			var input=  document.getElementById('numero1');
			input.addEventListener('input',function(){
			  if (this.value.length > 10) 
			     this.value = this.value.slice(0,10); 
			})
		</script>
	
	
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#numero22").val(value);
	          });
	      });
		</script>
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#telefono3").val(value);
	          });
	      });
		</script>
	
    </body>
</html>
				