

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Lubo - Datos Personales</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <!-- icon -->
		<link rel="icon" type="image/x-icon" href="../img/favicon/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
		<link rel="manifest" href="../img/favicon/site.webmanifest">
		<link rel="mask-icon" href="../img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
        <link href="../css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/theme.css" rel="stylesheet" type="text/css" media="all"/>
		<link rel="stylesheet" href="../css/frmregistro.css">
		<link rel="stylesheet" href="../css/frmdatos.css">
		<link rel="stylesheet" href="../css/filedrag.css">
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="../js/sw/servicios.js" ></script>
        <script type="text/javascript">
			function cargando() {
				var token = sessionStorage.getItem('Token');
				validar(token)
				var idPersona = sessionStorage.getItem('IdPersona')
				obtenerAuto(idPersona)
				// document.getElementById('siguiente').display = 'none'
				document.getElementById('log-user').innerHTML = 'Logout'
				document.getElementById('log-user').onclick = logout

			}
		</script>
		
		<script>
	    	function limpiarEstiloModelo(){
		    	var element = document.getElementById("modelo");
				element.classList.remove("input-error");
				
				var link = document.getElementById('modelo-error');
				link.style.display = 'none'; //or
	    	}
	    	
	    	function limpiarEstiloAn(){
		    	var element = document.getElementById("an");
				element.classList.remove("input-error");
				
				var link = document.getElementById('an-error');
				link.style.display = 'none'; //or
	    	}
	    	
	    	function limpiarEstiloPlacas(){
		    	var element = document.getElementById("placas");
				element.classList.remove("input-error");
				
				var link = document.getElementById('placas-error');
				link.style.display = 'none'; //or
	    	}
	    	
	    	function limpiarEstiloColor(){
		    	var element = document.getElementById("color");
				element.classList.remove("input-error");
				
				var link = document.getElementById('color-error');
				link.style.display = 'none'; //or
	    	}
	    	
	    	function limpiarEstiloPasajeros(){
		    	var element = document.getElementById("pasajeros");
				element.classList.remove("input-error");
				
				var link = document.getElementById('pasajeros-error');
				link.style.display = 'none'; //or
	    	}
	    	
	    	function limpiarEstiloSerie(){
		    	var element = document.getElementById("serie");
				element.classList.remove("input-error");
				
				var link = document.getElementById('serie-error');
				link.style.display = 'none'; //or
	    	}
	    	
	    </script>
    </head>
    <body onload='cargando()' >
				
		<div class="nav-container">
			<?php 
				$currentPage = 'menu-driver';
				include_once("../menus/menu-sticky-in.php");
			?>
		</div>
		
		<div class="main-container">
			<header class="fullscreen-element no-pad centered-text">
				<div class="background-image-holder parallax-background overlay">
					<img class="background-image" alt="Background Image" src="../img/2inicio.png">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-md-12 text-left">
							<span class="text-white alt-font"></span>
							<h1 class="text-white h1-hola"><b>Excelente decisión</b>
								<br>
								<span class="subtitulo-registro">
								Estás a punto de ser un luboDriver, sigue unos cuantos pasos para
								<br>
								finalizar tu registro
								</span>
							</h1>
							<h2 class="text-white h2-msj-welcome">Conducir nunca había sido tan satisfactorio, seguro y confiable</h2>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</header>
		<!-- end divider -->
			<section class="video-inline" id="registro">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-5 col-md-5">
							<form  name="xd" class="datos">
							<br>
							<p class="textop">Mi auto</p>
							<br>
							<br>
							<div class="form-group">
								<input type="text" id="modelo" class="campotexto" placeholder="Modelo" name="model" onkeypress="limpiarEstiloModelo()">
							</div>
							<center><div class="error-notice"><span id="modelo-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="text" id="an" class="campotexto bfh-phone" placeholder="Año" name="an" data-country="AN" onkeypress="limpiarEstiloAn()">
							</div>
							<center><div class="error-notice"><span id="an-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="text" id="placas" class="campotexto" placeholder="Placas" name="placas" onkeypress="limpiarEstiloPlacas()">
							</div>
							<center><div class="error-notice"><span id="placas-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="text" id="color" class="campotexto" placeholder="Color" name="color" onkeypress="limpiarEstiloColor()">
							</div>
							<center><div class="error-notice"><span id="color-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="text" id="pasajeros" class="campotexto bfh-phone" placeholder="Número de pasajeros" name="pasajeros" data-country="AN" onkeypress="limpiarEstiloPasajeros()">
							</div>
							<center><div class="error-notice"><span id="pasajeros-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="text" id="serie" class="campotexto" placeholder="Número de serie" name="serie" onkeypress="limpiarEstiloSerie()">
							</div>
							<center><div class="error-notice"><span id="serie-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="hidden" id="idAuto" class="campotexto"  name="idAuto" value = "0">
							</div>
							<center>
							<div class="form-group">
								<h3>Modo de trabajo</h3>
								<fieldset>
								<div class="radio-item">
								    <input type="radio" id="ritema" name="ritem" value="2">
								    <label for="ritema">Privado</label>
								</div>
								
								<div class="radio-item">
								    <input type="radio" id="ritemb" name="ritem" value="1">
								    <label for="ritemb">Taxi</label>
								</div>
								</fieldset>
							</div>
							</center>
							<center>
								<div class="error-notice"><span id="telefono-error" class="signup-error"></span></div>
							</center>
							<br>
					 		<center>
								<div class="form-group" align="center">
						  			<button id="boton"  onClick="registrarAuto($('#placas').val(),$('#modelo').val(),$('#color').val() ,$('#an').val(), $('#serie').val(),$('input:radio[name=ritem]:checked').val(),$('#pasajeros').val())" type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo3">
										Guardar
									</button>
									
								</div>	
							</center>
							<br>
							<br>				
						</form>
						</div>
						<div class="hidden-xs col-sm-1 col-md-1">
							
							<!-- <button onClick="logout()" >loguot</button> -->
						</div>
						<div class="hidden-xs col-sm-6 col-md-6 align-vertical" id="fotos">
								<center><h3>Sube 3 fotos de tu auto</h3></center>
								<br>
							<div class="col-md-4" >
								<form class="drag" id="img1" enctype="multipart/form-data" >
									<input id = "i1" class="drag_input" type="file" name="img">
									<p id ="pDrag1" class="drag_p" >Arrastra tu archivo o da click en esta area.</p>
									<button id="button1" type="button" class="drag_button" onClick="imgVehiculo(document.getElementById('idAuto').value,document.getElementById('img1'),1)">Cargar</button>
								</form>
							</div>
							<div class="col-md-4" >
								<form class="drag" id="img2" enctype="multipart/form-data" >
									<input id = "i2" class="drag_input" type="file" name="img">
									<p id ="pDrag2" class="drag_p" >Arrastra tu archivo o da click en esta area.</p>
									<button id="button2" type="button" class="drag_button" onClick="imgVehiculo(document.getElementById('idAuto').value,document.getElementById('img2'),2)">Cargar</button>
								</form>
							</div>
							<div class="col-md-4" >
								<form class="drag" id="img3" enctype="multipart/form-data" >
									<input id = "i3" class="drag_input" type="file" name="img">
									<p id ="pDrag3" class="drag_p" >Arrastra tu archivo o da click en esta area.</p>
									<button id="button3" type="button" class="drag_button" onClick="imgVehiculo(document.getElementById('idAuto').value,document.getElementById('img3'),3)">Cargar</button>
								</form>
							</div>
							<!-- <div class="col-md-12">
								<button  id="siguiente" onClick ="siguiente()" class="buttonBase" >Siguiente >></button>
							</div> -->
						</div>
							
					</div><!--end of row-->
					
				</div><!--end of container-->
				
			</section>
		</div>
				
        <div class="clearfix">
	        
		<div class="footer-container">
			<section class="sliderQR section-footer" style="background-color: transparent;">
		        <?php include_once('../menus/footer-in.php');?>
			</section>
		</div>
		
		
		
		
		
				
		<script src="../js/jquery.min.js"></script>
        <script src="../js/jquery.plugin.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.flexslider-min.js"></script>
        <script src="../js/smooth-scroll.min.js"></script>
        <script src="../js/skrollr.min.js"></script>
        <script src="../js/spectragram.min.js"></script>
        <script src="../js/scrollReveal.min.js"></script>
        <script src="../js/lightbox.min.js"></script>
        <script src="../js/scripts.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.format.js"></script>
        
        
        <script>		
		function siguiente(){
			alert('a mis-documentos.php')
		}

		$(document).ready(function(){
			$('#i1').change(function () {
				// $('#pDrag').text(this.files.length + " file(s) selected");
				console.log(this.files);
				document.getElementById("pDrag1").innerHTML = "Archivo seleccionado "+this.files[0].name;
				// document.getElementById('pDrag').text = this.files.length + "Archivo seleccionado"
			});
			$('#i2').change(function () {
				// $('#pDrag').text(this.files.length + " file(s) selected");
				console.log(this.files);
				document.getElementById("pDrag2").innerHTML = "Archivo seleccionado "+this.files[0].name;
				// document.getElementById('pDrag').text = this.files.length + "Archivo seleccionado"
			});
			$('#i3').change(function () {
				// $('#pDrag').text(this.files.length + " file(s) selected");
				console.log(this.files);
				document.getElementById("pDrag3").innerHTML = "Archivo seleccionado "+this.files[0].name;
				// document.getElementById('pDrag').text = this.files.length + "Archivo seleccionado"
			});
		});


    	$(document).ready(function (){
		    //validateB();
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
                document.getElementById("headerLogos").style.display="none";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		});
	</script>
	<script>
		document.getElementsByTagName("BODY")[0].onresize = function() {myFunction()};
		function myFunction() {
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		}
		
	</script>
        
        <script type="text/javascript">
	    	jQuery(document).ready(function() {
	    	jQuery('.telefono').keypress(function(tecla) {
	        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
	   			 });
			});
	    </script>
	
	
	    <script type="text/javascript">
			$(document).ready(function() {
				$("#frmValidate").hide()
				$("#datos").hide()
			});
			var input=  document.getElementById('numero1');
			input.addEventListener('input',function(){
			  if (this.value.length > 10) 
			     this.value = this.value.slice(0,10); 
			})
		</script>
	
	
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#numero22").val(value);
	          });
	      });
		</script>
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#telefono3").val(value);
	          });
	      });
		</script>
    </body>
</html>
				