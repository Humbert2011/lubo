<?php
date_default_timezone_set('America/Monterrey');//or change to whatever timezone you want
	
	require './fpdf/fpdf.php';
	class PDF extends FPDF
	{
		function Header()
		{
			//Importing Font and it's variations
			$this->AddFont('Proxima','','ProximaNova-Regular.php'); //Regular
			$fecha = $_GET['fecha'];
			$total = $_GET['total'];
			$subTotal = $_GET['subTotal'];
			$iva=$_GET['iva'];
			$tip=$_GET['tip'];
			$tipoPago=$_GET['tipoPago'];
			$name=$_GET['name'];
			$tipoServicio=$_GET['servicio'];
			$km=$_GET['km'];
			$dI=$_GET['dI'];
			$hI=$_GET['hI'];
			$dF=$_GET['dF'];
			$hF=$_GET['hF'];
			$brand=$_GET['brand'];
			$dots=$_GET['dots'];
			$this->Image('imagenes/F1.jpg' ,-0,0,209.88,297,'JPG');
			$this->Image("imagenes/$brand.jpg" , 17.5 ,166, 10,6.25,'JPG');			
			if($tipoPago != "Efectivo"){
				$this->Image("imagenes/$dots.jpg" ,30,168, 8,1.5,'JPG');
			}
			$this->SetFont('Proxima','',12);
			$this->setTextColor(117, 117, 117);
			$this->Cell(184,25,utf8_decode("$fecha"),0,1,'R');
			$this->SetFont('Proxima','',14);
			$this->setTextColor(0, 0, 0);
			$this->Cell(184.2,70,utf8_decode("$total"),0,1,'R');
			$this->SetFont('Proxima','',11);
			$this->setTextColor(117, 117, 117);			
			$this->Cell(184.2,-24,utf8_decode("$subTotal"),0,1,'R');
			$this->setTextColor(30, 30, 30);
			$this->Cell(184.2,66,utf8_decode("$subTotal"),0,1,'R');
			$this->setTextColor(117, 117, 117);	
			$this->Cell(183,-28,utf8_decode("$iva"),0,1,'R');
			$this->SetFont('Proxima','',12);
			$this->setTextColor(74, 74, 74);	
			$this->Cell(40.5,100,utf8_decode("$tipoPago"),0,1,'R');
			$this->SetFont('Proxima','',14);
			$this->setTextColor(0, 0, 0);
			$this->Cell(183,-100,utf8_decode("$total"),0,1,'R');
			$this->SetFont('Proxima','',11);
			$this->Cell(35);
			$this->Cell(50,172.5,utf8_decode(" $name"),0,1,'L');
			$this->SetFont('Proxima','',11);
			$this->Cell(16);
			$this->Cell(55,-147,utf8_decode("$tipoServicio"),0,1,'L');
			$this->SetFont('Proxima','',11);
			$this->setTextColor(117, 117, 117);
			$this->Cell(70,147,utf8_decode("($km) kilómetros"),0,1,'R');
			$this->Cell(15);
			$this->Cell(137,-117,utf8_decode("$dI"),0,1,'L');
			$this->Cell(15);
			$this->Cell(50,130,utf8_decode("$hI"),0,0,'L');
			$this->SetXY(25,246);
			$this->Cell(80,10,utf8_decode("$dF"),0,1,'L');
			$this->SetXY(25,252.5);
			$this->Cell(30,10,utf8_decode("$hF"),0,1,'L');
			// $this->setTextColor(117, 117, 117);	
			// $this->SetXY(163, 130);
			// $this->Cell(30,10,utf8_decode("$tip"),0,1,'R');
			// $this->Cell(0,10,utf8_decode("$brand"),0,1,'R');
			// $this->SetFont('Arial','B',11);
			// $this->Cell(0,10,utf8_decode('AUTORIZACIÓN DE PAGO'),0,1,'C');
			// $this->Ln();
		}
	}

	$fecha = date("d-m-Y H:i:s");
	$FechaInicio = date("d-m-Y");
	$FechaFin = date("d-m-Y");
	$Folio = 300;
	$Limite = date("d-m-Y");
	$nombreNegocio = "Stardust";
	$Monto = 200;
	$semana = 12;
	$pdf = new PDF();
    $pdf->AddPage('P', 'A4');
    //$pdf->SetFont('Arial','B',12);
    //$pdf->Cell(0,0,'Folio',0,1,'R');
	$pdf->SetMargins(30,30,20,1);
	// $pdf->Ln();
	// $str='Pago del servicio de parquímetros a través de Centros autorizados de';
	// $str = utf8_decode($str);
    // $pdf->SetFont('Arial','B',11);
	// $pdf->Cell(0,-4,$fecha,0,1,'R');
    // $pdf->Cell(0,-4,'',0,1,'R');
    // $pdf->Cell(0,-2,'HP - '.$semana,0,1,'R');
    // $pdf->Cell(0,53,'                   '.$Monto.'.00',0,1,'C');
    // $pdf->Cell(0,-35,'                   '.$Monto.'.00',0,1,'C');
	// $pdf->Cell(0,55,'('.$letras.')',0,1,'C');
	
	// $strNom = utf8_decode($nombreNegocio);
    // $pdf->Cell(0,-40,'                   '.$strNom,0,1,'C');
    // $pdf->Cell(0,100,$str,0,1,'C');
    // $pdf->Cell(0,-90,'cobro de la Fecha: '.$FechaInicio.' a la Fecha: '.$FechaFin,0,1,'C');
    // $pdf->SetFont('Arial','B',9);
    // $pdf->Cell(70,135,$fecha,0,1,'R');
    // $pdf->Cell(135,-135,$Folio,0,1,'R');
    // $pdf->Image('imagenes/sello.jpg' ,120,215, 40,40,'JPG');
    // $pdf->Cell(135,159,'Viernes '.$Limite,0,1,'R');
    //$pdf->Cell(0,15,'Nota: presentarce 15 minutos antes de la hora de la cita con una identificacion oficial ',0,1);
	

	//$pdf->Ln(20);
	$pdf->Output();


	
	
?>