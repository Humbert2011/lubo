<?php

date_default_timezone_set('America/Monterrey');//or change to whatever timezone you want
require './fpdf/fpdf.php';
class PDF extends FPDF
{
	function Header()
	{
		$this->AddFont('Proxima','','ProximaNova-Regular.php'); //Regular
		$fecha = $_GET['fecha'];
		$total = $_GET['total'];
		$subTotal = $_GET['subTotal'];
		$iva=$_GET['iva'];
		$tip=$_GET['tip'];
		$tipoPago=$_GET['tipoPago'];
		$name=$_GET['name'];
		$tipoServicio=$_GET['servicio'];
		$km=$_GET['km'];
		$dI=$_GET['dI'];
		$hI=$_GET['hI'];
		$dF=$_GET['dF'];
		$hF=$_GET['hF'];
		$brand=$_GET['brand'];
		$dots=$_GET['dots'];
		$this->Image('imagenes/F1.jpg' ,-0,0,209.88,297,'JPG');
		$this->Image("imagenes/$brand.jpg" , 17.5 ,166, 10,6.25,'JPG');			
		if($tipoPago != "Efectivo"){
			$this->Image("imagenes/$dots.jpg" ,30,168, 8,1.5,'JPG');
		}
		$this->SetFont('Proxima','',12);
		$this->setTextColor(117, 117, 117);
		$this->Cell(184,25,utf8_decode("$fecha"),0,1,'R');
		$this->SetFont('Proxima','',14);
		$this->setTextColor(0, 0, 0);
		$this->Cell(184.2,70,utf8_decode("$total"),0,1,'R');
		$this->SetFont('Proxima','',11);
		$this->setTextColor(117, 117, 117);			
		$this->Cell(184.2,-21,utf8_decode("$subTotal"),0,1,'R');
		$this->setTextColor(30, 30, 30);
		$this->Cell(184.2,62.5,utf8_decode("$subTotal"),0,1,'R');
		$this->setTextColor(117, 117, 117);	
		$this->Cell(184.2,-45,utf8_decode("$iva"),0,1,'R');
		$this->SetFont('Proxima','',12);
		$this->setTextColor(74, 74, 74);	
		$this->Cell(40.5,135,utf8_decode("$tipoPago"),0,1,'R');
		$this->SetFont('Proxima','',14);
		$this->setTextColor(0, 0, 0);
		$this->Cell(184.2,-137,utf8_decode("$total"),0,1,'R');
		$this->SetFont('Proxima','',11);
		$this->Cell(35);
		$this->Cell(50,212,utf8_decode(" $name"),0,1,'L');
		$this->SetFont('Proxima','',11);
		$this->Cell(16);
		$this->Cell(50,-187.5,utf8_decode("$tipoServicio"),0,1,'L');
		$this->SetFont('Proxima','',11);
		$this->setTextColor(117, 117, 117);
		$this->Cell(75,187,utf8_decode("($km) kilómetros"),0,1,'R');
		$this->Cell(15);
		$this->Cell(137,-157,utf8_decode("$dI"),0,1,'L');
		$this->Cell(15);
		$this->Cell(50,167,utf8_decode("$hI"),0,0,'L');
		$this->SetXY(25,247.5);
		$this->Cell(80,10,utf8_decode("$dF"),0,1,'L');
		$this->SetXY(25,252.5);
		$this->Cell(30,10,utf8_decode("$hF"),0,1,'L');
		$this->setTextColor(117, 117, 117);	
		$this->SetXY(163, 130);
		$this->Cell(30,10,utf8_decode("$tip"),0,1,'R');
		// $this->Cell(0,10,utf8_decode("$brand"),0,1,'R');
		// $this->SetFont('Arial','B',11);
		// $this->Cell(0,10,utf8_decode('AUTORIZACIÓN DE PAGO'),0,1,'C');
		// $this->Ln();
	}
}

$emailUser = $_GET['email'];

$fecha = date("d-m-Y H:i:s");
$FechaInicio = date("d-m-Y");
$FechaFin = date("d-m-Y");
$Folio = 300;
$Limite = date("d-m-Y");
$nombreNegocio = "Stardust";
$Monto = 200;
$semana = 12;
$pdf = new PDF();
$pdf->AddPage('P', 'A4');

$pdf->SetMargins(30,30,20,1);

$to = $emailUser;
$from = "soporte@stardust.com.mx";
$subject = "Tu tiket Lubo";

$message = "<p>Consulte el archivo adjunto.</p>";
$separator = md5(time());
$eol = PHP_EOL;
$filename = "tiket_lubo.pdf";
$pdfdoc = $pdf->Output('','S');
$attachment = chunk_split(base64_encode($pdfdoc));

$headers = "From: " . $from . $eol;
$headers .= "MIME-Version: 1.0" . $eol;
$headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol . $eol;

$body = '';

$body .= "Content-Transfer-Encoding: 7bit" . $eol;
$body .= "This is a MIME encoded message." . $eol; //had one more .$eol


$body .= "--" . $separator . $eol;
$body .= "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
$body .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
$body .= $message . $eol;


$body .= "--" . $separator . $eol;
$body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
$body .= "Content-Transfer-Encoding: base64" . $eol;
$body .= "Content-Disposition: attachment" . $eol . $eol;
$body .= $attachment . $eol;
$body .= "--" . $separator . "--";

$urlP = "http://stardust.com.mx/Lubo/email.php";
$data = array(
    "from"=>"Lubo <soporte@stardust.com.mx>",
    "to"=>$to,
    "subject"=>$subject,
    "message"=>$body,
    "headers"=>$headers
);
$opts = array('http' =>
array(
    'method'  => 'POST',
    'header'  => 'Content-type: application/json',
    'content' =>  json_encode($data)));
$context  = stream_context_create($opts);
$res = file_get_contents($urlP, false, $context);
$respuesta = json_decode($res);

if ($respuesta->success){
    // var_dump($respuesta);
    // echo $to;
    // var_dump($pdfdoc);
    // var_dump($attachment);
    header("Location:".$_SERVER['HTTP_REFERER']);
    // return '1';
}else{
    var_dump($respuesta);
    return '0';
}
echo "se mando"
?>
