let primer_punto = false;
let startPoint = {};
let endPoint = {};
let carM;

let config = {
    apiKey: "AIzaSyBW10lZmI8ED3WdzNna41hkyhrzgiYN4sM",
    authDomain: "lubo-dev.firebaseapp.com",
    databaseURL: "https://lubo-dev.firebaseio.com",
    projectId: "lubo-dev",
    storageBucket: "lubo-dev.appspot.com",
    messagingSenderId: "864668661662",
    appId: "1:864668661662:web:71aaddeb1630b9ac72c88f",
    measurementId: "G-R0Y6BQ5SSJ"
};
firebase.initializeApp(config);

let map = new L.Map('map', {
    center: [20.175397, -98.045813],
    zoom: 19,
    minZoom: 3,
});

let tileUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
layer = new L.TileLayer(tileUrl,
{
    attribution: 'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
    maxZoom: 19
});

map.addLayer(layer);

let carIcon = L.icon({
    iconUrl: 'car2.png',
    iconSize:     [40, 30], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

// valores de la url
let sear = new URLSearchParams(location.search);
let idViaje = sear.get('idViaje');
let idZona = sear.get('zona');
// 
let cars_Ref = firebase.database().ref().child(`/Routes/${idZona}/${idViaje}`);

cars_Ref.on('child_added', function (data) {
    //console.log(data.val().longitude);
    if (primer_punto === false){
        startPoint = { lat: data.val().latitude, lng : data.val().longitude };
        carM = L.Marker.movingMarker([startPoint], [], {icon: carIcon}).addTo(map);
        map.setView([startPoint.lat, startPoint.lng], 17);
        primer_punto = true;
    }
    else{
        endPoint = { lat: data.val().latitude, lng : data.val().longitude };
        carM.moveTo(endPoint, 2000);
        L.polyline([startPoint, endPoint], {
            weight: 3,
            color: 'black',
            opacity: 1
          }).addTo(map);
        startPoint = endPoint;
        map.setView([startPoint.lat, startPoint.lng], 17);
    }

});