

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Lubo - Viaja</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <!-- icon -->
		<link rel="icon" type="image/x-icon" href="../img/favicon/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
		<link rel="manifest" href="../img/favicon/site.webmanifest">
		<link rel="mask-icon" href="../img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
        <link href="../css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/theme.css" rel="stylesheet" type="text/css" media="all"/>
		<link rel="stylesheet" href="../css/frmregistro.css">
		<link rel="stylesheet" href="../css/frmcliente.css">
		
		<!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NX8TKGG');</script>
		<!-- End Google Tag Manager -->
		
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="../js/config/config.js"></script> <!-- url -->
        <script type="text/javascript" src="../js/sw/servicios.js"></script>
        <script type="text/javascript">
			function cargando() {
				// validar token
				var token = sessionStorage.getItem('Token');
				console.log(token)
				if (token != null) {
					document.getElementById('log-user').innerHTML = 'Logout'
					document.getElementById('log-user').onclick = logout
				}
			}
		</script>
		
		 <!-- Facebook Pixel Code -->
		<script>
				!function(f,b,e,v,n,t,s)
				{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
				fbq('init', '189972612211292');
				fbq('track', 'PageView');
			</script>
			<noscript><img height="1" width="1" style="display:none"
				src="https://www.facebook.com/tr?id=189972612211292&ev=PageView&noscript=1"
			/></noscript>
		<!-- End Facebook Pixel Code -->
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150520430-1"></script>
		<script>
		if(modoServidor === 'prod'){
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-150520430-1');
		}
		</script>
		<!-- End Global site tag (gtag.js) - Google Analytics -->
		
		
    </head>
    <body onload="cargando()">
		<!-- Google Tag Manager (noscript) -->
	    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX8TKGG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->	
		<div class="nav-container">
			<?php 
				$currentPage = 'menu-viaja';
				include_once("../menus/menu-sticky-in.php");
			?>
		</div>
		
		<div class="main-container">
			<header class="fullscreen-element no-pad centered-text">
				<div class="background-image-holder parallax-background overlay">
					<img class="background-image" alt="Background Image" src="../img/viaja.png">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-md-12 text-left">
							<span class="text-white alt-font"></span>
							<h1 class="text-white h1-hola"><b>Nos encargamos de hacer de cada viaje
								<br>
								lo mejor</b>
							</h1>
							<h2 class="text-white h2-msj-welcome">Descubrirás que con Lubo, ir de un lugar a otro, es más fácil.</h2>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</header>
		<!-- end divider -->
			
			
			
			<section class="pure-text-centered">
				<center>
				<h1 class="h1-ventajas"><b>Un abanico de beneficios para ti</b></h1>
				</center>
				<div class="container container-full">
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4" style="background-color: transparent;">
							<center>
						    	<img class="card-img-top m-4" src="../img/shield.png" alt="Card image" style="width:40%">
						    	<div class="card-body" style="margin-top: 40px;">
							       	<p class="card-text" style="">Puedes viajar tranquilo, todos nuestros conductores están previamente verificados.</p>
								</div>
							</center>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4" style="background-color: transparent;">
							<center>
						    	<img class="card-img-top m-4" src="../img/sillon.png" alt="Card image" style="width:40%">
							    <div class="card-body" style="margin-top: 40px;">
								    <p class="card-text">La comodidad es un punto muy importe a la hora de viajar, no te preocupes, nosotros nos encargamos.</p>
								</div>
							</center>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4" style="background-color: transparent;">
							<center>
						    	<img class="card-img-top m-4" src="../img/caja.png" alt="Card image" style="width:40%">
							    <div class="card-body" style="margin-top: 40px;">
								    <p class="card-text">Siempre puedes recuperar los objetos que olvidas en un auto de Lubo, sólo llámanos, con gusto vamos a atenderte.</p>
								</div>
							</center>
						</div>
						</div>
					</div><!--end of row-->
		
				</div><!--end of container-->
			</section>
			
			<section style="padding-bottom: 0px;">
				<div class="container-fluid" style="background-color: #870080;">
					<div class="row">
						<div class="container container-full">
							<div class="row">
								<div class="col-xs-12 col-sm-5 col-md-5 align-vertical no-align-mobile" style="background-color: transparent;">
									<center>
								    	<p id="textcomoutilizo">¿Cómo utilizo <br>lubo?</p>
									</center>
								</div>
								<div class="col-xs-12 col-sm-7 col-md-7" style="background-color: transparent;">
									<div class="col-8" id="accordion">
								    	<div class="row"  >
											<div class="col-7 col-sm-6 col-md-4 p-2 " id="divtextreg">
												<p id="textrealpago">¿Realizar tu registro?</p>	
											</div>
											<div class="col-5 col-sm-6 col-md-8 p-2" >
												<button type="button" class="btn btn-info" data-toggle="collapse" href="#collapseOne" id="btnver"><b>Ver</b>
											  	</button>
											</div>
										</div>
										<div class="row">
											<div id="collapseOne" class="collapse" data-parent="#accordion">
												   <p id="textbtnver">
												   	Descarga la App ingresa los datos que se te piden , para poder realizar tu <br> adecuado registro, descuida será todo muy sencillo.</p>
						   				    </div>			
										</div>
										<div class="row" >
											<div class="col-7 col-sm-6 col-md-4 p-2" id="divtextreg">
												<p id="textrealpago">Pide un Viaje</p>
											</div>
											<div class="col-5 col-sm-6 col-md-8 p-2" >	
												<div>
												  <button type="button" class="btn btn-info collapsed"  data-toggle="collapse" href="#collapseTwo" id="btnver"><b>Ver</b>
												  </button>
												</div>
											</div>
										</div>
										<div class="row">
											<div id="collapseTwo" class="collapse" data-parent="#accordion">
												<p id="textbtnver">
											    Ingresa a Lubo, da click en el botón necesito transporte y en automático  <br> se enviara una solicitud
											    al conductor más cercano, cuando se haya aceptado y validado <br>llegara por ti, una vez ahi ya solo te queda disfrutar del viaje.</p> 
										   </div>			
										</div>
										<div class="row" >
											<div class="col-7 col-sm-6 col-md-4 p-2" id="divtextreg" >
												<p id="textrealpago">¿Voy seguro?</p>
											</div>
											<div class="col-5 col-sm-6 col-md-8 p-2">
												<div>
												  <button type="button" class="btn btn-info collapsed" data-toggle="collapse" href="#collapseThree" id="btnver"><b>Ver</b>
												  </button>
												</div>
											</div>
										</div>
										<div class="row">
											<div id="collapseThree" class="collapse" data-parent="#accordion">
												<p id="textbtnver">
												Sin duda en lubo se siguen estrictas normas ser uno de nuestros conductores <br>
												descuida, son los mejores.</p> 
						   					</div>			
										</div>
										<div class="row" >
											<div class="col-7 col-sm-6 col-md-4 p-2" id="divtextreg" >
												<p id="textrealpago">¿Llegaré a tiempo?</p>	
											</div>
											<div class="col-5 col-sm-6 col-md-8 p-2">
												<div>
													<button type="button" class="btn btn-info collapsed" data-toggle="collapse" href="#collapsecuatro" id="btnver"><b>Ver</b>
													</button>
												</div>
											</div>
										</div>
										<div class="row">
											<div id="collapsecuatro" class="collapse" data-parent="#accordion" >
												<p id="textbtnver">
												Es una de nuestras mayores prioridades, asignaremos al conductor la ruta mas rápida y adecuada<br> para que llegues a tiempo a tu destino.</p> 
						   					</div>			
										</div>
									</div>
								</div>
							</div><!--end of row-->
						</div>
					</div><!--end of row-->
				</div>
			</section>
		<!-- end divider -->
		
		<section class="pure-text-centered" style="margin-bottom: 30px;">
				<center>
				<h1 class="h1-ventajas"><b>Obtén Lubo en 3 sencillos pasos</b></h1>
				</center>
				<div class="container container-full">
					<div class="row">
						<div class="col-xs-12 col-sm-1 col-md-1 align-vertical no-align-mobile">
							<center>
						    	<h1 class="no-margin-bottom text-purple h1-number">1</h1>
							</center>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3 align-vertical no-align-mobile" style="background-color: transparent;">
						    	<div class="card-body" style="margin-top: 40px;">
							       	<p class="card-title ">Ingresa a la tienda oficial</p>
								   	<p class="card-text">Google Play <br>App Store</p>		
								</div>
						</div>
						<div class="col-xs-12 col-sm-1 col-md-1 align-vertical no-align-mobile">
							<center>
						    	<h1 class="no-margin-bottom text-purple h1-number">2</h1>
							</center>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3 align-vertical no-align-mobile" style="background-color: transparent;">
						    	<div class="card-body" style="margin-top: 40px;">
							       	<p class="card-title ">Busca Lubo</p>
								   	<p class="card-text">Descárgala<br>Sólo te tomará unos segundos</p>		
								</div>
						</div>
						<div class="col-xs-12 col-sm-1 col-md-1 align-vertical no-align-mobile">
							<center>
						    	<h1 class="no-margin-bottom text-purple h1-number">3</h1>
							</center>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3 align-vertical no-align-mobile" style="background-color: transparent;">
						    	<div class="card-body" style="margin-top: 40px;">
							       	<p class="card-title ">!Listo!</p>
								   	<p class="card-text">A disfrutar de los beneficios<br>que tenemos para ti</p>		
								</div>
						</div>
					</div><!--end of row-->
		
				</div><!--end of container-->
			</section>
			
		</div>
		
		
		
        <div class="clearfix">
        </div>
		<div class="footer-container">
			<section class="sliderQR section-footer" style="background-color: transparent;">
		        <?php include_once('../menus/footer-in.php');?>
			</section>
		</div>
		
		
		
		
		
				
		<script src="../js/jquery.min.js"></script>
        <script src="../js/jquery.plugin.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.flexslider-min.js"></script>
        <script src="../js/smooth-scroll.min.js"></script>
        <script src="../js/skrollr.min.js"></script>
        <script src="../js/spectragram.min.js"></script>
        <script src="../js/scrollReveal.min.js"></script>
        <script src="../js/isotope.min.js"></script>
        <script src="../js/twitterFetcher_v10_min.js"></script>
        <script src="../js/lightbox.min.js"></script>
        <script src="../js/scripts.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.js"></script>
        <script src="../js/bootstrap-formhelpers-phone.format.js"></script>
        
        
        <script>
    	$(document).ready(function (){
		    //validateB();
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
                document.getElementById("headerLogos").style.display="none";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		});
	</script>
	<script>
		document.getElementsByTagName("BODY")[0].onresize = function() {myFunction()};
		function myFunction() {
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		}
		
	</script>
        
        <script type="text/javascript">
	    	jQuery(document).ready(function() {
	    	jQuery('.telefono').keypress(function(tecla) {
	        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
	   			 });
			});
	    </script>
	
	
	    <script type="text/javascript">
			$(document).ready(function() {
				$("#frmValidate").hide()
				$("#datos").hide()
			});
			var input=  document.getElementById('numero1');
			input.addEventListener('input',function(){
			  if (this.value.length > 10) 
			     this.value = this.value.slice(0,10); 
			})
		</script>
	
	
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#numero22").val(value);
	          });
	      });
		</script>
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#telefono3").val(value);
	          });
	      });
		</script>
	
    </body>
</html>
				