<nav class="top-bar overlay-bar">
	<div class="container">
	
	
	
		<div class="row nav-menu">
			<div class="col-sm-3 col-md-2 columns">
				<a id="menuInicio1" href="http://www.lubo.com.mx">
					<img class="logo logo-light" alt="Logo" src="assets/imagenes/luboWebWhite.svg">
					<img class="logo logo-dark" alt="Logo" src="assets/imagenes/luboLogoWeb.svg">
				</a>
			</div>
			<div class="col-sm-9 col-md-2 columns">
			</div>
			<div class="col-sm-9 col-md-8 columns">
				<ul class="menu">
					<li class="<?php if($currentPage =='menu-inicio'){echo 'active';}?>"><a id="menuInicio2" href="http://www.lubo.com.mx">Inicio</a>
					</li>
					<!--<li class="<?php if($currentPage =='menu-driver'){echo 'active';}?>"><a href="registro/iniciar">Ser un Lubo Driver</a>
					</li>-->
					<li class="has-dropdown <?php if($currentPage =='menu-driver'){echo 'active';}?>"><a href="#">Conduce</a>
						<ul class="subnav">
							<li><a href="driver/registro/iniciar">Registrarse</a></li>
							<li><a href="driver/requisitos/">Requisitos</a></li>
						</ul>
					</li>
					<li class="<?php if($currentPage =='menu-viaja'){echo 'active';}?>"><a  href="viaja">Viajar con Lubo</a>
					</li>
					<li class="<?php if($currentPage =='menu-nosotros'){echo 'active';}?>"><a href="nosotros">Nosotros</a>
					</li>
					<li class="has-dropdown <?php if($currentPage =='menu-login'){echo 'active';}?>"><a href="#" id="log-user">Ingresar</a>
						<ul class="subnav">
							<li><a href="login">Conductor</a></li>
							<li><a href="login-user">Pasajero</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div><!--end of row-->
		
		<div class="mobile-toggle">
			<i class="icon icon_menu"></i>
		</div>
		
	</div><!--end of container-->
</nav>