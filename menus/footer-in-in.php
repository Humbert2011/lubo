<div class="row row-footer">
    <div class="col-xs-4 col-sm col-md-4">
    	<img src="../../assets/imagenes/luboWebWhite.svg" class="lubo-footer">
    	<p class="white-text startdust-footer">Startdust Inc.</p>
	</div>
	<div class="hidden-xs hidden-sm hidden-md">
    	
	</div>
	<div class="col-xs-8 col-sm-8 col-md-8 col-stores" >
    	<table class="tablaTiendas" id="tablaTiendas" >
			<tr>
		    	<td align="left"><a href="https://apps.apple.com/mx/app/lubo/id1470979821" target="_blank"><img src="../../img/appStoreLogo.png" width="85%" alt="logo"></a></td>
				<td align="left" style="margin-left:-200px;"><a href="https://play.google.com/store/apps/details?id=com.stardust.lubo&hl=es_MX" target="_blank"><img src="../../img/playStoreLogo.png" width="85%" alt="logo"></a></td>
			</tr>
		</table>
		<br>
		<p class="white-text links-footer"><a href="../../terminos-y-condiciones">Términos y condiciones</a> &nbsp;&nbsp; <a href="../../privacidad"> Privacidad </a> &nbsp;&nbsp; <a href="https://ayuda.lubo.com.mx">Ayuda</a></p>
	</div>
</div>
<div class="container-full imgHeader" style="">
	<img src="../../img/footerLubo.png" class="img-responsive inline imgHeader" id="imgHeader" alt="">
	
</div>