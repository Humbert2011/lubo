<div class="container-fluid" style="margin-left: 40px;">
			<div class="row">
				<div class="col-md-12 text-left">
					<h1 class="text-white proximaNovaBold"><b> ¿Y tú como viajas?</b></h1>
					<img src="img/LuboWhite.png" style="width: 80px;" class="logo-footer">
					<br><br>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-left">
					<li class="text-white" style="display: inline;"><span class="proximaNovaBold subtitulo-footer">Producto</span>
						<a class="link-footer" href="driver/registro/iniciar">Se un socio conductor</a>
						<a class="link-footer" href="viaja">Viaja con nosotros</a>
						<a class="link-footer" href="#">Seguridad</a>
					</li>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-left">
					<li class="text-white" style="display: inline;"><span class="proximaNovaBold subtitulo-footer margin-30">Empresa</span>
						<a class="link-footer" href="nosotros">¿Quienes somos?</a>
						<a class="link-footer" href="#">Contacto</a>
					</li>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-left">
					<li class="text-white" style="display: inline;"><span class="proximaNovaBold subtitulo-footer margin-30">Recursos</span>
						<a class="link-footer" href="terminos-y-condiciones">Términos y condiciones</a>
						<a class="link-footer" href="privacidad">Privacidad</a>
						<a class="link-footer" href="https://ayuda.lubo.com.mx">Ayuda</a>
	
					</li>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-left">
					<li class="text-white" style="display: inline; font-size:25px;"><span class="proximaNovaBold subtitulo-footer margin-30">Redes</span>
						<br> <br>
	
						<a href="https://www.facebook.com/Lubo-110883050760362/" target="_blank"><img src="img/Facebook.png" class="icon-social" alt=""></a>
						<a href="https://www.instagram.com/lubomx/" target="_blank"><img src="img/Instagram.png" class="icon-social" alt=""></a>
						<a href="https://twitter.com/Lubo_Mx" target="_blank"><img src="img/Twitter.png" class="icon-social" alt=""></a>
						<a href="https://www.youtube.com/channel/UCgeff8tgjL5j3RMgAAp3EWA/" target="_blank"><img src="img/Youtube.png" class="icon-social" alt=""></a>
					</li>
				</div>
	
			</div>
			<!--end of row-->
			<div class="row">
				<div class="col-sm-12 col-lg-8">
					<h1 class="text-white " style="font-size: 15px; margin-top: 60px;">2020 Startdust Inc. S.A. de C.V.</h1>
				</div>
				<div class="col-sm-12 col-lg-4 center">
					<h4 class="titulo-apps centered">App de pasajero</h4>
					<a href="https://play.google.com/store/apps/details?id=com.stardust.lubo&hl=es_MX" target="_blank"><img src="img/playStoreLogo.png" class="col-md-6 logo-store-footer" alt=""></a>
					<a href="https://apps.apple.com/mx/app/lubo/id1470979821" target="_blank"><img src="img/appStoreLogo.png" class="col-md-6 logo-store-footer" alt=""></a>
				</div>
			</div>
		</div>