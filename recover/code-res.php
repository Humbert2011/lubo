<!DOCTYPE html>
<html>
<head>
	<title>LUBO - Restablecer contraseña</title>
	<link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
	<!--Estilos que cree-->
	<link rel="stylesheet" type="text/css" href="Bootstrap/css/styles.css">
	<script type="text/javascript" src="Bootstrap/js/bootstrap.min.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script  src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-sm-12">
				<!--Esta parte solo contiene la imagen hasta el Email-->
				<div class="row" align="center">
					<div class="col clearfix">
						<div class="col-sm-12 mx-auto">
							<img src="img/LuboPink.png" height="50px" class="logo-pink">
							<br>
							<br>
							<p class="h2 text-center" style="font-family: Helvetica">Restablecer contraseña</p>
							<p id="pAgrega" class="text-center" style="font-family: Helvetica; font-size: 17px">Agrega de favor el código que fue enviado<br>a tu cuenta de correo
							</p>
							<p id="pSentimos" class="text-center" style="font-family: Helvetica; font-size: 18px">Lo sentimos</p>
							<p id="pEmail" class="h4 text-center" style="font-family: Helvetica" value=""></p>
							<br>
						</div>
					</div>
				</div>
				<div id="dPrincipal" class="row">
					<div class="col-sm-3"></div>
					<div class="col-sm-6">
						<form id="frmSendCode" name="frmSendCode">
							<input id="btnCodigo" type="text" name="codigo" placeholder="Código de recuperación" class="input-group-text btn-block" onkeyup="changecolor()"><br>
							<p id="codeIncorrect" class="text-center" style="font-family: Helvetica; color: red">Código incorrecto</p>
							<input id="btnCode" type="submit" class="btn btn-dark btn-block" value="Continuar" disabled="true"><br>
						</form>
					</div>
					<div class="col-sm-3"></div>
				</div>
				<div class="row">
					<form>
						<p id="pExpired" class="text-center" style="font-family: Helvetica; font-size: 22px">Tu código ha expirado, solicita <br> <a href="#">aquí uno nuevo</a></p>
					</form>
				</div>
				<div class="row">
					<form>
						<p id="notCode" class="text-center" style="font-family: Helvetica; font-size: 22px">No hay código generado <br> para esta cuenta</p>
					</form>
				</div>
				<div class="row">
					<form>
						<p class="text-center" style="font-family: Helvetica; font-size: 14px">Si requieres ayuda, ponte en contacto con
							<br> <a href="#">Soporte Técnico Lubo</a>
						</p>
						<p class="h6 text-center small" style="font-family: Helvetica; font-size: 12px">2019 Lubo. Todos los derechos reservados</p>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

<script>
	var urlbase = "http://lubo.com.mx/ApiREST/public/"//para servidor
	//var urlbase = "https://hooli.stardust.com.mx/public/"

	$(document).ready(function(){
		cambiaTamaño()
		window.action = getId()
	})

	function cambiaTamaño(){
		alto = window.innerHeight
		ancho = window.innerWidth
		var img = document.getElementById('shutterstock');
		img.style.height = parseInt(alto)
		img.style.width = parseInt(ancho)
	}

	//Tomar el valor del id en la URL
	var idUser = <?php echo $_GET["u"] ?>
	//Toma el id de la URL
	function getId(){
		var URL = urlbase+"auth/validaId/"+idUser;
        $.get(URL,function(data, status){		 	   		 	  
		 	if (data["response"]){
		 		var resultado = data["result"]
		 		document.getElementById('pEmail').innerHTML = resultado['email']
		 		document.getElementById('pSentimos').style.display = 'none'
		 		document.getElementById('codeIncorrect').style.display = 'none'
		 		document.getElementById('notCode').style.display = 'none'
		 		document.getElementById('pExpired').style.display = 'none'
		 	}else{
		 		document.getElementById('pAgrega').style.display = 'none'
		 		document.getElementById('dPrincipal').style.display = 'none'
		 		document.getElementById('pExpired').style.display = 'none'
		 	}
		});
	}

	//Verificación del código enviado
	$("#frmSendCode").submit(function(e) {
        e.preventDefault();
        var parametros = $("#frmSendCode").serialize();
        var URL = urlbase+"auth/validaCodigoWeb/"+idUser;
        $.post(URL,parametros,function(data, status){
		 	   var response = data['response']
		 	   switch (response) {
		 	   	case true:
		 	   		var urlN = "update.php?id="+idUser
		 	   		window.open(urlN)
		 	   		window.close()
			   		break;		
		   		case false:
		   			if (data['errors'] == "El código ha caducado") {
		   				document.getElementById('pSentimos').style.display = 'block'
		   				document.getElementById('pAgrega').style.display = 'none'
		   				document.getElementById('dPrincipal').style.display = 'none'
		   				document.getElementById('pExpired').style.display = 'block'

		   			}
		   			if (data['errors'] == "El código es incorrecto") {
		   				document.getElementById('btnCodigo').style.borderColor='red'
		   				document.getElementById('codeIncorrect').style.display = 'block'
		   			}
		   			break;
			 	default:
			    	break
		 	   }
		  });
    });

	//Cambia el color del botón
    function changecolor(){
    	var code = document.getElementById('btnCodigo').value
    	if (code.length > 0) {
    		document.getElementById('btnCode').style.backgroundColor = '#ED3093'
    		document.getElementById('btnCode').style.borderColor = '#ED3093'
    		document.getElementById('btnCode').disabled = false
    	}else{
    		document.getElementById('btnCode').style.backgroundColor = 'black'
    		document.getElementById('btnCode').style.borderColor = 'black'
    		document.getElementById('btnCode').disabled = true
    	}
    }
	

</script>