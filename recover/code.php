<?php 
	// $err = isset($_GET['error']) ? $_GET['error'] : null ;
	if (!isset($_GET['u'])) {
		header('Location: ../');
		// echo 'No hay U';
	}
	else{
		$idUser = $_GET['u'];
	}
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>LUBO - Restablecer contraseña</title>
	 <!-- icon -->
	<link rel="icon" type="image/x-icon" href="../img/favicon/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
		<link href="../css/styleRPass.css" rel="stylesheet" type="text/css" media="all"/>
	<!--Estilos que cree-->
	<link rel="stylesheet" type="text/css" href="Bootstrap/css/styles.css">
	<script type="text/javascript" src="Bootstrap/js/bootstrap.min.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script  src="https://code.jquery.com/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="../js/config/config.js"></script>
	<script type="text/javascript" src="../js/sw/servicios.js" ></script>
	<script>
		$(document).ready(function (params) {
			// alert('lsito!')
			valId($('#idusuario').val())
		})
	</script>
</head>
<body>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-sm-12 text-center">
				<div class="elelment">	
					<div class="element-main">
					<h2><img src="../img/LuboPink.png" class="imgPin" width="150px"></h2>
						<h1>Restablecer Contraseña</h1>
						<p style="margin-top:16px;" class="pRegular">Agrega de favor el código que fue enviado<br>a tu cuenta de correo:</p>
						<p class='pRegular' id= "correo" style='font-size:18px;color:#4a4a4a; margin-top:-14px'><b>correo@hotmail.com</b></p>
						
						<!-- <form> -->
							
							<input type="text" class="" maxlength="6" onkeyup="tecla()" placeholder="Código de recuperación" style=""name="codigo" id="codigo" value="" onfocus="foco()">
							<input type="hidden" name="idusuario" id="idusuario" value="<?php echo $idUser; ?>">
							<input type="submit" onClick="valIdCod($('#idusuario').val(),$('#codigo').val())"  class="form-control" name="Aceptar" id="Aceptar" value="Continuar">
							<!-- <button class="form-control" name="Aceptar" id="Aceptar" >Acepatar</button> -->
						<!-- </form> -->
						<p id= "error" style='font-size:18px;color:#FF0000;'><b></b></p>
						<!-- <p class='pSemibold' id= "correo" style='font-size:18px;color:; margin-top:-14px'><b></b></p> -->
						<p style="color:#4a4a4a; font-size:14.5px;" class="pRegular">Si requieres ayuda, ponte en contacto con
				<br><span style="color:#4990E2;">Soporte Técnico Lubo</span></p>
					</div>
				</div>
				<div class="copy-right">
							<p class="" style="color:#fff; opacity:.5;">© 2017 Pikr. Todos los derechos reservados</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script>
function tecla() {
	cadena = $("#codigo").val()
	if(cadena.length > 0 ){
		document.getElementById('codigo').style = 'border: 1px solid #B9B9B9;'
		document.getElementById('error').innerHTML = ''
	}
	console.log(cadena.length)
}	
function foco() {
	document.getElementById('codigo').value = ''
	document.getElementById('codigo').style = 'border: 1px solid #B9B9B9;'
	document.getElementById('error').innerHTML = ''
}
</script>