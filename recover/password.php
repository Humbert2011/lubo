<!DOCTYPE HTML>
<html lang="es">
<head>
<meta charset="utf-8">
<title>Restablecer contraseña</title>
 <!-- icon -->
	<link rel="icon" type="image/x-icon" href="../img/favicon/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
<link href="../css/styleRPass.css" rel="stylesheet" type="text/css" media="all"/>
<link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
<!-- Custom Theme files -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<!--google fonts-->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script  src="https://code.jquery.com/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="../js/config/config.js"></script>
	<script type="text/javascript" src="../js/sw/servicios.js" ></script>
	<script>
		$(document).ready(function (params) {
			var mesag = sessionStorage.getItem('msg')
			if(mesag == null){
				$(location).attr('href','code.php')
			}
			document.getElementById('userId').value = mesag
		})
	</script>
</head>
<body>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-sm-12 text-center">
<!--element start here-->
				<div class="elelment">
					<div class="element-main">
						<h2><img src="../img/LuboPink.png" class="imgPin" width="150px"></h2>
						<h1>Restablecer Contraseña</h1>
						<p class="pRegular">Agrega tu nueva contraseña</p>
						<!-- <form > -->
						<input type="password" onkeyup="rest()"  onfocus="foco()" name="contraseña" id="password-1" class="pRegular" placeholder="Contraseña"></input><br>
						<input type="hidden" name="userId" id="userId" value="0">
						<p id= "alerta1" style='font-size:18px;color:#FF0000;'><b></b></p>
						<input type="submit"  onClick = "updatePass($('#password-1').val(),$('#userId').val())" class="form-control" name="update" id="update" value="Enviar">
						<!-- </form> -->
						<p style="color:#4a4a4a; font-size:14.5px;" class="pRegular">Si requieres ayuda, ponte en contacto con
				<br><span style="color:#4990E2;">Soporte Técnico Lubo</span></p>
					</div>
				</div>
				<div class="copy-right">
							<p style="color:#fff; opacity:.5;">© 2017 Lubo. Todos los derechos reservados</p>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
<!--element end here-->
<script src="../js/ojito/jquery.js"></script>
    <script src="../js/ojito/bootstrap.min.js"></script>
	<script src="../js/ojito/modernizr.custom.js"></script>
	<script src="../js/ojito/hideShowPassword.js"></script>	
<script>
		$(window).ready(function(){
			$('#password-1').hideShowPassword({
			  // Creates a wrapper and toggle element with minimal styles.
			  innerToggle: true,
			  // Makes the toggle functional in touch browsers without
			  // the element losing focus.
			  touchSupport: Modernizr.touch
			});
		});

	// borrar alerta
	function rest() {
		cadena = $("#password-1").val()
		if(cadena.length > 0 ){
			document.getElementById('password-1').style = 'border: 1px solid #B9B9B9;'
			document.getElementById('alerta1').innerHTML = ''
		}
		console.log(cadena.length)
	}
	function foco() {
		document.getElementById('password-1').value = ''
		document.getElementById('password-1').style = 'border: 1px solid #B9B9B9;'
		document.getElementById('alerta1').innerHTML = ''
	}
	</script>