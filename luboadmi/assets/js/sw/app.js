let listadoDriver = [];

let enter = (correo, password) => {
    // alert('hola')
    event.preventDefault()
        // auth(correo, password)
}

let auth = (correo, password) => {
    sessionStorage.removeItem('token');
    let sw = `${url}auth/autenticar`
    let parametros = {
        "Telefono": correo,
        "Password": password,
        "Tipo_usuario": "1"
    }
    fetch(sw, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(parametros), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
        .then(response => {
            if (response['response']) {
                console.log(response['result'])
                sessionStorage.setItem('token', response['result']['token'])
                self.location = "index.html";
                sessionStorage.setItem('idAdmin', res.result.persona);
            } else {
                if (response['errors'].length == 0) {
                    alert(response['message'])
                } else {
                    alert(response['errors'])
                }
            }
        });
}

let validarToken = () => {
    // url + 'auth/getData/' + token
    let token = sessionStorage.getItem('token')
    if (token == null) {
        self.location = 'login.html'
        return true
    }
    let sw = `${url}auth/getData/${token}`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res['response']) {
                console.log(res);
                sessionStorage.setItem('idAdmin', res.result.id);
                let tipoUser = res['result']['Tipo_de_usuario']
                if (tipoUser != 1) {
                    logout()
                    self.location = 'login.html'
                }
                if (res['result']['idZona'] == 0) {
                    sessionStorage.setItem('esSuperAdmin', true)
                } else {
                    sessionStorage.setItem('esSuperAdmin', res['result']['idZona'])
                }

            } else {
                if (res['errors'].length == 0) {
                    // alert(res['message'])
                    self.location = 'login.html'
                } else {
                    // alert(res['errors'])
                    self.location = 'login.html'
                }
            }
        })
}

let listarAdministradores = () => {
    // alert('listando')
    let sw = `${url}administradores/listar`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            // console.log(res);
            let result = res['data']
            let cuerpo = document.getElementById('tbody')
            let insertHTML = ''
            result.forEach(element => {
                if (element['Descripcion'] == null) element['Descripcion'] = 'Super Admin'
                insertHTML += `<tr><td>${element['id_tbPersona']}</td><td>${element['Nombre']}</td><td>${element['Apellidos']}</td><td>${element['Fecha_Registro']}</td><td>${element['Email']}</td><td>${element['Telefono']}</td><td>${element['Descripcion']}</td><td><button onclick='openAdmin(${element['id_tbPersona']})'class='btn btn-primary'>Detalle</button></td></tr>`
            })
            cuerpo.innerHTML = insertHTML
        })
}

let openAdmin = (idAdmin = 0) => {
    sessionStorage.setItem('idAdmin', idAdmin)
    self.location = "registrar-admin.html"
}

let irRegAdmin = () => {
    sessionStorage.removeItem('idAdmin');
    self.location = "registrar-admin.html"
}
let obtenerAdministrador = (idAdmin = 0) => {
    let sw = `${url}administradores/infoAdmin/${idAdmin}`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            // console.log(res['response']);
            if (res['response']) {
                let result = res['result']
                    // "id_tbPersona": "3",
                    // "Tipo_de_usuario": "1",
                document.getElementById('Nombre').value = result["Nombre"] //: "Humbert",
                document.getElementById('Apellidos').value = result["Apellidos"] //: "Martinez cuautenco",
                document.getElementById('Email').value = result["Email"] //: "humbert@stardust.com.mx",
                    // "Password": "$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0",
                document.getElementById('Telefono').value = result["Telefono"] //"Telefono": "7761052213",
                    // "Imagen": "http://lubo.com.mx/ApiRESTdev/img/tbpersona-2-IMG_20181128_153308523.jpg",
                document.getElementById('Sexo').value = result["Sexo"] //"Sexo": "M",
                    // "Fecha_Registro": "2019-05-16 11:00:27",
                    // "Status": "1",
                    // "MetodoPago": null,
                    // "Latitud": null,
                    // "Longitud": null,
                document.getElementById('idZona').value = result["idZona"]
            }
        })
}

let verPass = () => {
    let tipo = document.getElementById("Password");
    if (tipo.type === "password") {
        tipo.type = "text";
    } else {
        tipo.type = "password";
    }
}
let registrarAdmin = ($idAdmin = null) => {
    let out = true
    if (document.getElementById('Nombre').value == '') {
        out = false
        document.getElementById('NombreError').style.display = 'block'
    }
    if (document.getElementById('Apellidos').value == '') {
        out = false
        document.getElementById('ApellidosError').style.display = 'block'
    }
    if (document.getElementById('Sexo').value == 0) {
        out = false
        document.getElementById('SexoError').style.display = 'block'
    }
    if (document.getElementById('Telefono').value == '') {
        out = false
        document.getElementById('TelefonoError').style.display = 'block'
    }
    if (document.getElementById('Email').value == '') {
        out = false
        document.getElementById('EmailError').style.display = 'block'
    }
    // console.log($idAdmin);
    if ($idAdmin == null) {
        if (document.getElementById('Password').value == '') {
            out = false
            document.getElementById('PasswordError').style.display = 'block'
        }
    }
    if (out == true) {
        let idZona = document.getElementById('idZona').value
        if (idZona == 0) idZona = null
        let data = {
                "Nombre": document.getElementById('Nombre').value,
                "Apellidos": document.getElementById('Apellidos').value,
                "Sexo": document.getElementById('Sexo').value,
                "Telefono": document.getElementById('Telefono').value,
                "Email": document.getElementById('Email').value,
                "Password": document.getElementById('Password').value,
                "idZona": idZona,
                "CodigoPais": '52'
            }
            // console.log(data);
        let sw = `${url}`
        let metodo = ``
        if ($idAdmin == null) {
            sw += `persona/registrarAdmin`
            metodo = `POST`
        } else {
            sw += `administradores/actualizar/${$idAdmin}`
            metodo = `PUT`
        }
        // console.log(data);
        fetch(sw, {
                method: metodo,
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res['response']) {
                    self.location = 'lista-admin.html'
                } else {
                    // console.log(res);
                    alert(res['message'])
                }
            })
    }
}
let limpiar = (elemento) => {
    document.getElementById(`${elemento['id']}Error`).style.display = 'none'
        // console.log(elemento);
}

let listarConductores = ($is) => {
    let fechaActual = document.querySelector('#fechaActual');
    let fecha = new Date(); //Fecha actual
    let mes = fecha.getMonth() + 1; //obteniendo mes
    let dia = fecha.getDate(); //obteniendo dia
    let year = fecha.getFullYear(); //obteniendo año
    if (dia < 10)
        dia = '0' + dia; //agrega cero si el menor de 10
    if (mes < 10)
        mes = '0' + mes //agrega cero si el menor de 10
    let hora = fecha.getHours();
    let min = fecha.getMinutes();
    fechaActual.innerHTML = `Fecha actual: ${hora}:${min} ${dia}/${mes}/${year}`;
    // 
    let superAdmin = sessionStorage.getItem('esSuperAdmin')
    let sw = ''
    if ($is == 'true') {
        sw = `${url}conductores/listar`
    } else {
        sw = `${url}conductores/listarxzona/${$is}`
    }
    console.log(sw);
    console.log($is);
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res)
            let result = res['data']
            listadoDriver = result;
            console.log(result.length);
            document.getElementById(`numC`).innerHTML = `Numero de conductores: ${result.length}`
            let cuerpo = document.getElementById('tbody')
            if (superAdmin == 'true') {
                document.getElementById('zonaUser').style.visibility = 'visible';
            } else {
                document.getElementById('zonaUser').style.visibility = 'hidden';
            }

            result.forEach(element => {
                let fila = document.createElement(`tr`)
                let celda1 = document.createElement(`td`)
                let celda2 = document.createElement(`td`)
                let celda3 = document.createElement(`td`)
                let celda4 = document.createElement(`td`)
                let celda5 = document.createElement(`td`)
                let celda6 = document.createElement(`td`)
                let celda7 = document.createElement(`td`)
                let celda8 = document.createElement(`td`)
                let celda9 = document.createElement(`td`)

                let Status = 'Activo'

                let id = document.createTextNode(`${element['id_tbPersona']}`)
                let nombre = document.createTextNode(`${element['Nombre']}`)
                let apellidos = document.createTextNode(`${element['Apellidos']}`)
                if (element[`Registro`] != '5') Status = `Pendiente`
                let tipo = document.createTextNode(`${Status}`)

                let correo = document.createTextNode(`${element['Email']}`)
                let telefono = document.createTextNode(`${element['Telefono']}`)
                let registro = document.createTextNode(`${element['Registro']}`)
                let placa = document.createTextNode(`${element['Placa']}`)
                    // boton de detalle
                let boton = document.createElement("button")
                boton.type = `button`
                boton.innerHTML = 'Detalle'
                boton.className = 'btn btn-primary'
                    // boton eliminar conductor
                let Beliminar = document.createElement("button");
                Beliminar.type = `button`
                Beliminar.innerHTML = 'Eliminar'
                Beliminar.className = 'btn btn-danger'

                // boton.onclick = detalle(`${element['id_tbPersona']}`)
                celda1.appendChild(id)
                celda2.appendChild(nombre)
                celda3.appendChild(apellidos)
                celda4.appendChild(tipo)
                celda5.appendChild(correo)
                celda6.appendChild(telefono)
                celda7.appendChild(placa)
                celda8.appendChild(registro)
                celda9.appendChild(boton)
                celda9.appendChild(Beliminar)

                fila.appendChild(celda1)
                fila.appendChild(celda2)
                fila.appendChild(celda3)
                fila.appendChild(celda4)
                fila.appendChild(celda5)
                fila.appendChild(celda6)
                fila.appendChild(celda7)
                fila.appendChild(celda8)
                fila.appendChild(celda9)
                cuerpo.appendChild(fila)
                    // boton click de detalle de conductor
                boton.onclick = function() {
                        detalle(`${element['id_tbPersona']}`)
                    }
                    // click de boton eliminar conductor
                Beliminar.onclick = () => {
                    eliminarConductor(`${element['id_tbPersona']}`);
                }

                // boton.onmousedown = function(e) {
                //     // detalle(`${element['id_tbPersona']}`)
                //     // console.log(e.button);
                //     if (e.button == 2) {
                //         sessionStorage.setItem('idConductor', element['id_tbPersona'])
                //         window.open("perfil-conductor.html");
                //     }
                // }
            });

        })
}

let detalle = (id) => {
    sessionStorage.setItem('idConductor', id)
    self.location = `perfil-conductor.html`
}

const eliminarConductor = (id) => {
    let sw = `${url}persona/eliminar/${id}`
    if (confirm("Esta seguro de eliminar este condutor")) {
        fetch(sw, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                alert("Se elimino el conductor");
                self.location.reload();
            })
    }
}

let obtenerConductor = (id) => {
    let sw = `${url}persona/obtener/${id}`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res['response']) {
                console.log(res.result);
                let img = document.getElementById('perfil')
                img.src = res['result']['Imagen']
                console.log("img perfil:", res.result["Imagen"]);
                if (res.result["Imagen"] != null) {
                    document.getElementById('perfil').style.display = 'inline';
                }
                // 
                document.getElementById('Nombre').value = res['result']['Nombre']
                document.getElementById('Apellidos').value = res['result']['Apellidos']
                document.getElementById('Sexo').value = res['result']['Sexo']
                document.getElementById('Fecha_Registro').value = res['result']['Fecha_Registro']
                document.getElementById('Telefono').value = res['result']['Telefono']
                document.getElementById('Email').value = res['result']['Email']
                let status = 'Activo'
                if (res['result']['Status'] == 3) {
                    status = 'Pendiente'
                } else if (res['result']['Status'] == 0) {
                    status = 'Inactivo'
                }
                document.getElementById('Status').value = status
                document.getElementById('idZona').value = res['result']['idZona'];
                // 
                let verificar = `${url}img/verImgenPerfil/${id}`
                fetch(verificar, {
                        method: 'GET',
                        "headers": {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(result => result.json())
                    .then(result => {
                        if (result['response']) {
                            // console.log(result['result']);
                            img = document.getElementById('validar')
                                // console.log(result['result']['Imagen']);
                            img.src = result['result']['Imagen']
                            document.getElementById('validar').style.display = 'inline'
                            document.getElementById('validarBoton').style.display = 'inline'
                        }
                    })
            } else {
                if (res['errors'].length == 0) {
                    alert(res['message'])
                    self.location = 'Conductores.html'
                } else {
                    alert(res['errors'])
                    self.location = 'Conductores.html'
                }
            }
        })
    sw = `${url}registro/obtener/${id}`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response) {
                console.log(res);
                if (res.result.Status == 5) {
                    let elemento = document.querySelector('#ActivarDriver');
                    elemento.value = "Driver Activo";
                    elemento.style.background = "green";
                    elemento.addEventListener("mouseover", () => {
                        elemento.onclick = () => desactivarDriver();
                        elemento.value = "Desactivar Conductor";
                        elemento.style.background = "Red";
                    });
                    elemento.addEventListener("mouseout", () => {
                        elemento.value = "Driver Activo";
                        elemento.style.background = "green";
                        element.onclick = () => activarDriver();
                    })
                }

            } else {

            }
        })
}
const desactivarDriver = () => {
    if (confirm("Esta seguro de desactivar este Conductor?")) {
        let idPersona = sessionStorage.getItem('idConductor');
        let sw = `${url}registro/actualizar/${idPersona}`
        console.log(sw);
        parametros = {
            "Status": 3
        }
        fetch(sw, {
                method: 'PUT',
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res.response) {
                    alert("Conductor Desactivado");
                    self.location.reload();
                } else {
                    alert("No se realizo el cambio intentelo de nuevo")
                }
            })
    }
}

let listarZonas = (todoAdd = 0) => {
    let sw = `${url}zona/listar`;
    let ante = ``;
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            let data = null;
            let htmlInsert = '';
            let listazonas = document.getElementById('idZona');
            // console.log(listazonas);
            if (res.result['total'] > 0) {
                data = res.result['data']
                console.log(data)
                if (todoAdd == 1) {
                    ante = `<option value="0">Todas</option>`;
                } else if (todoAdd == 2) {
                    ante = `<option value="0">Zonas--></option>`;
                }
                htmlInsert += ante;
                data.forEach(element => {
                    // console.log(element['Descripcion'])
                    htmlInsert += `<option value = "${element['idZona']}">${element['Descripcion']}</option>`;
                    console.log(htmlInsert);
                });
            } else {
                console.log(res.result.total);
            }
            listazonas.innerHTML = htmlInsert;
        });
}

let recuperarPassword = (email) => {
    let sw = `${url}auth/recuperarPassword`;
    let parametros = {
        email: email,
        TipoUsuario: sessionStorage.getItem('tipoUser')
    }
    event.preventDefault();
    let confirmar = confirm("Es este su correo?");
    if (confirmar) {

        let tipoUser = sessionStorage.getItem('tipoUser');
        console.log(tipoUser, email);
        fetch(sw, {
                method: 'POST',
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res.response) {
                    alert(`Se invio un codigo al Email: ${email}`);
                    self.location = "login.html";
                } else {
                    alert("No se pudo enviar el mensaje a su correo, porfavor verifiquelo");
                }
            })
    } else {

    }

}

let updateConductor = () => {
    let confirmar = confirm(`Esta seguro de actulizar estos datos`);
    let idConductor = sessionStorage.getItem('idConductor');
    if (confirmar) {
        let sw = `${url}persona/actualizarPersona/${idConductor}`;
        let parametros = {
            "Nombre": document.querySelector('#Nombre').value,
            "Apellidos": document.querySelector('#Apellidos').value,
            "Sexo": document.querySelector('#Sexo').value,
            "Telefono": document.querySelector('#Telefono').value,
            "Email": document.querySelector('#Email').value,
            "idZona": document.querySelector('#idZona').value,
            "Password": document.querySelector('#Password').value
        };
        // console.log(parametros,sw);
        fetch(sw, {
                method: 'PUT',
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res.response) {
                    alert("Se actulizo la informacion");
                } else {
                    alert(res.error);
                }
            });

    }
}

let obtenerAuto = (id) => {
    let sw = `${url}vehiculo/obtener/${id}`;
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res['response']) {
                //console.log(res['result'])
                // let modotrabajo
                // if (res['idModoTrabajo'] == 1) {
                //     modotrabajo = 'Privado'
                // } else {
                //     modotrabajo = 'Taxi'
                // }

                document.getElementById('modelo').value = res['result']['Modelo'];
                document.getElementById('year').value = res['result']['Año'];
                document.getElementById('color').value = res['result']['Color'];
                document.getElementById('placa').value = res['result']['Placa'];
                document.getElementById('modoTrabajo').value = res['result']['idModoTrabajo'];
                document.getElementById('numPasajeros').value = res['result']['Num_Pasajeros'];
                document.getElementById('numSerie').value = res['result']['No_de_serie_del_vehiculo'];

                document.getElementById('idAuto').value = res['result']['id_tbVehiculo']

                buscarDA(res['result']['id_tbVehiculo'])
                let img = res['result']['1']
                    // console.log(img);
                let j = 1
                    // console.log(img.length);
                numImagenesAutos = img.length;
                if (img.length > 0) {
                    img.forEach(i => {
                        if (i['Imagen'] != null) {
                            document.getElementById(`imgvehiculo${j}`).src = i['Imagen']
                            document.getElementById(`imgvehiculo${j}`).style.display = 'inline'
                            document.getElementById(`idFotoAuto${j}`).value = i['id_tbFotosVehiculo'];
                            document.getElementById(`texto${j}`).innerHTML = "+Editar";
                            // console.log(document.getElementById(`idFotoAuto${j}`));
                            // console.log(i['id_tbFotosVehiculo']);
                        }
                        j++
                    });
                }
                if (img.length < 3) {
                    document.getElementById('documentosAYP').style.display = 'none';
                }
            } else {
                document.getElementById('imgAuto').style.display = 'none';
                document.getElementById('documentosAYP').style.display = 'none';
            }
        })

}

let numImagenesAutos = 0;

let buscarDoc = (id) => {
    let sw = `${url}img/listarDoc/${id}`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            // console.log(res);
            if (res['response']) {
                res['result'].forEach(r => {
                    if (r['Status'] != 2) {
                        agregarElemento(r['Descripcion'], r['Documento'], r['id_tipo_documento'], r['Status'], r['fechaExpiracion'])
                    }
                });
            }

        })
}
let buscarDA = (id) => {
    let sw = `${url}img/listarDA/${id}`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            // console.log(res);
            if (res['response']) {
                res['result'].forEach(r => {
                    if (r['Status'] != 2) {
                        agregarElemento(r['Descripcion'], r['Documento'], r['id_tipo_documento'], r['Status'], r['fechaExpiracion'])
                    }
                });
            }
        })
}
let numDoc = 0;
let agregarElemento = (des, url, idDocumento, Status = 0, fechaEX) => {
    // console.log(fechaEX);
    let simbolo = '';
    if (Status == 0) {
        simbolo = '&#x1f50d;';
    } else if (Status == 1) {
        simbolo = '&#x2714;';
    } else if (Status == 3) {
        simbolo = '&#x2718';
    }
    let idConOrAut = 0;
    if (idDocumento > 4) {
        idConOrAut = sessionStorage.getItem('idConductor');
    } else {
        idConOrAut = document.getElementById('idAuto').value;
    }
    document.getElementById(`${idDocumento}`).innerHTML = `<a target="_blank" href='${url}'>${des}</a> - <spam id='simbolo${idDocumento}'>${simbolo}</spam> -
    <spam id='fechaex${idDocumento}'>${fechaEX}</spam>
    <input onclick="aceptarDoc(${idDocumento},'${des}')" class = "aceptar" type="button" value="&#x2714;">
    <input onclick="rechazarDoc(${idDocumento},'${des}')" class = "rechazar" type="button" value="&#x2717;">
    <button class="fechaEX" onclick = "alAbrirFchaEx('${idDocumento}','${des}')" data-toggle="modal" data-target="#myModal">
        <img style="height: 20px;" src="../assets/img/calendar.png" alt="Fecha Expiracion" srcset="">
    </button>
    <div class="inputDocumentos">
    <form id="documento${idDocumento}">
        <p class="pDoc">Editar Doc.</p><input onchange="cargarDocumento(document.getElementById('documento${idDocumento}'),${idConOrAut},${idDocumento})" class="fileDoc" type="file" name="img" id="">
        </form>
    </div>`;
    numDoc++;
    if (numDoc >= 10) {
        let sw2 = `${url}registro/actualizar/${idPersona}`
            // console.log(sw2);
        parametros2 = {
            "Status": 4
        }
        fetch(sw2, {
                method: 'PUT',
                body: JSON.stringify(parametros2),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                // console.log(res);
            })
    }
    // console.log(numDoc);
    return true
}

let verImg = (url = null) => {
    document.getElementById('imgModal').src = `${url}`
    document.getElementById('modaal').style.display = "block";
}


let closeModal = () => {
    document.getElementById('modaal').style.display = "none";
}

let validarImgPerfil = () => {
    let conf = confirm('Esta seguro de validar esta imagen')
    if (conf) {
        let idConductor = sessionStorage.getItem('idConductor')
        let sw = `${url}img/validarImg/${idConductor}`
        fetch(sw, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                // console.log(res);
                if (res['response']) {
                    alert('Imagen validada de manera correcta');
                    location.reload();
                } else {
                    alert(res['errors']);
                }
            })
    }
}

let regAuto = () => {
    let r = confirm("Esta seguro de Registrar este auto!");
    idAuto = document.getElementById('idAuto').value;
    if (r) {

        idPersona = sessionStorage.getItem('idConductor');
        placa = document.getElementById('placa').value;
        modelo = document.getElementById('modelo').value;
        color = document.getElementById('color').value;
        an = document.getElementById('year').value;
        num = document.getElementById('numSerie').value;
        modo = document.getElementById('modoTrabajo').value;
        pasajeros = document.getElementById('numPasajeros').value;
        var parametros = {
                "idPersona": idPersona,
                "Placa": placa,
                "Modelo": modelo,
                "Color": color,
                "Año": an,
                "No_de_serie_del_vehiculo": num,
                "idModoTrabajo": modo,
                "Num_Pasajeros": pasajeros
            }
            console.log(parametros);
        let sw = '';
        let metodo = '';
        if (idAuto == 0) {
            sw = `${url}vehiculo/registrar`;
            metodo = 'POST';
        } else {
            sw = `${url}vehiculo/actualizar/${idAuto}`;
            metodo = 'PUT';
        }
        console.log(sw);
        fetch(sw, {
                method: metodo,
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res['response']) {
                    if (metodo == 'POST') {
                        let sw2 = `${url}registro/actualizar/${idPersona}`
                            // console.log(sw2);
                        parametros2 = {
                            "Status": 1
                        }
                        fetch(sw2, {
                                method: 'PUT',
                                body: JSON.stringify(parametros2),
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            })
                            .then(res => res.json())
                            .then(res => {
                                console.log(res);
                            })
                        document.getElementById(`idAuto`).value = res['result'];
                    }
                    document.getElementById('imgAuto').style.display = 'inline';

                    alert("Se dio de alta la informacion del auto")
                }else{
                    alert("algo salio mal intentelo de Nuevo")
                }
            })
        
    }
}

let subirImgAuto = (form, idAuto, idFoto, elementoInvocador = 0) => {
    let tipo = ``;
    let sw = ``;
    if (idFoto > 0) {
        sw = `${url}img/updateFotoAuto/${idFoto}`;
        tipo = `update`;
    } else {
        sw = `${url}img/cargar/vehiculo/${idAuto}`;
        tipo = `insert`;
    }
    // console.log(sw, tipo);
    $.ajax({
        type: 'POST', //para carga de imagenes solo funciona el post
        url: sw,
        data: new FormData(form),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            return true
        },
        success: function(msg) {
            if (msg['response']) {
                if (tipo == 'insert') {
                    //colocar id de la foto cargada en el identificador
                    document.getElementById(`idFotoAuto${elementoInvocador}`).value = msg['result'];
                    document.getElementById(`texto${elementoInvocador}`).innerHTML = "+Editar";
                    //sumar para saber si hay tres 
                    numImagenesAutos = numImagenesAutos + 1;
                    // console.log(numImagenesAutos);
                    //mostrar el panel de documentos si ya se cargaron las imagenes
                    if (numImagenesAutos >= 3) {
                        idPersona = sessionStorage.getItem('idConductor');
                        let sw2 = `${url}registro/actualizar/${idPersona}`
                            // console.log(sw2);
                        parametros2 = {
                            "Status": 2
                        }
                        fetch(sw2, {
                                method: 'PUT',
                                body: JSON.stringify(parametros2),
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            })
                            .then(res => res.json())
                            .then(res => {
                                // console.log(res);
                            })
                        document.getElementById('documentosAYP').style.display = 'inline';

                    }
                    // console.log(msg);
                }

                alert('Imagen Cargada de manera correcta');
                return true
            } else {
                // console.log(new FormData(form));
                alert(msg['errors']);
                // console.log(msg);
                return false
            }
        }
    })
}

let cargarDocumento = (form = 0, id = 0, tipo = 0) => {

    if (confirm('Quiere cargar este documento')) {
        // console.log(
        //     form, id, tipo
        // );
        let sw = `${url}img/cargar/${tipo}/${id}`;
        // console.log(sw);
        fetch(sw, {
                method: 'POST',
                body: new FormData(form),
            })
            .then(res => res.json())
            .then(res => {
                // console.log(res);
                if (res['response']) {
                    // if (numDoc <= 9) {
                    //     let idPersona = sessionStorage.getItem('idConductor');
                    //     let sw2 = `${url}registro/actualizar/${idPersona}`
                    //         // console.log(sw2);
                    //     parametros2 = {
                    //         "Status": 3
                    //     }
                    //     fetch(sw2, {
                    //             method: 'PUT',
                    //             body: JSON.stringify(parametros2),
                    //             headers: {
                    //                 'Content-Type': 'application/json'
                    //             }
                    //         })
                    //         .then(res => res.json())
                    //         .then(res => {
                    //             // console.log(res);
                    //         })
                    // }
                    // obtenerDocumento()
                    // agregarElemento = (des, url, idDocumento)
                    alert("Archivo cargado de manera correcta")
                    location.reload();
                } else {
                    alert(res['errors']);
                }
            });
    }
}

let datosBancarios = (idConductor) => {
    let sw = `${url}/infobancaria/listar/${idConductor}`;
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            // console.log(res['result']);
            if (res["response"]) {
                let inf = res['result'];
                document.getElementById('nombre_banco').value = inf['Nombre_Banco'];
                document.getElementById('num_cuenta').value = inf['Num_Cuenta'];
                document.getElementById('num_tarjeta').value = inf['Num_Tarjeta']
                document.getElementById('clave_interbancaria').value = inf['Clave_Inter_Bancaria'];
                document.getElementById('idDatosBancarios').value = inf['idDatosBancarios'];
                document.querySelector('#nombre_beneficiario').value = inf['Nombre_Beneficiario']
            } else {

            }
        });
}

let altaDatosBancarios = () => {
    if (confirm("Esta seguro de subir esta informacion Bancaria")) {
        let idDatosBancarios = document.getElementById("idDatosBancarios").value;
        let metodo = `POST`;
        let sw;
        if (idDatosBancarios == 0) {
            sw = `${url}infobancaria/registrar`;
        } else {
            metodo = `PUT`;
            sw = `${url}infobancaria/actualizar/${idDatosBancarios}`;
        }

        let parametros = {
            "Nombre_Banco": document.getElementById("nombre_banco").value,
            "Num_Cuenta": document.getElementById("num_cuenta").value,
            "Num_Tarjeta": document.getElementById("num_tarjeta").value,
            "Clave_Inter_Bancaria": document.getElementById("clave_interbancaria").value,
            "idPersona": sessionStorage.getItem('idConductor'),
            "Nombre_Beneficiario": document.querySelector('#nombre_beneficiario').value
        }

        fetch(sw, {
                method: metodo,
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res["response"]) {
                    alert("Informacion dada de alta");
                } else {
                    alert("No se actualizo la informacion Bancaria");
                }
            });
    }
}


let aceptarDoc = (idDoc = 0, Des = '') => {
    if (confirm(`¿Seguro que quiere aceptar ${Des}?`)) {
        let idAModificar = 0;
        if (idDoc < 5) {
            idAModificar = document.getElementById('idAuto').value;
        } else {
            idAModificar = sessionStorage.getItem('idConductor');
        }
        let sw = `${url}img/updateStatusDoc/${idAModificar}`;
        let parametros = {
            "idDoc": idDoc,
            "status": 1
        };
        fetch(sw, {
                method: 'PUT',
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res['response']) {
                    console.log(res);
                    document.getElementById(`simbolo${idDoc}`).innerHTML = '&#x2714;';

                } else {
                    document.getElementById(`simbolo${idDoc}`).innerHTML = '&#x2718;';
                }
            })
            // console.log(idAModificar);

    }
}


let rechazarDoc = (idDoc = 0, Des = '') => {
    if (confirm(`En verdad quiere rechazar ${Des}`)) {
        let idAModificar = 0;
        if (idDoc < 5) {
            idAModificar = document.getElementById('idAuto').value;
        } else {
            idAModificar = sessionStorage.getItem('idConductor');
        }
        let sw = `${url}img/updateStatusDoc/${idAModificar}`;
        let parametros = {
            "idDoc": idDoc,
            "status": 3
        };
        fetch(sw, {
                method: 'PUT',
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res['response']) {
                    document.getElementById(`simbolo${idDoc}`).innerHTML = '&#x2718;';
                } else {
                    document.getElementById(`simbolo${idDoc}`).innerHTML = '&#x2714;';
                }
            })
        console.log(idAModificar);
    }
}

let altaFechaEx = () => {
    idDoc = document.getElementById('idDocFechaEx').value;
    if (confirm(`Esta seguro de la fecha de Expiracion`)) {
        let fecha = document.getElementById('fechaEx').value;
        let idAModificar = 0;
        if (idDoc < 5) {
            idAModificar = document.getElementById('idAuto').value;
        } else {
            idAModificar = sessionStorage.getItem('idConductor');
        }
        let sw = `${url}img/updataFechaEx/${idAModificar}`;
        let parametros = {
            "idDoc": idDoc,
            "fechaEX": fecha
        };
        fetch(sw, {
                method: 'PUT',
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res['response']) {
                    document.getElementById(`fechaex${idDoc}`).innerHTML = moment(fecha).format('DD-MM-YYYY');
                    document.getElementById('modalClose').click();
                }
            })

    }
}

let alAbrirFchaEx = (idDoc = 0, des = '') => {
    document.getElementById('idDocFechaEx').value = idDoc;
    document.getElementById('desDocFechaEx').value = des;
    document.getElementById('hidDocFechaEx').innerHTML = `Agregue la fecha de expiracion ${des}`;
    let today = moment().format('YYYY-MM-DD');
    document.getElementById("fechaEx").value = today;
    // document.getElementById("fechaEx").min = today;
}

const mapaDetalleViaje = () => {
    let contenMap = document.getElementById('card-map');
    let origin = "20.173481,-98.051702";
    let destination = "20.161060,-98.042955";
    let sw = `${url}reportes/verMapaDetalleViaje/${origin}/${destination}`;
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res['response']) {
                let map_url = res['result']
                    // contenMap.innerHTML = `<img class="img-map" src="${map_url}"/>`;
                console.log("Se agrego mapa")
            } else {
                contenMap.innerHTML = `<h1>Error al cargar mapa</h1>`;
            }
        });
}

const cambioForm = (formulario = 1) => {
    event.preventDefault();
    console.log(formulario);
    if (formulario == 1) {
        document.getElementById("formDatosBancarios").style.display = "block"
        document.getElementById("formDatosFacturacion").style.display = "none"
    } else {
        document.getElementById("formDatosBancarios").style.display = "none"
        document.getElementById("formDatosFacturacion").style.display = "block"
    }
}

let cer = ``;
const cer64 = () => {
    console.log("cer");
    let sello = document.getElementById(`sello`);
    document.getElementById('archivoCER').innerHTML = sello.value;
    if (sello.files.length > 0) {

        let reader = new FileReader();
        reader.onload = function(e) {
            cer = reader.result.replace(/^data:.+;base64,/, '');
            console.log(cer);
            //    let dataByn = e.target.result;
            //    cer = window.btoa(dataByn);
            //    console.log(cer);
        };

        reader.readAsDataURL(sello.files[0]);
    } else {
        alert("seleccione el archivo tipo CER")
    }
}

let key = ``;
const key64 = () => {
    console.log("key");
    let llave = document.getElementById(`llave`);
    document.getElementById('archivoKEY').innerHTML = llave.value;
    if (llave.files.length > 0) {

        let reader = new FileReader();
        reader.onload = function(e) {
            key = reader.result.replace(/^data:.+;base64,/, '');
            console.log(key);
            //    let dataByn = e.target.result;
            //    key = window.btoa(dataByn);
            //    console.log(key);
        };

        reader.readAsDataURL(llave.files[0]);
    } else {
        alert("seleccione el archivo tipo KEY")
    }
}

const cargarCSD = () => {
    document.getElementById('botonGPerfilFacturacion').style.disabled = true;
    let error = false;
    if (cer == '' || cer == null) {
        alert("seleccione el archivo tipo CER")
        error = true;
    }
    if (key == '' || key == null) {
        alert("seleccione el archivo tipo KEY");
        error = true;
    }
    let idConductor = sessionStorage.getItem('idConductor');
    let razon = document.getElementById("Razon").value;
    if (razon.length == 0) {
        error = true;
    }
    let regimenFiscal = document.getElementById('RegimenFiscal').value;
    let descripcionRF = '';
    if (regimenFiscal == 0) {
        alert("seleccione un Regimen Fiscal");
        error = true;
    } else {
        descripcionRF = CatalogoRegimenFiscal.find(res => res.Value == regimenFiscal);
        console.log(descripcionRF);
    }
    let rfc = document.getElementById("Rfc").value;
    if (rfc.length == 0) {
        error = true;
    }
    let password = document.getElementById("PasswordSellos").value;
    if (password.length == 0) {
        error = true;
    }
    if (!error) {
        let parametros = {
            "Rfc": rfc,
            "regimenFiscal": regimenFiscal,
            "Certificate": cer,
            "PrivateKey": key,
            "PrivateKeyPassword": password,
            "idPersona": idConductor,
            "razon": razon,
            "descripcion": descripcionRF.Name
        };
        console.log(parametros);
        let hayPerfil = document.getElementById('hayPerfilFacturacion').value;
        if (hayPerfil == "false") {
            console.log("add");
            let sw = `${url}factura/agregarCSD`;
            fetch(sw, {
                    method: 'POST',
                    body: JSON.stringify(parametros),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(res => {
                    console.log(res);
                    if (res.response) {
                        alert("CSD agregados");
                    } else {
                        alert(`Error: ${res.errors}`);
                    }
                });
        } else {
            console.log("update");
            let sw = `${url}factura/actualizarCSD`;
            fetch(sw, {
                    method: 'PUT',
                    body: JSON.stringify(parametros),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(res => {
                    console.log(res);
                    if (res.response) {
                        alert("CSD agregados");
                    } else {
                        alert(`Error: ${res.errors}`);
                    }
                });
        }
    }
    document.getElementById('botonGPerfilFacturacion').style.disabled = false;
}

const verPasswordSellos = () => {
    let tipo = document.getElementById("PasswordSellos");
    if (tipo.type === "password") {
        tipo.type = "text";
    } else {
        tipo.type = "password";
    }
}

const buscarPerfilFacturacion = (idPersona) => {
    // let idPersona = sessionStorage.getItem('IdPersona');
    let sw = `${url}factura/perfilEx/${idPersona}`;
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (res.response) {
                document.getElementById('Razon').value = res.result.razon;
                document.getElementById('Rfc').value = res.result.rfc;
                document.getElementById('Rfc').disabled = true;
                document.getElementById('RegimenFiscal').value = res.result.regimenFiscal;
                document.getElementById('hayPerfilFacturacion').value = res.result.rfc;
                document.getElementById('botonEliminarPF').style.display = "block";
            }
        });
}

const eliminarCSD = () => {
    let conf = confirm('Esta seguro de Eliminar los datos del perfil de Facturacion')
    if (conf) {
        let rfc = document.getElementById("Rfc").value;
        let parametros = {
            "Rfc": rfc
        };
        console.log("delate");
        let sw = `${url}factura/eliminarCSD`;
        fetch(sw, {
                method: 'DELETE',
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res.response) {
                    alert("CSD Eliminados");
                    location.reload();

                } else {
                    alert(`Error: ${res.errors}`);
                }
            });
    }
}

const activarDriver = () => {
    let confirmacion = confirm("Estas seguro de activar este conductor?");
    if (confirmacion) {
        let idPersona = sessionStorage.getItem('idConductor');
        let sw = `${url}registro/actualizar/${idPersona}`
        console.log(sw);
        parametros = {
            "Status": 5
        }
        fetch(sw, {
                method: 'PUT',
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res.response) {
                    alert("Conductor Activado");
                    self.location.reload();
                } else {
                    alert("No se realizo el cambio intentelo de nuevo")
                }
            })
    }
}

const fechaActual = () => {
    let fecha = new Date(); //Fecha actual
    let mes = fecha.getMonth() + 1; //obteniendo mes
    let dia = fecha.getDate(); //obteniendo dia
    let year = fecha.getFullYear(); //obteniendo año
    if (dia < 10)
        dia = '0' + dia; //agrega cero si el menor de 10
    if (mes < 10)
        mes = '0' + mes //agrega cero si el menor de 10

    return {
        year,
        mes,
        dia
    };
}


const fechasIniciales = () => {
        let fecha = fechaActual();
        document.getElementById('fechaInicio').value = `${fecha.year}-${fecha.mes}-${fecha.dia}`;
        document.getElementById('fechaFin').value = `${fecha.year}-${fecha.mes}-${fecha.dia}`;
    }
    // reportes diarios 
let limit = 10;
let offset = 0;
let total = 0;

const reporteDiario = (status = 7) => {

    document.getElementById('verMas').disabled = false;
    let fechaIni = `${document.getElementById("fechaInicio").value} 00:00:00`;
    let fechaFin = `${document.getElementById("fechaFin").value} 23:59:59`;
    let cuerpo = document.getElementById('cuerpoReporteViajesD');

    let zonaId = sessionStorage.getItem('esSuperAdmin');
    console.log(zonaId);
    if (zonaId == "true") {
        zonaId = 0
    }

    let sw = `${url}reportes/reporteDiario/${zonaId}/${status}/${fechaIni}/${fechaFin}/${limit}/${offset}`;
    console.log(sw);
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res);
            // FechaFinViaje: "2020-08-10 11:21:13"
            // FechaIncioViaje: "2020-08-10 11:19:26"
            // GananciaConductor: 33.41
            // GananciaLubo: 6
            // ISRRetenidoConductor: -0.59
            // NombreConductor: "Jorge"
            // Pago: "Efectivo"
            // PagoViaje: 40
            // TipoServicio: "Estándar"
            // idViaje: "1817"
            if (res.response) {
                // document.getElementById("TituloReportes").innerHTML = `Viajes del ${fechaIni} al ${fechaFin}`;
                let data = res.result.Reporte;
                // total = parseInt(res.result.totales.Total);
                let Totales = res.result.Totales[0];
                data.forEach(element => {
                    let arrayDateInicio = element.FechaIncioViaje.split(' ');
                    let arrayFecha = arrayDateInicio[0].split('-');
                    let arrayDateFinal = element.FechaFinViaje.split(' ');
                    let arrayFechaFinal = arrayDateFinal[0].split('-');
                    
                    let fila = document.createElement(`tr`);
                    fila.innerHTML = `  <td>${element.idViaje}</td>
                                    <td>${element.NombreConductor}</td>
                                    <td class="text-center">${element.TipoServicio}</td>
                                    <td class="text-center">${arrayDateInicio[1]} ${arrayFecha[2]}-${arrayFecha[1]}-${arrayFecha[0]}</td>
                                    <td class="text-center">${arrayDateFinal[1]} ${arrayFechaFinal[2]}-${arrayFechaFinal[1]}-${arrayFechaFinal[0]}</td>
                                    <td class="text-center">${element.Pago}</td>
                                    <td class="text-center">$${element.GananciaConductor}</td>
                                    <td class="text-center">$${element.GananciaLubo} + $${element.ISRRetenidoConductor}</td>
                                    <td class="text-center">$${element.PagoViaje}</td>
                                    <td class="text-center"></td>`;
                    cuerpo.appendChild(fila);
                });
                // totales
                // document.getElementById('sumaMonto').innerHTML = `$${Totales.total_ganancia_conductor}`;
                // document.getElementById('sumaLubo').innerHTML = `$${Totales.total_ganancia_lubo}`;
                // document.getElementById('sumaTotal').innerHTML = `$${Totales.total_pago_viaje}`;
                // document.getElementById('sumaPropina').innerHTML = `$${totales.Propina}`;
                // 
            } else {
                document.getElementById('verMas').disabled = true;
            };

        })
}

const cambialimit = () => {
    limit = parseInt(document.getElementById("limiteViajes").value);
    offset = 0;
}

const reporteDiarioInicial = () => {
    reporteDiario();
}

const buscarReporteDiario = (status = 7) => {
    cambialimit();
    document.getElementById('cuerpoReporteViajesD').innerHTML = '';
    reporteDiario();
    if (limit > total || limit == 0) {
        document.getElementById('verMas').disabled = true;
    }
}

const verMas = (status = 7) => {
    offset = (offset) + (limit);
    console.log("Total", total);
    console.log("offset", offset);
    reporteDiario()
    if ((offset + limit) > total) {
        document.getElementById('verMas').disabled = true;
    }
}


const verCatalogoRegimenFiscal = () => {
    console.log(CatalogoRegimenFiscal);
    let catalogoRF = document.getElementById("RegimenFiscal");
    let catalogoinnerHTML = `<option value="0">Seleccione un regimen --> </option>`;
    CatalogoRegimenFiscal.forEach(element => {
        catalogoinnerHTML += `<option value="${element.Value}"> ${element.Name} </option>`
    });
    catalogoRF.innerHTML = catalogoinnerHTML;
}

const semanasCortes = () => {
    let sw = `${url}balance/listaSemanas`
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {

            if (res.response) {
                let selectSemana = document.getElementById('semanasCorte');
                let semanas = res.result;
                let innerSemanas = ``;
                semanas.forEach(element => {
                    // let option = document.createElement('option');
                    let fachaIni = element.InicioSemana.split(' ');
                    let fechaFin = element.FinSemana.split(' ');
                    innerSemanas = `<option value ="${element.idSemana}">Semana del ${fachaIni[0]}  al ${fechaFin[0]}</option>${innerSemanas}`;
                })
                selectSemana.innerHTML = innerSemanas;
            }
        })
}

const fechaCorte = () => {
    let fecha = fechaActual();
    document.getElementById('fechaHoy').innerHTML = `Fecha de corte ${fecha.dia}/${fecha.mes}/${fecha.year}`
}

const listarInfoCortesConductores = () => {
    // document.getElementById('verMasCortes').disabled = false;
    let semana = document.getElementById('semanasCorte').value;
    let sw = `${url}balance/InformacionBalance/${semana}/${limit}/${offset}`;
    console.log(sw);
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response) {
                let data = res.result.BalanceConductores;
                console.log(data);
                let cuerpo = document.getElementById('cuerpo');
                console.log(cuerpo);
                data.forEach(element => {
                    if (element.TotalViajes > 0) {
                        let transferir = `<a href="#" onclick = "datosPago(${element.idConductor},'${element.Nombre}',${element.Balance},${semana})" class="btn btn-link btn-info btn-just-icon like" data-toggle="modal" data-target="#modal-pagar-cuenta"><i class="material-icons">credit_card</i></a>`;
                        let classPagado = ``;
                        if (element.StatusBalance == "Pagado") {
                            transferir = "Pagado";
                            classPagado = `status-pagado`;
                        };
                        let styleBalance = ``;
                        if (element.Balance < 0) {
                            styleBalance = `status-nos-deben`;
                        }
                        let fila = document.createElement(`tr`);
                        fila.innerHTML = `  <tr>
                                        <td>${element.Nombre}</td>
                                        <td>${element.TipoServicio}</td>
                                        <td class="text-center">${element.TotalViajes}</td>
                                        <td>$${element.Efectivo} + $${element.IVAEfectivo}</td>
                                        <td>$${element.Tarjeta} + $${element.IVATarjeta}</td>
                                        <td class="text-center">$${element.Conductor}</td>
                                        <td class="text-center">$${element.ProcesadorPagoConductor}(Conductor) + ${element.ProcesadorPagoLubo}(Lubo)</td>
                                        <td class="text-center">$${element.Lubo}</td>
                                        <td class="text-right ${styleBalance}">$${element.Balance}</td>
                                        <td class="text-right ${classPagado}" id="balance${element.idConductor}" >
                                            ${transferir}
                                        </td>
                                        </tr>`;
                        cuerpo.appendChild(fila);
                    }
                });
                // totales
                let totales = res.result.Totales;
                document.getElementById('totalEfectivoIva').innerHTML = `$${totales.Efectivo} + $${totales.IVAEfectivo}`;
                document.getElementById('totalTarjetaIva').innerHTML = `$${totales.Tarjeta} + $${totales.IVATarjeta}`;
                document.getElementById('totalConductor').innerHTML = `$${totales.Conductor}`;
                document.getElementById('totalProcesadorPago').innerHTML = `$${totales.ProcesadorPagoConductor}(Conductor) + $${totales.ProcesadorPagoLubo}(Lubo)`;
                document.getElementById('totalLubo').innerHTML = `$${totales.Lubo}`;
            } else {
                // document.getElementById('verMasCortes').disabled = true;
            };

        })
}


// pago

const listarMetosPago = () => {
    let sw = `${url}balance/listaMetosPago`;
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            let selectTipoPago = document.getElementById('tipoPago');
            let innerSelect = `<option value="0">Selecciones un metodo</option>`;
            //    console.log(res);
            if (res.response) {
                let data = res.result;
                data.forEach(element => {
                    innerSelect += `<option value="${element.idMetodoPago}">${element.Descripcion}</option>`;
                });
                selectTipoPago.innerHTML = innerSelect;
            }
        })
}

let paramTemp = null;

const datosPago = (idConductor, nombre, monto, idSemana) => {
    let tipoTransaccion = 0;
    if (monto < 0) {
        tipoTransaccion = 5;
        monto = monto * (-1);
    } else {
        tipoTransaccion = 6;
    }
    paramTemp = {
            "idConductor": idConductor,
            "idSemana": idSemana,
            "monto": monto,
            "tipoTransaccion": tipoTransaccion,
            "Nombre": nombre
        }
        // alert("Datos de semana");
    document.getElementById('NombreModal').value = nombre;

    document.getElementById('montoModal').value = `$${monto}`;
    console.log(idConductor);
    datosBancariosPago(idConductor);
}


const datosBancariosPago = (idConductor) => {
    let sw = `${url}/infobancaria/listar/${idConductor}`;
    console.log(sw);
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            // console.log(res['result']);
            if (res.response) {
                let inf = res['result'];
                console.log(inf);
                document.getElementById('infoBancariaConductor').innerHTML = `Nombre del Beneficiario: ${inf.Nombre_Beneficiario} <br> Banco: ${inf.Nombre_Banco} <br> Clave: ${inf.Clave_Inter_Bancaria} `;
            } else {
                document.getElementById('infoBancariaConductor').innerHTML = `No hay informacion bancaria del conductor`;
            }
        });
}

const pagarSemana = () => {
    let idUsuario = sessionStorage.getItem("idAdmin");
    let FechaPago = document.getElementById('fechaPagoModal').value;
    let Factura = document.getElementById('numeroFacturaModal').value;
    let tipoPago = document.getElementById('tipoPago').value;
    let error = false;

    if (FechaPago == "") {
        alert("Seleccione una fecha de pago");
        error = true;
    }
    if (tipoPago == 0) {
        alert("Selcione un metodo de pago");
        error == true;
    }
    // console.log(parametrosPagarSemana);
    if (!error) {
        FechaPago += ` 00:00:00`;
        let parametrosPagarSemana = {
            "idConductor": paramTemp.idConductor,
            "idSemana": paramTemp.idSemana,
            "tipoTransaccion": paramTemp.tipoTransaccion,
            "monto": paramTemp.monto,
            "metodoPago": tipoPago,
            "idUsuario": idUsuario,
            "FechaPago": FechaPago,
            "Factura": Factura
        };
        let sw = `${url}balance/pagarSemana`;
        console.log(sw);
        console.log(parametrosPagarSemana);
        let confirmacion = confirm(`Confirma el pago de $${paramTemp.monto} a ${paramTemp.Nombre}`);
        if (confirmacion) {
            // let sw = `${url}balance/listaMetosPago`;
            fetch(sw, {
                    method: 'POST',
                    body: JSON.stringify(parametrosPagarSemana),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(res => {
                    if (res.response) {
                        alert("Balance pagado");
                        let transferir = document.getElementById(`balance${paramTemp.idConductor}`);
                        transferir.innerHTML = "Pagado";
                        transferir.classList.add('status-pagado');
                        document.getElementById('closeModal').click();
                    } else {
                        alert(res.errors)
                    }
                });
            // $('#modal-pagar-cuenta').modal('hide');
        }
    }


}

const cambialimitCorte = () => {
    limit = parseInt(document.getElementById("limiteConductores").value);
    offset = 0;
}

const buscarCorteSemanal = () => {
    // cambialimitCorte();
    cuerpo.innerHTML = '';
    limit = 0;
    offset = 0;
    listarInfoCortesConductores();
    // if (limit > total || limit == 0) {
    //     document.getElementById('verMasCortes').disabled = true;
    // }
}

const verMasCortes = () => {
    offset = (offset) + (limit);
    console.log("Total", total);
    console.log("offset", offset);
    listarInfoCortesConductores()
    if ((offset + limit) > total) {
        document.getElementById('verMasCortes').disabled = true;
    }
}

let listarImg = () => {
    let sw = `${url}/listadodata/listarImg/10/0`
    console.log(sw);
    // console.log();
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            // console.log(res)
            let result = res.result
            console.log(result)
                // console.log(result.length);
                // document.getElementById(`numC`).innerHTML = `Numero de conductores: ${result.length}`
            let cuerpo = document.getElementById('tbody')
            cuerpo.innerHTML = "";
            result.forEach(element => {
                let fila = document.createElement(`tr`);
                fila.innerHTML = `  <tr>
                                        <td>${element.id_tbPersona}</td>
                                        <td>${element.Nombre}</td>
                                        <td> ${element.Apellidos}</td>
                                        <td>${element.Telefono}</td>
                                        <td>
                                        <input class="btn btn-primary btn-rosa text-semibold" id="btnFotos" type="button" value="Validar foto" data-toggle="modal" data-target="#modal-new-foto" onclick = "mostarInfoUsuario('${element.id_tbPersona}', '${element.Imagen}')" >
                                        </td>
                                        </tr>`;
                cuerpo.appendChild(fila);
            });


        })
}

const mostarInfoUsuario = (urlIdUser, urlImg) => {
    document.getElementById('imgActual').src = `${urlImg}`;
    console.log(urlIdUser)
    let sw = `${url}/img/verImgenPerfil/${urlIdUser}`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            let result = res.result
            console.log(result)
            document.getElementById('imgPendiente').src = `${result.Imagen}`;
            let cuerpo = document.getElementById('classBtnF')
            cuerpo.innerHTML = "";
            let fila = document.createElement(`div`);
            fila.innerHTML = `<button onclick="imagenAceptada('${result.idPersona}')" id="BotonGuardarTarjeta" type="button" class="btn btn-primary btn-rosa text-semibold" style="float: left;">Aceptar</button>
            <button onclick="imagenRechazada('${result.idPersona}')" type="button" class="btn btn-secondary text-semibold" data-dismiss="modal" style="text-transform: none;">Rechazar</button>`;
            cuerpo.appendChild(fila);
        })

}
const imagenAceptada = (urlIdUser) => {
    let confirmacion = confirm("¿La foto fue revisada y es correcta?");
    console.log(urlIdUser)
    if (confirmacion) {
        let sw = `${url}img/validarImg/${urlIdUser}`
        console.log(sw);
        fetch(sw, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res.response) {
                    alert("La foto del ususario ha sido aprobada!!!");
                    location.reload();
                } else {
                    alert("No se realizo el cambio intentelo de nuevo")
                }
            })
    }
}
const imagenRechazada = (urlIdUser) => {
    let parametros = {
        "idPersona": urlIdUser
    };
    let sw = `${url}img/rechazarImagenPerfil`;
    fetch(sw, {
            method: 'DELETE',
            body: JSON.stringify(parametros), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response != false) {
                location.reload();
            } else {
                alert(`${res.errors}`)
            }
            location.reload();
        });

};

let listarDatosPendientes = () => {
    let sw = `${url}/listadodata/listarDatosPendientes/10/0`
    console.log(sw);
    // console.log();
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            // console.log(res)
            let result = res.result
            console.log(result)

            let cuerpo = document.getElementById('tbody')
            cuerpo.innerHTML = "";
            result.forEach(element => {
                let fila = document.createElement(`tr`);
                fila.innerHTML = `  <tr>
                                        <td>${element.id_tbPersona}</td>
                                        <td>${element.Nombre}</td>
                                        <td> ${element.Apellidos}</td>
                                        <td>${element.Telefono}</td>
                                        <td>
                                        <input class="btn btn-primary btn-rosa text-semibold" id="btnDatos" type="button" value="Validar Datos" data-toggle="modal" data-target="#modal-new-datos"onclick = "mostarDatosUsuario('${element.id_tbPersona}', '${element.Nombre}', '${element.Apellidos}')">
                                        </td>
                                        </tr>`;
                cuerpo.appendChild(fila);


            });



        })
}
const mostarDatosUsuario = (urlIdUser, urlNombre, urlApellidos) => {
    document.getElementById('nombre_anterior').value = `${urlNombre} ${urlApellidos}`;
    let sw = `${url}/actualizacion/verinfo/${urlIdUser}`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            let result = res.result
            console.log(result)
            document.getElementById('nombre_nuevo').value = `${result.Nombre} ${result.Apellidos}`;
            let cuerpo = document.getElementById('classBtn')
            cuerpo.innerHTML = "";
            let fila = document.createElement(`div`);
            fila.innerHTML = `<button onclick="datosAceptados('${result.idUsuario}')" id="BotonGuardarTarjeta" type="button" class="btn btn-primary btn-rosa text-semibold" style="float: left;">Aceptar</button>
                <button onclick="datosRechazados('${result.idUsuario}')" type="button" class="btn btn-secondary text-semibold" data-dismiss="modal" style="text-transform: none;">Rechazar</button>`;
            cuerpo.appendChild(fila);



        })

}
const datosAceptados = (urlIdUser) => {
    let confirmacion = confirm("¿Los datos fueron revisados y son correctos?");
    if (confirmacion) {
        let sw = `${url}actualizacion/datosCorrectos/${urlIdUser}/1`
        console.log(sw);
        fetch(sw, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res.response) {
                    alert("Los datos del ususario han sido aprobados!!!");
                    location.reload();
                } else {
                    alert("No se realizo el cambio intentelo de nuevo")
                }
            })
    }
}

const datosRechazados = (urlIdUser) => {
        let confirmacion = confirm("¿Los datos Fueron revisados y no son correctos?");
        if (confirmacion) {
            let sw = `${url}actualizacion/datosCorrectos/${urlIdUser}/2`
            console.log(sw);
            fetch(sw, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(res => {
                    console.log(res);
                    if (res.response) {
                        alert("Los datos del ususario no son apropiados!!");
                        location.reload();
                    } else {
                        alert("No se realizo el cambio intentelo de nuevo")
                    }
                })
        }
    }
    // let limit = 10;
    // let offset = 0;
    // let total = 0;
    // var fActual = new Date();
    // document.write(fActual.getFullYear() + "-" + (fActual.getMonth() + 1) + "-" + fActual.getDate());

const mostrarDias = (cambio = 1) => {
    //alert('mostrar mas');
    dashBoardAdmin(`${cambio}`);

    console.log(dashBoardAdmin());
};


let dashBoardAdmin = (cambio = 1) => {
    // document.getElementById('nombre_anterior').value = `${urlNombre} ${urlApellidos}`;
    let f = new Date();
    let dataActul = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
    let zonaId = sessionStorage.getItem('esSuperAdmin');
    console.log(zonaId);
    if (zonaId == "true") {
        zonaId = 0
    }
    let sw = `${url}balance/dashBoardAdmin/${dataActul}/${cambio}/${zonaId}`
    console.log(sw)
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            let result = res.result
            console.log(result)
            document.getElementById('conductorSemana').innerHTML = `$${result.GananciasConductores}`;
            document.getElementById('empresaSemana').innerHTML = `$${result.GananciasLubo}`;
            document.getElementById('viajeSemana').innerHTML = `${result.TotalViajesRealizados}`;
            document.getElementById('usuarioSemana').innerHTML = `${result.UsuariosRegistrados}`;
            return res;

        })

}
const listarUsuariosRegistrados = ($is) => {
    let sw = ''
    if ($is == 'true') {
        sw = `${url}registro/usuariosRegistrados/0/2/0/${offset}`
    } else {
        sw = `${url}registro/usuariosRegistrados/0/2/0/${offset}`
    }
    console.log(sw);
    // console.log($is);
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res)
            let result = res.result.DetalleUsuarios
            let totalUser = res.result.TotalUsuarios
            console.log(result);
            document.getElementById(`numC`).innerHTML = `Numero de usuarios: ${totalUser}`
            let cuerpo = document.getElementById('tbody')

            result.forEach(element => {
                let fila = document.createElement(`tr`)
                let celda1 = document.createElement(`td`)
                let celda2 = document.createElement(`td`)
                let celda3 = document.createElement(`td`)
                let celda4 = document.createElement(`td`)
                let celda5 = document.createElement(`td`)
                let celda6 = document.createElement(`td`)
                let celda7 = document.createElement(`td`)
                let celda8 = document.createElement(`td`)

                let Status = 'Activo'

                let id = document.createTextNode(`${element['idUsuario']}`)
                let nombre = document.createTextNode(`${element['Nombre']}`)
                let numeroViajes = document.createTextNode(`${element['TotalViajes']}`)
                    // if (element[`Status`] === '3') Status = `Pendiente`
                    // let tipo = document.createTextNode(`${Status}`)
                let fUV = element['UltimoViaje'].split('')
                    // console.log(fUV)
                let telefono = document.createTextNode(`${element['Telefono']}`)
                let correo = document.createTextNode(`${element['Email']}`)
                let fechaRegistro = document.createTextNode(`${element['Fecha_Registro']}`)

                if (fUV.length == 11) {
                    ultimoV = document.createTextNode(`Sin detalle`) //"00:00:00 0000/00/00"
                } else {
                    ultimoV = document.createTextNode(`${fUV[11]}${fUV[12]}${fUV[13]}${fUV[14]}${fUV[15]}${fUV[16]}${fUV[17]}${fUV[18]}${fUV[10]}${fUV[8]}${fUV[9]}/${fUV[5]}${fUV[6]}/${fUV[0]}${fUV[1]}${fUV[2]}${fUV[3]}`)
                }
                // console.log(ultimoV)
                let boton = document.createElement("button")
                boton.type = `button`
                boton.innerHTML = 'Detalle'
                boton.className = 'btn btn-primary'

                // boton.onclick = detalle(`${element['id_tbPersona']}`)
                celda1.appendChild(id)
                celda2.appendChild(nombre)
                celda3.appendChild(numeroViajes)
                celda4.appendChild(telefono)
                celda5.appendChild(correo)
                celda6.appendChild(fechaRegistro)
                celda7.appendChild(ultimoV)
                celda8.appendChild(boton)

                fila.appendChild(celda1)
                fila.appendChild(celda2)
                fila.appendChild(celda3)
                fila.appendChild(celda4)
                fila.appendChild(celda5)
                fila.appendChild(celda6)
                fila.appendChild(celda7)
                fila.appendChild(celda8)
                cuerpo.appendChild(fila)

                boton.onclick = function() {
                    detalleUser(`${element['idUsuario']}`)
                }
                boton.onmousedown = function(e) {
                    // detalle(`${element['idUsuario']}`)
                    // console.log(e.button);
                    if (e.button == 2) {
                        sessionStorage.setItem('idConductor', element['idUsuario'])
                        window.open("perfil-user.html");
                    }
                }
            });

        })
}
let detalleUser = (id) => {
    sessionStorage.setItem('idConductor', id)
    console.log(id)
    self.location = `perfil-user.html`
}
let obtenerUser = (id) => {
    let sw = `${url}persona/obtener/${id}`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res['response']) {
                console.log(res.result);
                let img = document.getElementById('perfil')
                img.src = res['result']['Imagen']
                console.log("img perfil:", res.result["Imagen"]);
                if (res.result["Imagen"] != null) {
                    document.getElementById('perfil').style.display = 'inline';
                }
                let verificar = `${url}img/verImgenPerfil/${id}`
                fetch(verificar, {
                        method: 'GET',
                        "headers": {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(result => result.json())
                    .then(result => {
                        if (result['response']) {
                            // console.log(result['result']);
                            img = document.getElementById('validar')
                                // console.log(result['result']['Imagen']);
                            img.src = result['result']['Imagen']
                            document.getElementById('validar').style.display = 'inline'
                            document.getElementById('validarBoton').style.display = 'inline'
                        }
                    })
                    // }
                document.getElementById('Nombre').value = res['result']['Nombre']
                document.getElementById('Apellidos').value = res['result']['Apellidos']
                    // document.getElementById('Sexo').value = res['result']['Sexo']
                document.getElementById('Fecha_Registro').value = res['result']['Fecha_Registro']
                document.getElementById('Telefono').value = res['result']['Telefono']
                document.getElementById('Email').value = res['result']['Email']
                    // let status = 'Activo'
                    // if (res['result']['Status'] == 3) {
                    //     status = 'Pendiente'
                    // } else if (res['result']['Status'] == 0) {
                    //     status = 'Inactivo'
                    // }
                    // document.getElementById('Status').value = status
                    // document.getElementById('idZona').value = res['result']['idZona'];
            } else {
                if (res['errors'].length == 0) {
                    alert(res['message'])
                    self.location = 'Conductores.html'
                } else {
                    alert(res['errors'])
                    self.location = 'Conductores.html'
                }
            }
        })
}
let updatePasword = () => {
    let confirmar = confirm(`Esta seguro de actulizar el Password`);
    let idConductor = sessionStorage.getItem('idConductor');
    if (confirmar) {
        let sw = `${url}auth/actualizaPass/${idConductor}`;
        let parametros = {
            "Password": document.querySelector('#Password').value
        };
        console.log(parametros);

        fetch(sw, {
                method: 'PUT',
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res.response) {
                    alert("Se actulizo la informacion");
                } else {
                    alert(res.error);
                }
            });

    }
}

const listarUsuariosforZonas = () => {
    // location.reload();
    let idZona = document.getElementById('idZona').value;
    console.log(idZona);
    let cuerpo = document.getElementById('tbody');
    cuerpo.innerHTML = '';
    listarConductores(idZona);

}
let listarImgDriver = () => {
    // let sw = `${url}/listadodata/listarImgDriver/1/10/0`
    let is = sessionStorage.getItem('esSuperAdmin')
    let sw = ''
    if (is == 'true') {
        sw = `${url}listadodata/listarImgDriver/0/10/0`
    } else {
        sw = `${url}listadodata/listarImgDriver/${is}/10/0`
    }

    console.log(sw);
    console.log(is);
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            // console.log(res)
            let result = res.result
            console.log(result)
                // console.log(result.length);
                // document.getElementById(`numC`).innerHTML = `Numero de conductores: ${result.length}`
            let cuerpo = document.getElementById('tbody')
            cuerpo.innerHTML = "";
            result.forEach(element => {
                let fila = document.createElement(`tr`);
                fila.innerHTML = `  <tr>
                                        <td>${element.id_tbPersona}</td>
                                        <td>${element.Nombre}</td>
                                        <td> ${element.Apellidos}</td>
                                        <td>${element.Telefono}</td>
                                        <td>
                                        <input class="btn btn-primary btn-rosa text-semibold" id="btnFotos" type="button" value="Validar foto" data-toggle="modal" data-target="#modal-new-foto" onclick = "mostarInfoUsuario('${element.id_tbPersona}', '${element.Imagen}')" >
                                        </td>
                                        </tr>`;
                cuerpo.appendChild(fila);
            });


        })
}

let listarDatosPendientesDriver = ($is) => {
    // let sw = `${url}/listadodata/listarDatosPendientesDriver/1/10/0`
    let is = sessionStorage.getItem('esSuperAdmin')
    let sw = ''
    if (is == 'true') {
        sw = `${url}listadodata/listarDatosPendientesDriver/0/10/0`
    } else {
        sw = `${url}listadodata/listarDatosPendientesDriver/${is}/10/0`
    }
    console.log(sw);
    console.log(is);
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            // console.log(res)
            let result = res.result
            console.log(result)

            let cuerpo = document.getElementById('tbody')
            cuerpo.innerHTML = "";
            result.forEach(element => {
                let fila = document.createElement(`tr`);
                fila.innerHTML = `  <tr>
                                        <td>${element.id_tbPersona}</td>
                                        <td>${element.Nombre}</td>
                                        <td> ${element.Apellidos}</td>
                                        <td>${element.Telefono}</td>
                                        <td>
                                        <input class="btn btn-primary btn-rosa text-semibold" id="btnDatos" type="button" value="Validar Datos" data-toggle="modal" data-target="#modal-new-datos"onclick = "mostarDatosUsuario('${element.id_tbPersona}', '${element.Nombre}', '${element.Apellidos}')">
                                        </td>
                                        </tr>`;
                cuerpo.appendChild(fila);


            });



        })
}
const cambiolimite = () => {
    limit = parseInt(document.getElementById("limiteUser").value);
    offset = 0;
    console.log(limit)
}

const fechasInicioYFin = () => {
    let fecha = fechaActual();
    document.getElementById('fechaInicio').value = `${fecha.year}-${fecha.mes}-${fecha.dia}`;
    document.getElementById('fechaFin').value = `${fecha.year}-${fecha.mes}-${fecha.dia}`;
}


const reporteSolicitud = () => {

    let fechaIni = `${document.getElementById("fechaInicio").value}`;
    let fechaFin = `${document.getElementById("fechaFin").value}`;
    let cuerpo = document.querySelector("#cuerpoReportesSolicitudes");

    let sw = `${url}reportes/reporteSolicitudes/${fechaIni}/${fechaFin}`;
    console.log(sw);
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res);

            if (res.response) {
                // document.getElementById("TituloReportes").innerHTML = `Viajes del ${fechaIni} al ${fechaFin}`;
                let data = res.result.Viajes;
                // total = parseInt(res.result.totales.Total);
                let Total = res.result.Total;
                // document.getElementById(`numC`).innerHTML = `Total de solicitudes: ${Totales}`
                data.forEach(element => {
                    let fila = document.createElement(`tr`);
                    fila.innerHTML = `  <td>${element.idViaje}</td>
                                    <td>${element.NombreUsuario}</td>
                                    <td class="text-center">${element.NombreConductor}</td>
                                    <td class="text-center">${element.FechaSolicitud}</td>
                                    <td class="text-center">${element.StatusViaje}</td>
                                    <td class="text-center">${element.Cancelado}</td>
                                    <td class="text-center">${element.Origen}</td>
                                    <td class="text-center">${element.Destino}</td>
                                    <td class="text-center">$${element.Costo}</td>
                                    <td class="text-center">${element.Distancia} Km</td>
                                    <td class="text-center"></td>`;
                    cuerpo.appendChild(fila);
                });
                let total = document.querySelector("#totalViajes");
                total.innerHTML = `Total de Solicitudes: ${Total}`;
            } else {
                document.getElementById('verMas').disabled = true;
            };

        })
}

const buscarReporteSolicitud = () => {

    document.querySelector("#cuerpoReportesSolicitudes").innerHTML = '';
    reporteSolicitud();

}

const fechasInicioYFinReporte = () => {
    let fecha = fechaActual();
    document.getElementById('fechaInicio').value = `${fecha.year}-${fecha.mes}-${fecha.dia}`;
    document.getElementById('fechaFin').value = `${fecha.year}-${fecha.mes}-${fecha.dia}`;
}


const reporteViajesReportados = () => {

    let fechaIni = `${document.getElementById("fechaInicio").value}`;
    let fechaFin = `${document.getElementById("fechaFin").value}`;
    let cuerpo = document.querySelector("#cuerpoReportesViajesR");

    let sw = `${url}reportes/reporteProblemas/${fechaIni}/${fechaFin}`;
    console.log(sw);
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res);

            if (res.response) {
                // document.getElementById("TituloReportes").innerHTML = `Viajes del ${fechaIni} al ${fechaFin}`;
                let data = res.result.Viajes;
                // total = parseInt(res.result.totales.Total);
                let Totales = res.result.Total;
                data.forEach(element => {
                    let fila = document.createElement(`tr`);
                    if (element.DetalleProblema == "") {
                        detalleP = "Sin detalle";
                    } else {
                        detalleP = element.DetalleProblema;
                    }
                    fila.innerHTML = `  <td>${element.idViaje}</td>
                                    <td>${element.NombreUsuario}</td>
                                    <td class="text-center">${element.NombreConductor}</td>
                                    <td class="text-center">${element.FechaSolicitud}</td>
                                    <td class="text-center">${element.StatusViaje}</td>
                                    <td class="text-center">${element.Origen}</td>
                                    <td class="text-center">${element.Destino}</td>
                                    <td class="text-center">$${element.Costo}</td>
                                    <td class="text-center">${element.Distancia} Km</td>
                                    <td class="text-center">${detalleP}</td>
                                    <td class="text-center">${element.Problema}</td>
                                    <td class="text-center"></td>`;
                    cuerpo.appendChild(fila);
                });
                let total = document.querySelector("#totalViajes");
                total.innerHTML = `Total de Viajes reportados: ${Totales}`;

            } else {
                document.getElementById('verMas').disabled = true;
            };

        })
}

const buscarReporteViajeR = () => {

    document.querySelector("#cuerpoReportesViajesR").innerHTML = '';
    reporteViajesReportados();

}
const orderDes = (data, campo) => {
    return data.sort(function(a, b) {
        if (a[`${campo}`] > b[`${campo}`]) {
            return 1;
        }
        if (a[`${campo}`] < b[`${campo}`]) {
            return -1;
        }
        return 0
    })
};

const orderAsc = (data, campo) => {
    return data.sort(function(a, b) {
        if (b[`${campo}`] > a[`${campo}`]) {
            return 1;
        }
        if (b[`${campo}`] < a[`${campo}`]) {
            return -1;
        }
        return 0
    })
};
const orderLista = (data, campo, orden) => {
    let orderdata = null;
    // if (order == null || order == "Asc") {
    //     console.log("Acs")
    //     orderdata = orderDes(data, campo);
    //     document.querySelector(`#${campo}`).value = "Des";
    // } else if (order == "Des") {
    //     console.log("Des")
    //     orderdata = orderAsc(data, campo);
    //     document.querySelector(`#${campo}`).value = "Asc";
    // }
    orderdata = orderDes(data, campo);
    console.log(orden);
    return orderdata;

}
const ordenarListaD = (campo) => {
    console.log(listadoDriver)
    let orden = document.querySelector(`#orden${campo}`).value
    console.log(orderLista(listadoDriver, campo, orden));
}