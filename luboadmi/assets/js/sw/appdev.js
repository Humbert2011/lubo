let listarViajes = (zona) => {
    let idZona = null
    if (zona == `true`) {
        idZona = 0

    } else {
        idZona = zona
    }
    let sw = `${url}reportes/listarV/2019-10-01/2019-12-20/${idZona}/7`;
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': ''
            }
        })
        .then(res => res.text())
        .then(res => {
            console.log(res);
            if (res.response) {
                if (res.result.total == "0") {
                    alert("No hay datos que mostrar")
                } else {
                    let cuerpo = document.getElementById('tbody')

                    res.result.data.forEach(element => {
                        let fila = document.createElement(`tr`)
                        let celda1 = document.createElement(`td`)
                        let celda2 = document.createElement(`td`)
                        let celda3 = document.createElement(`td`)
                        let celda4 = document.createElement(`td`)
                        let celda5 = document.createElement(`td`)
                        let celda6 = document.createElement(`td`)
                        let celda7 = document.createElement(`td`)
                        let celda8 = document.createElement(`td`)
                        let celda9 = document.createElement(`td`)

                        let idViaje = document.createTextNode(`${element['idViaje']}`)
                        let nombreUser = document.createTextNode(`${element['NombreUser']}`)
                        let nombreDriver = document.createTextNode(`${element['NombreDriver']}`)
                        let fechaInicio = document.createTextNode(`${element['FechaInicio']}`)
                        let fechaFin = document.createTextNode(`${element['FechaFin']}`)

                        let gananciasConductor = document.createTextNode(`${element['Empresa']}`)
                        let gananciasEmpresa = document.createTextNode(`${element['Conductor']}`)
                        let montoTotal = document.createTextNode(`${element['Total']}`)

                        let boton = document.createElement("button")
                        boton.type = `button`
                        boton.innerHTML = 'Detalle'
                        boton.className = 'btn btn-primary'

                        celda1.appendChild(idViaje)
                        celda2.appendChild(nombreUser)
                        celda3.appendChild(nombreDriver)
                        celda4.appendChild(fechaInicio)
                        celda5.appendChild(fechaFin)
                        celda6.appendChild(gananciasConductor)
                        celda7.appendChild(gananciasEmpresa)
                        celda8.appendChild(montoTotal)
                        celda9.appendChild(boton)

                        fila.appendChild(celda1)
                        fila.appendChild(celda2)
                        fila.appendChild(celda3)
                        fila.appendChild(celda4)
                        fila.appendChild(celda5)
                        fila.appendChild(celda6)
                        fila.appendChild(celda7)
                        fila.appendChild(celda8)
                        fila.appendChild(celda9)
                        cuerpo.appendChild(fila)

                        boton.onclick = function() {
                            detalleViaje(`${element['idViaje']}`)
                        }
                        boton.onmousedown = function(e) {
                            // detalle(`${element['id_tbPersona']}`)
                            // console.log(e.button);
                            if (e.button == 2) {
                                sessionStorage.setItem('idViaje', element['idViaje'])
                                window.open("DetalleViaje.html");
                            }
                        }

                    })

                }
            } else {
                alert("Mandar Error o Message..")
            }

            //console.log(res.results)
        })

}

let detalleViaje = (id) => {
    sessionStorage.setItem('idViaje', id)
    self.location = `DetalleViaje.html`
}

let obtenerViaje = (id) => {
    let sw = `${url}reportes/detalleViaje/${id}`
    console.log(sw)
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response) {
                document.getElementById('FechaSolicitud').value = res['result']['FechaSolicitud']
                document.getElementById('FechaInicio').value = res['result']['FechaInicio']
                document.getElementById('FechaFin').value = res['result']['FechaFin']
                document.getElementById('NombreUser').value = res['result']['NombreUser']
                document.getElementById('NombreDriver').value = res['result']['NombreDriver']
                document.getElementById('FechaPago').value = res['result']['FechaPago']
                document.getElementById('year').value = res['result']['Año']
                document.getElementById('modelo').value = res['result']['Modelo']
                document.getElementById('placa').value = res['result']['Placa']
                document.getElementById('imgvehiculo1').value = res['result']['Imagen']
                let status = 'Finalizado'
                if (res['result']['Status'] == 7) {
                    status = 'Finalizado'
                } else if (res['result']['Status'] == 8) {
                    status = 'Reportado'
                }
                document.getElementById('Status').value = status
                let zona = ``
                if (res['result']['idCiudad'] == 1) {
                    zona = 'Huauchinango'
                } else if (res['result']['idCiudad'] == 2) {
                    zona = 'Pachuca'
                } else if (res['result']['idCiudad'] == 3) {
                    zona = 'Hermosillo'
                } else {
                    zona = `No hay`
                }
                document.getElementById('Zona').value = zona
                    // console.log(res['result']['idZona']);
                    // document.getElementById('NombreCompleto').innerHTML = `${res['result']['Nombre']} ${res['result']['Apellidos']}`
            } else {
                if (res['errors'].length == 0) {
                    alert(res['message'])
                    self.location = 'Conductores.html'
                } else {
                    alert(res['errors'])
                    self.location = 'Conductores.html'
                }
            }
        })
}