// listar zonas  and init firebase 
document.addEventListener('DOMContentLoaded', (event) => {
    listarZonas(2);
    firebase.initializeApp(configFireBaseMapaDriver);    
    // firebase.analytics();

});
// iniciar servicios de firebase
const initFirebaseRealtime = (idZona) => {
    // referencia
    
    // fetch()
    // primera consulta de datos = 
    const dbFireBase = firebase.database().ref().child(`Drivers/${idZona}`);
    // solo una consulta de datos en ese momentos
    dbFireBase.once('value').then(snap => {
        // pre.innerText = JSON.stringify(snap.val());
        let listaDrivers = JSON.stringify(snap.val());
        listaDrivers = JSON.parse(listaDrivers);
        // console.log(listaDrivers);
        eliminatPuntos();
        for (const key in listaDrivers) {
            if (listaDrivers.hasOwnProperty(key)) {
                const element = listaDrivers[key];
                if(element.status == 1 && element.idDriver != null){
                    let newPunt = {lat: element.latitude, lng: element.longitude};
                    let titulo = `${element.name}`;
                    agregarPunto(element,newPunt,titulo,element.idDriver);
                }
            }
        }
        // numero de activos
        document.querySelector("#numeroConductores").innerHTML = ` Numero de conductores Activos : ${markers.length}`;
    });

    dbFireBase.on('child_changed',snap => {
        let valorSnap = snap.val();
        // console.log(valorSnap.idDriver)
        // buscar repetido y eliminarlo para dibijar el nuevo
        let duplicate = false;
        for (let i = 0; i < markers.length; i++) {
            let element = markers[i];
            if (element.idDriver == valorSnap.idDriver){
                markers[i].setMap(null)
                markers.splice(i,1);
                duplicate = true;
            }
        }
        let newPunt = {lat: valorSnap.latitude, lng: valorSnap.longitude};
        let titulo = `${valorSnap.name}`;
        agregarPunto(valorSnap,newPunt,titulo,valorSnap.idDriver,duplicate);
        // numero de activos
        document.querySelector("#numeroConductores").innerHTML = ` Numero de conductores Activos : ${markers.length}`;
    })

}
let map
let markers = [];
// iniciar mapa
function initMap(latitude,longitude){
    // The location
    let centroMap = {lat: latitude, lng: longitude};
    // console.log(centroMap);
    // The map, centered at zone
    map = new google.maps.Map(
                                document.getElementById('map'), {zoom: 12, center: centroMap}
                            );
}
// agregar puntos en el mapa
const agregarPunto = (element,punto,titulo,idDriver,duplicate = false) =>{
    if (!duplicate) {
        let cuerpoLista = document.querySelector("#listaDrivers");
        cuerpoLista.innerHTML += `
        <div class="card" style="margin-bottom:0px;margin-top:10px">
            <div class="card-header" id="lista${idDriver}" style="padding: 0px" >
            <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapse${idDriver}" aria-expanded="false" aria-controls="collapse${idDriver}">
                ${titulo} ${element.surname}
                </button>
            </h2>
            </div>
            <div id="collapse${idDriver}" class="collapse" aria-labelledby="lista${idDriver}" data-parent="#accordionExample">
            <div class="card-body">
                Modelo: ${element.car.model}<br>
                Placa: ${element.car.plate}
            </div>
            </div>
        </div>`   
    }
    // console.log(titulo,punto);    
    const icon = {
        url: `${refMenuInicio}img/car.png`, // url
        scaledSize: new google.maps.Size(20, 20), // size
    };

    let contentString = `${titulo}`;

    const infowindow = new google.maps.InfoWindow({
    content: contentString
    });

    let marker = new google.maps.Marker(
        {   position: punto,
            icon: icon,
            title: `${titulo}`,
            map: map,
            idDriver:idDriver
        }
    );
    // console.log(marker);
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
    markers.push(marker);
}

const eliminatPuntos =()=>{
    let cuerpoLista = document.querySelector("#listaDrivers");
    cuerpoLista.innerHTML = '';
    for (i in markers) {
        markers[i].setMap(null);
      }
      markers.length = 0;
}

// se selecciono la zona 
const zonaSelect = () => {
    let selectzona = document.querySelector("#idZona");
    // console.log(selectzona.value);
    let id = selectzona.value;
    // zona/obtener/1
    sw = `${url}zona/obtener/${id}`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response) {
                // console.log(res);
                let latitude  =  parseFloat(res.result.Latitud);
                let longitude = parseFloat(res.result.Longitud);
                initMap(latitude,longitude);
                initFirebaseRealtime(id);
            }
        })
    
}