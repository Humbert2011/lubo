

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Lubo - Nosotros</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <!-- icon -->
		<link rel="icon" type="image/x-icon" href="../img/favicon/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
		<link rel="manifest" href="../img/favicon/site.webmanifest">
		<link rel="mask-icon" href="../img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
        <link href="../css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/theme.css" rel="stylesheet" type="text/css" media="all"/>
		<link rel="stylesheet" href="../css/nosotros.css">
		
		<!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NX8TKGG');</script>
		<!-- End Google Tag Manager -->
		
		
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="../js/config/config.js"></script> <!-- url -->
		<script type="text/javascript" src="../js/sw/servicios.js" ></script>
        <script type="text/javascript">
			function cargando() {
				// validar token
				var token = sessionStorage.getItem('Token');
				console.log(token)
				if (token != null) {
					document.getElementById('log-user').innerHTML = 'Logout'
					document.getElementById('log-user').onclick = logout
				}
			}
		</script>
		<!-- Facebook Pixel Code -->
		<script>
				!function(f,b,e,v,n,t,s)
				{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
				fbq('init', '189972612211292');
				fbq('track', 'PageView');
			</script>
			<noscript><img height="1" width="1" style="display:none"
				src="https://www.facebook.com/tr?id=189972612211292&ev=PageView&noscript=1"
			/></noscript>
		<!-- End Facebook Pixel Code -->
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150520430-1"></script>
		<script>
		if(modoServidor === 'prod'){
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-150520430-1');
		}
		</script>
		<!-- End Global site tag (gtag.js) - Google Analytics -->
		
		
    </head>
    <body onload="cargando()" >
		<!-- Google Tag Manager (noscript) -->
	    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX8TKGG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="nav-container">
			<?php 
				$currentPage = 'menu-nosotros';
				include_once("../menus/menu-sticky-black-in.php");
			?>
		</div>
		
		<div class="main-container">
			<header class="fullscreen-element no-pad centered-text">
				<div class="background-image-holder parallax-background overla">
					<img class="background-image" style="background-color: transparent;" alt="Background Image" src="../img/somosimg.jpg">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-md-12 text-left">
							<span class="text-white alt-font"></span>
							<h1 class="text-white h1-hola" style="color: #4a4a4a;"><b>¿QUIENES<br> <span style="color: #ED3093;">SOMOS?</span></b>
							</h1>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</header>
		<!-- end divider -->
			
			
			
			<section class="video-inline">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 mycontent-left align-vertical">
							<h1 class="h1-mision">MISIÓN</h1>
						</div>
						<div class="col-xs-12 col-sm-1 col-md-1">
						</div>
						<div class="col-xs-12 col-sm-7 col-md-7">
							<p class="lead space-bottom-medium p-lubo">Brindar un servicio profesional, comodo y seguro, garantizando  la disponibilidad  de vehículos y la estandarización de tarifas, para nuestros usuarios, bindando una experiencia distinta de trasporte.
	Darle a nuestros conductores, certeza en cada viaje, control sobre su tiempo de trabajo y una mejor administración de sus ganancias.</p>
						</div>
					</div><!--end of row-->
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 mycontent-left align-vertical">
							<h1 class="h1-mision">VISIÓN</h1>
						</div>
						<div class="col-xs-12 col-sm-1 col-md-1">
						</div>
						<div class="col-xs-12 col-sm-7 col-md-7">
							<p class="lead space-bottom-medium p-lubo">Convertir a Lubo en la empresa de trasporte privado, más usada y de mayor demanda del país, brindando confianza y seguridad a nuestros clientes, creando una mejor cultura de servicio con tecnología de punta.</p>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
			
			<section class="video-inline">
				<div class="container container-full" style="padding-left: 0px; padding-right: 0px;">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<img src="../img/banner.jpg">
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
			
			<section class="video-inline">
				<div class="container container-full">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<center>
								<h2 class="h2-eleccion">Creamos Lubo, para todas esas personas que como tú desean crecer y disfrutar de algo diferente</h2>
							</center>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
			
			<section class="image-divider overlay duplicatable-content">
				<div class="background-image-holder parallax-background">
					<img class="background-image" alt="Background Image" src="../img/persons.jpg">
				</div>
				
				<div class="container">
					
					<div class="row">
						<div class="hidden-xs col-sm-6 col-md-6 align-vertica no-align-mobile">
							<h1 class="no-margin-bottom text-white h1-viaja">Descubrirás que con Lubo <br>todo es diferente</h1>
						</div>
						
						<div class="col-xs-12 col-sm-6 col-md-6">
							<h1 class="no-margin-bottom text-white h1-viaja text-right">Un nuevo futuro en el transporte <br> hagámoslo juntos</h1>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
			
			<section class="video-inline">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 align-vertical no-align-mobile">
							<center>
							<h2 class="h2-eleccion h1-para-ti">Nuestro mensaje para ti</h2>
							</center>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 ">
							<p class="lead space-bottom-medium p-lubo">
								Crecemos contigo, haciendo de Lubo una excelente aplicación para facilitar el transporte de las personas manteniendo la comodidad, seguridad y eficiencia de nuestro trabajo.
							</p>
							<p class="lead space-bottom-medium p-lubo">
								Como conductor deseamos que descubras una nueva manera de conducir sin las pesadas jornadas e inseguridades que hoy en día nos aquejan, haciendo crecer tus ganancias día a día gracias a tu excelente trabajo y nuestra guía.
							</p>
							<p class="lead space-bottom-medium p-lubo">
								Si eres usuario, haremos de cada viaje una nueva experiencia llena de confianza y seguridad además de la rapidez con la que nuestros conductores te llevarán a tu destino
							</p>
							<p class="lead space-bottom-medium p-lubo text-right p-juntos">
								Juntos somos lubo
							</p>
							
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
		</div>
		
		
		
        <div class="clearfix">
	        
		<div class="footer-container">
			<section class="sliderQR section-footer" style="background-color: transparent;">
		        <?php include_once('../menus/footer-in.php');?>
			</section>
		</div>
		
		
		
		
		
		
				
		<script src="../js/jquery.min.js"></script>
        <script src="../js/jquery.plugin.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.flexslider-min.js"></script>
        <script src="../js/smooth-scroll.min.js"></script>
        <script src="../js/skrollr.min.js"></script>
        <script src="../js/spectragram.min.js"></script>
        <script src="../js/scrollReveal.min.js"></script>
        <script src="../js/lightbox.min.js"></script>
        <script src="../js/scripts.js"></script>
        
        
        <script>
    	$(document).ready(function (){
		    //validateB();
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
                document.getElementById("headerLogos").style.display="none";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		});
	</script>
	<script>
		document.getElementsByTagName("BODY")[0].onresize = function() {myFunction()};
		function myFunction() {
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../img/footerLubo.png";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../img/footerMobile.png";
		    }
		}
		
	</script>
        
        <script type="text/javascript">
	    	jQuery(document).ready(function() {
	    	jQuery('.telefono').keypress(function(tecla) {
	        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
	   			 });
			});
	    </script>
	
	
	    <script type="text/javascript">
			$(document).ready(function() {
				$("#frmValidate").hide()
				$("#datos").hide()
			});
			var input=  document.getElementById('numero1');
			input.addEventListener('input',function(){
			  if (this.value.length > 10) 
			     this.value = this.value.slice(0,10); 
			})
		</script>
	
	
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#numero22").val(value);
	          });
	      });
		</script>
		<script>
	      $(document).ready(function () {
	          $("#numero1").keyup(function () {
	              var value = $(this).val();
	              $("#telefono3").val(value);
	          });
	      });
		</script>
	
    </body>
</html>
				