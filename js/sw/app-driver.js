let auth = (correo, password, tipoUser) => {
    event.preventDefault()
    sessionStorage.removeItem('Token');
    sessionStorage.removeItem('IdPersona');
    let parametros = {
            "Telefono": correo,
            "Password": password,
            "Tipo_usuario": tipoUser
        }
        // console.log(parametros)
    $.ajax({
        data: parametros,
        url: url + 'auth/autenticar',
        type: 'POST',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            document.getElementById('entrar').disabled = true
            return true
        },
        success: function(auth) {
            document.getElementById('entrar').disabled = false
            if (auth['response']) {
                // autenticar
                sessionStorage.setItem('Token', auth['result']['token']);
                sessionStorage.setItem('IdPersona', auth['result']['persona']);
                validar(1);

                let id = auth['result']['persona']; //persona

                $.ajax({
                    data: parametros,
                    url: url + 'registro/obtener/' + id,
                    type: 'GET',
                    dataType: 'json',
                    cache: false,
                    beforeSend: function() {
                        return true
                    },
                    success: function(data) {

                        if (data['response']) {
                            let result = data['result']
                            let status = result['Status']
                            // if (status <= 1) {
                            //     self.location = `${refMenuInicio}driver/registro/registro-auto.php#registro`;
                            // } else if (status >= 2 && status <= 4) {
                            //     self.location = `${refMenuInicio}driver/mis-documentos.html`;
                            // } else if (status == 5) {
                                self.location = `${refMenuInicio}driver/mis-viajes.html`;
                            // }
                        } else {
                            if (data['errors'].length == 0) {
                                alert(data['message'])
                            } else {
                                alert(data['errors'])
                            }
                        }
                        return true
                    }
                });

            } else {
                // console.log(auth['errors'].length)
                if (auth['errors'].length == 0) {
                    alert(auth['message'])
                } else {
                    alert(auth['errors'])
                }
            }
            return true
        }
    })
}

const validar = (l = 0, r = 0) => {
    let token = sessionStorage.getItem('Token');
    let idPersona = sessionStorage.getItem('IdPersona')
    console.log(token, idPersona)
    if (token != null) {

        // let token = sessionStorage.getItem('Token');
        sw = `${url}auth/getData/${token}`;
        fetch(sw, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res.response && res['result']['Tipo_de_usuario'] == 3) {
                    console.log(res)
                    let name = res['result']['Nombre'];
                    // console.log(name);
                    sessionStorage.setItem("Nombre", name);
                    let sw2 = `${url}registro/obtener/${res['result']['id']}`
                    fetch(sw2, {
                            method: 'GET',
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                        .then(res => res.json())
                        .then(res => {
                            if (res['response']) {
                                var result = res['result']
                                var status = result['Status']
                                    // console.log(status)
                                if (status <= 1) {
                                    if (r == 0) {
                                        // self.location = `${refMenuInicio}driver/registro-auto.html#registro`;
                                    }
                                } else if (status >= 2 && status <= 4) {
                                    if (r == 0) {
                                        // self.location = `${refMenuInicio}driver/mis-documentos.html`;
                                    }
                                }
                            } else {
                                if (res['errors'] == null) {
                                    alert(res['message'])
                                    console.log("val 1", res);
                                } else {
                                    alert(res['errors'])
                                    console.log("val 2", res);
                                }
                            }
                        })
                } else {
                    self.location = `${refMenuInicio}login-user.php`;
                }
            });
    } else {
        if (l === 1) {
            self.location = `${refMenuInicio}login.php`;
        }
    }
}

const validarnumero = (telefono, codigo) => {
    // alert(telefono)
    // ajax de enviar codigo
    parametros = {
        'CodigoPais': codigo,
        'Telefono': telefono,
        "TipoUsuario": 3
    }

    let output = true;
    $(".signup-error").html('');

    if (!($("#numero1").val())) {
        output = false;
        $("#numero1").addClass('input-error');
    }

    let phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

    if (!$("#numero1").val().match(phoneno)) {
        $("#telefono-error").html("Teléfono inválido!");
        $("#numero1").addClass('input-error');
        output = false;
    }

    if (output == false) {
        console.log("Error");
    } else {
        let sw = `${url}auth/validarNumero`
            // console.log(parametros);
        document.getElementById('botonderegistro').disabled = true;
        fetch(sw, {
                method: 'POST',
                body: JSON.stringify(parametros), // data can be `string` or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res['response']) {
                    self.location = `confirmacion.php?tel=${telefono}&codigo=${codigo}#frmValidate`
                } else {
                    alert(res['errors'])
                    console.log(res);
                    if (res['errors'] == "El número de télefono ya está registrado en otra cuenta.") {
                        self.location = "../../login.php"
                    }
                }
            });
        document.getElementById('botonderegistro').disabled = false
    }
}



const limpiarEstilo = () => {
    var element = document.getElementById("numero1");
    element.classList.remove("input-error");
}

const pushL = (l, event) => {
    let element = document.getElementById(l);
    let a = document.getElementById("a"),
        b = document.getElementById("b"),
        c = document.getElementById("c");
    // boton = document.getElementById("boton");
    let codigo = event.which || event.keyCode;
    if (element.value.length >= 1) {
        switch (l) {
            case "a":
                b.focus();
                break;
            case "b":
                c.focus();
                break;
            case "c":
                d.focus();
                break;
        }
    } else {
        console.log(codigo);
        if (codigo == 8) {
            switch (l) {
                case "b":
                    a.focus();
                    break;
                case "c":
                    b.focus();
                    break;
                case "d":
                    c.focus();
                    break;
            }
        }
    }
};

let validarcodigo = (a, b, c, d, telefono, CodigoPais) => {
    let codigo = a + b + c + d;
    let sw = `${url}auth/validarCodigo`;
    parametros = {
        "CodigoPais": CodigoPais,
        "Codigo": codigo,
        "Telefono": telefono
    }
    fetch(sw, {
            method: 'POST',
            body: JSON.stringify(parametros), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res['response']) {
                // $(location).attr('href', 'registro.php?telefono=' + telefono + '#registro')
                self.location = `registro.php?telefono=${telefono}&codigo=${CodigoPais}#registro`;
            } else {
                alert(res['errors'])
            }
        });
}

let listarZonas = () => {
    sw = `${url}zona/listar`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            let data = null;
            let htmlInsert = '';
            let ini = `<select id="idZona" onchange="limpiarEstiloSelectZona()" name = "idZona" class="campotexto" placeholder="Zona" style="font-size: 18px;
            padding: 8px 8px 8px 5px;
            -webkit-appearance: none;
            display: block;
            background: white;
            color: #636363;
            width: 80%;
            margin-left: 10%;
            border-radius: 5px;
            border: 1px solid #ECEAEA;
            box-shadow: 3px 3px 5px #B9B7B7;
        " > <option value='0'>Selecciona una zona--></option>`
            if (res.result['total'] > 0) {
                data = res.result['data']
                console.log(data)
                data.forEach(element => {
                    // console.log(element['Descripcion'])
                    htmlInsert += `<option value = "${element['idZona']}">${element['Descripcion']}</option>`
                });
            }
            let fin = '</select>'
            document.getElementById('zona').innerHTML = ini + htmlInsert + fin

            // console.log(res);
        });
}

// limpia de estilos de error
function limpiarEstiloNombre() {
    var element = document.getElementById("nombre");
    element.classList.remove("input-error");

    var link = document.getElementById('name-error');
    link.style.display = 'none'; //or
}

function limpiarEstiloApellidos() {
    var element = document.getElementById("apellidos");
    element.classList.remove("input-error");

    var link = document.getElementById('apellidos-error');
    link.style.display = 'none'; //or
}

function limpiarEstiloCorreo() {
    var element = document.getElementById("correo");
    element.classList.remove("input-error");

    var link = document.getElementById('email-error');
    link.style.display = 'none'; //or
}

function limpiarEstiloPassword() {
    var element = document.getElementById("contraseña");
    element.classList.remove("input-error");

    var link = document.getElementById('password-error');
    link.style.display = 'none'; //or
}

function limpiarEstiloSelectZona() {
    let link = document.getElementById('zona-error');
    link.style.display = 'none';
    $("#zona-error").html("");
    var element = document.getElementById("idZona");
    element.classList.remove("input-error");
}
// fin limia de estilos
// validar si estan parametros GET en url de registro

const validarurl = () => {
        let sear = new URLSearchParams(location.search);
        let telefono = sear.get('telefono');
        let codigo = sear.get('codigo');
        // console.log(telefono.length,codigo.length);
        if ((telefono == null || codigo == null) || (telefono.length != 10 || codigo.length == 0)) {
            self.location = `iniciar.php`;
        }
    }
    // 

let enviado = 0

let registrar = (nombre, apellidos, email, password, codigoPais, telefono, sexo, idZona) => {
        if (enviado === 1) {
            return false
        }
        tycos = document.getElementById('tycos').checked;
        document.getElementById('boton').disabled = true
        let output = true;
        if (tycos) {
            tycos = 1
        } else {
            tycos = 0
            output = false;
            alert('¡Acépte los términos y condiciones!')
        }
        $(".signup-error").html('');
        if ($("#idZona").val() == 0) {
            output = false;
            let link = document.getElementById('zona-error');
            link.style.display = 'block';
            $("#zona-error").html("Zona requerida!");
            $("#idZona").addClass('input-error');
        }
        if (!($("#nombre").val())) {
            output = false;
            $("#nombre").addClass('input-error');
        }
        if (!($("#apellidos").val())) {
            output = false;
            $("#apellidos").addClass('input-error');
        }
        if (!($("#correo").val())) {
            output = false;
            $("#correo").addClass('input-error');
        }
        if (!$("#correo").val().match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) {
            $("#email-error").html("Email inválido!");
            let link = document.getElementById('email-error');
            link.style.display = 'block';
            $("#email-error").html("Email no válido");
            $("#correo").addClass('input-error');
            output = false;
        }
        if (!($("#contraseña").val())) {
            output = false;
            $("#contraseña").addClass('input-error');
        }

        if (!document.querySelector('input[name="ritem"]:checked')) {
            alert('Elige un género');
            output = false;
        }

        if (output == false) {
            console.log("Error");
        } else {
            let parametros = {
                "Tipo_de_usuario": "3",
                "Nombre": nombre,
                "Apellidos": apellidos,
                "Email": email,
                "Password": password,
                "CodigoPais": codigoPais,
                "Telefono": telefono,
                "Sexo": sexo,
                "Status": "3",
                "idZona": idZona,
                "tycos": tycos
            }
            console.log(parametros);
            let sw = `${url}persona/registrarDriver`;
            fetch(sw, {
                    method: 'POST',
                    body: JSON.stringify(parametros), // data can be `string` or {object}!
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(res => {
                    console.log(res);
                    if (res['response']) {
                        // autenticar
                        let parametrosAuth = {
                            "Telefono": telefono,
                            "Password": password,
                            "Tipo_usuario": "3"
                        }
                        console.log(parametrosAuth);
                        let authSw = `${url}auth/autenticar`;
                        fetch(authSw, {
                                method: 'POST',
                                body: JSON.stringify(parametrosAuth), // data can be `string` or {object}!
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            })
                            .then(auth => auth.json())
                            .then(auth => {
                                console.log(auth);
                                if (auth['response']) {
                                    sessionStorage.setItem('Token', auth['result']['token']);
                                    sessionStorage.setItem('IdPersona', auth['result']['persona']);
                                    self.location = `registro-auto.php#registro`;
                                } else {
                                    if (auth['errors'].length == 0) {
                                        alert(auth['message'])
                                    } else {
                                        alert(auth['errors'])
                                    }
                                }
                            })
                    } else {
                        if (res['errors'].length == 0) {
                            alert(res['message'])
                        } else {
                            alert(res['errors'])
                        }
                    }
                })
        }
        enviado = 0
        document.getElementById('boton').disabled = false
        return true
            // $(location).attr('href','registro-auto.php#registro')
    }
    // 
    // auto
    // 
let autoReg

const obtenerAuto = (idPersona) => {
        let sw = `${url}vehiculo/obtener/${idPersona}`;
        fetch(sw, {
                method: 'GET',
                // body: JSON.stringify(parametros), // data can be `string` or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res['response']) {
                    autoReg = res['result'];
                    console.log(autoReg);
                    if (autoReg['0'] >= 3) {
                        registroUpdate()
                    }
                    document.getElementById('modelo').value = autoReg['Modelo']
                    document.getElementById('an').value = autoReg['Año']
                    document.getElementById('placas').value = autoReg['Placa']
                    document.getElementById('color').value = autoReg['Color']
                    document.getElementById('pasajeros').value = autoReg['Num_Pasajeros']
                    document.getElementById('serie').value = autoReg['No_de_serie_del_vehiculo']
                    document.getElementById('idAuto').value = autoReg['id_tbVehiculo']
                    document.getElementById('boton').disabled = true
                    for (let i = 0; i < autoReg[0]; i++) {
                        let el = i + 1
                        $('#contIMg' + el).hide(500)
                    }
                    // console.log(autoReg['id_tbVehiculo'])
                    if (autoReg['idModoTrabajo'] == 1) {
                        document.getElementById('ritema').checked = true;
                    } else if (autoReg['idModoTrabajo'] == 2) {
                        document.getElementById('ritemb').checked = true;
                    } else {
                        document.getElementById('ritemc').checked = true;
                    }
                    // obtener imagenes de auto
                    return true
                } else {
                    if (res['errors'].length == 0) {
                        // alert(data['message'])
                    } else {
                        // alert(data['errors'])
                        $('#fotos').hide()
                    }
                }
            })
    }
    // 
function limpiarEstiloModelo() {
    var element = document.getElementById("modelo");
    element.classList.remove("input-error");

    var link = document.getElementById('modelo-error');
    link.style.display = 'none'; //or
}

function limpiarEstiloAn() {
    var element = document.getElementById("an");
    element.classList.remove("input-error");

    var link = document.getElementById('an-error');
    link.style.display = 'none'; //or
}

function limpiarEstiloPlacas() {
    var element = document.getElementById("placas");
    element.classList.remove("input-error");

    var link = document.getElementById('placas-error');
    link.style.display = 'none'; //or
}

function limpiarEstiloColor() {
    var element = document.getElementById("color");
    element.classList.remove("input-error");

    var link = document.getElementById('color-error');
    link.style.display = 'none'; //or
}

function limpiarEstiloPasajeros() {
    var element = document.getElementById("pasajeros");
    element.classList.remove("input-error");

    var link = document.getElementById('pasajeros-error');
    link.style.display = 'none'; //or
}

function limpiarEstiloSerie() {
    var element = document.getElementById("serie");
    element.classList.remove("input-error");

    var link = document.getElementById('serie-error');
    link.style.display = 'none'; //or
}
// 
// registrar auto
let registrarAuto = (placa, modelo, color, an, num, modo, pasajeros) => {
        let idPersona = sessionStorage.getItem('IdPersona');
        let parametros = {
            "idPersona": idPersona,
            "Placa": placa,
            "Modelo": modelo,
            "Color": color,
            "Año": an,
            "No_de_serie_del_vehiculo": num,
            "idModoTrabajo": modo,
            "Num_Pasajeros": pasajeros
        }
        console.log(parametros);
        // alert('alta de auto')

        let output = true;
        $(".signup-error").html('');

        if (!($("#modelo").val())) {
            output = false;
            let link = document.getElementById('modelo-error');
            link.style.display = 'block';
            // $("#modelo-error").html("Modelo requerido!");
            $("#modelo").addClass('input-error');
        }
        if (!($("#an").val())) {
            output = false;
            let link = document.getElementById('an-error');
            link.style.display = 'block';
            // $("#an-error").html("Año requerido!");
            $("#an").addClass('input-error');
        }

        if (!($("#placas").val())) {
            output = false;
            let link = document.getElementById('placas-error');
            link.style.display = 'block';
            // $("#placas-error").html("Placas requeridas!");
            $("#placas").addClass('input-error');
        }

        if (!($("#color").val())) {
            output = false;
            let link = document.getElementById('color-error');
            link.style.display = 'block';
            // $("#color-error").html("Color requerido!");
            $("#color").addClass('input-error');
        }

        if (!($("#pasajeros").val())) {
            output = false;
            let link = document.getElementById('pasajeros-error');
            link.style.display = 'block';
            // $("#pasajeros-error").html("Número requerido!");
            $("#pasajeros").addClass('input-error');
        }

        if (!($("#serie").val())) {
            output = false;
            let link = document.getElementById('serie-error');
            link.style.display = 'block';
            // $("#serie-error").html("Serie requerida!");
            $("#serie").addClass('input-error');
        }

        if (!document.querySelector('input[name="ritem"]:checked')) {
            alert('Error, elige tu estilo de trabajo');
            // document.getElementById('telefono-error').innerHTML = `Error, elige tu estilo de trabajo`;
            output = false;
        }

        if (output == false) {
            // console.log("Error");
        } else {
            // alert("Hola");
            $.ajax({
                data: parametros,
                url: url + 'vehiculo/registrar',
                type: 'POST',
                dataType: 'json',
                cache: false,
                beforeSend: function() {
                    document.getElementById('boton').disabled = true
                    return true
                },
                success: function(auto) {
                    if (auto['response']) {
                        // alert("Alta de auto")
                        $.ajax({
                            data: { "Status": 1 },
                            url: url + 'registro/actualizar/' + idPersona,
                            type: 'PUT',
                            dataType: 'json',
                            cache: false,
                            beforeSend: function() {
                                //alert('Envindo Datos por ajax')
                                return true
                            },
                            success: function(data) {
                                if (data['response']) {
                                    alert('Auto dado de alta')
                                        // console.log(auto['result'])
                                    document.getElementById('idAuto').value = auto['result']
                                    autoReg = { "0": 0 }
                                    document.getElementById('boton').disabled = true
                                    $('#fotos').show()
                                        // $(location).attr('href','registro/registro-auto.php#registro')
                                } else {
                                    if (data['errors'].length == 0) {
                                        alert(data['message'])
                                            //document.getElementById('telefono-error').innerHTML = data['message']
                                    } else {
                                        alert(data['errors'])
                                            // document.getElementById('telefono-error').innerHTML = data['errors']
                                    }
                                }
                                return true
                            }
                        })
                    } else {
                        if (auto['errors'].length == 0) {
                            alert(auto['message'])
                                // document.getElementById('telefono-error').innerHTML = auto['message']
                        } else {
                            alert(auto['errors'])
                                // document.getElementById('telefono-error').innerHTML = auto['message']
                        }
                        document.getElementById('boton').disabled = false
                    }
                    return true
                }
            })
        }

        // urlSW = url + tipoUrl

    }
    //
    // selccionar img
const selecImgAuto = (num) => {
        let imagen = document.getElementById(`i${num}`);

        if (imagen.files.length > 0) {

            let reader = new FileReader();
            // es necesario el callback para saber cuando esta lita la convercion de la imagen en base 64
            reader.onload = function(e) {
                pre = document.getElementById(`imagen${num}`);
                pre.style.height = '85px';
                pre.src = e.target.result;
            };

            reader.readAsDataURL(imagen.files[0]);


            console.log(reader);
            console.log(reader.result);
        }
    }
    // fotos
let paso2 = () => {
    autoReg['0']++;
    // console.log(autoReg['0'])
    if (autoReg[0] > 2) {
        registroUpdate()
    }
}

let registroUpdate = () => {
    let idPersona = sessionStorage.getItem('IdPersona')
    $.ajax({
        data: { "Status": 2 },
        url: url + 'registro/actualizar/' + idPersona,
        type: 'PUT',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            return true
        },
        success: function(data) {
            if (data['response']) {
                alert('Fotos completas')
                    // $(location).attr('href', 'mis-documentos.php#documentos')
                self.location = `${refMenuInicio}driver/mis-documentos.html`;
            } else {
                if (data['errors'] == null) {
                    alert(data['message'])
                } else {
                    alert(data['errors'])
                }
            }
            return true
        }
    })
}

let imgVehiculo = (id, form, num) => {
    $.ajax({
        type: 'POST',
        url: url + 'img/cargar/vehiculo/' + id,
        data: new FormData(form),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            document.getElementById('bt' + num).disabled = true
            return true
        },
        success: function(msg) {
            if (msg['response']) {
                $('#contIMg' + num).hide(500)
                paso2()
            } else {
                document.getElementById('bt' + num).disabled = false
                if (msg['errors'].length == 0) {
                    alert(msg['message'])
                } else {
                    alert(msg['errors'])
                }
            }
            document.getElementById('bt' + num).disabled = false
            return true
        }
    });
}

// 

let limit = 10;
let offset = 0;
let total = 0;
const mostrarMenos = (tipoViajes = 7) => {
    //alert('mostrar mas');
    limit += 10;
    console.log('limit=' + limit + ', offset=' + offset);
    console.log(total);
    if (total >= limit) {
        listarViajes(`${tipoViajes }`);

    } else {
        alert("Este usurio no tiene mas viajes");
        limit = limit - 10;

    };
    console.log(listarViajes());
};

const listarViajes = (tipoViajes = 7) => {
    let id = sessionStorage.getItem('IdPersona');
    //let id = 250;
    // let sw = `${url}viaje/listarViajes/${limit}/${offset}/${id}/${tipoViajes}`;
    let sw = `${url}viaje/historialViajes/${limit}/${offset}/${id}/${tipoViajes}`;

    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res)
            if (res.response != false) {
                console.log(res)
                let data = res.result.Viajes;
                total = res.result.Total;
                cuerpo = document.getElementById('ListaDeViajes');
                cuerpo.innerHTML = "";
                data.forEach(element => {
                    console.log(element)
                    let arrayFechaYhora = element.FechaInicio;
                    let arrayFechaYhoraFin = element.FechaFin;
                    let arrayfecha = arrayFechaYhora.split(' ');
                    let arrayfechafin = arrayFechaYhoraFin.split(' ');
                    let arrayDate = arrayfecha[0].split('-');
                    let arrayDateFin = arrayfechafin[0].split('-');
                    fechaMes = meses.find(res => res.mes == arrayDate[1]);
                    fechaMesFin = meses.find(res => res.mes == arrayDateFin[1]);
                    let hora = arrayfecha[1].split(':');
                    let horafinal = arrayfechafin[1].split(':');
                    let date_format = '12';
                    let hour = hora[0];
                    let hourfinal = horafinal[0];
                    let minutes = hora[1];
                    let minutesfinal = horafinal[1];
                    let result = hour;
                    let resultado = hourfinal;
                    let ext = '';
                    if (date_format == '12') {
                        if (hour > 12) {
                            ext = 'PM';
                            hour = (hour - 12);

                            if (hour < 10) {
                                result = "0" + hour;
                            } else if (hour == 12) {
                                hour = "00";
                                ext = 'AM';
                            }
                        } else if (hour < 12) {
                            result = ((hour < 10) ? "0" + hour : hour);
                            ext = 'AM';
                        } else if (hour == 12) {
                            ext = 'PM';
                        }
                    }

                    result = result + ":" + minutes + ' ' + ext;

                    if (date_format == '12') {
                        if (hourfinal > 12) {
                            ext = 'PM';
                            hourfinal = (hourfinal - 12);

                            if (hourfinal < 10) {
                                resultado = "0" + hourfinal;
                            } else if (hourfinal == 12) {
                                hourfinal = "00";
                                ext = 'AM';
                            }
                        } else if (hourfinal < 12) {
                            resultado = ((hourfinal < 10) ? "0" + hourfinal : hourfinal);
                            ext = 'AM';
                        } else if (hourfinal == 12) {
                            ext = 'PM';
                        }
                    }

                    resultado = resultado + ":" + minutesfinal + ' ' + ext;
                    console.log(resultado);

                    let last4 = ``;
                    let ocultarPuntos = '';
                    let brand = ``;
                    let pago = element.idMetodoPago;
                    if (pago == 1) {
                        //pago = "cash.svg"
                        //last4 = `Efectivo`;
                        pago = `Efectivo`;
                        ocultarPuntos = 'visibility: hidden;';
                    } else {
                        pago = `Tarjeta`;
                        brand = 'visa.svg';
                        last4 = `4444`;
                        ocultarPuntos = 'visibility: visible;';
                    }

                    console.log(pago);
                    let calificacion = element.CalificacionDriver
                    console.log(calificacion)

                    // let starts = `
                    // <span class="fa fa-star checked"></span>
                    // <span class="fa fa-star checked"></span>
                    // <span class="fa fa-star checked"></span>
                    // <span class="fa fa-star checked"></span>
                    // <span class="fa fa-star"></span>`;   
                    let starts = ``;

                    for (i = 1; i <= 5; i++) {
                        if (calificacion >= i) {
                            starts += `<span class="fa fa-star checked"></span>`;
                        } else {
                            starts += `<span class="fa fa-star"></span>`
                        }
                    }
                    let fila = document.createElement(`div`)
                    fila.className = 'row';
                    fila.innerHTML = `
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card card-chart viaje-expandido">
                    <div class="card-body">
                        <input id="desplegar${element.idViaje}" value="0" type="hidden">
                        <a onclick = "desplegar(${element.idViaje})" data-toggle="collapse" href="" data-target=".multi-collapse-viaje${element.idViaje}" aria-expanded="true">
                        <h4 class="card-title"><span style="font-size: 28px;" id="simboloDesplegar${element.idViaje}" >+</span><span style="margin-left: 20px;">${arrayDate[0]} de ${fechaMes.valor} ${arrayDate[2]}, ${result}</span></h4>
                        <p class="card-category" style="margin-left: 30px;">
                          <span class="text-semibold" style="font-size: 16px;">${element.GananciaConductor} MXN</span> &nbsp;<span id ="number" class="text-nova-regular" style="font-size: 16px; color: #4A4A4A; margin-left: 10%; padding-top: 10px;">${pago}</span><br>
                        </p>
                        </a>
                        
                        <br>
                        
                        <div class="contenido-viaje collapse multi-collapse-viaje${element.idViaje}">
                        
                        
                            <p class="card-category">
                            <span class="text-semibold" style="font-size: 16px; margin-left: 30px;">Lubo Estandar</span></p>
                          
                            <div class="row">  	
                                <div class="col-sm-12">
                                    <section id="cd-timeline" class="cd-container">
                                        <div class="cd-timeline-block">
                                            <div class="cd-timeline-img cd-picture">
                                            </div> <!-- cd-timeline-img -->
                                
                                            <div class="cd-timeline-content">
                                                <p class="text-nova-regular">${element.CalleOrigen}, 73170, ${element.ColoniaOrigen}, ${element.CiudadOrigen} <br><span class="text-gray">${result}</span></p>
                                            </div> <!-- cd-timeline-content -->
                                        </div> <!-- cd-timeline-block -->
                                
                                        <div class="cd-timeline-block">
                                            <div class="cd-timeline-img cd-movie">
                                            </div> <!-- cd-timeline-img -->
                                
                                            <div class="cd-timeline-content">
                                                <p class="text-nova-regular">${element.CalleDestino}, 73170, ${element.ColoniaDestino}, ${element.CiudadDestino}<br><span class="text-gray">${resultado}</span></p>
                                            </div> <!-- cd-timeline-content -->
                                        </div> <!-- cd-timeline-block -->
                                        
                                    </section> <!-- cd-timeline -->
                                </div>   			
                            </div>
                            </div><!--end of collapse-->
                        </div> 
                        
                        <div class="collapse multi-collapse-viaje${element.idViaje}">
                            <hr class="hr-footer-viaje">
                            <div class="card-footer ">
                                <div class="stats text-nova-regular" style="margin-left: 30px;">
                                    Calificación &nbsp;
                                    ${starts}
                                </div>
                                <div class="stats text-nova-regular">
                                    <a href="detalle-viaje.html?id=${element.idViaje}&tipo=${element.Tipo}&calificacion=${element.CalificacionDriver}&metodoPago=${element.idMetodoPago}&tipoServicio=${element.TipoServicio}"><span class="link-blue">Ver detalles<i class="material-icons">keyboard_arrow_right</i></span></a>
                                </div>
                            </div>
                        </div>
                    </div><!--End card body-->
                </div>
            </div>`;
                    // `</div><!--end of row elemento viaje--></img>`;
                    cuerpo.appendChild(fila);
                });
            } else {
                let nombre = sessionStorage.getItem("Nombre");
                let element = document.getElementById('contenidoViaje').innerHTML = `<h1 id="encabezado" class="text-nova-bold color-gray-driver">Bienvenido ${nombre} :)</h1>
                <p class="text-driver color-gray-driver">
                    Estamos muy contentos de tenerte por aquí, por ahora tu perfil está vacío, inicia tu primer viaje para comenzar a disfrutar de los grandes beneficios de Lubo®.
                </p>
                <br>
                <br>
                <br>
                <p class="text-driver color-gray-driver">
                    Crezcamos juntos, movamos a la ciudad.
                </p>`;
            }


        })
};
const desplegar = (idViaje) => {
    let desplegar = document.getElementById(`desplegar${idViaje}`);
    if (desplegar.value == "0") {
        document.getElementById(`simboloDesplegar${idViaje}`).innerHTML = "-";
        desplegar.value = "1";
        // console.log(desplegar.value)
    } else if (desplegar.value == "1") {
        document.getElementById(`simboloDesplegar${idViaje}`).innerHTML = "+";
        desplegar.value = "0";
        // console.log(desplegar.value)
    }
}
const obtenerNombre = (name) => {
    let element = document.getElementById(`encabezado`);
    element.innerHTML = `Bienvenido ${name} :)`;
}

const detalleViaje = () => {
    // let id = sessionStorage.getItem('idUser');
    let id = sessionStorage.getItem('IdPersona');
    let sear = new URLSearchParams(location.search);
    let idViaje = sear.get('id');
    let tipo = sear.get('tipo');
    let metodoPago = sear.get('metodoPago')
    let calificacion = sear.get('calificacion')
    let tipoServicio = sear.get('tipoServicio')
    console.log(tipoServicio)
        // console.log('Este es otro ===> ' + tipo);
        // console.log(idViaje);
        //let idViaje = 188;
        //let token = sessionStorage.getItem('Token');
    console.log(id, idViaje, tipo)
    sw = `${url}viaje/detallesViaje/${id}/${idViaje}/${tipo}`;
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response != false) {
                // console.log(res)
                let data = res.result.Horario;
                let result = res.result.Desglose_tarifa;
                let gananciasEmp = res.result.Lubo_recibe;
                let gananciasDriver = res.result.Recibes;
                console.log(gananciasEmp);
                // document.getElementById('telefono').value = `${userData.Telefono}`;
                document.getElementById('direccionInicio').innerHTML = `${result.CalleOrigen},${result.ColoniaOrigen},${result.CiudadOrigen}`;
                document.getElementById('direccionFin').innerHTML = `${result.CalleDestino},${result.ColoniaDestino},${result.CiudadDestino}`;

                let arrayFechaYhora = data.FechaInicio;
                let arrayFechaYhoraFin = data.FechaFin;
                let arrayfecha = arrayFechaYhora.split(' ');
                let arrayfechafin = arrayFechaYhoraFin.split(' ');
                let arrayDate = arrayfecha[0].split('-');
                let arrayDateFin = arrayfechafin[0].split('-');
                fechaMes = meses.find(res => res.mes == arrayDate[1]);
                fechaMesFin = meses.find(res => res.mes == arrayDateFin[1]);
                let hora = arrayfecha[1].split(':');
                let horafinal = data.HoraFin.split(':');
                let date_format = '12';
                let hour = hora[0];
                let hourfinal = horafinal[0];
                let minutes = hora[1];
                console.log(minutes);
                let minutesfinal = horafinal[1];
                let result_inicio = hour;
                let resultado = hourfinal;
                let ext = '';
                if (date_format == '12') {
                    if (hour > 12) {
                        ext = 'PM';
                        hour = (hour - 12);

                        if (hour < 10) {
                            result_inicio = "0" + hour;
                        } else if (hour == 12) {
                            hour = "00";
                            ext = 'AM';
                        }
                    } else if (hour < 12) {
                        result_inicio = ((hour < 10) ? "0" + hour : hour);
                        ext = 'AM';
                    } else if (hour == 12) {
                        ext = 'PM';
                    }
                }

                result_inicio = result_inicio + ":" + minutes + ' ' + ext;

                console.log(result_inicio)

                if (date_format == '12') {
                    if (hourfinal > 12) {
                        ext = 'PM';
                        hourfinal = (hourfinal - 12);

                        if (hourfinal < 10) {
                            resultado = "0" + hourfinal;
                        } else if (hourfinal == 12) {
                            hourfinal = "00";
                            ext = 'AM';
                        }
                    } else if (hourfinal < 12) {
                        resultado = ((hourfinal < 10) ? "0" + hourfinal : hourfinal);
                        ext = 'AM';
                    } else if (hourfinal == 12) {
                        ext = 'PM';
                    }
                }

                resultado = resultado + ":" + minutesfinal + ' ' + ext;

                let start = ``;

                for (i = 1; i <= 5; i++) {
                    if (calificacion >= i) {
                        start += `<span class="fa fa-star checked"></span>`;
                    } else {
                        start += `<span class="fa fa-star"></span>`;
                    }
                }

                let pago = metodoPago;
                if (pago == 1) {
                    //pago = "cash.svg"
                    //last4 = `Efectivo`;
                    pago = `Efectivo`;
                    ocultarPuntos = 'visibility: hidden;';
                } else {
                    pago = `Tarjeta`;
                    // brand = 'visa.svg';
                    // last4 = `4444`;
                    // ocultarPuntos = 'visibility: visible;';
                }

                document.getElementById('metodoPago').innerHTML = `${pago}`;
                document.getElementById('horaInicio').innerHTML = `${result_inicio}`;
                document.getElementById('horaFin').innerHTML = `${resultado}`;
                document.getElementById('fechaPeticion').innerHTML = `${arrayDate[2]} de ${fechaMes.valor} ${arrayDate[0]}, ${result_inicio}`;
                document.getElementById('tarifaBase').innerHTML = `$ ${result.TarifaBase}`;
                document.getElementById('costoKm').innerHTML = `$ ${result.Costo_por_Km}`;
                document.getElementById('costoPorMin').innerHTML = `$ ${result.Costo_por_minuto}`;
                document.getElementById('cuotaSolicitud').innerHTML = `$ ${result.CuotaPeticion}`;
                document.getElementById('costoViaje').innerHTML = `$ ${result.PagoDelUsuario}`;
                document.getElementById('calificacion').innerHTML = `${start}`;
                document.getElementById('comision').innerHTML = `$ ${gananciasEmp.Total}`;
                document.getElementById('tipoLubo').innerHTML = `Lubo ${tipoServicio}`;
                document.getElementById('costoTotal').innerHTML = `$ ${gananciasDriver.Total}`;
                document.getElementById('kilometros').innerHTML = `+ por km (${result.DistanciaKilometros}km)`;
                document.getElementById('minutos').innerHTML = `+ por min (${result.TiempoMinutos} min)`;
                document.getElementById('distancia').innerHTML = `${result.DistanciaKilometros} km`;
                document.getElementById('tiempo').innerHTML = `${result.TiempoMinutos} min`;
                document.getElementById('iva').innerHTML = `$ ${gananciasDriver.IVA_driver}`;
                document.getElementById('isr').innerHTML = `$ ${gananciasDriver.Retencion_ISR}`;
            }
        });
}

const listarCalificaciones = () => {
    // let id = sessionStorage.getItem('247');
    let id = sessionStorage.getItem('IdPersona');
    let sw = `${url}calificacion/listarcalificacion/${id}`;
    console.log(sw);

    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.result != null) {
                let data = res.result.data;
                let result = res.result;
                console.log(data);
                cuerpo = document.getElementById('ListaDeCalificaciones');
                data.forEach(element => {
                    let fila = document.createElement(`div`)
                    fila.className = 'row bootstrap snippets';
                    fila.innerHTML = `<div class="col-md-12 col-sm-12">
                                                <div class="comment-wrapper">
                                                    <div class="panel panel-info">
                                                        <div class="panel-body">
                                                            <div class="clearfix"></div>
                                                            <ul class="media-list">
                                                                <li class="media">
                                                                    <img src="../assets/imagenes/comment-avatar.svg" alt="" class="img-circle avatar">

                                                                    <div class="media-body">
                                                                        <span class="text-muted pull-right">
                                    <small class="text-muted">
                                    	<div class="stats text-nova-regular" style="margin-left: 30px;">
										${element.Calificacion}&nbsp;
										<span class="fa fa-star checked" style="color: #ED3093;"></span>
                                                                    </div>
                                                                    </small>
                                                                    </span>
                                                                    <strong class="text-nova-bold">${element.Nombre}</strong>
                                                                    <p class="text-nova-regular">
                                                                        ${element.Comentario}
                                                                    </p>
                                                        </div>
                                                        </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>`
                    cuerpo.appendChild(fila);
                })
            } else {
                cuerpo = document.getElementById('ListaDeCalificaciones');
                let fila = document.createElement(`div`)
                fila.innerHTML = `<p class="text-driver color-gray-driver">
                                            Aún no tienes ninguna calificación
                                            </p>`
                cuerpo.appendChild(fila);
            }
        })
}
const listarVehiculo = () => {
    // let id = sessionStorage.getItem('247');
    let id = sessionStorage.getItem('IdPersona');
    let sw = `${url}vehiculo/obtener/${id}`;
    console.log(sw);

    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.result != null) {

                let data = res.result;
                let uno = data['1'][0];
                console.log(uno);
                //alert(uno['Imagen']);
                //console.log(uno['FechaAlta'])
                // document.getElementById('imgVehiculo').src = `${uno['Imagen']}`;

                cuerpo = document.getElementById('ListaDeVehiculos');
                console.log(data.Modelo);

                let fila = document.createElement(`div`)
                fila.className = 'row bootstrap snippets';
                fila.innerHTML = `<div class="card mb-3" style="max-width: 540px;">
                                            <div class="row no-gutters">
                                                <div class="col-md-4">
                                                    <img src="${uno['Imagen']}" class="card-img img-auto" alt="...">
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="card-body">
                                                    <a href="documentos-auto.html?id=${data.Modelo}">
                                                            <h5 class="card-title text-semibold">${data.Modelo}</h5>
                                                        </a>
                                                        <p class="text-nova-regular text-dato-auto">Año: ${data.Año}</p>
                                                        <p class="text-nova-regular text-dato-auto">Placas: ${data.Placa}</p>
                                                        <p class="text-nova-regular text-dato-auto">No. de series: ${data.No_de_serie_del_vehiculo}</p>
                                                        <p class="text-nova-regular text-dato-auto">Color: ${data.Color}</p>
                                                    </div>
                                                    <div class="card-footer text-semibold card-footer-auto">
                                                        Conduciendo <i class="material-icons">done</i>
                                                    </div>
                                                    <!--<div class="card-footer-auto"> 
											  	<smals class="text-semibold text-footer-card">Conduciendo <i class="material-icons">done</i></small>
											  </div>-->
                                                </div>
                                            </div>
                                        </div>`

                cuerpo.appendChild(fila);
            }
        })
}

const buscarPerfilFacturacion = () => {
    let idPersona = sessionStorage.getItem('IdPersona');
    let sw = `${url}factura/perfilEx/${idPersona}`;
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response) {
                console.log(res.result);
                let perfilHTML = document.getElementById('perfilF');
                perfilHTML.innerHTML = `<div class="col-sm-10">
                <ul class="list-inline" style="margin-left: 0px;">
                <li class="list-inline-item text-semibold" style="margin-top: -20px">
                    <span class="text-semibold" style="font-size: 17px; color: #4A4A4A; margin-left: 0px; padding-top: 10px;">${res.result.razon},Rfc: ${res.result.rfc}</span>
                    <p class="text-nova-regular">&nbsp</p>
                    
                </li>
                </ul>
            </div>
            <div class="col-lg-2">
                <li class="list-inline-item text-nova-regular li-detalle" style="margin-left: 60px; margin-top: -10px;">
                    <a href="nuevo-perfil-de-facturacion.html?rfc=${res.result.rfc}">Editar</a>
                    <!--<a href="" onclick="eliminarCSD('${res.result.rfc}')" data-toggle="modal" data-target="#modal-delete-card"><p class="text-nova-regular">Eliminar</p></a>-->
                </li>
            </div>`;
                document.getElementById('botonConfigurar').style.visibility = 'hidden';
                document.getElementById('mensajeNoPerfil').style.visibility = 'hidden';
            } else {
                document.getElementById('botonConfigurar').style.visibility = 'visible';
                document.getElementById('mensajeNoPerfil').style.visibility = 'visible';
            }
        })
}

let cer = ``;
const cer64 = () => {
    console.log("cer");
    let sello = document.getElementById(`sello`);
    if (sello.files.length > 0) {

        let reader = new FileReader();
        reader.onload = function(e) {
            cer = reader.result.replace(/^data:.+;base64,/, '');
            console.log(cer);
            //    let dataByn = e.target.result;
            //    cer = window.btoa(dataByn);
            //    console.log(cer);
        };

        reader.readAsDataURL(sello.files[0]);
    } else {
        alert("seleccione el archivo tipo CER")
    }
}

let key = ``;
const key64 = () => {
    console.log("key");
    let llave = document.getElementById(`llave`);
    if (llave.files.length > 0) {

        let reader = new FileReader();
        reader.onload = function(e) {
            key = reader.result.replace(/^data:.+;base64,/, '');
            console.log(key);
            //    let dataByn = e.target.result;
            //    key = window.btoa(dataByn);
            //    console.log(key);
        };

        reader.readAsDataURL(llave.files[0]);
    } else {
        alert("seleccione el archivo tipo KEY")
    }
}

const obtenerRfcPerfilFacturacion = () => {
    let sear = new URLSearchParams(location.search);
    let rfc = sear.get('rfc');
    console.log(rfc);

    if (rfc != null) {
        let idPersona = sessionStorage.getItem('IdPersona');
        let sw = `${url}factura/perfilEx/${idPersona}`;

        fetch(sw, {
                method: 'GET',
                // body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res.response) {
                    document.getElementById("Razon").value = res.result['razon'];
                    document.getElementById("RegimenFiscal").value = res.result.regimenFiscal;
                    document.getElementById("Rfc").value = res.result['rfc'];
                    document.getElementById("Rfc").disabled = true;
                } else {
                    alert("hubo un error inesperado intentelo de nuevo y recargue la pagina");
                }
            });
    }

}

const cargarCSD = () => {
    document.getElementById('botonGPerfilFacturacion').style.disabled = true;
    let error = false;
    if (cer == '' || cer == null) {
        alert("seleccione el archivo tipo CER")
        error = true;
    }
    if (key == '' || key == null) {
        alert("seleccione el archivo tipo KEY");
        error = true;
    }
    let idPersona = sessionStorage.getItem('IdPersona');
    let razon = document.getElementById("Razon").value;
    if (razon.length == 0) {
        error = true;
    }
    let regimenFiscal = document.getElementById('RegimenFiscal').value;
    let descripcionRF = '';
    if (regimenFiscal == 0) {
        alert("seleccione un Regimen Fiscal");
        error = true;
    } else {
        descripcionRF = CatalogoRegimenFiscal.find(res => res.Value == regimenFiscal);
        console.log(descripcionRF);
    }
    let rfc = document.getElementById("Rfc").value;
    if (rfc.length == 0) {
        error = true;
    }
    let password = document.getElementById("Password").value;
    if (password.length == 0) {
        error = true;
    }
    if (!error) {
        let parametros = {
            "Rfc": rfc,
            "Certificate": cer,
            "PrivateKey": key,
            "PrivateKeyPassword": password,
            "idPersona": idPersona,
            "razon": razon,
            "regimenFiscal": regimenFiscal,
            "descripcion": descripcionRF.Name
        };
        console.log(parametros);
        let sear = new URLSearchParams(location.search);
        let Rfc = sear.get('rfc');
        console.log(Rfc);
        if (Rfc == null) {
            console.log("add");
            let sw = `${url}factura/agregarCSD`;
            fetch(sw, {
                    method: 'POST',
                    body: JSON.stringify(parametros),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(res => {
                    console.log(res);
                    if (res.response) {
                        alert("CSD agregados");
                        self.location = `perfil-de-facturacion.html`;
                    } else {
                        alert(`Error: ${res.errors}`);
                    }
                });
        } else {
            console.log("update");
            let sw = `${url}factura/actualizarCSD`;
            fetch(sw, {
                    method: 'PUT',
                    body: JSON.stringify(parametros),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(res => {
                    console.log(res);
                    if (res.response) {
                        alert("CSD agregados");
                        self.location = `perfil-de-facturacion.html`;
                    } else {
                        alert(`Error: ${res.errors}`);
                    }
                });
        }
    }
    document.getElementById('botonGPerfilFacturacion').style.disabled = false;
}

const verPasswordSellos = () => {
    let tipo = document.getElementById("Password");
    if (tipo.type === "password") {
        tipo.type = "text";
    } else {
        tipo.type = "password";
    }
}

const verCatalogoRegimenFiscal = () => {
    console.log(CatalogoRegimenFiscal);
    let catalogoRF = document.getElementById("RegimenFiscal");
    let catalogoinnerHTML = `<option value="0">Seleccione un regimen --> </option>`;
    CatalogoRegimenFiscal.forEach(element => {
        catalogoinnerHTML += `<option value="${element.Value}"> ${element.Name} </option>`
    });
    catalogoRF.innerHTML = catalogoinnerHTML;
}

const listarSemanasGanancias = () => {
    let idPersona = sessionStorage.getItem('IdPersona');
    let sw = `${url}balance/listaSemanas`
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {

            if (res.response) {
                console.log(res.result);
                let data = res.result;
                let cuerpo = document.getElementById('listaSemanas');
                let inner = ``;

                let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Novimbre", "Diciembre"];

                data.forEach(element => {
                    let fechaServ = element.InicioSemana;
                    let fecha = fechaServ.split(' ');
                    let splitFecha = fecha[0].split('-');
                    let dia = splitFecha[2];
                    let mes = parseInt(splitFecha[1]);
                    let year = splitFecha[0];

                    inner = `<a href="resumen-semana.html?idSemana=${element.idSemana}&fechas=${element.InicioSemana}*${element.FinSemana}&idPersona=${idPersona}" target="_blank">
                                <p class="link-pink-week"><i class="material-icons">receipt</i>Domingo, ${dia} de ${meses[mes-1]}, ${year}</p>
                            </a>${inner}`;
                });
                cuerpo.innerHTML = inner;
            }
        })
}

const detallesSemanaConductor = () => {
    let sear = new URLSearchParams(location.search);
    let idSemana = sear.get('idSemana');
    let fechas = sear.get('fechas');
    let idPersona = sessionStorage.getItem('IdPersona');
    let sw = `${url}balance/corteSemanaConductor/${idPersona}/${idSemana}`
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response) {
                console.log(res.result);
                let dataSemanas = res.result.DetalleSemana;
                let cuerpo = document.getElementById('tbody');

                // let totalProcesadorPagoSemana

                // let desgloseProcesadorPagoSemana
                let innerSemana = ``
                dataSemanas.forEach(element => {
                    // procesadorPagoSC = element.ProcesadorPagoConductor
                    // procesadorPagoSL = element.ProcesadorPagoLubo
                    // let totalProcesadorPagoSemana = procesadorPagoSC + procesadorPagoSL
                    // totalProcesadorPagoSemana = totalProcesadorPagoSemana.toFixed(2);
                    innerSemana = `<tr>
                    <td align="center">${element.Dia}</td>
                    <td align="center">${element.Viajes}</td>
                    <td align="center">$${element.GananciaConductor}</td>
                    <td align="center">$${element.ConductorIVA}</td>
                    <td align="center">$${element.ConductorISR}</td>
                    <td align="center">$${element.ComisionLubo}</td>
                    <td align="center">$${element.ProcesadorPago}</td>
                    <td align="center">$${element.Total}</td>
                </tr>${innerSemana}`;
                });
                cuerpo.innerHTML = innerSemana;
                // totalesProcesadorPagoSemanaD = res.result.Totales.ProcesadorPagoConductor
                // totalesProcesadorPagoSemanaL = res.result.Totales.ProcesadorPagoLubo
                // let totalesProcesadorPagoSemana = totalesProcesadorPagoSemanaD + totalesProcesadorPagoSemanaL
                // totalesProcesadorPagoSemana = totalesProcesadorPagoSemana.toFixed(2);
                // console.log(totalesProcesadorPagoSemana)
                document.getElementById('totales').innerHTML = `
                <tr>
                    <th>Totales</th>
                    <th>${res.result.Totales.TotalViajes}</th>
                    <th>$${res.result.Totales.GananciaConductor}</th>
                    <th>$${res.result.Totales.ConductorIVA}</th>
                    <th>$${res.result.Totales.ConductorISR}</th>
                    <th>$${res.result.Totales.ComisionLubo}</th>
                    <th>$${res.result.Totales.ProcesadorPago}</th>
                    <th>$${res.result.Totales.Total}</th>
                </tr>`;
                let siclo = fechas.split('*');
                let fechaLarga1 = siclo[0];
                let fechaLarga2 = siclo[1];
                fechaLarga1 = fechaLarga1.split(' ');
                fechaLarga2 = fechaLarga2.split(' ');
                fechaLarga1 = fechaLarga1[0];
                fechaLarga2 = fechaLarga2[0];
                let fechaCorta1 = fechaLarga1.split('-');
                let fechaCorta2 = fechaLarga2.split('-');
                console.log(fechaLarga1, fechaLarga2);

                let mes1 = meses.find(res => res.mes == fechaCorta1[1]);
                let mes2 = meses.find(res => res.mes == fechaCorta2[1]);
                document.getElementById('lapsoSemanas').innerHTML = `Resumen de la semana ${mes1.valor} ${fechaCorta1[2]} - ${mes2.valor} ${fechaCorta2[2]}`;

                document.getElementById('TotalS').innerHTML = `$${res.result.DesgloseMontos.Ganancias}`;
                document.getElementById('TotalGnancias').innerHTML = `<b>$</b>${res.result.DesgloseMontos.Ganancias}`
                document.getElementById('cobrosEfectivo').innerHTML = `<b>$</b>${res.result.DesgloseMontos.Efectivo}`;
                document.getElementById('cobrosTrajeta').innerHTML = `<b>$</b>${res.result.DesgloseMontos.Tarjeta}`;
                document.getElementById('comisionesLubo').innerHTML = `<b>$</b>${res.result.DesgloseMontos.ComisionLubo}`;
                document.getElementById('subTotal').innerHTML = `<b>$</b>${res.result.DesgloseMontos.SubTotal}`;
                document.getElementById('procesadorPagos').innerHTML = `<b>$</b>${res.result.DesgloseMontos.ProcesadorPago}`;
                document.getElementById('iva').innerHTML = `<b>$</b>${res.result.DesgloseMontos.IVA}`;
                document.getElementById('isr').innerHTML = `<b>$</b>-${res.result.DesgloseMontos.ISR}`;
            }
        })
}

const pdfResumenSemana = () => {

    let fecha = document.getElementById("lapsoSemanas").innerHTML.split('Resumen de la semana ');
    fechas = fecha[1]
    let fechaSemanaC = document.getElementById("lapsoSemanas").innerHTML.split('Resumen de la semana ');
    fechaSemana = fechaSemanaC[1]
    let totalSemanaD = document.getElementById("TotalS").innerHTML.split('$');
    totalSemana = totalSemanaD[1]
    let body = document.getElementById("tbody").innerHTML.split('<td align="center">');
    console.log(body)
        //fila que se llena con los datos del primer dia
    fecha1 = body[1].split('</td>');
    date1 = fecha1[0];
    travel1 = body[2].split('</td>');
    viaje1 = travel1[0];
    // tip1 = body[3].split('</td>');
    // propina1 = tip1[0];
    tax1 = body[3].split('</td>');
    monto1 = tax1[0];
    impuestoalValorAgregado1 = body[4].split('</td>');
    ivaDriver1 = impuestoalValorAgregado1[0];
    impuestoSobrelaRenta1 = body[5].split('</td>');
    isrDriver1 = impuestoSobrelaRenta1[0];
    luboMonto1 = body[6].split('</td>');
    console.log(luboMonto1[0]);
    lubo1 = luboMonto1[0];
    // luboTax1 = body[7].split('</td>');
    // luboIVA1 = luboTax1[0];
    // console.log(luboIVA1)
    // luboISR1 = body[8].split('</td>');
    // isrLubo1 = luboISR1[0];
    procesadorPagoDL1 = body[7].split('</td>');
    procesadorPago1 = procesadorPagoDL1[0];
    all1 = body[8].split('</td>');
    total1 = all1[0];
    //fila que se llena con los datos del segundo dia
    fecha2 = body[9].split('</td>');
    date2 = fecha2[0];
    travel2 = body[10].split('</td>');
    viaje2 = travel2[0];
    // tip2 = body[14].split('</td>');
    // propina2 = tip2[0];
    tax2 = body[11].split('</td>');
    monto2 = tax2[0];
    impuestoalValorAgregado2 = body[12].split('</td>');
    ivaDriver2 = impuestoalValorAgregado2[0];
    impuestoSobrelaRenta2 = body[13].split('</td>');
    isrDriver2 = impuestoSobrelaRenta2[0];
    luboMonto2 = body[14].split('</td>');
    lubo2 = luboMonto2[0];
    // luboTax2 = body[15].split('</td>');
    // luboIVA2 = luboTax2[0];
    // luboISR2 = body[16].split('</td>');
    // isrLubo2 = luboISR2[0];
    procesadorPagoDL2 = body[16].split('</td>');
    procesadorPago2 = procesadorPagoDL2[0];
    all2 = body[16].split('</td>');
    total2 = all2[0];
    //fila que se llena con los datos del tercer dia
    fecha3 = body[17].split('</td>');
    date3 = fecha3[0];
    travel3 = body[18].split('</td>');
    viaje3 = travel3[0];
    // tip3 = body[25].split('</td>');
    // propina3 = tip3[0];
    tax3 = body[19].split('</td>');
    monto3 = tax3[0];
    impuestoalValorAgregado3 = body[20].split('</td>');
    ivaDriver3 = impuestoalValorAgregado3[0];
    impuestoSobrelaRenta3 = body[21].split('</td>');
    isrDriver3 = impuestoSobrelaRenta3[0];
    luboMonto3 = body[22].split('</td>');
    lubo3 = luboMonto3[0];
    // luboTax3 = body[27].split('</td>');
    // luboIVA3 = luboTax3[0];
    // luboISR3 = body[28].split('</td>');
    // isrLubo3 = luboISR3[0];
    procesadorPagoDL3 = body[23].split('</td>');
    procesadorPago3 = procesadorPagoDL3[0];
    all3 = body[24].split('</td>');
    total3 = all3[0];
    //fila que se llena con los datos del cuarto dia
    fecha4 = body[25].split('</td>');
    date4 = fecha4[0];
    travel4 = body[26].split('</td>');
    viaje4 = travel4[0];
    // tip4 = body[36].split('</td>');
    // propina4 = tip4[0];
    tax4 = body[27].split('</td>');
    monto4 = tax4[0];
    impuestoalValorAgregado4 = body[28].split('</td>');
    ivaDriver4 = impuestoalValorAgregado4[0];
    impuestoSobrelaRenta4 = body[29].split('</td>');
    isrDriver4 = impuestoSobrelaRenta4[0];
    luboMonto4 = body[30].split('</td>');
    lubo4 = luboMonto4[0];
    // luboTax4 = body[37].split('</td>');
    // luboIVA4 = luboTax4[0];
    // luboISR4 = body[38].split('</td>');
    // isrLubo4 = luboISR4[0];
    procesadorPagoDL4 = body[31].split('</td>');
    procesadorPago4 = procesadorPagoDL4[0];
    all4 = body[32].split('</td>');
    total4 = all4[0];
    //fila que se llena con los datos del quinto dia
    fecha5 = body[33].split('</td>');
    date5 = fecha5[0];
    travel5 = body[34].split('</td>');
    viaje5 = travel5[0];
    // tip5 = body[47].split('</td>');
    // propina5 = tip5[0];
    tax5 = body[35].split('</td>');
    monto5 = tax5[0];
    impuestoalValorAgregado5 = body[36].split('</td>');
    ivaDriver5 = impuestoalValorAgregado5[0];
    impuestoSobrelaRenta5 = body[37].split('</td>');
    isrDriver5 = impuestoSobrelaRenta5[0];
    luboMonto5 = body[38].split('</td>');
    lubo5 = luboMonto5[0];
    // luboTax5 = body[47].split('</td>');
    // luboIVA5 = luboTax5[0];
    // luboISR5 = body[48].split('</td>');
    // isrLubo5 = luboISR5[0];
    procesadorPagoDL5 = body[39].split('</td>');
    procesadorPago5 = procesadorPagoDL5[0];
    all5 = body[40].split('</td>');
    total5 = all5[0];
    //fila que se llena con los datos de la sexta semana
    fecha6 = body[41].split('</td>');
    date6 = fecha6[0];
    travel6 = body[42].split('</td>');
    viaje6 = travel6[0];
    // tip6 = body[58].split('</td>');
    // propina6 = tip6[0];
    tax6 = body[43].split('</td>');
    monto6 = tax6[0];
    impuestoalValorAgregado6 = body[44].split('</td>');
    ivaDriver6 = impuestoalValorAgregado6[0];
    impuestoSobrelaRenta6 = body[45].split('</td>');
    isrDriver6 = impuestoSobrelaRenta6[0];
    luboMonto6 = body[46].split('</td>');
    lubo6 = luboMonto6[0];
    // luboTax6 = body[57].split('</td>');
    // luboIVA6 = luboTax6[0];
    // luboISR6 = body[58].split('</td>');
    // isrLubo6 = luboISR6[0];
    procesadorPagoDL6 = body[47].split('</td>');
    procesadorPago6 = procesadorPagoDL6[0];
    all6 = body[48].split('</td>');
    total6 = all6[0];
    //fila que se llena con los datos del septimo dia
    fecha7 = body[49].split('</td>');
    date7 = fecha7[0];
    travel7 = body[50].split('</td>');
    viaje7 = travel7[0];
    // tip7 = body[69].split('</td>');
    // propina7 = tip7[0];
    tax7 = body[51].split('</td>');
    monto7 = tax7[0];
    impuestoalValorAgregado7 = body[52].split('</td>');
    ivaDriver7 = impuestoalValorAgregado7[0];
    impuestoSobrelaRenta7 = body[53].split('</td>');
    isrDriver7 = impuestoSobrelaRenta7[0];
    luboMonto7 = body[54].split('</td>');
    lubo7 = luboMonto7[0];
    // luboTax7 = body[67].split('</td>');
    // luboIVA7 = luboTax7[0];
    // luboISR7 = body[68].split('</td>');
    // isrLubo7 = luboISR7[0];
    procesadorPagoDL7 = body[55].split('</td>');
    procesadorPago7 = procesadorPagoDL7[0];
    all7 = body[56].split('</td>');
    total7 = all7[0];
    //Llenado del apartado de los totales
    let totalCount = document.getElementById("totales").innerHTML.split('<th>');
    totalesViajes = totalCount[1].split('</th>');
    totalViajeText = totalesViajes[0];
    //
    totalViaje = totalCount[2].split('</th>');
    totalViajeS = totalViaje[0];
    //
    // totalTip = totalCount[3].split('</th>');
    // propinaTotal = totalTip[0];
    //
    montoTotalD = totalCount[3].split('</th>');
    totalMontoD = montoTotalD[0];
    //
    totalMontoTaxD = totalCount[4].split('</th>');
    totalMontoIVAD = totalMontoTaxD[0];
    //
    totalMontoISRc = totalCount[5].split('</th>');
    totalISRd = totalMontoISRc[0];
    //
    totalMontoL = totalCount[6].split('</th>');
    totalLubo = totalMontoL[0];
    //
    // totalIVALubo = totalCount[7].split('</th>');
    // totalLuboIVA = totalIVALubo[0];
    // //
    // totalLuboISR = totalCount[8].split('</th>');
    // totalISRLubo = totalLuboISR[0];
    //
    procesadorPagoTotalS = totalCount[7].split('</th>');
    totalPPS = procesadorPagoTotalS[0];
    //
    montoTotal = totalCount[8].split('</th>');
    totalesM = montoTotal[0];
    //
    let cobroEfectivo = document.getElementById("cobrosEfectivo").innerHTML.split('<b>$</b>');
    cve = cobroEfectivo[1];
    let cobrotarjeta = document.getElementById("cobrosTrajeta").innerHTML.split('<b>$</b>');
    cvt = cobrotarjeta[1];
    let comisionLubo = document.getElementById("comisionesLubo").innerHTML.split('<b>$</b>');
    comLubo = comisionLubo[1];
    let procesadorPago = document.getElementById("procesadorPagos").innerHTML.split('<b>$</b>');
    procesadordePagos = procesadorPago[1];
    let subTotal = document.getElementById("subTotal").innerHTML.split('<b>$</b>');
    subTotal = subTotal[1];
    let impuestoalValorAgregado = document.getElementById("iva").innerHTML.split('<b>$</b>');
    totalIva = impuestoalValorAgregado[1];
    let impuestoSobrelaRenta = document.getElementById("isr").innerHTML.split('<b>$</b>');
    totalIsr = impuestoSobrelaRenta[1];


    // console.log(fechas, totalSemana)
    // console.log(body);
    // console.log(fecha1, tip1, tax1, luboTax1, all1);
    // console.log(date1, propina1, montoIVA1, luboIVA1, total1);
    // console.log(date2, propina2, montoIVA2, luboIVA2, total2);
    // console.log(date3, propina3, montoIVA3, luboIVA3, total3);
    // console.log(date4, propina4, montoIVA4, luboIVA4, total4);
    // console.log(totalCount);
    // console.log(totalViajeS, totalMontoIVA, totalLuboIVA, totalesM);
    console.log(cve, cvt, comLubo, procesadordePagos, totalIva, totalIsr)

    window.open(`Resumen-de-semana.php?fechas=${fechas}&fechaSemana=${fechaSemana}&totalS=${totalSemana}&date1=${date1}&viaje1=${viaje1}&monto1=${monto1}&ivaDriver1=${ivaDriver1}&isrDriver1=${isrDriver1}&lubo1=${lubo1}&procesadorPago1=${procesadorPago1}&total1=${total1}&date2=${date2}&viaje2=${viaje2}&monto2=${monto2}&ivaDriver2=${ivaDriver2}&isrDriver2=${isrDriver2}&lubo2=${lubo2}&procesadorPago2=${procesadorPago2}&total2=${total2}&date3=${date3}&viaje3=${viaje3}&monto3=${monto3}&ivaDriver3=${ivaDriver3}&isrDriver3=${isrDriver3}&lubo3=${lubo3}&procesadorPago3=${procesadorPago3}&total3=${total3}&date4=${date4}&viaje4=${viaje4}&monto4=${monto4}&ivaDriver4=${ivaDriver4}&isrDriver4=${isrDriver4}&lubo4=${lubo4}&procesadorPago4=${procesadorPago4}&total4=${total4}&date5=${date5}&viaje5=${viaje5}&monto5=${monto5}&ivaDriver5=${ivaDriver5}&isrDriver5=${isrDriver5}&lubo5=${lubo5}&procesadorPago5=${procesadorPago5}&total5=${total5}&date6=${date6}&viaje6=${viaje6}&monto6=${monto6}&ivaDriver6=${ivaDriver6}&isrDriver6=${isrDriver6}&lubo6=${lubo6}&procesadorPago6=${procesadorPago6}&total6=${total6}&date7=${date7}&viaje7=${viaje7}&monto7=${monto7}&ivaDriver7=${ivaDriver7}&isrDriver7=${isrDriver7}&lubo7=${lubo7}&procesadorPago7=${procesadorPago7}&total7=${total7}&totalViajeText=${totalViajeText}&totalViajeS=${totalViajeS}&totalMontoD=${totalMontoD}&totalMontoIVAD=${totalMontoIVAD}&totalISRd=${totalISRd}&totalLubo=${totalLubo}&totalPPS=${totalPPS}&totalesM=${totalesM}&cve=${cve}&cvt=${cvt}&comLubo=${comLubo}&procesadordePagos=${procesadordePagos}&subTotal=${subTotal}&totalIva=${totalIva}&totalIsr=${totalIsr}`, '_blank');
}


const datosBancarios = (idConductor) => {
    let sw = `${url}/infobancaria/listar/${idConductor}`;
    console.log(sw);
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (res["response"]) {
                let inf = res['result'];
                document.querySelector('#nombre_beneficiario').value = inf['Nombre_Beneficiario'];
                document.getElementById('nombre_banco').value = inf['Nombre_Banco'];
                document.getElementById('num_cuenta').value = inf['Num_Cuenta'];
                document.getElementById('num_tarjeta').value = inf['Num_Tarjeta']
                document.getElementById('clave_interbancaria').value = inf['Clave_Inter_Bancaria'];
                document.getElementById('idDatosBancarios').value = inf['idDatosBancarios'];
            } else {

            }
        });
}

const altaDatosBancarios = () => {
    if (confirm("Esta seguro de subir esta informacion Bancaria")) {
        let idDatosBancarios = document.getElementById("idDatosBancarios").value;
        let metodo = `POST`;
        let sw;
        if (idDatosBancarios == 0) {
            sw = `${url}infobancaria/registrar`;
        } else {
            metodo = `PUT`;
            sw = `${url}infobancaria/actualizar/${idDatosBancarios}`;
        }

        let parametros = {
            "Nombre_Beneficiario": document.querySelector('#nombre_beneficiario').value,
            "Nombre_Banco": document.getElementById("nombre_banco").value,
            "Num_Cuenta": document.getElementById("num_cuenta").value,
            "Num_Tarjeta": document.getElementById("num_tarjeta").value,
            "Clave_Inter_Bancaria": document.getElementById("clave_interbancaria").value,
            "idPersona": sessionStorage.getItem('IdPersona')
        }

        fetch(sw, {
                method: metodo,
                body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res["response"]) {
                    alert("Informacion dada de alta");
                } else {
                    alert("No se actualizo la informacion Bancaria");
                }
            });
    }
}