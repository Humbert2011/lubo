let  recuperar = (email) => {
    let direccion = url + 'auth/recuperarPassword'
    data = {
        email: email,
        TipoUsuario: sessionStorage.getItem('tipoUser')
    }
    // console.log(data)
    $.ajax({
        data: data,
        url: direccion,
        type: 'POST',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            document.getElementById('correo').disabled = true
            return true
        },
        success: function(data) {
            if (data['response']) {
                $('#mensaje').show(500)
                sec = 5; //timpo de envio
                setInterval(function() {
                    if (sec <= 0) {
                        $(location).attr('href', 'login.php')
                    }
                    document.getElementById('tiempo').innerHTML = sec + '(s)'
                    console.log(sec + '(s)')
                    sec = sec - 1
                }, 1000)
            } else {
                if (data['errors'].length == 0) {
                    alert(data['message'])
                } else {
                    alert(data['errors'])
                }
            }
            document.getElementById('correo').disabled = false
            return true
        }
    })
    // console.log(data)
        // alert('Recuperar')
    return true
}

let validarnumero = (telefono, codigo) => {
    // alert(telefono)
    // ajax de enviar codigo
    parametros = {
        'CodigoPais': codigo,
        'Telefono': telefono,
        "TipoUsuario": 3
    }

    let output = true;
    $(".signup-error").html('');

    if (!($("#numero1").val())) {
        output = false;
        $("#numero1").addClass('input-error');
    }

    let phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

    if (!$("#numero1").val().match(phoneno)) {
        $("#telefono-error").html("Teléfono inválido!");
        $("#numero1").addClass('input-error');
        output = false;
    }

    if (output == false) {
        console.log("Error");
    } else {
        console.log(parametros);
        $.ajax({
            data: parametros,
            url: url + 'auth/validarNumero',
            type: 'POST',
            dataType: 'json',
            cache: false,
            beforeSend: function() {
                //alert('Envindo Datos por ajax')botonderegistro
                document.getElementById('botonderegistro').disabled = true
                return true
            },
            success: function(data) {
                if (data['response']) {
                    $(location).attr('href', 'confirmacion.php?tel=' + telefono + '&codigo=' + codigo + '#frmValidate')
                } else {
                    alert(data['errors'])
                    if (data['message'] == "1") {
                        $(location).attr('href', '../login.php')
                    }
                }
                // return true
            }
        })
        document.getElementById('botonderegistro').disabled = false
    }


    // $(location).attr('href','confirmacion.php?tel='+telefono+'&codigo='+codigo+'#frmValidate')
}

let validarcodigo = (a, b, c, d, telefono,CodigoPais) => {
    // ajax de validar codigo
    let codigo = a + b + c + d;
    parametros = {
            "CodigoPais":CodigoPais,
            "Codigo": codigo,
            "Telefono": telefono
        }
        // auth/validarCodigo
    $.ajax({
        data: parametros,
        url: url + 'auth/validarCodigo',
        type: 'POST',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            //alert('Envindo Datos por ajax')
            return true
        },
        success: function(data) {
            if (data['response']) {
                $(location).attr('href', 'registro.php?telefono=' + telefono + '#registro')
            } else {
                alert(data['errors'])
            }
            return true
        }
    })
}
let enviado = 0

let registrar = (nombre, apellidos, email, password, telefono, sexo, idZona) => {
    if (enviado === 1) {
        return false
    }
    tycos = document.getElementById('tycos').checked;
    document.getElementById('boton').disabled = true
    let output = true;
    if (tycos) {
        tycos = 1
    } else {
        tycos = 0
        output = false;
        alert('¡Acépte los términos y condiciones!')
    }
    $(".signup-error").html('');
    if ($("#idZona").val() == 0) {
        output = false;
        let link = document.getElementById('zona-error');
        link.style.display = 'block';
        $("#zona-error").html("Zona requerida!");
        $("#idZona").addClass('input-error');
    }
    if (!($("#nombre").val())) {
        output = false;
        /*let link = document.getElementById('name-error');
        link.style.display = 'block';
        $("#name-error").html("Nombre requerido!");*/
        $("#nombre").addClass('input-error');
    }
    if (!($("#apellidos").val())) {
        output = false;
        /*let link = document.getElementById('apellidos-error');
        link.style.display = 'block';
        $("#apellidos-error").html("Apellidos requeridos!");*/
        $("#apellidos").addClass('input-error');
    }
    if (!($("#correo").val())) {
        output = false;
        /*let link = document.getElementById('email-error');
        link.style.display = 'block';
        $("#email-error").html("Correo requerido!");*/
        $("#correo").addClass('input-error');
    }
    if (!$("#correo").val().match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) {
        $("#email-error").html("Email inválido!");
        let link = document.getElementById('email-error');
        link.style.display = 'block';
        $("#email-error").html("Email no válido");
        $("#correo").addClass('input-error');
        output = false;
    }
    if (!($("#contraseña").val())) {
        output = false;
        /*zlet link = document.getElementById('password-error');
        link.style.display = 'block';
        $("#password-error").html("Contraseña - requerida!");*/
        $("#contraseña").addClass('input-error');
    }

    if (!document.querySelector('input[name="ritem"]:checked')) {
        alert('Elige un género');
        output = false;
    }

    if (output == false) {
        console.log("Error");
    } else {
        let parametros = {
            "Tipo_de_usuario": "3",
            "Nombre": nombre,
            "Apellidos": apellidos,
            "Email": email,
            "Password": password,
            "Telefono": telefono,
            "Sexo": sexo,
            "Status": "3",
            "idZona": idZona,
            "tycos": tycos
        }
        console.log(parametros);
        $.ajax({
            data: parametros,
            url: url + 'persona/registrarDriver',
            type: 'POST',
            dataType: 'json',
            cache: false,
            beforeSend: function() {
                return true
            },
            success: function(data) {
                console.log(data);
                if (data['response']) {
                    // autenticar
                    parametros = {
                        "Telefono": telefono,
                        "Password": password,
                        "Tipo_usuario": "3"
                    }
                    console.log(parametros);
                    $.ajax({
                        data: parametros,
                        url: url + 'auth/autenticar',
                        type: 'POST',
                        dataType: 'json',
                        cache: false,
                        beforeSend: function() {
                            return true
                        },
                        success: function(auth) {
                            console.log(auth);
                            if (auth['response']) {
                                // autenticar
                                // console.log(auth['result']['token'])
                                sessionStorage.setItem('Token', auth['result']['token']);
                                $(location).attr('href', 'registro-auto.php#registro')
                            } else {
                                if (auth['errors'].length == 0) {
                                    alert(auth['message'])
                                } else {
                                    alert(auth['errors'])
                                }
                            }

                            // return true

                        }
                    })
                } else {
                    if (data['errors'].length == 0) {
                        alert(data['message'])
                    } else {
                        alert(data['errors'])
                    }
                }
            }
        })
    }
    enviado = 0
    document.getElementById('boton').disabled = false
    return true
        // $(location).attr('href','registro-auto.php#registro')
}
let persona = {}

let validar = (token)=>{
    switch (token) {
        case null:
            $(location).attr('href', '../login.php')
            break;
    }
    $.ajax({
        // data:  parametros,
        url: url + 'auth/getData/' + token,
        type: 'GET',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            //alert('Envindo Datos por ajax')
            return true
        },
        success: function(auth) {
            if (auth['response'] && auth['result']['Tipo_de_usuario'] == 3) {
                // persona = auth['result']
                idPersona = auth['result']['id']
                persona = auth['result']
                sessionStorage.setItem('IdPersona', idPersona)
                    // console.log(persona)
                return true
            } else {
                $(location).attr('href', '../login.php')
            }
            return true
        }
    })
}

let registrarAuto = (placa, modelo, color, an, num, modo, pasajeros) => {

    let idPersona = sessionStorage.getItem('IdPersona');

    let parametros = {
            "idPersona": idPersona,
            "Placa": placa,
            "Modelo": modelo,
            "Color": color,
            "Año": an,
            "No_de_serie_del_vehiculo": num,
            "idModoTrabajo": modo,
            "Num_Pasajeros": pasajeros
        }
        // console.log(parametros);
        // alert('alta de auto')

    let output = true;
    $(".signup-error").html('');

    if (!($("#modelo").val())) {
        output = false;
        let link = document.getElementById('modelo-error');
        link.style.display = 'block';
        $("#modelo-error").html("Modelo requerido!");
        $("#modelo").addClass('input-error');
    }
    if (!($("#an").val())) {
        output = false;
        let link = document.getElementById('an-error');
        link.style.display = 'block';
        $("#an-error").html("Año requerido!");
        $("#an").addClass('input-error');
    }

    if (!($("#placas").val())) {
        output = false;
        let link = document.getElementById('placas-error');
        link.style.display = 'block';
        $("#placas-error").html("Placas requeridas!");
        $("#placas").addClass('input-error');
    }

    if (!($("#color").val())) {
        output = false;
        let link = document.getElementById('color-error');
        link.style.display = 'block';
        $("#color-error").html("Color requerido!");
        $("#color").addClass('input-error');
    }

    if (!($("#pasajeros").val())) {
        output = false;
        let link = document.getElementById('pasajeros-error');
        link.style.display = 'block';
        $("#pasajeros-error").html("Número requerido!");
        $("#pasajeros").addClass('input-error');
    }

    if (!($("#serie").val())) {
        output = false;
        let link = document.getElementById('serie-error');
        link.style.display = 'block';
        $("#serie-error").html("Serie requerida!");
        $("#serie").addClass('input-error');
    }

    if (!document.querySelector('input[name="ritem"]:checked')) {
        alert('Error, elige tu estilo de trabajo');
        output = false;
    }

    if (output == false) {
        // console.log("Error");
    } else {
        // alert("Hola");
        $.ajax({
            data: parametros,
            url: url + 'vehiculo/registrar',
            type: 'POST',
            dataType: 'json',
            cache: false,
            beforeSend: function() {
                document.getElementById('boton').disabled = true
                return true
            },
            success: function(auto) {
                if (auto['response']) {
                    // alert("Alta de auto")
                    $.ajax({
                        data: { "Status": 1 },
                        url: url + 'registro/actualizar/' + idPersona,
                        type: 'PUT',
                        dataType: 'json',
                        cache: false,
                        beforeSend: function() {
                            //alert('Envindo Datos por ajax')
                            return true
                        },
                        success: function(data) {
                            if (data['response']) {
                                alert('Auto dado de alta')
                                    // console.log(auto['result'])
                                document.getElementById('idAuto').value = auto['result']
                                autoReg = { "0": 0 }
                                document.getElementById('boton').disabled = true
                                $('#fotos').show()
                                    // $(location).attr('href','registro/registro-auto.php#registro')
                            } else {
                                if (data['errors'].length == 0) {
                                    alert(data['message'])
                                } else {
                                    alert(data['errors'])
                                }
                            }
                            return true
                        }
                    })
                } else {
                    if (auto['errors'].length == 0) {
                        alert(auto['message'])
                    } else {
                        alert(auto['errors'])
                    }
                    document.getElementById('boton').disabled = false
                }
                return true
            }
        })
    }

    // urlSW = url + tipoUrl

}
let autoReg

let obtenerAuto = (idPersona) => {
    $.ajax({
        data: { "Status": 1 },
        url: url + 'vehiculo/obtener/' + idPersona,
        type: 'GET',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            //alert('Envindo Datos por ajax')
            return true
        },
        success: function(data) {
            if (data['response']) {
                autoReg = data['result'];
                if (autoReg['0'] >= 3) {
                    registroUpdate()
                }
                document.getElementById('modelo').value = autoReg['Modelo']
                document.getElementById('an').value = autoReg['Año']
                document.getElementById('placas').value = autoReg['Placa']
                document.getElementById('color').value = autoReg['Color']
                document.getElementById('pasajeros').value = autoReg['Num_Pasajeros']
                document.getElementById('serie').value = autoReg['No_de_serie_del_vehiculo']
                document.getElementById('idAuto').value = autoReg['id_tbVehiculo']
                document.getElementById('boton').disabled = true
                for (let i = 0; i < autoReg[0]; i++) {
                    let el = i + 1
                    $('#img' + el).hide(500)
                }
                // console.log(autoReg['id_tbVehiculo'])
                if (autoReg['idModoTrabajo'] == 1) {
                    document.getElementById('ritema').checked = true;
                } else if (autoReg['idModoTrabajo'] == 2) {
                    document.getElementById('ritemb').checked = true;
                } else {
                    document.getElementById('ritemc').checked = true;
                }
                // obtener imagenes de auto
                return true
            } else {
                if (data['errors'].length == 0) {
                    // alert(data['message'])
                } else {
                    // alert(data['errors'])
                    $('#fotos').hide()
                }
            }
            return true
        }
    })
}

let login = (correo, password,tipoUser) => {
    sessionStorage.removeItem('Token');
    sessionStorage.removeItem('IdPersona');
    let parametros = {
            "Telefono": correo,
            "Password": password,
            "Tipo_usuario": tipoUser
        }
    // console.log(parametros)
    $.ajax({
        data: parametros,
        url: url + 'auth/autenticar',
        type: 'POST',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            document.getElementById('entrar').disabled = true
            return true
        },
        success: function(auth) {
            document.getElementById('entrar').disabled = false
            if (auth['response']) {
                // autenticar
                sessionStorage.setItem('Token', auth['result']['token'])
                
                validar(auth['result']['token'])

                let id = auth['result']['persona']; //persona

                $.ajax({
                    data: parametros,
                    url: url + 'registro/obtener/' + id,
                    type: 'GET',
                    dataType: 'json',
                    cache: false,
                    beforeSend: function() {
                        return true
                    },
                    success: function(data) {

                        if (data['response']) {
                            let result = data['result']
                            let status = result['Status']
                            if (status <= 1) {
                                $(location).attr('href', 'registro-conductor/registro-auto.php#registro')
                            } else {
                                $(location).attr('href', 'registro-conductor/mis-documentos.php#documentos')
                            }
                        } else {
                            if (data['errors'].length == 0) {
                                alert(data['message'])
                            } else {
                                alert(data['errors'])
                            }
                        }
                        return true
                    }
                });

            } else {
                // console.log(auth['errors'].length)
                if (auth['errors'].length == 0) {
                    alert(auth['message'])
                } else {
                    alert(auth['errors'])
                }
            }
            return true
        }
    })
}

let paso2 = () => {
    autoReg['0']++;
    // console.log(autoReg['0'])
    if (autoReg[0] > 2) {
        registroUpdate()
    }
}

let registroUpdate = () => {
    let idPersona = sessionStorage.getItem('IdPersona')
    $.ajax({
        data: { "Status": 2 },
        url: url + 'registro/actualizar/' + idPersona,
        type: 'PUT',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            return true
        },
        success: function(data) {
            if (data['response']) {
                alert('Fotos completas')
                $(location).attr('href', 'mis-documentos.php#documentos')
            } else {
                if (data['errors'] == null) {
                    alert(data['message'])
                } else {
                    alert(data['errors'])
                }
            }
            return true
        }
    })
}

let imgVehiculo = (id, form, num) => {
    $.ajax({
        type: 'POST',
        url: url + 'img/cargar/vehiculo/' + id,
        data: new FormData(form),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            document.getElementById('button' + num).disabled = true
            return true
        },
        success: function(msg) {
            if (msg['response']) {
                $('#img' + num).hide(500)
                paso2()
            } else {
                document.getElementById('button' + num).disabled = false
                if (msg['errors'].length == 0) {
                    alert(msg['message'])
                } else {
                    alert(msg['errors'])
                }
            }
            document.getElementById('button' + num).disabled = false
            return true
        }
    });
}

let valId =(userId)=> {
    // auth/validaId/"+idUser;
    $.ajax({
        type: 'GET',
        url: url + "auth/validaId/" + userId,
        // data: new FormData(form),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            return true
        },
        success: function(msg) {
            console.log(msg)
            if (msg['response']) {
                // $('#img'+num).hide(500)
                // paso2()
                document.getElementById('correo').innerHTML = msg['result']['email']
            } else {
                if (msg['errors'].length == 0) {
                    // alert(msg['message'])
                    error = msg['message']
                } else {
                    // alert(msg['errors'])
                    error = msg['errors']
                }
                document.getElementById('error').innerHTML = error
                document.getElementById('Aceptar').disabled = true
                document.getElementById('codigo').disabled = true
            }
            return true
        }
    });

}

let valIdCod = (userId, codigo) => {
    // auth/validaCodigoWeb/"+idUser
    parametros = {
        "codigo": codigo
    }
    if (codigo.length == 6) {
        $.ajax({
            data: parametros,
            url: url + "auth/validaCodigoWeb/" + userId,
            type: 'POST',
            dataType: 'json',
            cache: false,
            beforeSend: function() {
                document.getElementById('Aceptar').disabled = true
                return true
            },
            success: function(msg) {
                // console.log(msg)
                if (msg['response']) {
                    sessionStorage.setItem('msg', userId)
                    $(location).attr('href', 'password.php')
                } else {
                    if (msg['errors'].length == 0) {
                        // alert(msg['message'])
                        error = msg['message']
                    } else {
                        // alert(msg['errors'])
                        error = msg['errors']
                    }
                    document.getElementById('error').innerHTML = error
                        // document.getElementById('Aceptar').disabled = true
                    document.getElementById('codigo').style = 'border: red 2px solid;'
                }
                document.getElementById('Aceptar').disabled = false
                return true
            }
        });
    } else {
        document.getElementById('error').innerHTML = 'Formato de código incorrecto'
            // document.getElementById('Aceptar').disabled = true
        document.getElementById('codigo').style = 'border: red 2px solid;'
    }
    document.getElementById('Aceptar').disabled = false
}

let updatePass = (password, idUser) => {
    if (password.length < 6) {
        document.getElementById('alerta1').innerHTML = 'Password incorrecta debe de contener un minimo de 6 elementos'
        document.getElementById('password-1').style = 'border: red 2px solid;'
        return true
    } else {
        parametros = {
            "Password": password
        }
        console.log(parametros)
        $.ajax({
            data: parametros,
            url: url + "auth/actualizaPass/" + idUser,
            type: 'PUT',
            dataType: 'json',
            cache: false,
            beforeSend: function() {
                document.getElementById('update').disabled = true
                return true
            },
            success: function(msg) {
                console.log(msg);
                if (msg['response']) {
                    $(location).attr('href', 'solicitudexitosa.php')
                }
            }
        })
    }
}

let listarZonas = () => {
    sw = `${url}zona/listar`
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            let data = null;
            let htmlInsert = '';
            let ini = `<select id="idZona" name = "idZona" class="campotexto" placeholder="Zona" style="font-size: 18px;
            padding: 8px 8px 8px 5px;
            -webkit-appearance: none;
            display: block;
            background: white;
            color: #636363;
            width: 80%;
            margin-left: 10%;
            border-radius: 5px;
            border: 1px solid #ECEAEA;
            box-shadow: 3px 3px 5px #B9B7B7;
        " > <option value='0'>Selecciona una zona ---></option>`
            if (res['total'] > 0) {
                data = res['data']
                // console.log(data)
                data.forEach(element => {
                    // console.log(element['Descripcion'])
                    htmlInsert += `<option value = "${element['idZona']}">${element['Descripcion']}</option>`
                });
            }
            let fin = '</select>'
            document.getElementById('zona').innerHTML = ini + htmlInsert + fin

            // console.log(res);
        });
}