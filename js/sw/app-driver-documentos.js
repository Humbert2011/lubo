const obtCar = (idPersona) => {
    let sw = `${url}vehiculo/obtener/${idPersona}`;
    fetch(sw, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(res => {
            let idV = res.result.id_tbVehiculo;
            console.log("Id del vehiculo: " + idV);
            console.log("Id del conductor: " + idPersona);
            listarDocumentosAuto(idV);
        })
}


const listarDocumentosAuto = (idVehiculo) => {
    //let id = 20;// id del vehiculo
    console.log(idVehiculo);
    let sw = `${url}img/listarDA/${idVehiculo}`;
    let modeloAuto = new URLSearchParams(location.search);
    let modAut = modeloAuto.get('id');
    //console.log(modAut);
    //console.log(sw);
    fetch(sw, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json())
        .then(res => {

            cuerpo4 = document.getElementById('documentosAutoR1');
            if (res != null) {
                //console.log(data);
                let data = res.result;
                let documentosAutoArreglo;
                let status;
                let i = 0;
                data.forEach(element => {
                    // console.log(data.Modelo);
                    documentosAutoArreglo = Object.values(data[i]);
                    if (documentosAutoArreglo[0] == null) { //cambiar la condicion a !=
                        //console.log("HAY DOCUMENTO");
                        status = documentosAutoArreglo[4]
                        //console.log("Tipo de documento: " + documentosAutoArreglo[5] +
                        //  "status: " + documentosAutoArreglo[4]);

                        if (status == 3) {
                            //console.log("Rechazado");

                            let fila = document.createElement(`div`)
                            fila.classList.add('col-sm-12');
                            fila.classList.add('col-md-12');
                            fila.classList.add('col-lg-4');
                            fila.innerHTML =
                                `<center>
                                                    <br>
                                                    <form id="documento`+ documentosAutoArreglo[1] + `">
                                                        <div class="clase">
                                                            <div id="barraEstatus`+ documentosAutoArreglo[1] + `" 
                                                                class="box stack-top">Rechazado</div>
                                                            <input id="img`+ documentosAutoArreglo[1] + `" type="file" name="img"
                                                            style="width : 170px; heigth : auto; margin-top:30px;"    
                                                            onchange='cambio("tit`+ documentosAutoArreglo[1] + `",` + documentosAutoArreglo[1] + `,` + 3 + `)' class="documentoAlta">
                                                            <button id="tramite`+ documentosAutoArreglo[1] + `"  type="button"
                                                                class="btn-circle-error" disabled="true"
                                                                style="margin-top: 35px;"><i id="icono`+ documentosAutoArreglo[1] + `"
                                                                    class="material-icons"
                                                                    style="margin-left: -2px; margin-top: 2px;">warning</i></button>
                                                            <div class="card-body">
                                                                <h7 class="card-title"><b>`+ documentosAutoArreglo[5] + `</b></h7>
                                                                <br>
                                                                <p id="tit`+ documentosAutoArreglo[1] + `">Elije otro archivo.</p>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <br>
                                                    <button id="bt`+ documentosAutoArreglo[1] + `" type="button"
                                                        class="btn btnsubir color-pink-background h"
                                                        onclick="cargarDocumento(`+ documentosAutoArreglo[2] + `,` + documentosAutoArreglo[1] + `,document.getElementById('documento` + documentosAutoArreglo[1] + `'), ` + 3 + `)">
                                                        <b class="texto">Subir</b>
                                                    </button>
                                                    <br>
                                                </center>
                                            </div>`
                            cuerpo4.appendChild(fila);


                        } else if (status == 1) {
                            //console.log("Aceptado");
                            let fila = document.createElement(`div`)
                            fila.classList.add('col-sm-12');
                            fila.classList.add('col-md-12');
                            fila.classList.add('col-lg-4');
                            fila.innerHTML =
                                `<center>
                                                    <br>
                                                    <form id="documento`+ documentosAutoArreglo[1] + `">
                                                        <div class="clase">
                                                            
                                                            <input id="img`+ documentosAutoArreglo[1] + `" type="file" name="img"
                                                            disabled="true"    
                                                            onchange='cambio("tit`+ documentosAutoArreglo[1] + `",` + documentosAutoArreglo[1] + `,` + 3 + `)' class="documentoAlta">
                                                            <button id="tramite`+ documentosAutoArreglo[1] + `"  type="button"
                                                                class="btn-circle1 color" disabled="true"
                                                                style="margin-top: 35px;"><i id="icono`+ documentosAutoArreglo[1] + `"
                                                                    class="material-icons"
                                                                    style="margin-left: -2px; margin-top: 2px;">done</i></button>
                                                            <div class="card-body">
                                                                <h7 class="card-title"><b>`+ documentosAutoArreglo[5] + `</b></h7>
                                                                
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <br>
                                                   
                                                    <br>
                                                </center>
                                            </div>`
                            cuerpo4.appendChild(fila);

                        } else if (status == 2) {
                            //console.log("No ha subido archivo");
                            let fila = document.createElement(`div`)
                            fila.classList.add('col-sm-12');
                            fila.classList.add('col-md-12');
                            fila.classList.add('col-lg-4');
                            fila.innerHTML =
                                `<center>
                                                    <br>
                                                    <form id="documento`+ documentosAutoArreglo[1] + `">
                                                        <div class="clase">
                                                            <div id="barraEstatus`+ documentosAutoArreglo[1] + `" hidden="false"
                                                                class="box stack-top">No hay archivos cargados</div>
                                                            <input id="img`+ documentosAutoArreglo[1] + `" type="file" name="img"
                                                            style="width : 170px; heigth : auto; margin-top:30px;"   
                                                            onchange='cambio("tit`+ documentosAutoArreglo[1] + `",` + documentosAutoArreglo[1] + `,` + 3 + `)' class="documentoAlta">
                                                            <button id="tramite`+ documentosAutoArreglo[1] + `" hidden="false" type="button"
                                                                class="btn-circle-error" disabled="true"
                                                                style="margin-top: 35px;"><i id="icono`+ documentosAutoArreglo[1] + `"
                                                                    class="material-icons"
                                                                    style="margin-left: -2px; margin-top: 2px;">warning</i></button>
                                                            <div class="card-body">
                                                                <h7 class="card-title"><b>`+ documentosAutoArreglo[5] + `</b></h7>
                                                                <br>
                                                                <p id="tit`+ documentosAutoArreglo[1] + `">Arrastra tu archivo o da click en esta
                                                                    área.</p>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <br>
                                                    <button id="bt`+ documentosAutoArreglo[1] + `" type="button"
                                                        class="btn btnsubir color-pink-background h"
                                                        onclick="cargarDocumento(`+ documentosAutoArreglo[2] + `,` + documentosAutoArreglo[1] + `,document.getElementById('documento` + documentosAutoArreglo[1] + `'), ` + 3 + `)">
                                                        <b class="texto">Subir</b>
                                                    </button>
                                                    <br>
                                                </center>
                                            </div>`
                            cuerpo4.appendChild(fila);

                        } else if (status == 0) {
                            //console.log("pendiente de validacion");
                            let fila = document.createElement(`div`)
                            fila.classList.add('col-sm-12');
                            fila.classList.add('col-md-12');
                            fila.classList.add('col-lg-4');
                            fila.innerHTML =
                                `<center>
                                                    <br>
                                                    <form id="documento`+ documentosAutoArreglo[1] + `">
                                                        <div class="clase">
                                                        <div id="barraEstatus`+ documentosAutoArreglo[1] + `"
                                                        class="box stack-top">Pendiente de validacion</div>
                                                            <input id="img`+ documentosAutoArreglo[1] + `" type="file" name="img"
                                                            disabled="true"   
                                                            onchange='cambio("tit`+ documentosAutoArreglo[1] + `",` + documentosAutoArreglo[1] + `,` + 3 + `)' class="documentoAlta">
                                                            <button id="tramite`+ documentosAutoArreglo[1] + `"  type="button"
                                                                class="btn-circle1 color" disabled="true"
                                                                style="margin-top: 35px;"><i id="icono`+ documentosAutoArreglo[1] + `"
                                                                    class="material-icons"
                                                                    style="margin-left: -2px; margin-top: 2px;">done</i></button>
                                                            <div class="card-body">
                                                                <h7 class="card-title"><b>`+ documentosAutoArreglo[5] + `</b></h7>
                                                                
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <br>
                                                   
                                                    <br>
                                                </center>
                                            </div>`
                            cuerpo4.appendChild(fila);
                        }

                    } else {
                        // console.log("NO hay documento");
                    }
                    i++;
                })

            } else {
                // console.log("Algo anda mal");
            }
            if (document.getElementById('IdModeloAuto') != null) {
                document.getElementById('IdModeloAuto').innerHTML = `${modAut}`;
            }
        })

}



const listarDocumentosPersona = () => {
    //let id = 246;
    let id = sessionStorage.getItem('IdPersona');
    // console.log(id);
    let sw = `${url}img/listarDoc/${id}`;
    //console.log(sw);


    fetch(sw, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(res => {
            //console.log(res.result[0]);

            //cuerpo = document.getElementById('documentosPersona');
            cuerpo1 = document.getElementById('documentosPersonaR1');
            cuerpo2 = document.getElementById('documentosPersonaR2');
            cuerpo3 = document.getElementById('documentosPersonaR3');
            //console.log(res);
            if (res != null) {
                let data = res.result;
                //console.log(data);
                let todosDocumentos;
                //console.log(todosDocumentos);
                let i = 0;
                let status;

                data.forEach(element => {
                    todosDocumentos = Object.values(data[i]); //todo lo que hay en cada dato del result
                    // console.log("Tipos de documento: " + todosDocumentos[1]+" "+todosDocumentos[5]);

                    let corrigeEscritura;
                    if (todosDocumentos[1] == 7) {
                        corrigeEscritura = 'INE atrás';
                    } else if (todosDocumentos[1] == 8) {
                        corrigeEscritura = 'Licencia de conducir';
                    } else if (todosDocumentos[1] == 10) {
                        corrigeEscritura = 'Comprobante de domicilio';
                    } else {
                        corrigeEscritura = todosDocumentos[5];
                    }

                    //console.log("Corrige escrituda con un valor de: " + corrigeEscritura);
                    if (todosDocumentos[0] != null) { //si hay documento



                        if (todosDocumentos[4] == 3) { //Que hacer cuando está rechazado
                            //console.log(todosDocumentos[5] + ' : rechazado');
                            status = 0;
                            ///INICIA rechazado

                            let fila = document.createElement(`div`)
                            fila.classList.add('col-sm-12');
                            fila.classList.add('col-md-12');
                            fila.classList.add('col-lg-4');
                            fila.innerHTML = `
                        
                            <center>
                                <br>
                                <form id="documento`+ todosDocumentos[1] + `">
                                    <div class="clase">
                                        <div id="barraEstatus`+ todosDocumentos[1] + `" 
                                            class="box stack-top">Rechazado
                                        </div>
                                        <input id="img`+ todosDocumentos[1] + `" type="file" name="img" 
                                        style="width : 170px; heigth : auto; margin-top:30px;"
                                        onchange='cambio("tit`+ todosDocumentos[1] + `", ` + todosDocumentos[1] + `, ` + 3 + `)' 
                                        class="documentoAlta"
                                        >
                                        <button id="tramite`+ todosDocumentos[1] + `" type="button" class="btn-circle-error"
                                            style="margin-top: 35px;">
                                            <i id="icono`+ todosDocumentos[1] + `" class="material-icons"
                                                style="margin-left: -2px; margin-top: 2px;">
                                                warning
                                            </i>
                                        </button>
                                        <div class="card-body">
                                            <h7 class="card-title"><b>`+ corrigeEscritura + `</b></h7>
                                            <br>
                                            <p id="tit`+ todosDocumentos[1] + `"   style="width : 130px; heigth : 24px; font-size:15px; line-height: 150%;">Elije otro archivo.</p>
                                        </div>
                                    </div>
                                </form>
                                <br>

                                <button id="bt`+ todosDocumentos[1] + `" type="button"
                                    class="btn btnsubir color-pink-background h"
                                    
                                    onclick="cargarDocumento(`+ todosDocumentos[2] + `,` + todosDocumentos[1] + `,document.getElementById('documento` + todosDocumentos[1] + `'), ` + 3 + `)">

                                    <b class="texto">Subir</b>
                                    <br>
                            </center>
                        </div>`  //cambio de row

                            if (todosDocumentos[1] == 6 || todosDocumentos[1] == 7 || todosDocumentos[1] == 11) {
                                cuerpo1.appendChild(fila);
                            } else if (todosDocumentos[1] == 9 || todosDocumentos[1] == 5 || todosDocumentos[1] == 10) {
                                cuerpo2.appendChild(fila);
                            } else {
                                cuerpo3.appendChild(fila);

                            }


                            //TERMINA RECHAZADO



                        } else if (todosDocumentos[4] == 1) { // que hacer cuando está aceptado
                            // console.log(todosDocumentos[5] + ' : aceptado');

                            status = 1;

                            ///inicia aceptado
                            let fila = document.createElement(`div`)
                            fila.classList.add('col-sm-12');
                            fila.classList.add('col-md-12');
                            fila.classList.add('col-lg-4');
                            fila.innerHTML = `
                                                    
                                                        <center>
                                                            <br>
                                                            <form id="documento`+ todosDocumentos[1] + `">
                                                                <div class="clase">
                                                                   
                                                                    
                                                                    <input id="img`+ todosDocumentos[1] + `" type="file" name="img"
                                                                    disabled="true"    
                                                                    onchange='cambio("tit`+ todosDocumentos[1] + `", ` + todosDocumentos[1] + `, ` + status + `)' class="documentoAlta">
                                                                    <button id="tramite`+ todosDocumentos[1] + `" type="button" class="btn-circle1  color"
                                                                        disabled="true"
                                                                        style="margin-top: 35px;">
                                                                        <i id="icono`+ todosDocumentos[1] + `" class="material-icons"
                                                                            style="margin-left: -2px; margin-top: 2px;">
                                                                            done
                                                                        </i></button>
                            
                                                                    <div class="card-body">
                                                                        <h7 class="card-title"><b>`+ corrigeEscritura + `</b></h7>
                            
                                                                        <p id="tit`+ todosDocumentos[1] + `"></p>
                                                                        <br>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            <br>
                            
                                                           
                                                        </center>
                                                    </div>`
                            if (todosDocumentos[1] == 6 || todosDocumentos[1] == 7 || todosDocumentos[1] == 11) {
                                cuerpo1.appendChild(fila);
                            } else if (todosDocumentos[1] == 9 || todosDocumentos[1] == 5 || todosDocumentos[1] == 10) {
                                cuerpo2.appendChild(fila);
                            } else {
                                cuerpo3.appendChild(fila);

                            }

                            //////termina aceptado
                        } else if (todosDocumentos[4] == 2) { // que hacer cuando no se ha subido
                            // console.log(todosDocumentos[5] + ' : No ha subido archivo');
                            status = 2;

                            ///INICIA no ha subido
                            let fila = document.createElement(`div`)
                            fila.classList.add('col-sm-12');
                            fila.classList.add('col-md-12');
                            fila.classList.add('col-lg-4');
                            fila.innerHTML = `
                        
                            <center>
                                <br>
                                <form id="documento`+ todosDocumentos[1] + `">
                                    <div class="clase">
                                        <div id="barraEstatus`+ todosDocumentos[1] + `" 
                                            hidden= "true" class="box stack-top">No hay archivos cargados</div>
                                        
                                        <input id="img`+ todosDocumentos[1] + `" type="file" name="img"
                                        style="width : 170px; heigth : auto; margin-top:30px;"    
                                        onchange='cambio("tit`+ todosDocumentos[1] + `", ` + todosDocumentos[1] + `,` + 3 + `)' class="documentoAlta">
                                        <button id="tramite`+ todosDocumentos[1] + `" type="button" class="btn-circle-error"
                                            disabled="true" hidden="false"
                                            style="margin-top: 35px;">
                                            <i id="icono`+ todosDocumentos[1] + `" class="material-icons"
                                                style="margin-left: -2px; margin-top: 2px;">
                                                warning
                                            </i></button>

                                        <div class="card-body">
                                            <h7 class="card-title"><b>`+ corrigeEscritura + `</b></h7>

                                            <p id="tit`+ todosDocumentos[1] + `" style="width : 130px; heigth : 24px; font-size:15px; line-height: 150%;"><br>Arrastra tu archivo o da click en esta
                                                área.</p>
                                            <br>
                                        </div>
                                    </div>
                                </form>
                                <br>

                                <button id="bt`+ todosDocumentos[1] + `" type="button"
                                    class="btn btnsubir color-pink-background h"
                                    onclick="cargarDocumento(`+ todosDocumentos[2] + `,` + todosDocumentos[1] + `,document.getElementById('documento` + todosDocumentos[1] + `'), ` + 3 + `)">

                                    <b class="texto">Subir</b>
                                    <br>
                            </center>
                        </div>`
                            if (todosDocumentos[1] == 6 || todosDocumentos[1] == 7 || todosDocumentos[1] == 11) {
                                cuerpo1.appendChild(fila);
                            } else if (todosDocumentos[1] == 9 || todosDocumentos[1] == 5 || todosDocumentos[1] == 10) {
                                cuerpo2.appendChild(fila);
                            } else {
                                cuerpo3.appendChild(fila);

                            }
                            /// TERMINA no ha subido

                        } else if (todosDocumentos[4] == 0) { // que hacer cuando está pendiente de validacion
                            // console.log(todosDocumentos[5] + ' : Pendiente de validacion');

                            status = 1;

                            ///inicia pendiente de validacion
                            let fila = document.createElement(`div`)
                            fila.classList.add('col-sm-12');
                            fila.classList.add('col-md-12');
                            fila.classList.add('col-lg-4');
                            fila.innerHTML = `
                                                    
                                                        <center>
                                                            <br>
                                                            <form id="documento`+ todosDocumentos[1] + `">
                                                                <div class="clase">
                                                                <div id="barraEstatus`+ todosDocumentos[1] + `" 
                                                                class="box stack-top">Pendiente de validacion</div>
                                                                    
                                                                    <input id="img`+ todosDocumentos[1] + `" type="file" name="img"
                                                                    disabled="true"    
                                                                    onchange='cambio("tit`+ todosDocumentos[1] + `", ` + todosDocumentos[1] + `, ` + status + `)' class="documentoAlta">
                                                                    <button id="tramite`+ todosDocumentos[1] + `" type="button" class="btn-circle1  color"
                                                                        disabled="true"
                                                                        style="margin-top: 35px;">
                                                                        <i id="icono`+ todosDocumentos[1] + `" class="material-icons"
                                                                            style="margin-left: -2px; margin-top: 2px;">
                                                                            done
                                                                        </i></button>
                            
                                                                    <div class="card-body">
                                                                        <h7 class="card-title"><b>`+ corrigeEscritura + `</b></h7>
                            
                                                                        <p id="tit`+ todosDocumentos[1] + `"></p>
                                                                        <br>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            <br>
                            
                                                           
                                                        </center>
                                                    </div>`
                            if (todosDocumentos[1] == 6 || todosDocumentos[1] == 7 || todosDocumentos[1] == 11) {
                                cuerpo1.appendChild(fila);
                            } else if (todosDocumentos[1] == 9 || todosDocumentos[1] == 5 || todosDocumentos[1] == 10) {
                                cuerpo2.appendChild(fila);
                            } else {
                                cuerpo3.appendChild(fila);

                            }

                            //////termina pendiente de validacion
                        }





                    } else {
                        //console.log(todosDocumentos[5] + ' : NO HAY DOCUMENTO CARGADO');
                        //console.log("NO hay");
                        let fila = document.createElement(`div`)
                        fila.classList.add('col-sm-12');
                        fila.classList.add('col-md-12');
                        fila.classList.add('col-lg-4');
                        fila.innerHTML = `
                        
                            <center>
                                <br>
                                <form id="documento`+ todosDocumentos[1] + `">
                                    <div class="clase">
                                        <div id="barraEstatus`+ todosDocumentos[1] + `" 
                                            hidden= "true" class="box stack-top">No hay archivos cargados</div>
                                        
                                        <input id="img`+ todosDocumentos[1] + `" type="file" name="img"
                                        style="width : 170px; heigth : auto; margin-top:30px;"   
                                        onchange='cambio("tit`+ todosDocumentos[1] + `", ` + todosDocumentos[1] + `,` + 3 + `)' class="documentoAlta">
                                        <button id="tramite`+ todosDocumentos[1] + `" type="button" class="btn-circle-error"
                                            disabled="true" hidden="false"
                                            style="margin-top: 35px;">
                                            <i id="icono`+ todosDocumentos[1] + `" class="material-icons"
                                                style="margin-left: -2px; margin-top: 2px;">
                                                warning
                                            </i></button>

                                        <div class="card-body">
                                            <h7 class="card-title"><b>`+ corrigeEscritura + `</b></h7>

                                            <p id="tit`+ todosDocumentos[1] + `" style="width : 130px; heigth : 24px; font-size:15px; line-height: 150%;"><br>Arrastra tu archivo o da click en esta
                                                área.</p>
                                            <br>
                                        </div>
                                    </div>
                                </form>
                                <br>

                                <button id="bt`+ todosDocumentos[1] + `" type="button"
                                    class="btn btnsubir color-pink-background h"
                                    onclick="cargarDocumento(`+ todosDocumentos[2] + `,` + todosDocumentos[1] + `,document.getElementById('documento` + todosDocumentos[1] + `'), ` + 3 + `)">

                                    <b class="texto">Subir</b>
                                    <br>
                            </center>
                        </div>`
                        if (todosDocumentos[1] == 6 || todosDocumentos[1] == 7 || todosDocumentos[1] == 11) {
                            cuerpo1.appendChild(fila);
                        } else if (todosDocumentos[1] == 9 || todosDocumentos[1] == 5 || todosDocumentos[1] == 10) {
                            cuerpo2.appendChild(fila);
                        } else {
                            cuerpo3.appendChild(fila);

                        }
                        /// no hay documento
                    }


                    i++;
                });
            } else {

            }

        })

}

const cargarDocumento = (id, tipo, form, status) => {
    /////////
    $.ajax({
        type: 'POST',
        url: url + 'img/cargar/' + tipo + '/' + id,
        data: new FormData(form),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            document.getElementById('bt' + tipo).disabled = true
            document.getElementById('img' + tipo).disabled = true
            return true
        },
        success: function (msg) {

            var b = document.getElementById('tramite' + tipo);
            var barraStatuts = document.getElementById('barraEstatus' + tipo);
            var i = document.getElementById('icono' + tipo);
            barraStatuts.removeAttribute("hidden", "false");
            b.removeAttribute("hidden", "false");
            if (msg['response']) {
                alert("Tu imagen ha sido subida satisfactoriamente");

                document.getElementById('tramite' + tipo).style.display = 'inline'
                document.getElementById('tit' + tipo).innerHTML = "Archivo Cargado"


                b.setAttribute("class", "btn-circle1 color");


                if (status == 0) {

                    barraStatuts.innerHTML = "Rechazado";
                    b.setAttribute("class", "btn-circle-error");
                    i.innerHTML = "warning"

                } else if (status == 1) {
                    barraStatuts.innerHTML = "Aceptado";
                    i.innerHTML = "done"
                } else if (status == 2) {
                    barraStatuts.innerHTML = "No ha subido archivos";
                    b.setAttribute("class", "btn-circle-error");
                    i.innerHTML = "warning"
                } else {
                    barraStatuts.innerHTML = "Pendiente de validacion";
                    i.innerHTML = "done"
                }


                //console.log(url);
                return true
            } else {

                if (msg['errors'] == null) {
                    alert(msg['message'])
                    console.log("No hay error");

                } else {
                    alert("No se encontró el archivo")
                    b.removeAttribute("hidden", "false");
                    b.setAttribute("class", "btn-circle-error");
                    i.innerHTML = "warning"
                    barraStatuts.innerHTML = "Elije un archivo";

                }

                document.getElementById('bt' + tipo).disabled = false
                document.getElementById('img' + tipo).disabled = false
                return true
            }
        }
    })
    ////////

}



const cambio = (p, tipo, status) => {
    document.getElementById(p).innerHTML = "Archivo Cargado"
    var b = document.getElementById('tramite' + tipo);
    var i = document.getElementById('icono' + tipo);
    var barraStatuts = document.getElementById('barraEstatus' + tipo);
    b.removeAttribute("hidden", "false");
    barraStatuts.removeAttribute("hidden", "false");




    if (status == 3) { //rechazado
        b.setAttribute("class", "btn-circle-error");
        i.innerHTML = "warning"
    } else if (status == 1) { //aceptado
        b.setAttribute("class", "btn-circle1 color");
        i.innerHTML = "done"
    } else if (status == 2) { //No ha subido
        b.setAttribute("class", "btn-circle-error");
        i.innerHTML = "warning"
    } else {//pendiente
        b.setAttribute("class", "btn-circle1 color");
        i.innerHTML = "done"
    }
    barraStatuts.innerHTML = "click en SUBIR"
}




const obtenerConductor = () => {
    //let id = 244;
    //console.log(id);

    let id = sessionStorage.getItem('IdPersona');
    let sw = `${url}persona/obtener/${id}`;
    fetch(sw, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(res => {
            obtCar(id);
        })
}



