const auth = (correo, password, Tipo_usuario) => {
    event.preventDefault()
    sessionStorage.removeItem('Token');
    let sw = `${url}auth/autenticar`;
    sessionStorage.setItem('Tipo_usuario', Tipo_usuario);
    let parametros = {
        "Telefono": correo,
        "Password": password,
        "Tipo_usuario": Tipo_usuario
    }
    fetch(sw, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(parametros), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res['response']) {
                console.log(res['result']['token'])
                sessionStorage.setItem('Token', res['result']['token']);
                console.log(res['result']['persona']);
                sessionStorage.setItem('idUser', res['result']['persona']);
                console.log(res['result']['persona']);
                // sessionStorage.setItem('EmailStatus', res['result']['persona']);
                self.location = "./user/mis-viajes.html"
            } else {
                if (res['errors'].length == 0) {
                    alert(res['message'])
                } else {
                    alert(res['errors'])
                }
            }
        });
};

const validar = () => {
    let token = sessionStorage.getItem('Token');
    sw = `${url}auth/getData/${token}`;
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response && res['result']['Tipo_de_usuario'] == 2) {
                sessionStorage.setItem('EmailStatus', res.result['EmailStatus']);
                sessionStorage.setItem('EmailUser', res.result['Email'])
                console.log(res)
            } else {
                self.location = '../login-user.php'
            }
        });
}

const mapaDetalleViaje = () => {
    let sw = `${url}reportes/verMapaDetalleViaje/20.173481,-98.051702/20.161060,-98.042955`;
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response) {
                document.getElementById('card-map').innerHTML = `<img src="${res.result}" />`;
            } else {
                alert("Error al cargar mapa!");
            }
        });

};

let limit = 10;
let offset = 0;
let total = 0;
const mostrarMenos = (tipoViajes = 7) => {
    //alert('mostrar mas');
    limit += 10;
    //offset = 10;
    console.log('limit=' + limit + ', offset=' + offset);
    console.log(total);
    if (total >= limit) {
        listarViajes(`${tipoViajes }`);

    } else {
        alert("Este usurio no tiene mas viajes");
        limit = limit - 10;


    };
    // console.log(listarViajes());
};;

const listarViajes = (tipoViajes = 7) => {
    // console.log('limit=' + limit + ', offset=' + offset);
    let id = sessionStorage.getItem('idUser');

    let sw = `${url}viaje/historialViajes/${limit}/${offset}/${id}/${tipoViajes}`;

    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            // console.log(res)
            if (res.response != false) {
                let data = res.result.Viajes;
                total = res.result.Total;
                cuerpo = document.getElementById('ListaDeViajes');
                cuerpo.innerHTML = "";
                console.log(data)
                data.forEach(element => {
                    // console.log(element.Brand)
                    tarjeta = tarjetas.find(res => res.brand == element.Brand);
                    // console.log(tarjeta.valor);
                    let arrayFechaYhora = element.FechaInicio;
                    let arrayFechaYhoraFin = element.FechaFin;
                    let arrayfecha = arrayFechaYhora.split(' ');
                    let arrayfechafin = arrayFechaYhoraFin.split(' ');
                    let arrayDate = arrayfecha[0].split('-');
                    let arrayDateFin = arrayfechafin[0].split('-');
                    fechaMes = meses.find(res => res.mes == arrayDate[1]);
                    fechaMesFin = meses.find(res => res.mes == arrayDateFin[1]);
                    let hora = arrayfecha[1].split(':');
                    let horafinal = arrayfechafin[1].split(':');
                    let date_format = '12';
                    let hour = hora[0];
                    let hourfinal = horafinal[0];
                    let minutes = hora[1];
                    let minutesfinal = horafinal[1];
                    let result = hour;
                    let resultado = hourfinal;
                    let ext = '';
                    if (date_format == '12') {
                        if (hour > 12) {
                            ext = 'PM';
                            hour = (hour - 12);

                            if (hour < 10) {
                                result = "0" + hour;
                            } else if (hour == 12) {
                                hour = "00";
                                ext = 'AM';
                            }
                        } else if (hour < 12) {
                            result = ((hour < 10) ? "0" + hour : hour);
                            ext = 'AM';
                        } else if (hour == 12) {
                            ext = 'PM';
                        }
                    }
                    result = result + ":" + minutes + ' ' + ext;
                    // console.log(result);
                    if (date_format == '12') {
                        if (hourfinal > 12) {
                            ext = 'PM';
                            hourfinal = (hourfinal - 12);

                            if (hourfinal < 10) {
                                resultado = "0" + hourfinal;
                            } else if (hourfinal == 12) {
                                hourfinal = "00";
                                ext = 'AM';
                            }
                        } else if (hourfinal < 12) {
                            resultado = ((hourfinal < 10) ? "0" + hourfinal : hourfinal);
                            ext = 'AM';
                        } else if (hourfinal == 12) {
                            ext = 'PM';
                        }
                    }
                    resultado = resultado + ":" + minutesfinal + ' ' + ext;
                    // console.log(resultado);
                    let last4 = ``;
                    let ocultarPuntos = '';
                    let pago = element.idMetodoPago;
                    if (pago == 1) {
                        pago = "cash.svg"
                        last4 = `Efectivo`;
                        ocultarPuntos = 'visibility: hidden;';
                    } else {
                        pago = tarjeta.valor;
                        last4 = element.Ultimos_4_digitos;
                        ocultarPuntos = 'visibility: visible;';
                    }
                    ocultarFactura = element.EstadoTransaccion;
                    if (ocultarFactura == 2) {
                        ocultarFactura = 'visibility: visible;';
                    } else {
                        ocultarFactura = 'visibility: hidden;';
                    }

                    console.log(ocultarFactura);
                    //document.getElementById(`btnMostrarMas`).style = `${ ocultarBtn }`;
                    let calificacion = element.CalificacionDriver
                        //console.log(calificacion)


                    let starts = ``;

                    for (i = 1; i <= 5; i++) {
                        if (calificacion >= i) {
                            starts += `<span class="fa fa-star checked"></span>`;
                        } else {
                            starts += `<span class="fa fa-star"></span>`
                        }
                    }


                    let fila = document.createElement(`div`)
                    fila.className = 'row';
                    fila.innerHTML = `
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 offset-lg-.1">
                <div class="card card-chart viaje-expandido">
                    <div class="card-body">
                        <input id="desplegar${element.idViaje}" value="0" type="hidden">
                        <a onclick = "desplegar(${element.idViaje})" data-toggle="collapse" href="" data-target=".multi-collapse-viaje${element.idViaje}" aria-expanded="true">
                        <h4 class="card-title"><span style="font-size: 28px;" id="simboloDesplegar${element.idViaje}" >+</span><span style="margin-left: 20px;">${arrayDate[0]} de ${fechaMes.valor} ${arrayDate[2]}, ${result}</span></h4>
                        <p class="card-category" style="margin-left: 30px;">
                          <span class="text-semibold" style="font-size: 16px;">${element.TotalViaje} MXN</span> &nbsp;<img src="../assets/imagenes/${pago}" width="33px" style="margin-left: 10%;">&nbsp; <img id="imgDots" src="../assets/imagenes/dots.svg" width="20px" style="${ocultarPuntos}"><span id ="number" class="text-nova-regular" style="font-size: 16px; color: #4A4A4A; margin-left: 5px; padding-top: 10px;">${last4}</span><br>
                        </p>
                        </a>
                        
                        <br>
                        
                        <div class="contenido-viaje collapse multi-collapse-viaje${element.idViaje}">
                        
                        
                            <p class="card-category">
                            <span class="text-semibold" style="font-size: 16px; margin-left: 30px;">Tu viaje en Lubo ${element.TipoServicio} con ${element.Conductor}</span></p>
                          
                            <div class="row">  	
                                <div class="col-sm-12">
                                    <section id="cd-timeline" class="cd-container">
                                        <div class="cd-timeline-block">
                                            <div class="cd-timeline-img cd-picture">
                                            </div> <!-- cd-timeline-img -->
                                
                                            <div class="cd-timeline-content">
                                                <p class="text-nova-regular">${element.CalleOrigen} No ${element.NumExtOrigen}, ${element.ColoniaOrigen}, ${element.CiudadOrigen}  <br><span class="text-gray">${result}</span></p>
                                            </div> <!-- cd-timeline-content -->
                                        </div> <!-- cd-timeline-block -->
                                
                                        <div class="cd-timeline-block">
                                            <div class="cd-timeline-img cd-movie">
                                            </div> <!-- cd-timeline-img -->
                                
                                            <div class="cd-timeline-content">
                                                <p class="text-nova-regular">${element.CalleDestino} No ${element.NumExtDestino}, ${element.ColoniaDestino}, ${element.CiudadDestino} . <br><span class="text-gray">${resultado}</span></p>
                                            </div> <!-- cd-timeline-content -->
                                        </div> <!-- cd-timeline-block -->
                                        
                                    </section> <!-- cd-timeline -->
                                </div>   			
                            </div>
                            </div><!--end of collapse-->
                        </div>
                        
                        <div class="collapse multi-collapse-viaje${element.idViaje}">
                            <hr class="hr-footer-viaje">
                            <div class="card-footer ">
                                <div class="stats text-nova-regular" style="margin-left: 30px;">
                                    Calificación &nbsp;
                                    ${starts}
                                </div>
                                <div id="objFactura" class="stats text-nova-regular" style="${ocultarFactura}">
                                    <a href ="#" onclick = "obtenerFactura('${element.idViaje}')" class="link-blue" ><span >Obtener factura</span></a>
                                </div>
                                <div class="stats text-nova-regular">
                                    <a href="detalle-viaje.html?id=${element.idViaje}&tipo=${element.Tipo}&last4=${element.Ultimos_4_digitos}&branding=${pago}&name=${element.Conductor}&servicio=${element.TipoServicio}&calificacion=${element.CalificacionUsuario}&idMetodoPago=${element.idMetodoPago}"><span class="link-blue">Ver detalles<i class="material-icons">keyboard_arrow_right</i></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
                    // `</div><!--end of row elemento viaje--></img>`;
                    cuerpo.appendChild(fila);
                    // console.log(element.Tipo);

                });
            } else {
                let ocultarBtn = '';
                if (res.response == false) {
                    ocultarBtn = 'visibility: hidden;';
                    // } else {
                    //     ocultarBtn = 'visibility: visible;';
                    // }
                    document.getElementById(`btnMostrarMas`).style = `${ocultarBtn}`;
                    document.getElementById(`btnTipo`).style = `${ocultarBtn}`;
                    cuerpo = document.getElementById('ListaDeViajes');
                    cuerpo.innerHTML = "";
                    let fila = document.createElement(`div`)
                    fila.innerHTML = ` <h1 class="text-nova-bold color-gray-driver">Bienvenido :)</h1>
            <p class="text-driver color-gray-driver">
            Estamos muy contentos de tenerte por aquí, por ahora no tienes viajes en tu historial, realiza tu primer viaje para comenzar a viajar cool con Lubo®.
            </p>
            <br>
            <br>
            <br>`
                    cuerpo.appendChild(fila);
                }

            }

        })

};

const desplegar = (idViaje) => {
    let desplegar = document.getElementById(`desplegar${idViaje}`);
    if (desplegar.value == "0") {
        document.getElementById(`simboloDesplegar${idViaje}`).innerHTML = "-";
        desplegar.value = "1";
        // console.log(desplegar.value)
    } else if (desplegar.value == "1") {
        document.getElementById(`simboloDesplegar${idViaje}`).innerHTML = "+";
        desplegar.value = "0";
        // console.log(desplegar.value)
    }
}
const detalleViaje = () => {
    let id = sessionStorage.getItem('idUser');
    let sear = new URLSearchParams(location.search);
    let idViaje = sear.get('id');
    let tipo = sear.get('tipo');
    let digitos = sear.get('last4');
    let brand = sear.get('branding');
    let calificacionUser = sear.get('calificacion');
    let idMetodoPago = sear.get('idMetodoPago');
    console.log(idMetodoPago)
        // console.log(calificacionUser)
        // console.log('Este es otro ===> ' + tipo);
        // console.log('Este es el last4 ===> ' + digitos);
        // console.log('Este es el brand ===> ' + brand);
        // console.log(idViaje);
    sw = `${url}viaje/detallesViaje/${id}/${idViaje}/${tipo}`;
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response) {
                console.log(res)
                let data = res.result.Viaje;
                let result = res.result;
                document.getElementById('direccionInicio').innerHTML = `${result.CalleOrigen},${result.ColoniaOrigen},${result.CiudadOrigen}`;
                document.getElementById('direccionFin').innerHTML = `${result.CalleDestino},${result.ColoniaDestino},${result.CiudadDestino}`;
                document.getElementById('tarifaTotal').innerHTML = `$ ${result.PagoDelUsuario} `;

                let arrayFechaYhora = result.FechaInicio;
                let arrayFechaYhoraFin = result.FechaFin;
                let arrayfecha = arrayFechaYhora.split(' ');
                let arrayfechafin = arrayFechaYhoraFin.split(' ');
                let arrayDate = arrayfecha[0].split('-');
                let arrayDateFin = arrayfechafin[0].split('-');
                fechaMes = meses.find(res => res.mes == arrayDate[1]);
                fechaMesFin = meses.find(res => res.mes == arrayDateFin[1]);
                let hora = arrayfecha[1].split(':');
                let horafinal = arrayfechafin[1].split(':');
                let date_format = '12';
                let hour = hora[0];
                let hourfinal = horafinal[0];
                let minutes = hora[1];
                let minutesfinal = horafinal[1];
                let result_inicio = hour;
                let resultado = hourfinal;
                let ext = '';
                if (date_format == '12') {
                    if (hour > 12) {
                        ext = 'PM';
                        hour = (hour - 12);

                        if (hour < 10) {
                            result_inicio = "0" + hour;
                        } else if (hour == 12) {
                            hour = "00";
                            ext = 'AM';
                        }
                    } else if (hour < 12) {
                        result_inicio = ((hour < 10) ? "0" + hour : hour);
                        ext = 'AM';
                    } else if (hour == 12) {
                        ext = 'PM';
                    }
                }

                result_inicio = result_inicio + ":" + minutes + ' ' + ext;

                //                console.log(result_inicio)

                if (date_format == '12') {
                    if (hourfinal > 12) {
                        ext = 'PM';
                        hourfinal = (hourfinal - 12);

                        if (hourfinal < 10) {
                            resultado = "0" + hourfinal;
                        } else if (hourfinal == 12) {
                            hourfinal = "00";
                            ext = 'AM';
                        }
                    } else if (hourfinal < 12) {
                        resultado = ((hourfinal < 10) ? "0" + hourfinal : hourfinal);
                        ext = 'AM';
                    } else if (hourfinal == 12) {
                        ext = 'PM';
                    }
                }

                resultado = resultado + ":" + minutesfinal + ' ' + ext;

                let start = ``;

                for (i = 1; i <= 5; i++) {
                    if (calificacionUser >= i) {
                        start += `<span class="fa fa-star checked"></span>`;
                    } else {
                        start += `<span class="fa fa-star"></span>`;
                    }
                }

                let last4 = ``;
                let ocultarPuntos = '';
                let pago = `${idMetodoPago}`;
                if (pago == 1) {
                    pago = "cash.svg"
                    last4 = result.MetodoPago;
                    console.log(last4)
                    ocultarPuntos = 'visibility: hidden;';
                } else {
                    pago = `${brand}`;
                    last4 = `${digitos}`;
                    ocultarPuntos = 'visibility: visible;';
                }

                document.getElementById('obtenerFactura').setAttribute("onclick", `obtenerFactura(${idViaje});`);
                document.getElementById('enviarFacturaCorreo').setAttribute("onclick", `enviarFacturaEmail(${idViaje});`);
                document.getElementById('horaInicio').innerHTML = `${result_inicio}`;
                document.getElementById('horaFin').innerHTML = `${resultado}`;
                document.getElementById('viajeHora').innerHTML = `${arrayDate[2]} de ${fechaMes.valor} ${arrayDate[0]}, ${result_inicio}`;
                document.getElementById('metodoPago').innerHTML = `${last4}`;
                document.getElementById('imgPago').src = `../assets/imagenes/${pago}`;
                document.getElementById('imgDots').style = `${ocultarPuntos}`;
                document.getElementById('tipoTransporte').innerHTML = `${result.TipoServicio}`;
                document.getElementById('calificacion').innerHTML = `${start}`;
                document.getElementById('fechaRecibo').innerHTML = `${arrayDate[2]} de ${fechaMes.valor} ${arrayDate[0]}, ${result_inicio}`;
                document.getElementById('priceTotal').innerHTML = `$ ${result.PagoDelUsuario}`;
                document.getElementById('priceSubtotal').innerHTML = `$ ${result.SubTotal}`;
                document.getElementById('tarifaBase').innerHTML = `$ ${result.TarifaBase}.00`;
                document.getElementById('km').innerHTML = `${result.DistanciaKilometros}`;
                document.getElementById('costoKm').innerHTML = `$ ${result.Costo_por_Km}`;
                document.getElementById('min').innerHTML = `${result.TiempoMinutos}`;
                document.getElementById('costoMin').innerHTML = `$ ${result.Costo_por_minuto}`;
                document.getElementById('cuotaSolicitud').innerHTML = `$ ${result.CuotaPeticion}`;
                document.getElementById('iva').innerHTML = `$ ${result.IVA}`;
                // document.getElementById('propina').innerHTML = `$ ${result.PropinaViaje}`;
                // document.getElementById('isr').innerHTML = `$ ${result.ISR}`;
                document.getElementById('tipoPago').innerHTML = `${last4}`;
                document.getElementById('imgTicked').src = `../assets/imagenes/${pago}`;
                document.getElementById('imgPuntos').style = `${ocultarPuntos}`;
                document.getElementById('priceTotal1').innerHTML = `$ ${result.PagoDelUsuario}`;
                document.getElementById('kilometros').innerHTML = `${result.DistanciaKilometros} km`;
                document.getElementById('minutos').innerHTML = `${result.TiempoMinutos} min`;
            }
        });
}

const obtenerPersona = () => {
    let token = sessionStorage.getItem('Token');
    let mailStatus = sessionStorage.getItem('EmailStatus');
    console.log(mailStatus)
    if (mailStatus != "false") {
        mailStatus = 'Verificado'
        document.getElementById("statusEmail").innerHTML = `<p class="text-nova-regular error-notice verificado" style="margin-top: -20px;">${mailStatus}</p>`;
    } else {
        mailStatus = 'No verificado'
        document.getElementById("statusEmail").innerHTML = `<p class="text-nova-regular error-notice error-text-perfil" style="margin-top: -20px;">${mailStatus}</p>`;
    }
    console.log(mailStatus)
    sw = `${url}auth/getData/${token}`;
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response && res['result']['Tipo_de_usuario'] == 2) {
                let userData = res.result;
                document.getElementById('nombreUser').innerHTML = `${userData.Nombre} ${userData.Apellidos}`;
                document.getElementById('telUser').innerHTML = `${userData.Telefono}`;
                document.getElementById('nombrePersona').value = `${userData.Nombre} ${userData.Apellidos}`;
                document.getElementById('email').value = `${userData.Email}`;
                if (userData.Imagen == "") {
                    document.getElementById('imgPerfil').src = "../assets/imagenes/comment-avatar.svg";
                } else {
                    document.getElementById('imgPerfil').src = `${userData.Imagen}`;
                }
                document.getElementById('telefono').value = `${userData.Telefono}`;

            } else {
                // self.location = '../login-user.php'
            }
        });

}
const actualizarPersona = () => {

    if (confirm("Estas seguro de actualizar estos datos!")) {
        let id = sessionStorage.getItem('idUser');
        let sw = `${url}actualizacion/actualizar`;
        let parametros = {
            "idUsuario": id,
            "Nombre": document.getElementById('nombreUpdate').value,
            "Apellidos": document.getElementById('apellidoUpdate').value
                //(document.getElementById('nombreUpdate').value,document.getElementById('apellidoUpdate').value)
        };
        console.log(parametros)
        fetch(sw, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(parametros), // data can be `string` or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                console.log(res)
                if (res.response) {
                    location.reload();
                    // console.log(res)
                } else {
                    alert(res.errors);
                    document.getElementById('BotonGuardarDatos').disabled = false;
                }
            });
    } else {

    }
}

const cargarDocumento = (form = 0) => {

    if (confirm('Quiere cargar este documento')) {

        let id = sessionStorage.getItem('idUser');
        let sw = `${url}img/cargar/perfil/${id}`;

        fetch(sw, {
                method: 'POST',
                body: new FormData(form),
            })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res['response']) {
                    alert("Archivo se ha cargado de manera correcta")
                } else {
                    alert(res['errors']);
                }
            });
    }
    alert("El archivo esta en espera para ser validado");
}

const actualizarCorreo = () => {

    if (confirm("Estas seguro de actualizar tu  E-mail!!!")) {
        let id = sessionStorage.getItem('idUser');
        let tipoUser = sessionStorage.getItem('Tipo_usuario');
        console.log(tipoUser)
        let sw = `${url}auth/actualizarEmail`;
        let parametros = {
            "idPersona": id,
            "Email": document.getElementById('correoUpdate').value,
            "Tipo_de_usuario": tipoUser
        };
        console.log(parametros)
        fetch(sw, {
                method: 'PUT', // or 'POST'
                body: JSON.stringify(parametros), // data can be `string` or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {

                console.log(res)
                if (res.response) {
                    location.reload();
                    //console.log(res)
                } else {
                    alert(res.errors);
                    document.getElementById('BotonEnviarCorreo').disabled = false;
                }
            });
    } else {

    }
}


let listarTarjeta = () => {
    let id = sessionStorage.getItem('idUser');
    let sw = `${url}tarjeta/listar/${id}`
    let htmlinnert = ``;
    fetch(sw, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response) {
                let data = res.result;
                console.log(data)
                data.forEach(element => {
                    // console.log(res);
                    tarjeta = tarjetas.find(res => res.brand == element.brand);
                    // console.log(tarjeta);
                    htmlinnert += `<div class="row" style="margin-bottom: -30px;">
                    <div class="col-sm-12">
                        <ul class="list-inline" style="margin-left: 20px;">
                            <li class="list-inline-item text-semibold">
                                <img src="../assets/imagenes/${tarjeta.valor}" width="33px" style="margin-left: 0px;">&nbsp; <img src="../assets/imagenes/dots.svg" width="20px" style=""><span class="text-nova-regular" style="font-size: 16px; color: #4A4A4A; margin-left: 5px; padding-top: 10px;">${element.last4}</span>
                                <p class="text-nova-regular">Vto.${element.exp_month}/${element.exp_year}</p>
                                </li>
                                <li class="list-inline-item text-nova-regular li-detalle" style="margin-left: 70px;"><!--Editar-->
                                    <a href="" data-toggle="modal" data-target="#modal-delete-card" onclick ="seleccionarTarjeta('${element.id}',${element.last4})">
                                        <p class="text-nova-regular">Eliminar</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <br>`;
                })
                document.getElementById('listaDeTarjetas').innerHTML = htmlinnert;
            } else {
                console.log(res.errors);
            }

        })
}

const seleccionarTarjeta = (idTarjeta, last4) => {
    console.log(idTarjeta)
    document.getElementById('idTarjetaSeleccionada').value = idTarjeta;
    document.getElementById('labelTarjeta').innerHTML = `Tarjeta terminada en ${last4}`;
}
const formatoFechaEx = (event) => {
    let cadenaFecha = document.getElementById('fecha_ex').value;
    let codigo = event.which || event.keyCode;
    // console.log(codigo);
    if (cadenaFecha.length == 2) {
        if (codigo != 8) {
            document.getElementById('fecha_ex').value = `${cadenaFecha}/`;
        }
    }
}
const tarjetaRegistrar = () => {
    document.getElementById('BotonGuardarTarjeta').disabled = true;
    Conekta.setPublicKey(conekta);
    Conekta.setLanguage("es");
    console.log(conekta);
    dataFechaEx = document.getElementById('fecha_ex').value;
    fechaArray = dataFechaEx.split('/');
    let tokenParams = {
        "card": {
            "number": document.getElementById('num_tarjeta').value,
            "name": document.getElementById('nombre_usuario').value,
            "exp_year": fechaArray[1],
            "exp_month": fechaArray[0],
            "cvc": document.getElementById('cvv').value
        }
    };
    Conekta.Token.create(tokenParams, successResponseHandler, errorResponseHandler);

}

const successResponseHandler = (tokenTarjeta) => {
    console.log(tokenTarjeta);
    let id = sessionStorage.getItem('idUser');
    let sw = `${url}tarjeta/registrar`;
    let parametros = {
        "id": id,
        "token_id": tokenTarjeta.id
    };
    fetch(sw, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(parametros), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res)
            if (res.response) {
                location.reload();
                // console.log(res)
            } else {
                alert(res.errors);
                document.getElementById('BotonGuardarTarjeta').disabled = false;
            }
        });

}

const errorResponseHandler = (error) => {
    console.log(error);
    // alert(`${error.message_to_purchaser} - ${error.error_code}`);
    alert(`${error.message_to_purchaser}`)
    document.getElementById('BotonGuardarTarjeta').disabled = false;
}

const eliminarTarjeta = (idTarjeta) => {
    let id = sessionStorage.getItem('idUser');
    let parametros = {
        "id": id,
        "idTarjeta": idTarjeta
    };
    let sw = `${url}tarjeta/eliminar`;

    fetch(sw, {
            method: 'DELETE',
            body: JSON.stringify(parametros), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (res.response) {
                location.reload();
            } else {
                alert(`${res.errors}`)
            }
        });


};

// perfiles de impuestos

const listarPerfilesImpuestos = () => {
    let id = sessionStorage.getItem('idUser');
    let sw = `${url}/perfilimpuestos/listar/${id}`;
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            let result = res.result;
            // 
            let cuerpo = document.getElementById('listaPerfilesImpuestos');
            // 
            result.forEach(element => {
                let cadenaInfo = '';
                if (element.idmetodo_de_pago == 1) {
                    cadenaInfo = `${element.NombreLegal}, Efectivo`;
                } else {
                    cadenaInfo = `${element.NombreLegal}, ${element.brand}  ****${element.last4}`;
                }

                let fila = document.createElement(`div`)
                fila.className = 'row';
                fila.style = 'margin-bottom: -30px;';
                fila.innerHTML = `
                <div class="col-sm-10">
                 <ul class="list-inline" style="margin-left: 20px;">
                    <li class="list-inline-item text-semibold">
                        <span class="text-semibold" style="font-size: 17px; color: #4A4A4A; margin-left: 0px; padding-top: 10px;"> ${cadenaInfo} </span>
                        <p class="text-nova-regular">&nbsp</p>
                        
                    </li>
                  </ul>
                 </div>
                 <div class="col-lg-2">
                    <li class="list-inline-item text-nova-regular li-detalle" style="margin-left: 80px; margin-top: -10px;"><a href= "nuevo-perfil-de-impuestos.html?idperfil=${element.idtbperfilImpuestos}"> Editar </a>
                        <a href="" onclick = "selccionarEliminarPerfil(${element.idtbperfilImpuestos},${element.last4},'${element.brand}',${element.idmetodo_de_pago})" data-toggle="modal" data-target="#modal-delete-card"><p class="text-nova-regular">Eliminar</p></a>
                    </li>
                 </div>
                `;
                let br = document.createElement('br');
                cuerpo.appendChild(fila);
                cuerpo.appendChild(br);
            });
        });
}

const selccionarEliminarPerfil = (idperfil, last4, brand, idmetodo_de_pago) => {
    console.log(idperfil, last4, brand, idmetodo_de_pago);
    if (idmetodo_de_pago == 1) {
        cadenaInfo = `Efectivo`;
    } else {
        cadenaInfo = `${brand}  ****${last4}`;
    }
    document.getElementById('perfilAEliminar').value = idperfil;
    document.getElementById('ModalNombrePerfil').innerHTML = `Perfil,${cadenaInfo}`;
}

const eliminarPerfil = () => {
    let idperfil = document.getElementById('perfilAEliminar').value;
    let sw = `${url}/perfilimpuestos/eliminar`;
    let parametros = {
        "idPerfilImpuestos": idperfil
    };
    // console.log(parametros);
    fetch(sw, {
            method: 'DELETE',
            body: JSON.stringify(parametros), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.response == true) {
                location.reload();
            } else {
                alert(`Oucrrio un error: ${res.errors}`);
            }
        });
}

const obtenerPerfil = () => {
    let dir = new URLSearchParams(location.search);
    var idperfil = dir.get('idperfil');
    if (idperfil > 0) {
        document.getElementById('botonAregarPerfil').setAttribute('onClick', "actualizarPerfil()");
        let sw = `${url}perfilimpuestos/obtener/${idperfil}`;
        fetch(sw, {
                method: 'GET',
                // body: JSON.stringify(parametros), // data can be `string` or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res.response) {
                    let result = res.result;
                    document.getElementById('NombreLegal').value = result.NombreLegal;
                    document.getElementById('Calle').value = result.Calle;
                    document.getElementById('NumExt').value = result.NumExt;
                    document.getElementById('NumInt').value = result.NumInt;
                    document.getElementById('Cp').value = result.Cp;
                    infoCp(result.Colonia);
                    // document.getElementById('Colonia').value = result.Colonia;

                    document.getElementById('Municipio').value = result.Municipio;
                    document.getElementById('Ciudad').value = result.Ciudad;
                    document.getElementById('Estado').value = result.Estado;
                    document.getElementById('Rfc').value = result.Rfc;

                    // document.getElementById('metodo_de_pago');
                    listarMetodosPerfil(result.idmetodo_de_pago, result.idTarjeta, result.last4, result.brand);

                    document.getElementById('tipo_facturacion').value = result.TipoFacturacion;

                    let inputHidden = document.createElement("INPUT");
                    inputHidden.setAttribute("type", "hidden");
                    inputHidden.id = 'idperfilI';
                    inputHidden.value = idperfil;

                    document.getElementById('idPerfilImpuestos').appendChild(inputHidden);

                } else {
                    alert(`${res.errors}`);
                }
            })
    }
}

const agregarPerfil = () => {
    let parametros = validarCamposPerfilImpuestos();
    console.log(parametros);
    if (parametros === false) {
        alert("Llene todos los campos requeridos");
    } else {
        let sw = `${url}perfilimpuestos/agregar`;
        fetch(sw, {
                method: 'POST',
                body: JSON.stringify(parametros), // data can be `string` or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res.response) {
                    console.log(res.result);
                    self.location = 'perfil-de-impuestos.html'
                } else {
                    alert(res.errors);
                }
            })
    }

}

const actualizarPerfil = () => {
    let parametros = validarCamposPerfilImpuestos();
    console.log(parametros);
    if (parametros === false) {
        alert("Llene todos los campos requeridos");
    } else {
        let sw = `${url}perfilimpuestos/actualizar`;
        fetch(sw, {
                method: 'PUT',
                body: JSON.stringify(parametros), // data can be `string` or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res.response) {
                    console.log(res.result);
                    self.location = 'perfil-de-impuestos.html'
                } else {
                    alert(res.errors);
                }
            })
    }

}


const validarCamposPerfilImpuestos = () => {
    let errors = 0;

    let NombreLegal = document.getElementById('NombreLegal');
    let Calle = document.getElementById('Calle');
    let NumExt = document.getElementById('NumExt');
    let NumInt = document.getElementById('NumInt');
    let Cp = document.getElementById('Cp');
    let Colonia = document.getElementById('Colonia');
    let Municipio = document.getElementById('Municipio');
    let Ciudad = document.getElementById('Ciudad');
    let Estado = document.getElementById('Estado');
    let Rfc = document.getElementById('Rfc');

    let Metodo = document.getElementById('metodo_de_pago');
    let arrayMetodo = Metodo.value.split(',');
    let idmetodo_de_pago = arrayMetodo[0];
    let idTarjeta = arrayMetodo[1];

    let TipoFacturacion = document.getElementById('tipo_facturacion');

    borrarError(NombreLegal);
    borrarError(Calle);
    borrarError(NumExt);
    borrarError(NumInt);
    borrarError(Cp);
    borrarError(Municipio);
    borrarError(Estado);
    borrarError(Rfc);

    if (NombreLegal.value.length <= 0) {
        crearError(NombreLegal);
        errors++;
    }
    if (Calle.value.length <= 0) {
        crearError(Calle);
        errors++;
    }
    if (NumExt.value.length <= 0) {
        crearError(NumExt);
        errors++;
    }
    if (NumInt.value.length <= 0) {
        // crearError(NumInt);
        // errors++;
        NumInt.value = 0;
    }
    if (Cp.value.length <= 0) {
        crearError(Cp);
        errors++;
    }
    if (Colonia.value == 0 || Colonia.value.length == 0) {
        alert("Seleccione una Colonia");
        errors++;
    }
    if (Municipio.value.length <= 0) {
        crearError(Municipio);
        errors++;
    }
    if (Estado.value.length <= 0) {
        crearError(Estado);
        errors++;
    }
    if (Rfc.value.length <= 0) {
        crearError(Rfc);
        errors++;
    }
    if (idmetodo_de_pago <= 0) {
        // crearError(Metodo);
        alert("Seleccione un Metodo de Pago");
        errors++;
    }
    if (TipoFacturacion.value.length <= 0) {
        crearError(TipoFacturacion);
        errors++;
    }
    let parametros;
    if (document.getElementById('idperfilI')) {
        parametros = {
            "NombreLegal": NombreLegal.value,
            "Calle": Calle.value,
            "NumExt": NumExt.value,
            "NumInt": NumInt.value,
            "Cp": Cp.value,
            "Colonia": Colonia.value,
            "Municipio": Municipio.value,
            "Ciudad": Ciudad.value,
            "Estado": Estado.value,
            "Rfc": Rfc.value,
            "idmetodo_de_pago": idmetodo_de_pago,
            "idTarjeta": idTarjeta,
            "TipoFacturacion": TipoFacturacion.value,
            "idPersona": sessionStorage.getItem('idUser'),
            "idtbperfilImpuestos": document.getElementById('idperfilI').value
        };
    } else {
        parametros = {
            "NombreLegal": NombreLegal.value,
            "Calle": Calle.value,
            "NumExt": NumExt.value,
            "NumInt": NumInt.value,
            "Cp": Cp.value,
            "Colonia": Colonia.value,
            "Municipio": Municipio.value,
            "Ciudad": Ciudad.value,
            "Estado": Estado.value,
            "Rfc": Rfc.value,
            "idmetodo_de_pago": idmetodo_de_pago,
            "idTarjeta": idTarjeta,
            "TipoFacturacion": TipoFacturacion.value,
            "idPersona": sessionStorage.getItem('idUser')
        };
    }


    if (errors == 0) {
        return parametros
    } else {
        return false;
    }

}

const crearError = (elemento) => {
    let error = document.createElement(`div`)
    error.className = 'error-notice error-text-impuesto';
    error.innerHTML = `Campo requerido`;

    let padre = elemento.parentNode;
    let abuelo = padre.parentNode;
    abuelo.appendChild(error);
}

const borrarError = (elemento) => {
    let padre = elemento.parentNode;
    let abuelo = padre.parentNode;
    let num = abuelo.getElementsByTagName('*').length;
    if (num > 3) {
        aborrar = abuelo.lastChild;
        abuelo.removeChild(aborrar);
    }
}

const infoCp = (colonia = 0) => {
    let cp = document.getElementById('Cp');
    if (cp.value.length == 5) {
        let sw = `${url}/perfilimpuestos/cp/${cp.value}`;
        fetch(sw, {
                method: 'GET',
                // body: JSON.stringify(parametros), // data can be `string` or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res.response) {
                    let result = res.result;
                    document.getElementById('Ciudad').value = result.ciudad;
                    document.getElementById('Estado').value = result.estado;
                    document.getElementById('Municipio').value = result.municipio
                    let colonias = result.colonias;
                    let inHtml = `<option value="0">Seleccione una colonia</option>`;

                    if (colonia == 0) {
                        colonias.forEach(element => {
                            inHtml += `<option>${element}</option>`;
                        });
                    } else {
                        colonias.forEach(element => {
                            let def;
                            if (element == colonia) {
                                def = `selected`;
                            }
                            inHtml += `<option ${def}>${element}</option>`;

                        });
                    }



                    document.getElementById('Colonia').innerHTML = inHtml;
                } else {
                    alert(res.errors)
                }
            }).catch(error => {
                console.log(errors);
            })
    }

}

const listarMetodosPerfil = (metodo_de_pago = false, idTarjeta = '', last4 = false, brand = false) => {
        let id = sessionStorage.getItem('idUser');
        let sw = `${url}perfilimpuestos/metodosSinPerfil/${id}`;
        fetch(sw, {
                method: 'GET',
                // body: JSON.stringify(parametros), // data can be `string` or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res.response) {
                    let agregar = `<option value="0">Seleccione una Metodo de Pago </option>`;
                    if (metodo_de_pago != false) {

                        if (metodo_de_pago == 1) {
                            agregar += `<option value="1,''" selected>Efectivo</option>`;
                        } else {
                            agregar += `<option value="${metodo_de_pago},${idTarjeta}" selected>${brand}****${last4}</option>`;
                        }
                    }
                    let select = document.getElementById('metodo_de_pago');
                    let data = res.result;
                    data.forEach(element => {
                        if (element.status) {
                            if (element.idMetodoPago == 1) {
                                agregar += `<option value="1,''">Efectivo</option>`;
                            } else {
                                agregar += `<option value="2,${element.info.idTarjetaConekta}">${element.info.brand}****${element.info.last4}</option>`;
                            }
                        }
                    })
                    select.innerHTML = agregar;
                } else {
                    alert(res.errors)
                }
            })
            .catch(error => {
                console.log(error);
            })
    }
    //
const obtenerFactura = (idViaje) => {
    let sw = `${url}factura/crearFactura`;
    let parametros = {
        "idViaje": idViaje
    };
    fetch(sw, {
            method: 'POST',
            body: JSON.stringify(parametros), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (!res.response) {
                // console.log(res);
            }
            // descargar factura
            if (res.response || (res.response == false && res.errors == "ex")) {
                let descargarFactura = `${url}factura/descargarFactura/${idViaje}`;
                fetch(descargarFactura, {
                        method: 'GET',
                        // body: JSON.stringify(parametros), // data can be `string` or {object}!
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(res => res.json())
                    .then(res => {
                        if (res.response) {
                            console.log(res.result.Content);
                            let pdfString = res.result.Content;
                            let pdfWindow = window.open("")
                            pdfWindow.title = "Factura Lubo";
                            pdfWindow.document.write(
                                "<iframe frameBorder='0' width='100%' height='100%' src='data:application/pdf;base64, " +
                                encodeURI(pdfString) + "'></iframe>"
                            )
                        } else {
                            alert("Error al obtener factura");
                        }

                    });
            } else {
                let errorFacturama = res.errors[0];
                let error = errorFacturama.Message;
                if (error != null) {
                    console.log(error);
                    alert("El RFC No es válido: Cuando no se utilice un RFC genérico, el RFC debe estar en la lista de RFC inscritos no cancelados en el SAT");
                } else {
                    alert(res.errors)
                }
            }


        })
}

// mandar factura por email 
const enviarFacturaEmail = (idViaje) => {
    let emailUser = sessionStorage.getItem('EmailUser');
    document.getElementById('loaderEmail').style.visibility = 'visible';

    let sw = `${url}factura/enviarEmail/${idViaje}/${emailUser}`
    fetch(sw, {
            method: 'GET',
            // body: JSON.stringify(parametros), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (res.response) {
                alert(`Se mando la factura al correo ${emailUser}`);
            } else {
                alert(`No se pudo mandar la factura - revice si se ha obtenido la factura antes`);
            }
            document.getElementById('loaderEmail').style.visibility = 'hidden';
        })

    // let val = false
    // setInterval(function(){
    //     if (val) {
    //         document.getElementById('loaderEmail').style.visibility = 'hidden';
    //         val = false;
    //     } else {
    //         document.getElementById('loaderEmail').style.visibility = 'visible';
    //         val = true
    //     }
    // }, 3000);
}

// 
const pdfRecibo = () => {

    let fecha = document.getElementById("fechaRecibo").innerHTML;
    let total = document.getElementById("priceTotal").innerHTML;
    let subTotal = document.getElementById("priceSubtotal").innerHTML;
    let iva = document.getElementById("iva").innerHTML;
    // let tip = document.getElementById("propina").innerHTML
    let tipoPago = document.getElementById("tipoPago").innerHTML;
    let sear = new URLSearchParams(location.search);
    let name = sear.get('name');
    let servicio = sear.get('servicio');
    let km = document.getElementById("kilometros").innerHTML;
    let direccionInicio = document.getElementById("direccionInicio").innerHTML;
    let horaInicio = document.getElementById("horaInicio").innerHTML;
    let direccionFin = document.getElementById("direccionFin").innerHTML;
    let horaFin = document.getElementById("horaFin").innerHTML;
    let card = sear.get('branding').split('.');
    let dots = document.getElementById("imgDots").src.split('/');
    console.log(dots.length - 1);
    puntos = dots[dots.length - 1].split('.');
    let brand = document.getElementById('imgTicked').innerHTML = `${card}`; //.src = `../assets/imagenes/${pago}`;
    // console.log(card[0]);
    // console.log(puntos[0]);
    //console.log(tip);
    // self.location = `Recibo-de-pago.php?fecha=${fecha}&total=${total}&subTotal=${subTotal}&iva=${iva}&tipoPago=${tipoPago}&name=${name}&servicio=${servicio}&km=${km}&dI=${direccionInicio}&hI=${horaInicio}&dF=${direccionFin}&hF=${horaFin}&brand=${card[0]}&dots=${puntos[0]}`;
    window.open(`Recibo-de-pago.php?fecha=${fecha}&total=${total}&subTotal=${subTotal}&iva=${iva}&tipoPago=${tipoPago}&name=${name}&servicio=${servicio}&km=${km}&dI=${direccionInicio}&hI=${horaInicio}&dF=${direccionFin}&hF=${horaFin}&brand=${card[0]}&dots=${puntos[0]}`, '_blank');
}

const pdfEnviarRecibo = () => {
    alert('El recibo sera enviado a su email')
    let fecha = document.getElementById("fechaRecibo").innerHTML;
    let total = document.getElementById("priceTotal").innerHTML;
    let subTotal = document.getElementById("priceSubtotal").innerHTML;
    let iva = document.getElementById("iva").innerHTML;
    let tip = document.getElementById("propina").innerHTML
    let tipoPago = document.getElementById("tipoPago").innerHTML;
    let sear = new URLSearchParams(location.search);
    let name = sear.get('name');
    let servicio = sear.get('servicio');
    let km = document.getElementById("kilometros").innerHTML;
    let direccionInicio = document.getElementById("direccionInicio").innerHTML;
    let horaInicio = document.getElementById("horaInicio").innerHTML;
    let direccionFin = document.getElementById("direccionFin").innerHTML;
    let horaFin = document.getElementById("horaFin").innerHTML;
    let card = sear.get('branding').split('.');
    let dots = document.getElementById("imgDots").src.split('/');
    puntos = dots[6].split('.');
    let brand = document.getElementById('imgTicked').innerHTML = `${card}`; //.src = `../assets/imagenes/${pago}`;
    let emailUser = sessionStorage.getItem('EmailUser');
    console.log(emailUser)
        // console.log(card[0]);
        // console.log(puntos[0]);
    self.location = `enviarTicked.php?fecha=${fecha}&total=${total}&subTotal=${subTotal}&iva=${iva}&tip=${tip}&tipoPago=${tipoPago}&name=${name}&servicio=${servicio}&km=${km}&dI=${direccionInicio}&hI=${horaInicio}&dF=${direccionFin}&hF=${horaFin}&brand=${card[0]}&dots=${puntos[0]}&email=${emailUser}`;

}