const actuReg = (Status) => {
    let idPersona = sessionStorage.getItem('IdPersona')
    $.ajax({
        data: { "Status": Status },
        url: url + 'registro/actualizar/' + idPersona,
        type: 'PUT',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            return true
        },
        success: function(data) {
            if (data['response']) {
                // alert('Fotos completas')
                // $(location).attr('href', 'mis-documentos.php#documentos')
            } else {
                if (data['errors'] == null) {
                    alert(data['message'])
                } else {
                    alert(data['errors'])
                }
            }
            return true
        }
    })
}

const cargarDocumento = (id, tipo, form) => {
    $.ajax({
        type: 'POST',
        url: url + 'img/cargar/' + tipo + '/' + id,
        data: new FormData(form),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            document.getElementById('bt' + tipo).disabled = true
            document.getElementById('img' + tipo).disabled = true
            return true
        },
        success: function(msg) {
            if (msg['response']) {
                // console.log(msg)
                contarDocs()
                actuReg(3)
                document.getElementById('tramite' + tipo).style.display = 'inline'
                return true
            } else {
                if (msg['errors'] == null) {
                    alert(msg['message'])
                } else {
                    alert(msg['errors'])
                }
                document.getElementById('bt' + tipo).disabled = false
                document.getElementById('img' + tipo).disabled = false
                return true
            }
        }
    })
}

const obtenerDocs = (id) => {
    // console.log(id)
    console.log(id);
    $.ajax({
        // data:  parametros,
        url: url + 'img/listarDoc/' + id,
        type: 'GET',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            //alert('Envindo Datos por ajax')
            return true
        },
        success: function(msg) {
            if (msg['response']) {
                console.log(msg);
                let elementos = msg['result']
                    // 	console.log(elementos.length)
                elementos.forEach(function(item) {
                    // console.log(item['id_tipo_documento'])
                    if(item['Status'] != 2){
                        contarDocs()
                        tipo = item['id_tipo_documento']
                        document.getElementById('tramite' + tipo).style.display = 'inline'
                        document.getElementById('bt' + tipo).disabled = true
                        document.getElementById('img' + tipo).disabled = true
                    }
                });
            } else {
                if (msg['errors'] == null) {
                    // alert(msg['message'])
                } else {
                    // alert(msg['errors'])
                }
            }
            return true
        }
    })
}
let auto

const obtenerA = (idPersona) => {
    $.ajax({
        // data:  {"Status":1},
        url: url + 'vehiculo/obtener/' + idPersona,
        type: 'GET',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            //alert('Envindo Datos por ajax')
            return true
        },
        success: function(data) {
            if (data['response']) {
                auto = data['result'];
                // console.log(auto['id_tbVehiculo'])
                obtenerDA(auto['id_tbVehiculo'])
                sessionStorage.setItem('IdAuto', auto['id_tbVehiculo'])
                return true
            } else {
                if (data['errors'] == null) {
                    // alert(data['message'])
                } else {
                    // alert(data['errors'])
                    $('#fotos').hide()
                }
            }
            return true
        }
    })
}
// obtener listado de documentos del auto
const obtenerDA = (id) => {
    $.ajax({
        // data:  parametros,
        url: url + 'img/listarDA/' + id,
        type: 'GET',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            //alert('Envindo Datos por ajax')
            return true
        },
        success: function(msg) {
            if (msg['response']) {
                let elementos = msg['result']
                console.log(elementos)
                elementos.forEach(function(item) {
                    if(item['Status'] != 2){
                        contarDocs()
                        tipo = item['id_tipo_documento']
                        document.getElementById('tramite' + tipo).style.display = 'inline'
                        document.getElementById('bt' + tipo).disabled = true
                        document.getElementById('img' + tipo).disabled = true
                    }
                });
            } else {
                if (msg['errors'] == null) {
                    // alert(msg['message'])
                } else {
                    // alert(msg['errors'])
                }
            }
            return true
        }
    })
}

const cambio = (p) => {
    document.getElementById(p).innerHTML = "Archivo Cargado"
}

const terminar = () => {
    if (numDocs == 10) {
        document.getElementById('id01').style.display = 'block'
        topFunction()
    } else {
        // console.log(numDocs)
        alert('Hacen falta algunos documentos')
    }
}

const exitModal = () => {
    document.getElementById('id01').style.display = 'none'
        // topFunction()
}
let numDocs = 0

const contarDocs = () => {
    numDocs++
    console.log(numDocs)
    switch (numDocs) {
        case 10:
            actuReg(4)
            document.getElementById('id01').style.display = 'block'
            topFunction()
            break;
    }
}