// urls de servicios web
let url = false;
let refMenuInicio = false;
let conekta = false;
let configFireBaseMapaDriver = false;
let configFireBasePeticiones = false;
const modoServidor = 'local';
// const modoServidor = 'dev';
// const modoServidor = 'prod';

if (modoServidor === 'local') {
    url = `http://localhost/lubo_back_dev/public/`;
    refMenuInicio = `http://localhost/lubo/`;
    conekta = 'key_DY6cGoLsFLGawX9sowxgj2g';
    configFireBaseMapaDriver = {
        apiKey: "AIzaSyBW10lZmI8ED3WdzNna41hkyhrzgiYN4sM",
        authDomain: "lubo-dev.firebaseapp.com",
        databaseURL: "https://lubo-dev.firebaseio.com",
        projectId: "lubo-dev",
        storageBucket: "lubo-dev.appspot.com",
        messagingSenderId: "864668661662",
        appId: "1:864668661662:web:48d488d37367ea6472c88f",
        measurementId: "G-P7F4KM6YXT"
    };
    configFireBasePeticiones = {
        apiKey: "AIzaSyBW10lZmI8ED3WdzNna41hkyhrzgiYN4sM",
        authDomain: "lubo-dev.firebaseapp.com",
        databaseURL: "https://lubo-dev.firebaseio.com",
        projectId: "lubo-dev",
        storageBucket: "lubo-dev.appspot.com",
        messagingSenderId: "864668661662",
        appId: "1:864668661662:web:cf89ebd62fcfee5e72c88f",
        measurementId: "G-LSBF9WW19W"
    }
} else if (modoServidor === 'dev') {
    url = `https://dev.lubo.com.mx/lubo_back_dev/public/`;
    refMenuInicio = `https://dev.lubo.com.mx/lubo/`;
    conekta = 'key_DY6cGoLsFLGawX9sowxgj2g';
    configFireBaseMapaDriver = {
        apiKey: "AIzaSyBW10lZmI8ED3WdzNna41hkyhrzgiYN4sM",
        authDomain: "lubo-dev.firebaseapp.com",
        databaseURL: "https://lubo-dev.firebaseio.com",
        projectId: "lubo-dev",
        storageBucket: "lubo-dev.appspot.com",
        messagingSenderId: "864668661662",
        appId: "1:864668661662:web:48d488d37367ea6472c88f",
        measurementId: "G-P7F4KM6YXT"
    };
    configFireBasePeticiones = {
        apiKey: "AIzaSyBW10lZmI8ED3WdzNna41hkyhrzgiYN4sM",
        authDomain: "lubo-dev.firebaseapp.com",
        databaseURL: "https://lubo-dev.firebaseio.com",
        projectId: "lubo-dev",
        storageBucket: "lubo-dev.appspot.com",
        messagingSenderId: "864668661662",
        appId: "1:864668661662:web:cf89ebd62fcfee5e72c88f",
        measurementId: "G-LSBF9WW19W"
    };
} else if (modoServidor === 'prod') {
    url = `https://api.lubo.com.mx/lubo/public/`;
    refMenuInicio = `https://lubo.com.mx/`;
    conekta = 'key_UjWDsad3eqPfAgWDuGRjpxg';
    configFireBaseMapaDriver = {
        apiKey: "AIzaSyD-yrkETahTlt-29bNxN6ZxqfGwPO_fXMM",
        authDomain: "lubo-b3831.firebaseapp.com",
        databaseURL: "https://lubo-b3831.firebaseio.com",
        projectId: "lubo-b3831",
        storageBucket: "lubo-b3831.appspot.com",
        messagingSenderId: "483478447859",
        appId: "1:483478447859:web:3c44b7e15da85b62867986"
    };
    configFireBasePeticiones = {

    }
}

const tarjetas = [
    { brand: 'AMERICAN_EXPRESS', valor: "american-express-card.svg" },
    { brand: 'american_express', valor: "american-express-card.svg" },
    { Brand: 'AMERICAN_EXPRESS', valor: "american-express-card.svg" },
    { Brand: 'american_express', valor: "american-express-card.svg" },
    { brand: 'MC', valor: "master.svg" },
    { brand: 'mc', valor: "master.svg" },
    { Brand: 'MC', valor: "master.svg" },
    { Brand: 'mc', valor: "master.svg" },
    { brand: 'mastercard', valor: "master.svg" },
    { Brand: 'mastercard', valor: "master.svg" },
    { brand: 'VISA', valor: "visa.svg" },
    { brand: "visa", valor: "visa.svg" },
    { Brand: 'VISA', valor: "visa.svg" },
    { Brand: "visa", valor: "visa.svg" }
];
const meses = [
    { mes: '01', valor: "Enero" },
    { mes: '02', valor: "Febrero" },
    { mes: '03', valor: "Marzo" },
    { mes: '04', valor: "Abril" },
    { mes: '05', valor: "Mayo" },
    { mes: '06', valor: "Junio" },
    { mes: '07', valor: "Julio" },
    { mes: '08', valor: "Agosto" },
    { mes: '09', valor: "Septiembre" },
    { mes: '10', valor: "Octubre" },
    { mes: '11', valor: "Noviembre" },
    { mes: '12', valor: "Diciembre" }
];


document.addEventListener('DOMContentLoaded', (event) => { //evento que se lanza al finalizar la carga de la pagina
    let menu1 = document.getElementById('menuInicio1');
    if (menu1 != null) {
        menu1.href = `${refMenuInicio}`;
    }
    let menu2 = document.getElementById('menuInicio2');
    if (menu2 != null) {
        menu2.href = `${refMenuInicio}`;
    }
    // nombre del archivo actual 
    let rutaAbsoluta = self.location.href;
    let posicionUltimaBarra = rutaAbsoluta.lastIndexOf("/");
    let rutaRelativa = rutaAbsoluta.substring(posicionUltimaBarra + "/".length, rutaAbsoluta.length);

    let nombreRuta = rutaRelativa.split('.');
    let nomArchivo = nombreRuta[0];
    console.log(nomArchivo);
    // menu driver
    let munuDriver = document.getElementById('menuDriver');
    if (munuDriver != null) {
        fetch('./menu/menu-driver.html', {
                method: 'GET',
                headers: {
                    'Content-Type': 'text/html'
                }
            })
            .then(res => res.text())
            .then(res => {
                munuDriver.innerHTML = res;
                let element = document.getElementById(`${nomArchivo}`); //elemento activo del menu
                if (element != null) {
                    console.log(element);
                    element.classList.add("active");
                } else {
                    if (nomArchivo == "resumen-semana") {
                        document.getElementById("mis-ganancias").classList.add("active");
                    }
                    if (nomArchivo == "nuevo-perfil-de-facturacion") {
                        document.getElementById("perfil-de-facturacion").classList.add("active");
                    }
                }
            })
    }
    // menu user
    let menuUser = document.getElementById('menuUser');
    if (menuUser != null) {
        fetch('./menu/menu-user.html', {
                method: 'GET',
                headers: {
                    'Content-Type': 'text/html'
                }
            })
            .then(res => res.text())
            .then(res => {
                menuUser.innerHTML = res;
                let element = document.getElementById(`${nomArchivo}`); //elemento activo del menu
                if (element != null) {
                    console.log(element);
                    element.classList.add("active");
                } else {
                    if (nomArchivo == "nuevo-perfil-de-impuestos") {
                        document.getElementById("perfil-de-impuestos").classList.add("active");
                    }
                    if (nomArchivo == "detalle-viaje") {
                        document.getElementById("mis-viajes").classList.add("active");
                    }
                }
            })
    }
    // menu admin
    let menuAdmin = document.getElementById('menuAdmin');
    if (menuAdmin != null) {
        fetch('./menu/menu-admin.html', {
                method: 'GET',
                headers: {
                    'Content-Type': 'text/html'
                }
            })
            .then(res => res.text())
            .then(res => {
                menuAdmin.innerHTML = res;
                let element = document.getElementById(`${nomArchivo}`); //elemento activo del menu
                console.log(element);
                if (element != null) {
                    console.log(element);
                    element.classList.add("active");
                    if (nomArchivo == "reporte-diario-de-viajes" || nomArchivo == "reporte-de-conductores" || nomArchivo == "corte-semanal" || nomArchivo == "reporte-solicitudes" || nomArchivo == "reporte-viajes-reportados") {
                        document.getElementById("pagesExamples").classList.add("show");
                    }
                    // conductores menu
                    if (nomArchivo == "datosDriver" || nomArchivo == "conductores") {
                        document.getElementById("pagesExamplesDriver").classList.add("show");
                    }
                }
                if (element != null) {
                    console.log(element);
                    element.classList.add("active");
                    if (nomArchivo == "listadoUser" || nomArchivo == "usuarios") {
                        document.getElementById("pagesExamplesUser").classList.add("show");
                    }
                } else {
                    if (nomArchivo == "perfil-conductor") {
                        document.getElementById('conductores').classList.add("active");
                        document.getElementById("pagesExamplesDriver").classList.add("show");
                    }
                    if (nomArchivo == "perfil-user") {
                        document.getElementById('listadoUser').classList.add("active");
                        document.getElementById("pagesExamplesUser").classList.add("show");
                    }
                    if (nomArchivo == "registrar-admin") {
                        document.getElementById('lista-admin').classList.add("active");
                    }
                }
            })
    }
});

// logout

const logout = () => {

    let confirmacion = confirm("Quiere Cerrar Sesion!");

    if (confirmacion) {

        // cerrar session de conductor o user

        let token = sessionStorage.getItem('Token');
        console.log(token)
        if (token != null) {
            sw = `${url}auth/getData/${token}`;
            fetch(sw, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(res => {
                    if (res['response']) {
                        // console.log(res);
                        sessionStorage.removeItem('Token');
                        sessionStorage.removeItem('IdPersona');
                        let Tipo_de_usuario = res['result']['Tipo_de_usuario'];
                        if (Tipo_de_usuario == 3) {
                            self.location = `${refMenuInicio}login.php`;
                        } else if (Tipo_de_usuario == 2) {
                            self.location = `${refMenuInicio}login-user.php`;
                        }

                    } else {
                        alert('Error intentelo de nuevo')
                    }
                });
        }

        // cerrar session de admin

        token = sessionStorage.getItem('token');
        if (token != null) {
            sw = `${url}auth/getData/${token}`;
            fetch(sw, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(res => {
                    if (res['response']) {
                        // console.log(res);
                        sessionStorage.removeItem('idAdmin');
                        sessionStorage.removeItem('token');
                        sessionStorage.removeItem('esSuperAdmin');
                        self.location = `${refMenuInicio}luboadmi/html/login.html`
                    } else {
                        alert('Error intentelo de nuevo')
                    }
                });
        }
    }

}

// funcion para recuperacion de password dependiento del usuario

let ligaRecuperarPass = (tipoUser) => {
    sessionStorage.setItem('tipoUser', tipoUser);
}

// 

const CatalogoRegimenFiscal = [{
        "Natural": false,
        "Moral": true,
        "Name": "General de Ley Personas Morales",
        "Value": "601"
    },
    {
        "Natural": false,
        "Moral": true,
        "Name": "Personas Morales con Fines no Lucrativos",
        "Value": "603"
    },
    {
        "Natural": true,
        "Moral": false,
        "Name": "Sueldos y Salarios e Ingresos Asimilados a Salarios",
        "Value": "605"
    },
    {
        "Natural": true,
        "Moral": false,
        "Name": "Arrendamiento",
        "Value": "606"
    },
    {
        "Natural": false,
        "Moral": true,
        "Name": "Régimen de Enajenación o Adquisición de Bienes",
        "Value": "607"
    },
    {
        "Natural": true,
        "Moral": false,
        "Name": "Demás ingresos",
        "Value": "608"
    },
    {
        "Natural": false,
        "Moral": true,
        "Name": "Consolidación",
        "Value": "609"
    },
    {
        "Natural": false,
        "Moral": true,
        "Name": "Residentes en el Extranjero sin Establecimiento Permanente en México",
        "Value": "610"
    },
    {
        "Natural": true,
        "Moral": false,
        "Name": "Ingresos por Dividendos (socios y accionistas)",
        "Value": "611"
    },
    {
        "Natural": true,
        "Moral": false,
        "Name": "Personas Físicas con Actividades Empresariales y Profesionales",
        "Value": "612"
    },
    {
        "Natural": true,
        "Moral": false,
        "Name": "Ingresos por intereses",
        "Value": "614"
    },
    {
        "Natural": true,
        "Moral": false,
        "Name": "Régimen de los ingresos por obtención de premios",
        "Value": "615"
    },
    {
        "Natural": true,
        "Moral": false,
        "Name": "Sin obligaciones fiscales",
        "Value": "616"
    },
    {
        "Natural": false,
        "Moral": true,
        "Name": "Sociedades Cooperativas de Producción que optan por diferir sus ingresos",
        "Value": "620"
    },
    {
        "Natural": true,
        "Moral": false,
        "Name": "Incorporación Fiscal",
        "Value": "621"
    },
    {
        "Natural": false,
        "Moral": true,
        "Name": "Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras",
        "Value": "622"
    },
    {
        "Natural": false,
        "Moral": true,
        "Name": "Opcional para Grupos de Sociedades",
        "Value": "623"
    },
    {
        "Natural": false,
        "Moral": true,
        "Name": "Coordinados",
        "Value": "624"
    },
    {
        "Natural": false,
        "Moral": true,
        "Name": "Hidrocarburos",
        "Value": "628"
    },
    {
        "Natural": true,
        "Moral": false,
        "Name": "De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales",
        "Value": "629"
    },
    {
        "Natural": true,
        "Moral": false,
        "Name": "Enajenación de acciones en bolsa de valores",
        "Value": "630"
    }
];