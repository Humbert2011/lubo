<nav class="top-bar overlay-bar">
				<div class="container">
				
					<div class="row utility-menu">
						<div class="col-sm-12">
							<div class="utility-inner clearfix">
								<span class="alt-font"><i class="icon icon_pin"></i> Huauchinango, Pue</span>
								<span class="alt-font"><i class="icon icon_mail"></i> contacto@stardust.com.mx</span>
							
								<div class="pull-right">
									<!--<a href="login.html" class="btn btn-primary login-button btn-xs">Login</a>
									<a href="#" class="btn btn-primary btn-filled btn-xs">Signup</a>-->
									<!--<a href="#" class="language"><img alt="English" src="img/english.png"></a>
									<a href="#" class="language"><img alt="English" src="img/denmark.png"></a>-->
								</div>
							</div>
						</div>
					</div><!--end of row-->
				
				
					<div class="row nav-menu">
						<div class="col-sm-3 col-md-2 columns">
							<a href="index.php">
								<img class="logo logo-light" alt="Logo" src="img/logo-light.png">
								<img class="logo logo-dark" alt="Logo" src="img/logo-dark.png">
							</a>
						</div>
					
						<div class="col-sm-9 col-md-10 columns">
							<ul class="menu">
								<li><a  href="index.php">Inicio</a>
								<li class="has-dropdown"><a href="#">Nosotros</a>
									<ul class="subnav">
										<li><a href="acercade.php">Acerca de</a></li>
										<li><a href="servicios.php">Servicios</a></li>
									</ul>
								</li>
								<li><a href="proyectos.php">Proyectos</a>
								</li>
								<li><a  href="blog.php">Blog</a>
								</li>
								<li ><a href="contacto.php">Contacto</a>
								</li>
							</ul>

							<ul class="social-icons text-right">
								<!--<li>
									<a href="#">
										<i class="icon social_twitter"></i>
									</a>
								</li>-->
							
								<li>
									<a href="#">
										<i class="icon social_facebook"></i>
									</a>
								</li>
							
								<!--<li>
									<a href="#">
										<i class="icon social_instagram"></i>
									</a>
								</li>-->
							</ul>
						</div>
					</div><!--end of row-->
					
					<div class="mobile-toggle">
						<i class="icon icon_menu"></i>
					</div>
					
				</div><!--end of container-->
			</nav>