<!DOCTYPE HTML>
<html lang="es">
<head>
<meta charset="utf-8">
<title>Verificar Email</title>
 <!-- icon -->
<link rel="icon" type="image/x-icon" href=" img/favicon/favicon.ico" />
<link href=" css/styleRPass.css" rel="stylesheet" type="text/css" media="all"/>
<link rel="icon" type="image/x-icon" href=" images/favicon.ico" />
<!-- Custom Theme files -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<!--google fonts-->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<script src="./js/config/config.js"></script>
<script>
	const validarEmail = () => {
		let sear = new URLSearchParams(location.search);
		let user = sear.get('u');
		let sw = `${url}auth/verificarurl/${user}`;
		console.log(sw);
		fetch(sw, {
                method: 'GET',
                // body: JSON.stringify(parametros),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
		.then(res => res.json())
		.then(res => {
			console.log(res);
		})
	};
	window.onload = validarEmail();
</script>
</head>
<body>
<!--element start here-->
<div class="elelment">
	<div class="element-main">
		<h2><img src=" img/LuboPink.png" class="imgPin" width="150px"></h2>
		<h1>Email verificado</h1>
		<br>
		<br>
		<br>
		<h2><a href="http://www.lubo.com.mx"><img src=" img/ok.png" class="imgPin" width="100"></a></h2>
		<br>
		<br>
		<h3>¡Solicitud Exitosa!</h3>
<p style="color:#4a4a4a; font-size:14.5px;" class="pRegular">Gracias por utilizar Lubo</p>
	</div>
</div>
<div class="copy-right">
			<p style="color:#fff; opacity:.5;">© 2017 Pikr. Todos los derechos reservados</p>
</div>

<!--element end here-->
<script src=" js/jquery.js"></script>
    <script src=" js/bootstrap.min.js"></script>
	<script src=" js/modernizr.custom.js"></script>
	<script src=" js/hideShowPassword.js"></script>	
<script>
		$(window).ready(function(){
			$('#password-1').hideShowPassword({
			  // Creates a wrapper and toggle element with minimal styles.
			  innerToggle: true,
			  // Makes the toggle functional in touch browsers without
			  // the element losing focus.
			  touchSupport: Modernizr.touch
			});
		});
	</script>
</body>
</html>