

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Lubo - Datos Personales</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- icon -->
		<link rel="icon" type="image/x-icon" href="../../img/favicon/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="../../img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../../img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../../img/favicon/favicon-16x16.png">
		<link rel="manifest" href="../../img/favicon/site.webmanifest">
		<link rel="mask-icon" href="../../img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
        
        <link href="../../css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../css/theme.css" rel="stylesheet" type="text/css" media="all"/>
        <link rel="stylesheet" href="bootstrap.css">
        
        <link rel="stylesheet" href="helpers.css">
        <link rel="stylesheet" href="style.css">
        
        
		<link rel="stylesheet" href="../../css/frmregistro.css">
		<link rel="stylesheet" href="../../css/frmdatos.css">
		
		<!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NX8TKGG');</script>
		<!-- End Google Tag Manager -->
		
		
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="../../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="../../js/config/config.js"></script><!-- url -->
		<script type="text/javascript" src="../../js/sw/app-driver.js"></script>
		
		<script>
			 window.oninit = validar();
			 window.oninit = validarurl();
			 document.addEventListener('DOMContentLoaded', (event) => {
				listarZonas();
       		 });
		</script>
        
        <script>
	    	// function limpiarEstilo(){
		    // 	var element = document.getElementById("numero1");
			// 	element.classList.remove("input-error");
			// }
	    </script>
	    
	    <!-- Facebook Pixel Code -->
			<script>
				!function(f,b,e,v,n,t,s)
				{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
				fbq('init', '189972612211292');
				fbq('track', 'PageView');
			</script>
			<noscript><img height="1" width="1" style="display:none"
				src="https://www.facebook.com/tr?id=189972612211292&ev=PageView&noscript=1"
			/></noscript>
		<!-- End Facebook Pixel Code -->
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150520430-1"></script>
		<script>
		if(modoServidor === 'prod'){
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-150520430-1');
		}
		</script>
		<!-- End Global site tag (gtag.js) - Google Analytics -->
	    

    </head>
    <body>
		<!-- Google Tag Manager (noscript) -->
	    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX8TKGG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->	
		<div class="nav-container">
			<?php 
				$currentPage = 'menu-driver';
				include_once("../../menus/menu-sticky-in-in.php");
			?>
		</div>
		
		<div class="main-container">
			<header class="fullscreen-element no-pad centered-text">
				<div class="background-image-holder parallax-background overlay">
					<img class="background-image" alt="Background Image" src="../../img/2inicio.png">
				</div>
				
				<section class="pb_cover_v3 overflow-hidden cover-bg-indigo cover-bg-opacity text-left pb_gradient_v1 pb_slant-light cover-registro" id="section-home" style="background-color: transparent; margin-top: -30px;">
			      <div class="container">
			        <div class="row align-items-center justify-content-center">
			          <div class="col-md-6">
			            <h2 class="heading mb-3 h1-hola titulo-registro">Excelente decisión</h2>
			            <div class="sub-heading">
			              <p class="mb-4">
			              <span class="subtitulo-registro">
							Estás a punto de ser un luboDriver, sigue unos cuantos pasos para
							
							finalizar tu registro
							</span></p>
			            </div>
			          </div>
			          <div class="col-md-5 relative align-self-center">
			
			            <form action="#" class="bg-white rounded pb_form_v1 form-i formulario-registro" id="frmRegistry">
			            	<h2 class="mb-4 mt-0 text-center textop">Registrarse</h2>
			              <!--<div class="form-group">
			                <input type="text" class="form-control pb_height-50 reverse" placeholder="Número telefónico">
			              </div>-->
						  	<div class="form-group">
								<input type="text" class="form-control" id="nombre" required="nombre" class="campotexto" placeholder="Nombre" onkeypress="limpiarEstiloNombre()">
							</div>
							<center><div class="error-notice"><span id="name-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="text" class="form-control" id="apellidos" required="apellidos" class="campotexto" placeholder="Apellidos" name=""  onkeypress="limpiarEstiloApellidos()">
							</div>
							<center><div class="error-notice"><span id="apellidos-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="email" class="form-control" id="correo" required="correo" class="campotexto" placeholder="Correo Electronico" name="" onkeypress="limpiarEstiloCorreo()">
							</div>
							<center><div class="error-notice"><span id="email-error" class="signup-error"></span></div></center>
							<div class="form-group">
								<input type="password" class="form-control" id="contraseña" required="contraseña" class="campotexto" placeholder="Contraseña" name="" onkeypress="limpiarEstiloPassword()">
							</div>
							<center><div class="error-notice"><span id="password-error" class="signup-error"></span></div></center>
							<!-- zonas -->
							<div class="form-group" id="zona">
							</div>
							<center><div class="error-notice"><span id="zona-error" class="signup-error"></span></div></center>
							<!-- end zonas -->
							<center>
							<div class="form-group">
								<h4>Género</h4>
								<fieldset>
								<div class="radio-item">
								    <input type="radio" id="ritemb" name="ritem" value="H">
								    <label for="ritemb">Hombre</label>
								</div>
								<div class="radio-item">
								    <input type="radio" id="ritema" name="ritem" value="M">
								    <label for="ritema">Mujer</label>
								</div>
								</fieldset>
							</div>
							</center>
							<!-- tycos -->
							<div class="form-group" style="margin-top: -15px;">
								<a href="http://lubo.com.mx/terminos-y-condiciones" target="_blank">Términos y condiciones</a>
								<br>
								<div class="form-group">
									<label><input type="checkbox" id="tycos" name="tycos"> Acepto</label><br>
								</div>
							</div>
							<center><div class="error-notice"><span id="tycos-error" class="signup-error"></span></div></center>
								
							<br>
							<input type="hidden" name="telefono" id="telefono" value="<?php echo $_GET['telefono'] ?>" >
							<input type="hidden" name="codigoPais" id="codigoPais" value="<?php echo $_GET['codigo'] ?>">
							<center>
								<!-- <div class="error-notice"><span id="telefono-error" class="signup-error"></span></div> -->
							</center>
					 		<center>
								<div class="form-group" align="center" style="margin-top: -50px;">
						  			<button id="boton"  onClick="registrar($('#nombre').val(),$('#apellidos').val(),$('#correo').val(),$('#contraseña').val(),$('#codigoPais').val(),$('#telefono').val(),$('input:radio[name=ritem]:checked').val(),$('#idZona').val())" type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo3">
										Registro
									</button>
								</div>	
							</center>
							
			  
			            </form>
			
			          </div>
			        </div>
			      </div>
			    </section>
			    <!-- END section -->
			</header>
		<!-- end divider -->
			
			
			
			<section class="video-inline">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<center>
							<br>
							<h2 class="h2-eleccion">Ve tus ganancias del día o si lo prefieres de tu semana.</h2>
							</center>
						</div>
						
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
		</div>
		
		
		
        <div class="clearfix">
	        
		<div class="footer-container">
			<section class="sliderQR section-footer" style="background-color: transparent;">
		        <?php include_once('../../menus/footer-in-in.php');?>
			</section>
		</div>
		
		
		
		
		<script src="../../js/jquery.min.js"></script>
        <script src="../../js/jquery.plugin.min.js"></script>
        <script src="../../js/bootstrap.min.js"></script>
        <script src="../../js/jquery.flexslider-min.js"></script>
        <script src="../../js/smooth-scroll.min.js"></script>
        <script src="../../js/skrollr.min.js"></script>
        <script src="../../js/scrollReveal.min.js"></script>
        <script src="../../js/scripts.js"></script>
        
        
        <script>
    	$(document).ready(function (){
		    //validateB();
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../../img/footerLubo.png";
                document.getElementById("headerLogos").style.display="none";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../../img/footerMobile.png";
		    }
		});
	</script>
	
	<script>
		document.getElementsByTagName("BODY")[0].onresize = function() {myFunction()};
		function myFunction() {
		    var x = "Total Width: " + screen.width;
		    if ($(window).width() >= 768){
	            image = document.getElementById('imgHeader');
				image.src ="../../img/footerLubo.png";
		    }
		    if ($(window).width() <= 767){
    			image = document.getElementById('imgHeader');
				image.src ="../../img/footerMobile.png";
		    }
		}
		
	</script>
        
        <script type="text/javascript">
	    	// jQuery(document).ready(function() {
	    	// jQuery('.telefono').keypress(function(tecla) {
	        // if(tecla.charCode < 48 || tecla.charCode > 57) return false;
	   		// 	 });
			// });
	    </script>
	
	
	    <script type="text/javascript">
			$(document).ready(function() {
				$("#frmValidate").hide()
				$("#datos").hide()
			});
			var input=  document.getElementById('numero1');
			input.addEventListener('input',function(){
			  if (this.value.length > 10) 
			     this.value = this.value.slice(0,10); 
			})
		</script>
	
	
		<script>
	    //   $(document).ready(function () {
	    //       $("#numero1").keyup(function () {
	    //           var value = $(this).val();
	    //           $("#numero22").val(value);
	    //       });
		//   });
		$(document).on('keypress',function(e) {
			if(e.which == 13) {
				e.preventDefault();
				// alert('inf')
				validarnumero($('#numero1').val(),$('#imp2').val())
			}
		});
		</script>
		<script>
	    //   $(document).ready(function () {
	    //       $("#numero1").keyup(function () {
	    //           var value = $(this).val();
	    //           $("#telefono3").val(value);
	    //       });
	    //   });
		</script>
	
    </body>
</html>
				