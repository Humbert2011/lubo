<?php
date_default_timezone_set('America/Monterrey');//or change to whatever timezone you want
	
	require './fpdf/fpdf.php';
	class PDF extends FPDF
	{
		function Header()
		{
			//Importing Font and it's variations
			$this->AddFont('Proxima','','ProximaNova-Regular.php'); //Regular
			$this->AddFont('Proxima','B','Proxima-Nova-Semibold.php');
			// Proxima-Nova-Semibold
			$fecha = $_GET['fechas'];
			$cadena=explode(" ",$fecha);
			// var_dump( $cadena);
			$fechaSemana = $_GET['fechaSemana'];
			$totalS = $_GET['totalS'];

			$date1 = $_GET['date1'];
			$viaje1 =$_GET['viaje1'];
			$monto1=$_GET['monto1'];
			$ivaDriver1=$_GET['ivaDriver1'];
			$isrDriver1=$_GET['isrDriver1'];
			$lubo1=$_GET['lubo1'];
			$luboIVA1=$_GET['luboIVA1'];
			$isrLubo1=$_GET['isrLubo1'];
			$procesadorPago1=$_GET['procesadorPago1'];
			$total1=$_GET['total1'];

			$date2 = $_GET['date2'];
			$viaje2 =$_GET['viaje2'];
			$monto2=$_GET['monto2'];
			$ivaDriver2=$_GET['ivaDriver2'];
			$isrDriver2=$_GET['isrDriver2'];
			$lubo2=$_GET['lubo2'];
			$luboIVA2=$_GET['luboIVA2'];
			$isrLubo2=$_GET['isrLubo2'];
			$procesadorPago2=$_GET['procesadorPago2'];
			$total2=$_GET['total2'];

			$date3 = $_GET['date3'];
			$viaje3 =$_GET['viaje3'];
			$monto3=$_GET['monto3'];
			$ivaDriver3=$_GET['ivaDriver3'];
			$isrDriver3=$_GET['isrDriver3'];
			$lubo3=$_GET['lubo3'];
			$luboIVA3=$_GET['luboIVA3'];
			$isrLubo3=$_GET['isrLubo3'];
			$procesadorPago3=$_GET['procesadorPago3'];
			$total3=$_GET['total3'];
			
			$date4 = $_GET['date4'];
			$viaje4 =$_GET['viaje4'];
			$monto4 = $_GET['monto4'];
			$ivaDriver4=$_GET['ivaDriver4'];
			$isrDriver4=$_GET['isrDriver4'];
			$lubo4=$_GET['lubo4'];
			$luboIVA4=$_GET['luboIVA4'];
			$isrLubo4=$_GET['isrLubo4'];
			$procesadorPago4=$_GET['procesadorPago4'];
			$total4=$_GET['total4'];

			$date5 = $_GET['date5'];
			$viaje5 =$_GET['viaje5'];
			$monto5 = $_GET['monto5'];
			$ivaDriver5=$_GET['ivaDriver5'];
			$isrDriver5=$_GET['isrDriver5'];
			$lubo5=$_GET['lubo5'];
			$luboIVA5=$_GET['luboIVA5'];
			$isrLubo5=$_GET['isrLubo5'];
			$procesadorPago5=$_GET['procesadorPago5'];
			$total5=$_GET['total5'];

			$date6 = $_GET['date6'];
			$viaje6 =$_GET['viaje6'];
			$monto6 = $_GET['monto6'];
			$ivaDriver6=$_GET['ivaDriver6'];
			$isrDriver6=$_GET['isrDriver6'];
			$lubo6=$_GET['lubo6'];
			$luboIVA6=$_GET['luboIVA6'];
			$isrLubo6=$_GET['isrLubo6'];
			$procesadorPago6=$_GET['procesadorPago6'];
			$total6=$_GET['total6'];

			$date7 = $_GET['date7'];
			$viaje7 =$_GET['viaje7'];
			$monto7 = $_GET['monto7'];
			$ivaDriver7=$_GET['ivaDriver7'];
			$isrDriver7=$_GET['isrDriver7'];
			$lubo7=$_GET['lubo7'];
			$luboIVA7=$_GET['luboIVA7'];
			$isrLubo7=$_GET['isrLubo7'];
			$procesadorPago7=$_GET['procesadorPago7'];
			$total7=$_GET['total7'];

			$totalViajeText=$_GET['totalViajeText'];
			$totalViajeS=$_GET['totalViajeS'];
			$totalMontoD=$_GET['totalMontoD'];
			$totalMontoIVAD=$_GET['totalMontoIVAD'];
			$totalISRd=$_GET['totalISRd'];
			$totalLubo=$_GET['totalLubo'];
			$totalLuboIVA=$_GET['totalLuboIVA'];
			$totalISRLubo=$_GET['totalISRLubo'];
			$totalPPS=$_GET['totalPPS'];
			$totalesM=$_GET['totalesM'];

			$cve=$_GET['cve'];
			$cvt=$_GET['cvt'];
			$comLubo=$_GET['comLubo'];
			$procesadordePagos=$_GET['procesadordePagos'];
			$subTotal=$_GET['subTotal'];
			$totalIva=$_GET['totalIva'];
			$totalIsr=$_GET['totalIsr'];
			$transferencia =$cvt-$comLubo;
			$deposito=$comLubo-$cvt;
			if($comLubo >= $cvt){
				$montoTD = "Lubo te hará una transferencia por el monto de $$deposito";
			}
			if($comLubo <= $cvt){
				$montoTD="Debes hacer un depósito a Lubo por el monto de $$transferencia";
			}

			$this->Image('imagenes/F1.jpg' ,-0,0,209.88,297,'JPG');
			
			$this->SetFont('Proxima','',12);
			$this->setTextColor(117, 117, 117);
			$this->Cell(184,19,utf8_decode("$cadena[0]"),0,1,'R');
			$this->SetFont('Proxima','',20);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(77, 23);
			$this->Cell(60,30,utf8_decode("$fechaSemana"),0,1,'L');
			$this->SetFont('Proxima','B',34);
			$this->setTextColor(237, 48, 147);
			$this->SetXY(17, 40);
			$this->Cell(60,30,utf8_decode("$$totalS"),0,1,'L');
			//
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);			
			$this->Cell(35,27.5,utf8_decode("$date1"),0,1,'R');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(38, 79);
			$this->Cell(30,10,utf8_decode("$viaje1"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(57, 79);	
			$this->Cell(30,10,utf8_decode("$monto1"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(78, 79);
			$this->Cell(30,10,utf8_decode("$ivaDriver1"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(97, 79);
			$this->Cell(30,10,utf8_decode("$isrDriver1"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(115, 79);
			$this->Cell(30,10,utf8_decode("$lubo1"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(116, 81.5);
			// $this->Cell(30,10,utf8_decode("$luboIVA1"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(131, 81.5);
			// $this->Cell(30,10,utf8_decode("$isrLubo1"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(135, 79);
			$this->Cell(30,10,utf8_decode("$procesadorPago1"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(157, 79);
			$this->Cell(30,10,utf8_decode("$total1"),0,1,'C');
			//
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);			
			$this->Cell(35,17,utf8_decode("$date2"),0,1,'R');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(38, 92.5);
			$this->Cell(30,10,utf8_decode("$viaje2"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(57, 92.5);	
			$this->Cell(30,10,utf8_decode("$monto2"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(78, 92.5);
			$this->Cell(30,10,utf8_decode("$ivaDriver2"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(97, 92.5);
			$this->Cell(30,10,utf8_decode("$isrDriver2"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(115, 92.5);
			$this->Cell(30,10,utf8_decode("$lubo2"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(116, 95);
			// $this->Cell(30,10,utf8_decode("$luboIVA2"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(131, 95);
			// $this->Cell(30,10,utf8_decode("$isrLubo2"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(135, 92.5);
			$this->Cell(30,10,utf8_decode("$procesadorPago2"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(157, 92.5);
			$this->Cell(30,10,utf8_decode("$total2"),0,1,'C');
			//
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);		
			$this->Cell(35,17,utf8_decode("$date3"),0,1,'R');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(38, 106);
			$this->Cell(30,10,utf8_decode("$viaje3"),0,1,'C');
			$this->SetFont('Proxima','',10);;
			$this->setTextColor(0, 0, 0);
			$this->SetXY(57, 106);	
			$this->Cell(30,10,utf8_decode("$monto3"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(78, 106);
			$this->Cell(30,10,utf8_decode("$ivaDriver3"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(97, 106);
			$this->Cell(30,10,utf8_decode("$isrDriver3"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(115, 106);
			$this->Cell(30,10,utf8_decode("$lubo3"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(116, 108.5);
			// $this->Cell(30,10,utf8_decode("$luboIVA3"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(131, 108.5);
			// $this->Cell(30,10,utf8_decode("$isrLubo3"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(135, 106);
			$this->Cell(30,10,utf8_decode("$procesadorPago3"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(157, 106);
			$this->Cell(30,10,utf8_decode("$total3"),0,1,'C');
			//
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);		
			$this->Cell(35,17,utf8_decode("$date4"),0,1,'R');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(38, 119);
			$this->Cell(30,10,utf8_decode("$viaje4"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(57, 119);	
			$this->Cell(30,10,utf8_decode("$monto4"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(78, 119);
			$this->Cell(30,10,utf8_decode("$ivaDriver4"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(97, 119);
			$this->Cell(30,10,utf8_decode("$isrDriver4"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(115, 119);
			$this->Cell(30,10,utf8_decode("$lubo4"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(116, 122);
			// $this->Cell(30,10,utf8_decode("$luboIVA4"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(131, 122);
			// $this->Cell(30,10,utf8_decode("$isrLubo4"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(135, 119);
			$this->Cell(30,10,utf8_decode("$procesadorPago4"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(157, 119);
			$this->Cell(30,10,utf8_decode("$total4"),0,1,'C');
			//
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);		
			$this->Cell(35,17,utf8_decode("$date5"),0,1,'R');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(38, 132.5);
			$this->Cell(30,10,utf8_decode("$viaje5"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(57, 132.5);	
			$this->Cell(30,10,utf8_decode("$monto5"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(78, 132.5);
			$this->Cell(30,10,utf8_decode("$ivaDriver5"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(97, 132.5);
			$this->Cell(30,10,utf8_decode("$isrDriver5"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(115, 132.5);
			$this->Cell(30,10,utf8_decode("$lubo5"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(116, 135);
			// $this->Cell(30,10,utf8_decode("$luboIVA5"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(131, 135);
			// $this->Cell(30,10,utf8_decode("$isrLubo5"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(135, 132.5);
			$this->Cell(30,10,utf8_decode("$procesadorPago5"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(157, 132.5);
			$this->Cell(30,10,utf8_decode("$total5"),0,1,'C');
			//
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);		
			$this->Cell(35,17,utf8_decode("$date6"),0,1,'R');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(38, 146);
			$this->Cell(30,10,utf8_decode("$viaje6"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(57, 146);	
			$this->Cell(30,10,utf8_decode("$monto6"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(78, 146);
			$this->Cell(30,10,utf8_decode("$ivaDriver6"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(97, 146);
			$this->Cell(30,10,utf8_decode("$isrDriver6"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(115, 146);
			$this->Cell(30,10,utf8_decode("$lubo6"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(116, 148.5);
			// $this->Cell(30,10,utf8_decode("$luboIVA6"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(131, 148.5);
			// $this->Cell(30,10,utf8_decode("$isrLubo6"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(135, 146);
			$this->Cell(30,10,utf8_decode("$procesadorPago6"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(157, 146);
			$this->Cell(30,10,utf8_decode("$total6"),0,1,'C');
			//
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);		
			$this->Cell(35,17,utf8_decode("$date7"),0,1,'R');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(38, 159.5);
			$this->Cell(30,10,utf8_decode("$viaje7"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(57, 159.5);	
			$this->Cell(30,10,utf8_decode("$monto7"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(78, 159.5);
			$this->Cell(30,10,utf8_decode("$ivaDriver7"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(97, 159.5);
			$this->Cell(30,10,utf8_decode("$isrDriver7"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(115, 159.5);
			$this->Cell(30,10,utf8_decode("$lubo7"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(116, 162);
			// $this->Cell(30,10,utf8_decode("$luboIVA7"),0,1,'C');
			// $this->SetFont('Proxima','',10);
			// $this->SetXY(131, 162);
			// $this->Cell(30,10,utf8_decode("$isrLubo7"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->SetXY(135, 159.5);
			$this->Cell(30,10,utf8_decode("$procesadorPago7"),0,1,'C');
			$this->SetFont('Proxima','',10);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(157, 159.5);
			$this->Cell(30,10,utf8_decode("$total7"),0,1,'C');
			//
			// $this->SetFont('Proxima','',11);
			// $this->setTextColor(0, 0, 0);		
			// $this->Cell(20,32,utf8_decode("$totalViajeText"),0,1,'R');
			$this->SetFont('Proxima','B',11);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(38, 179.5);
			$this->Cell(30,10,utf8_decode("$totalViajeS"),0,1,'C');
			$this->SetFont('Proxima','B',11);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(57, 179.5);	
			$this->Cell(30,10,utf8_decode("$totalMontoD"),0,1,'C');
			$this->SetFont('Proxima','B',11);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(78, 179.5);
			$this->Cell(30,10,utf8_decode("$totalMontoIVAD"),0,1,'C');
			$this->SetFont('Proxima','B',11);
			$this->SetXY(97, 179.5);
			$this->Cell(30,10,utf8_decode("$totalISRd"),0,1,'C');
			$this->SetFont('Proxima','B',11);
			$this->SetXY(115, 179.5);
			$this->Cell(30,10,utf8_decode("$totalLubo"),0,1,'C');
			// $this->SetFont('Proxima','B',11);
			// $this->SetXY(116, 181);
			// $this->Cell(30,10,utf8_decode("$totalLuboIVA"),0,1,'C');
			// $this->SetFont('Proxima','B',11);
			// $this->SetXY(131, 181);
			// $this->Cell(30,10,utf8_decode("$totalISRLubo"),0,1,'C');
			$this->SetFont('Proxima','B',11);
			$this->SetXY(135, 179.5);
			$this->Cell(30,10,utf8_decode("$totalPPS"),0,1,'C');
			$this->SetFont('Proxima','B',11);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(157, 179.5);
			$this->Cell(30,10,utf8_decode("$totalesM"),0,1,'C');

			$this->SetFont('Proxima','',13);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(150, 197.5);
			$this->Cell(30,10,utf8_decode("$$cve"),0,1,'R');

			$this->SetFont('Proxima','',13);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(150, 205);
			$this->Cell(30,10,utf8_decode("$$cvt"),0,1,'R');

			$this->SetFont('Proxima','',13);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(150, 212);
			$this->Cell(30,10,utf8_decode("-$$comLubo"),0,1,'R');

			$this->SetFont('Proxima','',13);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(150, 220);
			$this->Cell(30,10,utf8_decode("-$$procesadordePagos"),0,1,'R');

			$this->SetFont('Proxima','',13);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(150, 227);
			$this->Cell(30,10,utf8_decode("$$subTotal"),0,1,'R');

			$this->SetFont('Proxima','',13);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(150, 234.5);
			$this->Cell(30,10,utf8_decode("$$totalIva"),0,1,'R');

			$this->SetFont('Proxima','',13);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(150, 242);
			$this->Cell(30,10,utf8_decode("$$totalIsr"),0,1,'R');

			$this->SetFont('Proxima','B',13);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(150, 256);
			$this->Cell(30,10,utf8_decode("$$totalS"),0,1,'R');

			$this->SetFont('Proxima','',13);
			$this->setTextColor(0, 0, 0);
			$this->SetXY(135, 271);
			$this->Cell(30,10,utf8_decode("$montoTD"),0,1,'R');
			// $this->Cell(15);
			// $this->Cell(130,-112,utf8_decode("$dI"),0,1,'L');
			// $this->Cell(15);
			// $this->Cell(50,125,utf8_decode("$hI"),0,1,'L');
			// $this->Cell(15);
			// $this->Cell(130,-95.5,utf8_decode("$dF"),0,1,'L');
			// $this->Cell(15);
			// $this->Cell(50,109.5,utf8_decode("$hF"),0,1,'L');
			// $this->Cell(0,10,utf8_decode("$brand"),0,1,'R');
			// $this->SetFont('Arial','B',11);
			// $this->Cell(0,10,utf8_decode('AUTORIZACIÓN DE PAGO'),0,1,'C');
			 $this->Ln();
		}
	}

	// $fecha = date("d-m-Y H:i:s");
	// $FechaInicio = date("d-m-Y");
	// $FechaFin = date("d-m-Y");
	$Folio = 300;
	// $Limite = date("d-m-Y");
	$nombreNegocio = "Stardust";
	$Monto = 200;
	$semana = 12;
	$pdf = new PDF();
    $pdf->AddPage('P', 'A4');
    //$pdf->SetFont('Arial','B',12);
    //$pdf->Cell(0,0,'Folio',0,1,'R');
	$pdf->SetMargins(30,30,20,1);
	// $pdf->Ln();
	// $str='Pago del servicio de parquímetros a través de Centros autorizados de';
	// $str = utf8_decode($str);
    // $pdf->SetFont('Arial','B',11);
	// $pdf->Cell(0,-4,$fecha,0,1,'R');
    // $pdf->Cell(0,-4,'',0,1,'R');
    // $pdf->Cell(0,-2,'HP - '.$semana,0,1,'R');
    // $pdf->Cell(0,53,'                   '.$Monto.'.00',0,1,'C');
    // $pdf->Cell(0,-35,'                   '.$Monto.'.00',0,1,'C');
	// $pdf->Cell(0,55,'('.$letras.')',0,1,'C');
	
	// $strNom = utf8_decode($nombreNegocio);
    // $pdf->Cell(0,-40,'                   '.$strNom,0,1,'C');
    // $pdf->Cell(0,100,$str,0,1,'C');
    // $pdf->Cell(0,-90,'cobro de la Fecha: '.$FechaInicio.' a la Fecha: '.$FechaFin,0,1,'C');
    // $pdf->SetFont('Arial','B',9);
    // $pdf->Cell(70,135,$fecha,0,1,'R');
    // $pdf->Cell(135,-135,$Folio,0,1,'R');
    // $pdf->Image('imagenes/sello.jpg' ,120,215, 40,40,'JPG');
    // $pdf->Cell(135,159,'Viernes '.$Limite,0,1,'R');
    //$pdf->Cell(0,15,'Nota: presentarce 15 minutos antes de la hora de la cita con una identificacion oficial ',0,1);
	
	
	//$pdf->Ln(20);
	$pdf->Output();


	
	
?>